package dto.excelFile;

import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

@Sheet
public class ExcelObject {
		
//	https://github.com/millij/poi-object-mapper
	

	@SheetColumn("actions")
	public String actions;
	
	@SheetColumn("externalReference")
	public String externalReference;
	
	@SheetColumn("fundingReference")
	public String fundingReference;
	
	@SheetColumn("amount")
	public String amount;

	@SheetColumn("amountFees")
	public String amountFees;	
	
	@SheetColumn("merchant")
	public String merchant;
	
	@SheetColumn("bt_code")
	public String bt_code;
	
	@SheetColumn("contractStatus")
	public String contractStatus;
	
	@SheetColumn("eventType")
	public String eventType;
	
	@SheetColumn("eventDate")
	public String eventDate;

	@SheetColumn("merchantGUID")
    public String merchantGUID;

	@SheetColumn("merchantName")
	public String merchantName;
	
	@SheetColumn("merchantSUID")
    public String merchantSUID;
	
	@SheetColumn("customerGUID")
    public String customerGUID;
	
	@SheetColumn("customerSUID")
    public String customerSUID;
	
	@SheetColumn("businessTransactionCode")
    public String businessTransactionCode;
	
	@SheetColumn("businessTransactionType")
    public String businessTransactionType;
	
	@SheetColumn("numberOfInstallment")
    public String numberOfInstallment;
	
	@SheetColumn("channel")
    public String channel;
	
	@SheetColumn("paymentAmount")
    public String paymentAmount;
	
	@SheetColumn("currencyCode")
    public String currencyCode;
	
	@SheetColumn("financingId")
    public String financingId;
	
	@SheetColumn("invoiceId")
    public String invoiceId;
	
	@SheetColumn("installmentAmount")
    public String installmentAmount;
	
	@SheetColumn("installmentNumber")
    public String installmentNumber;
	
	@SheetColumn("capital")
    public String capital;
	
	@SheetColumn("interestFix")
    public String interestFix;
	
	@SheetColumn("interestVariable")
    public String interestVariable;
	
	@SheetColumn("adminFee")
    public String adminFee;
	
	@SheetColumn("insurance")
    public String insurance;
	
	@SheetColumn("interestAdd")
    public String interestAdd;
	
	@SheetColumn("adminFeeAdd")
    public String adminFeeAdd;
	
	@SheetColumn("insuranceAdd")
    public String insuranceAdd;
	
	@SheetColumn("interestRemove")
    public String interestRemove;
	
	@SheetColumn("adminFeeRemove")
    public String adminFeeRemove;
	
	@SheetColumn("insuranceRemove")
    public String insuranceRemove;
	
	@SheetColumn("stampTax")
    public String stampTax;
	
	@SheetColumn("stampCreditTax")
    public String stampCreditTax;
	
	@SheetColumn("stampCreditTaxAdd")
    public String stampCreditTaxAdd;
	
	@SheetColumn("stampCreditTaxRemove")
    public String stampCreditTaxRemove;
	
	@SheetColumn("stampTaxAdd")
    public String stampTaxAdd;
	
	@SheetColumn("stampTaxRemove")
    public String stampTaxRemove;
	
	@SheetColumn("settlementAmountPaymentDefault")
    public String settlementAmountPaymentDefault;
	
	@SheetColumn("paymentDefaultFee")
    public String paymentDefaultFee;
	
	@SheetColumn("cancellationAmount")
    public String cancellationAmount;
	
	@SheetColumn("paymentMethod")
    public String paymentMethod;
	
	@SheetColumn("paymentProcessorName")
    public String paymentProcessorName;
	
	@SheetColumn("paymentProcessorTransferId")
    public String paymentProcessorTransferId;
	
	@SheetColumn("merchantFundingMode")
    public String merchantFundingMode;
	
	@SheetColumn("merchantCommissionFix")
    public String merchantCommissionFix;
	
	@SheetColumn("merchantCommissionVariable")
    public String merchantCommissionVariable;
	
	@SheetColumn("merchantPamFix")
    public String merchantPamFix;
	
	@SheetColumn("merchantPamVariable")
    public String merchantPamVariable;
	
	@SheetColumn("vatTaxFee")
    public String vatTaxFee;
	
	@SheetColumn("commissionAdd")
    public String commissionAdd;
	
	@SheetColumn("pamAdd")
    public String pamAdd;
	
	@SheetColumn("commissionRemove")
    public String commissionRemove;
	
	@SheetColumn("pamRemove")
    public String pamRemove;
	
	@SheetColumn("cancellationFee")
    public String cancellationFee;
	
	@SheetColumn("vatTaxFeeAdd")
    public String vatTaxFeeAdd;
	
	@SheetColumn("vatTaxFeeRemove")
    public String vatTaxFeeRemove;
	
	@SheetColumn("refundAmount")
    public String refundAmount;
	
	@SheetColumn("refundCapital")
    public String refundCapital;
	
	@SheetColumn("refundAdminFee")
    public String refundAdminFee;
	
	@SheetColumn("refundInterest")
    public String refundInterest;
	
	@SheetColumn("refundInsurance")
    public String refundInsurance;
	
	@SheetColumn("countryCode")
    public String countryCode;
	
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getActions() {
		return actions;
	}
	public void setActions(String actions) {
		this.actions = actions;
	}
	public String getExternalReference() {
		return externalReference;
	}
	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}
	public String getfundingReference() {
		return fundingReference;
	}
	public void setfundingReference(String fundingReference) {
		this.fundingReference = fundingReference;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getBt_code() {
		return bt_code;
	}
	public void setBt_code(String bt_code) {
		this.bt_code = bt_code;
	}

	public String getAmountFees() {
		return amountFees;
	}
	public void setAmountFees(String amountFees) {
		this.amountFees = amountFees;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setcontractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	
    public String getMerchantGUID() {
		return merchantGUID;
	}
	public void setMerchantGUID(String merchantGUID) {
		this.merchantGUID = merchantGUID;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMerchantSUID() {
		return merchantSUID;
	}
	public void setMerchantSUID(String merchantSUID) {
		this.merchantSUID = merchantSUID;
	}
	public String getCustomerGUID() {
		return customerGUID;
	}
	public void setCustomerGUID(String customerGUID) {
		this.customerGUID = customerGUID;
	}
	public String getCustomerSUID() {
		return customerSUID;
	}
	public void setCustomerSUID(String customerSUID) {
		this.customerSUID = customerSUID;
	}
	public String getBusinessTransactionCode() {
		return businessTransactionCode;
	}
	public void setBusinessTransactionCode(String businessTransactionCode) {
		this.businessTransactionCode = businessTransactionCode;
	}
	public String getBusinessTransactionType() {
		return businessTransactionType;
	}
	public void setBusinessTransactionType(String businessTransactionType) {
		this.businessTransactionType = businessTransactionType;
	}
	public String getNumberOfInstallment() {
		return numberOfInstallment;
	}
	public void setNumberOfInstallment(String numberOfInstallment) {
		this.numberOfInstallment = numberOfInstallment;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getFinancingId() {
		return financingId;
	}
	public void setFinancingId(String financingId) {
		this.financingId = financingId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public String getInstallmentNumber() {
		return installmentNumber;
	}
	public void setInstallmentNumber(String installmentNumber) {
		this.installmentNumber = installmentNumber;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public String getInterestFix() {
		return interestFix;
	}
	public void setInterestFix(String interestFix) {
		this.interestFix = interestFix;
	}
	public String getInterestVariable() {
		return interestVariable;
	}
	public void setInterestVariable(String interestVariable) {
		this.interestVariable = interestVariable;
	}
	public String getAdminFee() {
		return adminFee;
	}
	public void setAdminFee(String adminFee) {
		this.adminFee = adminFee;
	}
	public String getInsurance() {
		return insurance;
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	public String getInterestAdd() {
		return interestAdd;
	}
	public void setInterestAdd(String interestAdd) {
		this.interestAdd = interestAdd;
	}
	public String getAdminFeeAdd() {
		return adminFeeAdd;
	}
	public void setAdminFeeAdd(String adminFeeAdd) {
		this.adminFeeAdd = adminFeeAdd;
	}
	public String getInsuranceAdd() {
		return insuranceAdd;
	}
	public void setInsuranceAdd(String insuranceAdd) {
		this.insuranceAdd = insuranceAdd;
	}
	public String getInterestRemove() {
		return interestRemove;
	}
	public void setInterestRemove(String interestRemove) {
		this.interestRemove = interestRemove;
	}
	public String getAdminFeeRemove() {
		return adminFeeRemove;
	}
	public void setAdminFeeRemove(String adminFeeRemove) {
		this.adminFeeRemove = adminFeeRemove;
	}
	public String getInsuranceRemove() {
		return insuranceRemove;
	}
	public void setInsuranceRemove(String insuranceRemove) {
		this.insuranceRemove = insuranceRemove;
	}
	public String getStampTax() {
		return stampTax;
	}
	public void setStampTax(String stampTax) {
		this.stampTax = stampTax;
	}
	public String getStampCreditTax() {
		return stampCreditTax;
	}
	public void setStampCreditTax(String stampCreditTax) {
		this.stampCreditTax = stampCreditTax;
	}
	public String getStampCreditTaxAdd() {
		return stampCreditTaxAdd;
	}
	public void setStampCreditTaxAdd(String stampCreditTaxAdd) {
		this.stampCreditTaxAdd = stampCreditTaxAdd;
	}
	public String getStampCreditTaxRemove() {
		return stampCreditTaxRemove;
	}
	public void setStampCreditTaxRemove(String stampCreditTaxRemove) {
		this.stampCreditTaxRemove = stampCreditTaxRemove;
	}
	public String getStampTaxAdd() {
		return stampTaxAdd;
	}
	public void setStampTaxAdd(String stampTaxAdd) {
		this.stampTaxAdd = stampTaxAdd;
	}
	public String getStampTaxRemove() {
		return stampTaxRemove;
	}
	public void setStampTaxRemove(String stampTaxRemove) {
		this.stampTaxRemove = stampTaxRemove;
	}
	public String getSettlementAmountPaymentDefault() {
		return settlementAmountPaymentDefault;
	}
	public void setSettlementAmountPaymentDefault(String settlementAmountPaymentDefault) {
		this.settlementAmountPaymentDefault = settlementAmountPaymentDefault;
	}
	public String getPaymentDefaultFee() {
		return paymentDefaultFee;
	}
	public void setPaymentDefaultFee(String paymentDefaultFee) {
		this.paymentDefaultFee = paymentDefaultFee;
	}
	public String getCancellationAmount() {
		return cancellationAmount;
	}
	public void setCancellationAmount(String cancellationAmount) {
		this.cancellationAmount = cancellationAmount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentProcessorName() {
		return paymentProcessorName;
	}
	public void setPaymentProcessorName(String paymentProcessorName) {
		this.paymentProcessorName = paymentProcessorName;
	}
	public String getPaymentProcessorTransferId() {
		return paymentProcessorTransferId;
	}
	public void setPaymentProcessorTransferId(String paymentProcessorTransferId) {
		this.paymentProcessorTransferId = paymentProcessorTransferId;
	}
	public String getMerchantFundingMode() {
		return merchantFundingMode;
	}
	public void setMerchantFundingMode(String merchantFundingMode) {
		this.merchantFundingMode = merchantFundingMode;
	}
	public String getMerchantCommissionFix() {
		return merchantCommissionFix;
	}
	public void setMerchantCommissionFix(String merchantCommissionFix) {
		this.merchantCommissionFix = merchantCommissionFix;
	}
	public String getMerchantCommissionVariable() {
		return merchantCommissionVariable;
	}
	public void setMerchantCommissionVariable(String merchantCommissionVariable) {
		this.merchantCommissionVariable = merchantCommissionVariable;
	}
	public String getMerchantPamFix() {
		return merchantPamFix;
	}
	public void setMerchantPamFix(String merchantPamFix) {
		this.merchantPamFix = merchantPamFix;
	}
	public String getMerchantPamVariable() {
		return merchantPamVariable;
	}
	public void setMerchantPamVariable(String merchantPamVariable) {
		this.merchantPamVariable = merchantPamVariable;
	}
	public String getVatTaxFee() {
		return vatTaxFee;
	}
	public void setVatTaxFee(String vatTaxFee) {
		this.vatTaxFee = vatTaxFee;
	}
	public String getCommissionAdd() {
		return commissionAdd;
	}
	public void setCommissionAdd(String commissionAdd) {
		this.commissionAdd = commissionAdd;
	}
	public String getPamAdd() {
		return pamAdd;
	}
	public void setPamAdd(String pamAdd) {
		this.pamAdd = pamAdd;
	}
	public String getCommissionRemove() {
		return commissionRemove;
	}
	public void setCommissionRemove(String commissionRemove) {
		this.commissionRemove = commissionRemove;
	}
	public String getPamRemove() {
		return pamRemove;
	}
	public void setPamRemove(String pamRemove) {
		this.pamRemove = pamRemove;
	}
	public String getCancellationFee() {
		return cancellationFee;
	}
	public void setCancellationFee(String cancellationFee) {
		this.cancellationFee = cancellationFee;
	}
	public String getVatTaxFeeAdd() {
		return vatTaxFeeAdd;
	}
	public void setVatTaxFeeAdd(String vatTaxFeeAdd) {
		this.vatTaxFeeAdd = vatTaxFeeAdd;
	}
	public String getVatTaxFeeRemove() {
		return vatTaxFeeRemove;
	}
	public void setVatTaxFeeRemove(String vatTaxFeeRemove) {
		this.vatTaxFeeRemove = vatTaxFeeRemove;
	}
	public String getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}
	public String getRefundCapital() {
		return refundCapital;
	}
	public void setRefundCapital(String refundCapital) {
		this.refundCapital = refundCapital;
	}
	public String getRefundAdminFee() {
		return refundAdminFee;
	}
	public void setRefundAdminFee(String refundAdminFee) {
		this.refundAdminFee = refundAdminFee;
	}
	public String getRefundInterest() {
		return refundInterest;
	}
	public void setRefundInterest(String refundInterest) {
		this.refundInterest = refundInterest;
	}
	public String getRefundInsurance() {
		return refundInsurance;
	}
	public void setRefundInsurance(String refundInsurance) {
		this.refundInsurance = refundInsurance;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

}
