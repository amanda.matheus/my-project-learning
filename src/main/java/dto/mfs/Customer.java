package dto.mfs;

import java.util.List;

public class Customer {

    public String contractref;
    public List<Invoice> invoices;
}
