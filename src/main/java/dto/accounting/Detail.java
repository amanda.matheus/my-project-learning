package dto.accounting;

public class Detail {
	 	public String lineNumber;
	    public String eventType;
	    public String eventDate;
	    public String countryCode;
	    public String fundingReference;
	    public String merchantGUID;
	    public String merchantName;
	    public String merchantSUID;
	    public String customerGUID;
	    public String customerSUID;
	    public String businessTransactionCode;
	    public String businessTransactionType;
	    public String numberOfInstallment;
	    public String channel;
	    public String paymentAmount;
	    public String currencyCode;
	    public String financingId;
	    public String invoiceId;
	    public String installmentAmount;
	    public String installmentNumber;
	    public String capital;
	    public String interestFix;
	    public String interestVariable;
	    public String adminFee;
	    public String insurance;
	    public String interestAdd;
	    public String adminFeeAdd;
	    public String insuranceAdd;
	    public String interestRemove;
	    public String adminFeeRemove;
	    public String insuranceRemove;
	    public String stampTax;
	    public String stampCreditTax;
	    public String stampCreditTaxAdd;
	    public String stampCreditTaxRemove;
	    public String stampTaxAdd;
	    public String stampTaxRemove;
	    public String settlementAmountPaymentDefault;
	    public String paymentDefaultFee;
	    public String cancellationAmount;
	    public String paymentMethod;
	    public String paymentProcessorName;
	    public String paymentProcessorTransferId;
	    public String merchantFundingMode;
	    public String merchantCommissionFix;
	    public String merchantCommissionVariable;
	    public String merchantPamFix;
	    public String merchantPamVariable;
	    public String vatTaxFee;
	    public String commissionAdd;
	    public String pamAdd;
	    public String commissionRemove;
	    public String pamRemove;
	    public String cancellationFee;
	    public String vatTaxFeeAdd;
	    public String vatTaxFeeRemove;
	    public String refundAmount;
	    public String refundCapital;
	    public String refundAdminFee;
	    public String refundInterest;
	    public String refundInsurance;
	    public String period_start;
	    public String period_end;
	    public String payouts_currency;
	    public String payout_carried_forward_amount;
	    public String current_period_amount;
	    public String net_amount;
	    public String status;
	    public String payout_fee;
	    public String processed_amount;
	    public String refund_amount;
	    public String chargeback_amount;
	    public String payouts_to_card_amount;
	    public String processing_fees;
	    public String interchange_fees;
	    public String scheme_and_other_network_fees;
	    public String premium_and_apm_fees;
	    public String chargeback_fees;
	    public String payout_to_card_fees;
	    public String payment_gateway_fees;
	    public String rolling_reserve_amount;
	    public String tax;
	    public String admin_fees;
	    public String general_adjustments;
	    
	    
	   

}
