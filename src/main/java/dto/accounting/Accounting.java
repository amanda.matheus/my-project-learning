package dto.accounting;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//import java.awt.List;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import dto.excelFile.ExcelObject;
import io.github.millij.poi.SpreadsheetReadException;
import io.github.millij.poi.ss.reader.XlsxReader;
import pageObjects.backOffice.HomePageBackOffice;
import utils.Utils;

public class Accounting {


	public WebDriver driver;
	public WebDriverWait wait;
	public Utils utils;
	public Detail details;
	
	public Accounting(WebDriver driver) {

		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		utils = PageFactory.initElements(driver, Utils.class);
		details = PageFactory.initElements(driver, Detail.class);
	}
	
	public void validateGenerationEventType(String eventType, String tenant) throws FileNotFoundException, IOException {
		
		// Declares Gson lib from google to read Json
		Gson gson = new Gson();
		//Reads Json and convert it into a java object AccountingFile.
	    AccountingFile accFile = gson.fromJson(new FileReader(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName), AccountingFile.class);
	    //Get size from Java  object
	    int countJson = accFile.detail.size();
	    System.out.println(countJson);
			    
	    for (int i = 0; i < countJson; i++) {
			    	
	    	String y = accFile.detail.get(i).eventType;
	    	
    		
	    	if (!y.equals(eventType)) {
	    		
	    		if (i==countJson) {
	    			writeErrorFile(tenant,eventType,"It doesnt have", "Generate", "The event was not found" );
	    			System.out.println("ficheiro erro");
	    		}
	    		continue;
	    	
	    		//if it reaches this part, it gets out of the loop because it found both the contract and event.
	    	}else {
	    		System.out.println("It does have the eventType " + eventType );
	    		break;
	    	}
	    }
			    
	}
	
	public void validateGenerationEventTypeByAction(String eventType, String filePathExcel,String action, String tenant) throws JsonSyntaxException, JsonIOException, SpreadsheetReadException, IOException {
		
		// Declares Gson lib from google to read Json
		Gson gson = new Gson();
		//Reads Json and convert it into a java object AccountingFile.
		FileReader readerJson = new FileReader(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName);
	    AccountingFile accFile = gson.fromJson(readerJson, AccountingFile.class);
	    //Get size from Java  object
	    int countJson = accFile.detail.size();
	    System.out.println(countJson);
	    
	    File file = new File(filePathExcel);
		
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			
		} else {
			
		    //Declares XLsx reader 
			XlsxReader reader = new XlsxReader();
			//Reads the file and convert it into a java excelObject
			List<ExcelObject> excelObject = reader.read(ExcelObject.class, file);
			  //Get size from Java  object
			int countExcel = excelObject.size();
			
			//Iterator to catch the contractNumber
			for(int z = 0; z < countExcel; z++) {
				System.out.println("Action - " + excelObject.get(z).actions + " " + action+ " Status - " + excelObject.get(z).contractStatus + " contractNumber - " + excelObject.get(z).fundingReference );
				
				if (!excelObject.get(z).actions.equalsIgnoreCase(action)) {
					continue;
				} else if (excelObject.get(z).contractStatus.contains("WAITING DELIVERY") ||
						   excelObject.get(z).contractStatus.contains("REJECTED") ||
						   excelObject.get(z).contractStatus.contains("ABANDONED")||
						   excelObject.get(z).contractStatus.contains("DELETED") ||
						   excelObject.get(z).contractStatus.contains("PRE ACCEPTED")||
						   excelObject.get(z).contractStatus.equals("")){//verify if contract was accpeted, otherwise skips it.
			
					continue;
				 } else if (excelObject.get(z).fundingReference.equals("") || excelObject.get(z).fundingReference.equals(null) ) {
							System.out.println(excelObject.get(z).fundingReference);
					continue;
				 }
				System.out.println("EContract " + excelObject.get(z).fundingReference + " " + excelObject.get(z).contractStatus );
				//With the contract number, now we build an iterator to verify in the Json java object if the contract exist.
			    for (int i = 0; i < countJson; i++) {
			    	
			    	String x = accFile.detail.get(i).fundingReference;
			    	String y = accFile.detail.get(i).eventType;
	
			    	//if it found both the contract and event.
			    	if ((excelObject.get(z).fundingReference.equals(x)) & (accFile.detail.get(i).eventType.equals(eventType))) {
			    		System.out.println("JContract " + x + " JEvent " + y);
			    		break;
			    	} else {
			    		if ((i==countJson-1) || (accFile.detail.get(i).eventType.equals("27"))) {
			    			writeErrorFile(tenant,eventType,excelObject.get(z).fundingReference, "Generate", "The event was not found" );
			    			System.out.println("ficheiro erro");
			    		}
			    		continue;
			    	}
	
			    }
			}
		}	
		readerJson.close();
	}
		
	public void validateGenerationError(String filePathExcel, String tenant) throws JsonSyntaxException, JsonIOException, SpreadsheetReadException, IOException {
		
		// Declares Gson lib from google to read Json
		Gson gson = new Gson();
		//Reads the file and convert it into a java excelObject
		System.out.println(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName);
	    AccountingFile accFile = gson.fromJson(new FileReader(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName), AccountingFile.class);
	    //Get size from Java  object
	    int countJson = 0;
	    if (accFile.getFooterNumberOfEvents()==0) {
	    	countJson = 0;
	    } else {
	    	countJson = accFile.detail.size();	
	    }
	
	    
	    File file = new File(filePathExcel);
		
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			
		} else {
			
			   //Declares XLsx reader 
			XlsxReader reader = new XlsxReader();
			//Reads the file and convert it into a java excelObject
			List<ExcelObject> excelObject = reader.read(ExcelObject.class, file);
			//Get size from Java  object
			int countExcel = excelObject.size();
			
			//Iterator to catch the contractNumber
			for(int z = 0; z < countExcel; z++) {
				
				String excelFundingReference = excelObject.get(z).fundingReference;
			    String excelEventType = excelObject.get(z).eventType;
			  //With the contract number, now we build an iterator to verify in the Json java object if the contract exist. 
			    for (int i = 0; i < countJson; i++) {
			    	
			    	String jsonFundingReference = accFile.detail.get(i).fundingReference;
			    	String jsonEventType = accFile.detail.get(i).eventType;
			    	//verifies before if it is the event 27, because there's no fundingReference
			    	if (jsonEventType.equals("27")) {
			    		//if it goes through here, there's no possibility of it being inside the file, because the 27th event is the last event.
		    			continue;
		    			// if the contract isn't the same as the one inside the file and the event wasn't informed in the method between if and continue.
			    	} else if ((!jsonFundingReference.equals(excelFundingReference)) && (!excelEventType.equals(jsonEventType))) {
			    		continue;
			    		//if it reaches this part of the loop, is because the contract and event was found. Then it erases this line from the files.
			    	}else if ((jsonFundingReference.equals(excelFundingReference)) && (jsonEventType.equals(excelEventType))) {
			    		System.out.println("Remoção do evento: JContract " + jsonFundingReference + " JEvent " + jsonEventType);
			    		removeRowErrorFile(filePathExcel, jsonFundingReference, jsonEventType,"Generate" );
			    		
			    	}
	
			    }
			}
		}	
		
	}
	
	public void writeErrorFile(String tenant, String eventType, String fundingReference, String sheet, String error) throws FileNotFoundException, IOException {
		
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String today = now.format(formatter);
		
		//generates the name of the error file in the error folder
		String fileNameAcc = today + "_" + tenant + "_Error";
		String fileNamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\" + fileNameAcc + ".xlsx";
		
		File file = new File(fileNamePath);
		XSSFWorkbook workbook;
		Sheet sheet1;
				
		//creates a file if it doesn't exist
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			
			workbook = new XSSFWorkbook();
			XSSFCellStyle style = workbook.createCellStyle();
			style.setWrapText(true);
			sheet1 = workbook.createSheet(sheet); 
			Row rowhead = sheet1.createRow(0);
		    rowhead.createCell(0).setCellValue("eventDate");
		    rowhead.createCell(1).setCellValue("fundingReference");
		    rowhead.createCell(2).setCellValue("eventType");
		    rowhead.createCell(3).setCellValue("error");
		    Row row = sheet1.createRow(1);
		    row.setRowStyle(style);
		    row.createCell(0).setCellValue(today);
		    row.createCell(1).setCellValue(fundingReference);
		    row.createCell(2).setCellValue(eventType); 
		    row.createCell(3).setCellValue(error); 
		    row.getCell(3).setCellStyle(style);
		    //includes a new line if it already exists
		} else {
	        try ( 
	            // Make current input to exist file
	            InputStream is = new FileInputStream(file)) {
	                workbook = new XSSFWorkbook(is);
	            }
	        XSSFCellStyle style = workbook.createCellStyle();
			style.setWrapText(true);
	        int sheetExist = 0;
	        if (workbook.getNumberOfSheets() != 0) {
	            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
	            	System.out.println(workbook.getSheetName(i));
	               if (workbook.getSheetName(i).equals(sheet)) {
	            	   sheetExist=1;
	            	   break;
	                }
	            }  
	        }
	        
	        if (sheetExist==1){
	        	sheet1 = workbook.getSheet(sheet);
	        } else {
	            // Create new sheet to the workbook if empty
	        	sheet1 = workbook.createSheet(sheet);
	        	Row rowhead = sheet1.createRow(0);
			    rowhead.createCell(0).setCellValue("eventDate");
			    rowhead.createCell(1).setCellValue("fundingReference");
			    rowhead.createCell(2).setCellValue("eventType");
			    rowhead.createCell(3).setCellValue("error");
	        } 
	        Row row = sheet1.createRow(sheet1.getLastRowNum()+1);
		    row.setRowStyle(style);
		    row.createCell(0).setCellValue(today);
		    row.createCell(1).setCellValue(fundingReference);
		    row.createCell(2).setCellValue(eventType); 
		    row.createCell(3).setCellValue(error);  
		    row.getCell(3).setCellStyle(style);
	    }	
		
		for (int i = 0; i < 10; i++) {
			sheet1 = workbook.getSheet(sheet);
        	sheet1.autoSizeColumn(i);
        } 
        FileOutputStream fileOut = new FileOutputStream(fileNamePath);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();	    
		
		System.out.println("Your xlsx file has been generated!");
		
	}
	
	public void removeRowErrorFile(String filenamePath, String fundingReference, String eventType, String sheet) throws FileNotFoundException, IOException {
		// it deletes the substitutes the row.
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
            	
		Row row = null;
		if (file.exists()){ 
			try ( 
		            
		            InputStream is = new FileInputStream(file)) {
						workbook = new XSSFWorkbook(is);
		            }
		    Sheet sheet1 = workbook.getSheet(sheet); 

		    for (int rowIndex = 1; rowIndex <= sheet1.getLastRowNum(); rowIndex++) {
				row = sheet1.getRow(rowIndex);
				if (row!=null) {
					if (row.getCell(1).getStringCellValue().equals(fundingReference) && row.getCell(2).getStringCellValue().equals(eventType)) {
							System.out.println("delete");
							sheet1.shiftRows(row.getRowNum() + 1, sheet1.getLastRowNum() + 1, -1);
							rowIndex--;
//							sheet1.shiftRows(rowIndex,rowIndex+1,1);
							break;
					}
				}	
			}				
		    
		    FileOutputStream fileOut = new FileOutputStream(filenamePath);
		     workbook.write(fileOut);
		     fileOut.close();
		     workbook.close();	    
			
		}
		 
		System.out.println("Your xlsx file has been updated!");
		
	}
	
	public void validateFieldsEventType(String eventTypeString, String tenant) throws JsonSyntaxException, JsonIOException, IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		
		//decalres the GSON lib from google to read JSON
		Gson gson = new Gson();
		//reads the Json and converts it in java object accountingFile
	    AccountingFile accFile = gson.fromJson(new FileReader(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName), AccountingFile.class);
	   // gets the java size
	    int count = accFile.detail.size();
	    
	    //iterator to run all the java object
	    for (int i = 0; i < count; i++) {
	    	
	
	    	//if it's not available it skips to the next
	    	if (!accFile.detail.get(i).eventType.equals(eventTypeString)) {
	    		continue;
	    	}
	    	//validates the existence of the fields from each event
	    	switch (eventTypeString) {
			case "01":
				
				accFile.validateFieldsEventType01(i);
				break;				
			case "02":
			
				accFile.validateFieldsEventType02(i);
				break;			
			case "03":
				
				accFile.validateFieldsEventType03(i);
				break;
			case "04":
					
				accFile.validateFieldsEventType04(i);
				break;
			case "05":
					
				accFile.validateFieldsEventType05(i);
				break;
			case "06":
					
				accFile.validateFieldsEventType06(i);
				break;
			case "07":
					
				accFile.validateFieldsEventType07(i);
				break;
			case "08":
					
				accFile.validateFieldsEventType08(i);
				break;
			case "09":
					
				accFile.validateFieldsEventType09(i);
				break;
			case "10":
					
				accFile.validateFieldsEventType10(i);
				break;
			case "11":
					
				accFile.validateFieldsEventType11(i);
				break;
			case "12":
					
				accFile.validateFieldsEventType12(i);
				break;
			case "13":
					
				accFile.validateFieldsEventType13(i);
				break;
			case "14":
					
				accFile.validateFieldsEventType14(i);
				break;
			case "15":
					
				accFile.validateFieldsEventType15(i, tenant);
				break;
			case "16":
					
				accFile.validateFieldsEventType16(i);
				break;
			case "18":
					
				accFile.validateFieldsEventType18(i);
				break;
			case "19":
					
				accFile.validateFieldsEventType19(i);
				break;
			case "20":
					
				accFile.validateFieldsEventType20(i);
				break;
			case "21":
					
				accFile.validateFieldsEventType21(i);
				break;
			case "22":
					
				accFile.validateFieldsEventType22(i);
				break;
			case "23":
					
				accFile.validateFieldsEventType23(i);
				break;
			case "24":
					
				accFile.validateFieldsEventType24(i);
				break;
			case "25":
					
				accFile.validateFieldsEventType25(i);
				break;
			case "26":
					
				accFile.validateFieldsEventType26(i);
				break;
			case "27":
					
				accFile.validateFieldsEventType27(i);
				break;
			case "02v2":
			
				accFile.validateFieldsEventType02v2(i);
				break;		
			case "04v2":
					
				accFile.validateFieldsEventType04v2(i);
				break;
			case "06v2":
					
				accFile.validateFieldsEventType06v2(i);
				break;
			case "07v2":
					
				accFile.validateFieldsEventType07v2(i);
				break;
			case "08v2":
					
				accFile.validateFieldsEventType08v2(i);
				break;
			case "10v2":
					
				accFile.validateFieldsEventType10v2(i);
				break;
			case "11v2":
					
				accFile.validateFieldsEventType11v2(i);
				break;
			case "12v2":
					
				accFile.validateFieldsEventType12v2(i);
				break;
			case "13v2":
					
				accFile.validateFieldsEventType13v2(i);
				break;
			case "16v2":
					
				accFile.validateFieldsEventType16v2(i);
				break;
			case "18v2":
					
				accFile.validateFieldsEventType18v2(i);
				break;
			case "19v2":
					
				accFile.validateFieldsEventType19v2(i);
				break;
			case "20v2":
					
				accFile.validateFieldsEventType20v2(i);
				break;
			case "21v2":
					
				accFile.validateFieldsEventType21v2(i);
				break;
			case "22v2":
					
				accFile.validateFieldsEventType22v2(i);
				break;
			case "23v2":
					
				accFile.validateFieldsEventType23v2(i);
				break;
			case "24v2":
					
				accFile.validateFieldsEventType24v2(i);
				break;
			case "25v2":
					
				accFile.validateFieldsEventType25v2(i);
				break;
			case "26v2":
					
				accFile.validateFieldsEventType26v2(i);
				break;
	
			default:
					System.out.println("Please insert correct eventType");
			}
	    		
	    }
	    
//	    System.out.println(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\dailyFile\\"+ HomePageBackOffice.fileName);
//	    utils.deleteFile(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\dailyFile\\"+ HomePageBackOffice.fileName);
	}
	
	public void createWeekErrorFile(String tenant) throws SpreadsheetReadException, FileNotFoundException, IOException {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
				
		for (int i = 8; i>1; i--) {
				LocalDate today = LocalDate.now();
			
				String fileName = today.minusDays(i).format(formatter) +  "_" + tenant + "_Error.xlsx";
				String fileNamePath = System.getProperty("user.home") + 
						"\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\" + fileName;
				
				System.out.println(fileName);
				File file = new File(fileNamePath);
				
						
				if (!file.exists() && !file.isDirectory()){ //checking file availability
					continue;
				} else {

					XlsxReader reader = new XlsxReader();
					List<ExcelObject> excelObject = reader.read(ExcelObject.class, file);
					int countExcel = excelObject.size();
					System.out.println(countExcel);
					
					if (countExcel==0) {
						continue;
					} else {
						for(int z = 0; z < countExcel; z++) {
							writeErrorFileWeek(tenant,excelObject.get(z).eventDate, excelObject.get(z).eventType, excelObject.get(z).fundingReference,"Generate","The event was not found."  );
						}
						System.out.println("Your xlsx file has been generated!");
					}
				}
//				utils.deleteFile(fileNamePath);
	
			}
	}
	
	public void writeErrorFileWeek(String tenant, String eventDate, String eventType, String fundingReference, String sheet, String error) throws FileNotFoundException, IOException {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String dateBegin = utils.getLastWeekBegin().format(formatter);
		String dateEnd = utils.getLastWeekEnd().format(formatter);
							
		String fileNameAcc = dateBegin + "_" + dateEnd + "_" + tenant + "_Error";
		String fileNamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\" + fileNameAcc + ".xlsx";
		
		File file = new File(fileNamePath);
		XSSFWorkbook workbook;
		Sheet sheet1;
				
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			
			workbook = new XSSFWorkbook();
			sheet1 = workbook.createSheet(sheet); 
			Row rowhead = sheet1.createRow(0);
		    rowhead.createCell(0).setCellValue("eventDate");
		    rowhead.createCell(1).setCellValue("fundingReference");
		    rowhead.createCell(2).setCellValue("eventType");
		    rowhead.createCell(3).setCellValue("error");
		    Row row = sheet1.createRow(1);
		    row.createCell(0).setCellValue(eventDate);
		    row.createCell(1).setCellValue(fundingReference);
		    row.createCell(2).setCellValue(eventType);
		    row.createCell(3).setCellValue(error);
		//inclui uma nova linha se já existe    
		} else {
	        try ( 
	            // Make current input to exist file
	            InputStream is = new FileInputStream(file)) {
	                workbook = new XSSFWorkbook(is);
	            }
	        int sheetExist = 0;
	        if (workbook.getNumberOfSheets() != 0) {
	            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
	            	System.out.println(workbook.getSheetName(i));
	               if (workbook.getSheetName(i).equals(sheet)) {
	            	   sheetExist=1;
	            	   break;
	                }
	            }  
	        }
	        
	        if (sheetExist==1){
	        	sheet1 = workbook.getSheet(sheet);
	        } else {
	            // Create new sheet to the workbook if empty
	        	sheet1 = workbook.createSheet(sheet);
	        	Row rowhead = sheet1.createRow(0);
			    rowhead.createCell(0).setCellValue("eventDate");
			    rowhead.createCell(1).setCellValue("fundingReference");
			    rowhead.createCell(2).setCellValue("eventType");
			    rowhead.createCell(3).setCellValue("error");
	        } 
	        Row row = sheet1.createRow(sheet1.getLastRowNum()+1);
		    row.createCell(0).setCellValue(eventDate);
		    row.createCell(1).setCellValue(fundingReference);
		    row.createCell(2).setCellValue(eventType); 
		    row.createCell(3).setCellValue(error); 
	    }	
		
		for (int i = 0; i < 10; i++) {
			sheet1 = workbook.getSheet(sheet);
        	sheet1.autoSizeColumn(i);
        } 
        FileOutputStream fileOut = new FileOutputStream(fileNamePath);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();	    
		
	}
			
	public void validateGenerationEventTypeOfTheWeek(String eventType, String tenant) throws JsonSyntaxException, JsonIOException, SpreadsheetReadException, IOException {
				
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String dateBegin = utils.getLastWeekBegin().format(formatter);
		String dateEnd = utils.getLastWeekEnd().format(formatter);
		
		String fileNameError = dateBegin + "_" + dateEnd + "_" + tenant + "_Error";
		String fileNamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\" + fileNameError + ".xlsx";
		
	    File file = new File(fileNamePath);
		
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			
		} else {
			    
			XlsxReader reader = new XlsxReader();
			List<ExcelObject> excelObject = reader.read(ExcelObject.class, file);
			int countExcel = excelObject.size();
			
		
			for(int z = 0; z < countExcel; z++) {
				
				assert (!excelObject.get(z).eventType.contains(eventType)); 
	
			 }
			}
		}	    
	
	public void validateDataEventTypeByAction(String eventType, String filePathExcel,String filePathJson, String action, String tenant) throws JsonSyntaxException, JsonIOException, SpreadsheetReadException, IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		
		
		Gson gson = new Gson();
		
		FileReader readerJson = new FileReader(filePathJson);
	    AccountingFile json = gson.fromJson(readerJson, AccountingFile.class);
	    
	    int countJson = json.detail.size();
	    System.out.println("tamanho json" + countJson);
	    
	    File file = new File(filePathExcel);
		System.out.println(file.exists());
		if (file.exists()){ 
			
		   System.out.println("ficheiro existe" + filePathExcel);
			XlsxReader reader = new XlsxReader();
			List<ExcelObject> xlsx = reader.read(ExcelObject.class, file);
			int countExcel = xlsx.size();
			
			for(int z = 0; z < countExcel; z++) {
							
				if (!xlsx.get(z).actions.contains(action)) {
					continue;
				} else if (xlsx.get(z).contractStatus.contains("WAITING DELIVERY") ||
						   xlsx.get(z).contractStatus.contains("REJECTED") ||
						   xlsx.get(z).contractStatus.contains("ABANDONED")||
						   xlsx.get(z).contractStatus.contains("DELETED") ||
						   xlsx.get(z).contractStatus.contains("PRE ACCEPTED")||
						   xlsx.get(z).contractStatus.equals("")){
			
					continue;
				 } else if (xlsx.get(z).fundingReference.equals("") || xlsx.get(z).fundingReference.equals(null) ) {
						System.out.println(xlsx.get(z).fundingReference);
						 continue;
				 } else if (action.contains("ContractCreated")) {
					 if ((xlsx.get(z).merchantName.contains("Marketplace")) || (xlsx.get(z).merchantName.contains("Multicapture"))) {
						 continue;
					 }
				 } 
				
			    for (int i = 0; i < countJson; i++) {
			    				
			    	if ((xlsx.get(z).fundingReference.equals(json.detail.get(i).fundingReference)) & (json.detail.get(i).eventType.equals(eventType))) {
			    		switch (eventType) {
						case "01":
							if (!json.validateDataEventType01(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType01(i,xlsx.get(z)) );
							}			
							break;				
						case "02":
							if (!json.validateDataEventType02(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType02(i,xlsx.get(z)));
							}							
							break;	
						case "06":
							if (!json.validateDataEventType06(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType06(i,xlsx.get(z)));
							}								
							break;
						case "10":
							if (!json.validateDataEventType10(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType10(i,xlsx.get(z)));
							}							
							break;
						case "11":
							if (!json.validateDataEventType11(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType11(i,xlsx.get(z)));
							}							
							break;
						case "12":
							if (!json.validateDataEventType12(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType12(i,xlsx.get(z)));
							}							
							break;
						case "13":
							if (!json.validateDataEventType13(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType13(i,xlsx.get(z)));
							}							
							break;
						case "14":
							if (!json.validateDataEventType14(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType14(i,xlsx.get(z)) );
							}							
							break;
						case "15":
							if (!json.validateDataEventType15(i, xlsx.get(z),xlsx.get(z).countryCode).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType15(i, xlsx.get(z), xlsx.get(z).countryCode));
							}							
							break;
						case "16":
							if (!json.validateDataEventType16(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType16(i,xlsx.get(z)));
							}							
							break;
						case "18":
							if (!json.validateDataEventType18(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType18(i,xlsx.get(z)));
							}							
							break;
						case "19":
							if (!json.validateDataEventType19(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType19(i,xlsx.get(z)) );
							}								
							break;
						case "21":
							if (!json.validateDataEventType21(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType21(i,xlsx.get(z)));
							}	
							break;
						case "22":
							if (!json.validateDataEventType22(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType22(i,xlsx.get(z)));
							}								
							break;
						case "23":
							if (!json.validateDataEventType23(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType23(i,xlsx.get(z)));
							}							
							break;
						case "24":
							if (!json.validateDataEventType24(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType24(i,xlsx.get(z)));
							}								
							break;
						case "25":
							if (!json.validateDataEventType25(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType25(i,xlsx.get(z)));
							}								
							break;
						case "26":
							if (!json.validateDataEventType26(i,xlsx.get(z)).equals("")) {
								writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType26(i,xlsx.get(z)));
							}								
							break;
						}
			    		
			    		
			    		if ((xlsx.get(z).fundingReference.equals(json.detail.get(i).fundingReference)) & (json.detail.get(i).eventType.equals(eventType)) & (xlsx.get(z).installmentNumber.equals(json.detail.get(i).installmentNumber))) {
						    	
			    			switch (eventType) {
			    			case "03":
								if (!json.validateDataEventType03(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType03(i,xlsx.get(z)));
								}							
								break;
			    			case "04":
								if (!json.validateDataEventType04(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType04(i,xlsx.get(z)));
								}							
								break;								
							case "05":
								if (!json.validateDataEventType05(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType05(i,xlsx.get(z)));
								}								
								break;								
			    			case "07":
								if (!json.validateDataEventType07(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType07(i,xlsx.get(z)));
								}						
								break;
							case "08":
								if (!json.validateDataEventType08(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType08(i,xlsx.get(z)));
								}	
								break;
							case "09":
								if (!json.validateDataEventType09(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType09(i,xlsx.get(z)));
								}							
								break;
			    			case "20":
								if (!json.validateDataEventType20(i,xlsx.get(z)).equals("")) {
									writeErrorFile(tenant,eventType,xlsx.get(z).fundingReference, "Data", json.validateDataEventType20(i,xlsx.get(z)));
								}							
								break;
								
			    			}
			    			
			    		}
			    		
			    	} else {
			    		continue;
			    	}
	
			    }
			}
		}	
		readerJson.close();
	}
	
	
}
