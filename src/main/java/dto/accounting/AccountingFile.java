package dto.accounting;


import java.lang.reflect.Field;
import java.util.List;

import dto.excelFile.ExcelObject;

public class AccountingFile {
	public Header header;
    public List<Detail> detail;
    public Footer footer;
    
    
 public void validateFieldsEventType01(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
	//fields in Event
	 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantGUID", "merchantName", "merchantSUID", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "currencyCode"};
		Class<Detail> auxClass = Detail.class;
		Detail myDetail = detail.get(i);
		for (String attribute:attributeListFieldsInEvents) {
			Field field = auxClass.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail);
			assert(itemValue!=null);}
		//fields should not be in Event
 	String[] attributeListFieldsNotInEvents = {"financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
		Class<Detail> auxClass2 = Detail.class;
		Detail myDetail2 = detail.get(i);
		for (String attribute:attributeListFieldsNotInEvents) {
			Field field = auxClass2.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail2);
			assert(itemValue==null);}

    } 
 
	 public void validateFieldsEventType02(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "paymentAmount", "currencyCode", "financingId"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
	}
		
 	public void validateFieldsEventType03(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		//fields in Event
	 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "businessTransactionCode", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod" };
		Class<Detail> auxClass = Detail.class;
		Detail myDetail = detail.get(i);
		for (String attribute:attributeListFieldsInEvents) {
			Field field = auxClass.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail);
			assert(itemValue!=null);}
		//fields should not be in Event
 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
		Class<Detail> auxClass2 = Detail.class;
		Detail myDetail2 = detail.get(i);
		for (String attribute:attributeListFieldsNotInEvents) {
			Field field = auxClass2.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail2);
			assert(itemValue==null);}
	}
 	
	
	public void validateFieldsEventType04(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		//fields in Event
	 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove"};
		Class<Detail> auxClass = Detail.class;
		Detail myDetail = detail.get(i);
		for (String attribute:attributeListFieldsInEvents) {
			Field field = auxClass.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail);
			assert(itemValue!=null);}
		//fields should not be in Event
 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
		Class<Detail> auxClass2 = Detail.class;
		Detail myDetail2 = detail.get(i);
		for (String attribute:attributeListFieldsNotInEvents) {
			Field field = auxClass2.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail2);
			assert(itemValue==null);}
 
	}
	
	public void validateFieldsEventType05(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		//fields in Event
	 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "businessTransactionCode", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod"};
		Class<Detail> auxClass = Detail.class;
		Detail myDetail = detail.get(i);
		for (String attribute:attributeListFieldsInEvents) {
			Field field = auxClass.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail);
			assert(itemValue!=null);}
		//fields should not be in Event
 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
		Class<Detail> auxClass2 = Detail.class;
		Detail myDetail2 = detail.get(i);
		for (String attribute:attributeListFieldsNotInEvents) {
			Field field = auxClass2.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail2);
			assert(itemValue==null);}
	 	
		}
	
	
	public void validateFieldsEventType06(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		//fields in Event
	 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount" };
		Class<Detail> auxClass = Detail.class;
		Detail myDetail = detail.get(i);
		for (String attribute:attributeListFieldsInEvents) {
			Field field = auxClass.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail);
			assert(itemValue!=null);}
		//fields should not be in Event
 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "stampCreditTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
		Class<Detail> auxClass2 = Detail.class;
		Detail myDetail2 = detail.get(i);
		for (String attribute:attributeListFieldsNotInEvents) {
			Field field = auxClass2.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail2);
			assert(itemValue==null);}
	 
		}
	
	
	public void validateFieldsEventType07(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		//fields in Event
	 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId"};
		Class<Detail> auxClass = Detail.class;
		Detail myDetail = detail.get(i);
		for (String attribute:attributeListFieldsInEvents) {
			Field field = auxClass.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail);
			assert(itemValue!=null);}
		//fields should not be in Event
 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
		Class<Detail> auxClass2 = Detail.class;
		Detail myDetail2 = detail.get(i);
		for (String attribute:attributeListFieldsNotInEvents) {
			Field field = auxClass2.getDeclaredField(attribute);
			field.setAccessible(true);
			Object itemValue = field.get(myDetail2);
			assert(itemValue==null);}
	 
		}
	
		public void validateFieldsEventType08(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "paymentDefaultFee" };
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}

		}
	
		public void validateFieldsEventType09(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "businessTransactionCode", "currencyCode", "installmentNumber", "stampTax", "paymentDefaultFee"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}

		}
		
		public void validateFieldsEventType10(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "cancellationAmount","currencyCode"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType11(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "paymentAmount", "currencyCode", "invoiceId"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		}
		
		public void validateFieldsEventType12(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "paymentAmount", "currencyCode", "invoiceId"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType13(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "currencyCode", "invoiceId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType14(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantGUID", "merchantName", "merchantSUID", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "currencyCode"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		
		}
		
		public void validateFieldsEventType15(int i, String tenant) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			
		if (tenant.equals("PT")) { //the field stampTaxAdd for Portugal is stampTax
			//fields in Event
			 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "customerSUID", "businessTransactionCode", "currencyCode", "stampTax", "paymentDefaultFee"};
				Class<Detail> auxClass = Detail.class;
				Detail myDetail = detail.get(i);
				for (String attribute:attributeListFieldsInEvents) {
					Field field = auxClass.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail);
					assert(itemValue!=null);}
				//fields should not be in Event
		 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTaxAdd", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxRemove", "settlementAmountPaymentDefault", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
		 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments" };
				Class<Detail> auxClass2 = Detail.class;
				Detail myDetail2 = detail.get(i);
				for (String attribute:attributeListFieldsNotInEvents) {
					Field field = auxClass2.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail2);
					assert(itemValue==null);}
		} else {
			//fields in Event
			 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference",  "merchantSUID", "customerSUID", "businessTransactionCode", "currencyCode", "stampTaxAdd", "paymentDefaultFee"};
				Class<Detail> auxClass = Detail.class;
				Detail myDetail = detail.get(i);
				for (String attribute:attributeListFieldsInEvents) {
					Field field = auxClass.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail);
					assert(itemValue!=null);}
				//fields should not be in Event
		 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxRemove", "settlementAmountPaymentDefault", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
		 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments" };
				Class<Detail> auxClass2 = Detail.class;
				Detail myDetail2 = detail.get(i);
				for (String attribute:attributeListFieldsNotInEvents) {
					Field field = auxClass2.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail2);
					assert(itemValue==null);}
		}
			
			
			
		}
		
		public void validateFieldsEventType16(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTaxAdd", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType18(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "currencyCode", "merchantFundingMode", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		}
		
		public void validateFieldsEventType19(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "currencyCode", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID","customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		
		}
		
		public void validateFieldsEventType20(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType21(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "currencyCode", "financingId", "cancellationAmount"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}

		
		public void validateFieldsEventType22(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", "currencyCode", "invoiceId", "merchantFundingMode", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		
		}

		
		public void validateFieldsEventType23(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "paymentMethod", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType24(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance", };
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType25(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
			
		}
		
		public void validateFieldsEventType26(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
	 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		}
		
		public void validateFieldsEventType27(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			//fields in Event
		 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode",  "period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount",  "paymentProcessorTransferId","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
			Class<Detail> auxClass = Detail.class;
			Detail myDetail = detail.get(i);
			for (String attribute:attributeListFieldsInEvents) {
				Field field = auxClass.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail);
				assert(itemValue!=null);}
			//fields should not be in Event
	 	String[] attributeListFieldsNotInEvents = { "paymentProcessorName", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod","interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax","interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd","fundingReference", "customerSUID", "currencyCode", "capital", "interestFix","merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance"};
			Class<Detail> auxClass2 = Detail.class;
			Detail myDetail2 = detail.get(i);
			for (String attribute:attributeListFieldsNotInEvents) {
				Field field = auxClass2.getDeclaredField(attribute);
				field.setAccessible(true);
				Object itemValue = field.get(myDetail2);
				assert(itemValue==null);}
		}
		
		public int getFooterNumberOfEvents() {
			return footer.getNumberOfEvents();
		}
		
		public String validateDataEventType01(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		
			 String[] attributeListFieldsInEvents = { "countryCode", "fundingReference", "merchantGUID", "merchantName", "merchantSUID", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "currencyCode"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType02(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = {"countryCode", "fundingReference", "merchantSUID", "paymentAmount", "currencyCode"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType03(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = { "countryCode", "fundingReference", "customerSUID", "businessTransactionCode", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod" };
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType04(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = {"countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType05(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents =  {"countryCode", "fundingReference", "customerSUID", "businessTransactionCode", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur = retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType06(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents =  {"countryCode", "fundingReference", "customerSUID", "currencyCode", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount" };
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur = retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType07(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents =   {"countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod", "paymentProcessorName"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType08(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents =    {"countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "paymentDefaultFee" };
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		public String validateDataEventType09(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents =    { "countryCode", "fundingReference", "customerSUID", "businessTransactionCode", "currencyCode", "installmentNumber", "stampTax", "paymentDefaultFee"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType10(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents =    { "countryCode", "fundingReference", "merchantSUID", "cancellationAmount","currencyCode"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType11(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = { "countryCode", "fundingReference", "merchantSUID", "paymentAmount", "currencyCode", "invoiceId"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		}		
		
		public String validateDataEventType12(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={"countryCode", "fundingReference", "merchantSUID", "paymentAmount", "currencyCode", "invoiceId"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
	   }
		public String validateDataEventType13(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={ "countryCode", "fundingReference", "merchantSUID", "currencyCode", "invoiceId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
	   }
		
		public String validateDataEventType14(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = {"countryCode", "fundingReference", "merchantGUID", "merchantName", "merchantSUID", "customerGUID", "customerSUID", "businessTransactionCode", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "currencyCode"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		 }
		
		public String validateDataEventType15(int i, ExcelObject z, String tenant) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			if (tenant.equals("PT")) { //the field stampTaxAdd for Portugal is stampTax
				String[] attributeListFieldsInEvents =  {"countryCode", "fundingReference", "merchantSUID", "customerSUID", "businessTransactionCode", "currencyCode", "stampTax", "paymentDefaultFee"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
			} else {
				String[] attributeListFieldsInEvents =  {"countryCode", "fundingReference",  "merchantSUID", "customerSUID", "businessTransactionCode", "currencyCode", "stampTaxAdd", "paymentDefaultFee"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
			}
			
		 }
		
		public String validateDataEventType16(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={"countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		public String validateDataEventType18(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={ "countryCode", "fundingReference", "merchantSUID", "currencyCode", "merchantFundingMode", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
				
		public String validateDataEventType19(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = {"countryCode", "fundingReference", "merchantSUID", "currencyCode", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType20(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents = {"countryCode", "fundingReference", "customerSUID", "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod", "paymentProcessorName"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		    }
		
		public String validateDataEventType21(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={"countryCode", "fundingReference", "merchantSUID", "currencyCode", "cancellationAmount"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		public String validateDataEventType22(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={"countryCode", "fundingReference", "merchantSUID", "currencyCode", "merchantFundingMode", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		public String validateDataEventType23(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={"countryCode", "fundingReference", "customerSUID", "currencyCode", "paymentMethod", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		public String validateDataEventType24(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={ "countryCode", "fundingReference", "customerSUID", "currencyCode", "paymentMethod", "paymentProcessorName", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance", };
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		public String validateDataEventType25(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={"countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
	
		public String validateDataEventType26(int i, ExcelObject z) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

			 String[] attributeListFieldsInEvents ={ "countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName"};
				Class<Detail> json = Detail.class;
				Class<ExcelObject> xlsx = ExcelObject.class;
				Detail myJson = detail.get(i);
				ExcelObject myXlsx = z;
				String retur = "";
				for (String attribute:attributeListFieldsInEvents) {
					Field fieldJson = json.getDeclaredField(attribute);
					Field fieldXlsx = xlsx.getDeclaredField(attribute);
					fieldJson.setAccessible(true);
					fieldXlsx.setAccessible(true);
					Object jsonValue = fieldJson.get(myJson);
					Object xlsxValue = fieldXlsx.get(myXlsx);
					if (!String.valueOf(xlsxValue).equalsIgnoreCase(String.valueOf(jsonValue))) {
						
						retur += "\r\n" + "Field " + fieldJson.getName() + " " + "Excel:" + String.valueOf(xlsxValue) + " Json:" +  String.valueOf(jsonValue);
						System.out.println(retur);
					}
										
				}
				if (!retur.equals("")) {
					retur= retur.replaceFirst("\r\n", "");
				}
				return retur;
		  }
		
		
		
		
		
		 public void validateFieldsEventType02v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", 
						 "merchantSUID", "paymentAmount", "currencyCode", "financingId","businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID",  "businessTransactionType", "numberOfInstallment", "channel", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
			}
						
			public void validateFieldsEventType04v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
				//fields in Event
			 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", 
					 "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax",
					 "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove",  "businessTransactionCode"};
				Class<Detail> auxClass = Detail.class;
				Detail myDetail = detail.get(i);
				for (String attribute:attributeListFieldsInEvents) {
					Field field = auxClass.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail);
					assert(itemValue!=null);}
				//fields should not be in Event
		 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
		 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
				Class<Detail> auxClass2 = Detail.class;
				Detail myDetail2 = detail.get(i);
				for (String attribute:attributeListFieldsNotInEvents) {
					Field field = auxClass2.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail2);
					assert(itemValue==null);}
		 
			}
			
			
			public void validateFieldsEventType06v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
				//fields in Event
			 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", 
					 "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove",
					 "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount", "businessTransactionCode" };
				Class<Detail> auxClass = Detail.class;
				Detail myDetail = detail.get(i);
				for (String attribute:attributeListFieldsInEvents) {
					Field field = auxClass.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail);
					assert(itemValue!=null);}
				//fields should not be in Event
		 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "stampTax", "stampCreditTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
		 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
				Class<Detail> auxClass2 = Detail.class;
				Detail myDetail2 = detail.get(i);
				for (String attribute:attributeListFieldsNotInEvents) {
					Field field = auxClass2.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail2);
					assert(itemValue==null);}
			 
				}
			
			
			public void validateFieldsEventType07v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
				//fields in Event
			 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID",
					 "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", 
					 "insurance", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", 
	"paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "businessTransactionCode"};
				Class<Detail> auxClass = Detail.class;
				Detail myDetail = detail.get(i);
				for (String attribute:attributeListFieldsInEvents) {
					Field field = auxClass.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail);
					assert(itemValue!=null);}
				//fields should not be in Event
		 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID",  "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
		 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
				Class<Detail> auxClass2 = Detail.class;
				Detail myDetail2 = detail.get(i);
				for (String attribute:attributeListFieldsNotInEvents) {
					Field field = auxClass2.getDeclaredField(attribute);
					field.setAccessible(true);
					Object itemValue = field.get(myDetail2);
					assert(itemValue==null);}
			 
				}
			
				public void validateFieldsEventType08v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID",
						 "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", 
						 "insurance", "stampTax", "paymentDefaultFee", "businessTransactionCode" };
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}

				}
			public void validateFieldsEventType10v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID",
						 "cancellationAmount","currencyCode", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType11v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", 
						 "merchantSUID", "paymentAmount", "currencyCode", "invoiceId","businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
				}
				
				public void validateFieldsEventType12v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID",
						 "paymentAmount", "currencyCode", "invoiceId", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType13v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID", 
						 "currencyCode", "invoiceId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix",
						 "merchantPamVariable", "vatTaxFee", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
			public void validateFieldsEventType16v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", 
						 "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", 
						 "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxRemove",
						 "stampTaxAdd", "stampTaxRemove", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTaxAdd", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType18v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID",
						 "currencyCode", "merchantFundingMode", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", 
						 "vatTaxFeeAdd", "vatTaxFeeRemove", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
				}
				
				public void validateFieldsEventType19v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "merchantSUID",
						 "currencyCode", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", 
						 "merchantPamVariable", "vatTaxFee", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID","customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
				
				}
				
				public void validateFieldsEventType20v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID",
						 "currencyCode", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance",
						 "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax",
						 "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "paymentMethod", "paymentProcessorName", 
						 "paymentProcessorTransferId", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType21v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", 
						 "merchantSUID", "currencyCode", "financingId", "cancellationAmount", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}

				
				public void validateFieldsEventType22v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference",
						 "merchantSUID", "currencyCode", "invoiceId", "merchantFundingMode", "commissionAdd", "pamAdd", "commissionRemove", 
						 "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "customerGUID", "customerSUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
				
				}

				
				public void validateFieldsEventType23v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID",
						 "currencyCode", "paymentMethod", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "paymentProcessorName", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType24v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "paymentMethod",
						 "paymentProcessorName", "paymentProcessorTransferId", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest",
						 "refundInsurance", "businessTransactionCode" };
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "settlementAmountPaymentDefault", "paymentDefaultFee", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType25v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID", "currencyCode", "capital", "interestFix", 
						 "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", "insuranceAdd", "interestRemove", "adminFeeRemove",
						 "insuranceRemove", "stampTax", "settlementAmountPaymentDefault", "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount", "paymentProcessorTransferId", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
					
				}
				
				public void validateFieldsEventType26v2(int i) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
					//fields in Event
				 String[] attributeListFieldsInEvents = {"lineNumber", "eventType", "eventDate", "countryCode", "fundingReference", "customerSUID",
						 "currencyCode", "capital", "interestFix", "interestVariable", "adminFee", "insurance", "interestAdd", "adminFeeAdd", 
						 "insuranceAdd", "interestRemove", "adminFeeRemove", "insuranceRemove", "stampTax", "settlementAmountPaymentDefault", 
						 "paymentDefaultFee", "paymentMethod", "paymentProcessorName", "paymentProcessorTransferId", "businessTransactionCode"};
					Class<Detail> auxClass = Detail.class;
					Detail myDetail = detail.get(i);
					for (String attribute:attributeListFieldsInEvents) {
						Field field = auxClass.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail);
						assert(itemValue!=null);}
					//fields should not be in Event
			 	String[] attributeListFieldsNotInEvents = {"merchantGUID", "merchantName", "merchantSUID", "customerGUID", "businessTransactionType", "numberOfInstallment", "channel", "paymentAmount", "financingId", "invoiceId", "installmentAmount", "installmentNumber", "stampCreditTax", "stampCreditTaxAdd", "stampCreditTaxRemove", "stampTaxAdd", "stampTaxRemove", "cancellationAmount", "merchantFundingMode", "merchantCommissionFix", "merchantCommissionVariable", "merchantPamFix", "merchantPamVariable", "vatTaxFee", "commissionAdd", "pamAdd", "commissionRemove", "pamRemove", "cancellationFee", "vatTaxFeeAdd", "vatTaxFeeRemove", "refundAmount", "refundCapital", "refundAdminFee", "refundInterest", "refundInsurance",
			 			"period_start","period_end","payouts_currency","payout_carried_forward_amount","current_period_amount","net_amount","status","payout_fee","processed_amount","refund_amount","chargeback_amount","payouts_to_card_amount","processing_fees","interchange_fees","scheme_and_other_network_fees","premium_and_apm_fees","chargeback_fees","payout_to_card_fees","payment_gateway_fees","rolling_reserve_amount","tax","admin_fees","general_adjustments"};
					Class<Detail> auxClass2 = Detail.class;
					Detail myDetail2 = detail.get(i);
					for (String attribute:attributeListFieldsNotInEvents) {
						Field field = auxClass2.getDeclaredField(attribute);
						field.setAccessible(true);
						Object itemValue = field.get(myDetail2);
						assert(itemValue==null);}
				}
				
				
	
	
}
