package dto.accounting;

public class Header {
	public int lineNumber;
    public String countryCode;
    public String fileName;
    public String generationDate;
    public int fileSequenceNumber;
}
