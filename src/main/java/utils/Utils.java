
package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import javax.imageio.ImageIO;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

	
	public WebDriver driver;
	
	
	public WebDriverWait wait;

	
	public Utils(WebDriver driver) {

		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
	}
	
	public void moveFileToProjectDirectory(String downloadPathFile, String projectPathFile) throws IOException {

		Path temp = Files.move(Paths.get(downloadPathFile), Paths.get(projectPathFile));

		if (temp != null) {
			System.out.println("File renamed and moved successfully");
		} else {
			System.out.println("Failed to move the file");
		}
	}

	
	public void renameFile(String fileNameOld, String fileNameNew) throws InterruptedException, IOException {
		
		File file = new File(fileNameOld);
		File file2 = new File(fileNameNew);

		if (file2.exists()) {
		   throw new java.io.IOException("file exists");
		} 
		
		System.out.println(file.getAbsolutePath());
		System.out.println(file.isFile());

		Thread.sleep(1000);
		boolean success = file.renameTo(file2);
		
		if (!success) {
			throw new java.io.IOException("Não renomeou");
		}
		Thread.sleep(1000);
	
	}
	
	
	public String readingTextFromPDF(String filePath) throws IOException {
		
		File file = new File(filePath);
		
		PDDocument document = PDDocument.load(file);
		PDFTextStripper pdfStripper = new PDFTextStripper();
		String text = pdfStripper.getText(document);	
		document.close();
		
		return text;
	}
	
   
    public void extractImagesFromPDF(String filePath, String imagePath) throws Exception{

    	File file = new File(filePath);
    	PDDocument document = PDDocument.load(file);
    	
    	PDPageTree list = document.getPages();

	    for (PDPage page : list) {
	    	
	        PDResources pdResources = page.getResources();
	        for (COSName c : pdResources.getXObjectNames()) {
	        	
	            PDXObject o = pdResources.getXObject(c);
	            if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
	                File filei = new File(imagePath + "_" + System.nanoTime() + ".jpeg");
	                ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject)o).getImage(), "png", filei);
	            }
	        }
        }
	    document.close();
    }
    
	/* 
	 * used to scroll down the page (also used to search for an element that the screen can't reach
	 * 
	 */
    public void scrollDown() {

		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
    
    
    
    public void deleteFile(String filepath) throws IOException {
//    	File file = new File(filepath);    
//
//    	System.out.println(file.exists());
//    	if (file.exists()) {
//			file.delete();
//			System.out.println("File erased.");
//		}   
    	System.out.println(filepath);
    	try {
            boolean result = Files.deleteIfExists(Paths.get(filepath));
            if (result) {
                System.out.println("File is deleted!");
            } else {
                System.out.println("Sorry, unable to delete the file.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	    
    	
//    	System.out.println(file.exists());
    	
    	
    }
    
    
    public ZonedDateTime dateTimeTenant(String tenant) {
		
		ZonedDateTime now = null;
		
		if (tenant.equals("PT")) {
			now = ZonedDateTime.now();
		} else if ((tenant.equals("BE")) || (tenant.equals("IT")) || (tenant.equals("ES")) || (tenant.equals("DE"))) {
			now = ZonedDateTime.now(ZoneId.of("Europe/Rome"));
		} else if (tenant.equals("RO")) {
			now = ZonedDateTime.now(ZoneId.of("Europe/Bucharest"));
		} 
		return now;
	}
    

	public LocalDateTime getLastWeekEnd() {
		
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		LocalDateTime lastSunday = null;

		switch (day) {

		case Calendar.MONDAY:
			lastSunday = LocalDateTime.now().minusDays(1);
			break;

		case Calendar.TUESDAY:

			lastSunday = LocalDateTime.now().minusDays(2);
			break;

		case Calendar.WEDNESDAY:
			lastSunday = LocalDateTime.now().minusDays(3);
			break;

		case Calendar.THURSDAY:
			lastSunday = LocalDateTime.now().minusDays(4);
			break;

		case Calendar.FRIDAY:
			lastSunday = LocalDateTime.now().minusDays(5);
			break;

		default:
			System.out.println("This test should not run in the weekend");
		}
		
	
		
		return lastSunday;
	}
	
	public LocalDateTime getLastWeekBegin() {
		
		
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		LocalDateTime lastMonday = null;
		
		
		System.out.println(day);
		System.out.println(Calendar.MONDAY);
		switch (day) {

		case Calendar.MONDAY:
			lastMonday = LocalDateTime.now().minusDays(8);
			break;

		case Calendar.TUESDAY:
			lastMonday = LocalDateTime.now().minusDays(9);
			break;

		case Calendar.WEDNESDAY:
			lastMonday = LocalDateTime.now().minusDays(10);
			break;

		case Calendar.THURSDAY:
			lastMonday = LocalDateTime.now().minusDays(11);
			break;

		case Calendar.FRIDAY:
			lastMonday = LocalDateTime.now().minusDays(12);
			break;

		default:
			System.out.println("This test should not run in the weekend");
		}
		
				
		return lastMonday;
	}
	
//	public void writeContract(String tenant, String action, String externalReference, String contractNumber,
//			String amount, String amountFees, String merchant, String bt_code, String contractStatus) throws IOException {
//		
//		ZonedDateTime now = dateTimeTenant(tenant);
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
//		String today = now.format(formatter);
//		
//		String fileName = today + "_" + tenant;
//		String filenamePath = System.getProperty("user.home")
//				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\" + fileName + ".xlsx";
//		
//		File file = new File(filenamePath);
//		XSSFWorkbook workbook;
//		
//				
//		if (!file.exists() && !file.isDirectory()){ //checking file availability
//			workbook = new XSSFWorkbook();
//			Sheet sheet1 = workbook.createSheet(action); 
//			Row rowhead = sheet1.createRow(0);
//		    rowhead.createCell(0).setCellValue("actions");
//		    rowhead.createCell(1).setCellValue("externalReference");
//		    rowhead.createCell(2).setCellValue("contractNumber");
//		    rowhead.createCell(3).setCellValue("amount");
//		    rowhead.createCell(4).setCellValue("amountFees");
//		    rowhead.createCell(5).setCellValue("merchant");		
//		    rowhead.createCell(6).setCellValue("bt_code");	    
//		    rowhead.createCell(7).setCellValue("contractStatus");
//		    Row row = sheet1.createRow(1);
//		    row.createCell(0).setCellValue(action);
//		    row.createCell(1).setCellValue(externalReference);
//		    row.createCell(2).setCellValue(contractNumber);
//		    row.createCell(3).setCellValue(amount);
//		    row.createCell(4).setCellValue(amountFees);
//		    row.createCell(5).setCellValue(merchant);
//		    row.createCell(6).setCellValue(bt_code);
//		    row.createCell(7).setCellValue(contractStatus);
//		    
//		} else {
//	        try ( 
//	            // Make current input to exist file
//	            InputStream is = new FileInputStream(file)) {
//	                workbook = new XSSFWorkbook(is);
//	            }
//	        XSSFSheet sheet1 = workbook.getSheet(action);   
//			int sheetExist = 0;
//	        if (workbook.getNumberOfSheets() != 0) {
//	            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
//	            	System.out.println(workbook.getSheetName(i));
//	               if (workbook.getSheetName(i).equals(action)) {
//	            	   sheetExist=1;
//	            	   break;
//	                }
//	            }  
//	        }
//	        
//	        if (sheetExist==1){
//	        	sheet1 = workbook.getSheet(action);
//	        }
//	        else {
//	            // Create new sheet to the workbook if empty
//	        	sheet1 = workbook.createSheet(action);
//	        	Row rowhead = sheet1.createRow(0);
//			    rowhead.createCell(0).setCellValue("actions");
//			    rowhead.createCell(1).setCellValue("externalReference");
//			    rowhead.createCell(2).setCellValue("contractNumber");
//			    rowhead.createCell(3).setCellValue("amount");
//			    rowhead.createCell(4).setCellValue("amountFees");
//			    rowhead.createCell(5).setCellValue("merchant");		
//			    rowhead.createCell(6).setCellValue("bt_code");	    
//			    rowhead.createCell(7).setCellValue("contractStatus");
//	        } 
//	        Row row = sheet1.createRow(sheet1.getLastRowNum()+1);
//		    row.createCell(0).setCellValue(action);
//		    row.createCell(1).setCellValue(externalReference);
//		    row.createCell(2).setCellValue(contractNumber);
//		    row.createCell(3).setCellValue(amount);
//		    row.createCell(4).setCellValue(amountFees);
//		    row.createCell(5).setCellValue(merchant);
//		    row.createCell(6).setCellValue(bt_code);
//		    row.createCell(7).setCellValue(contractStatus);
//	    }	
//		
//		for (int i = 0; i < 10; i++) {
//			Sheet sheet1 = workbook.getSheet(action);
//        	sheet1.autoSizeColumn(i);
//        } 
//        FileOutputStream fileOut = new FileOutputStream(filenamePath);
//        workbook.write(fileOut);
//        fileOut.close();
//        workbook.close();	    
//		
//		System.out.println("Your xls file has been generated!");
//		
//		
//	}
//	
//	
	public void writeContract(String tenant, String action, String externalReference, String contractNumber,
			String amount, String amountFees, String merchant, String bt_code, String contractStatus,
			String merchantGUID, String merchantName, String merchantSUID,
			String customerGUID, String customerSUID, String businessTransactionCode
			, String businessTransactionType, String numberOfInstallment, String channel
			, String paymentAmount, String currencyCode, String financingId, String invoiceId, String installmentAmount
			, String installmentNumber, String capital, String interestFix, String interestVariable
			, String adminFee, String insurance, String interestAdd
			, String adminFeeAdd, String insuranceAdd, String interestRemove, String adminFeeRemove
			, String insuranceRemove, String stampTax, String stampCreditTax, String stampCreditTaxAdd
			, String stampCreditTaxRemove, String stampTaxAdd, String stampTaxRemove
			, String settlementAmountPaymentDefault, String paymentDefaultFee, String cancellationAmount
			, String paymentMethod, String paymentProcessorName, String paymentProcessorTransferId
			, String merchantFundingMode, String merchantCommissionFix, String merchantCommissionVariable
			, String merchantPamFix	, String merchantPamVariable, String vatTaxFee, String commissionAdd
			, String pamAdd, String commissionRemove, String pamRemove
			, String cancellationFee, String vatTaxFeeAdd, String vatTaxFeeRemove, String refundAmount 
			, String refundCapital, String refundAdminFee, String refundInterest, String refundInsurance
) throws IOException {
		
		ZonedDateTime now = dateTimeTenant(tenant);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String today = now.format(formatter);
		
		String fileName = today + "_" + tenant;
		String filenamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\" + fileName + ".xlsx";
		
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
				
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			workbook = new XSSFWorkbook();
			Sheet sheet1 = workbook.createSheet(action); 
			Row rowhead = sheet1.createRow(0);
		    rowhead.createCell(0).setCellValue("actions");
		    rowhead.createCell(1).setCellValue("externalReference");
		    rowhead.createCell(2).setCellValue("fundingReference");		    
		    rowhead.createCell(3).setCellValue("amount");
		    rowhead.createCell(4).setCellValue("amountFees");	 
		    rowhead.createCell(5).setCellValue("merchant");	   
		    rowhead.createCell(6).setCellValue("bt_code");	
		    rowhead.createCell(7).setCellValue("contractStatus");
		    rowhead.createCell(8).setCellValue("merchantGUID");
		    rowhead.createCell(9).setCellValue("merchantName");	
		    rowhead.createCell(10).setCellValue("merchantSUID");
		    rowhead.createCell(11).setCellValue("customerGUID");
		    rowhead.createCell(12).setCellValue("customerSUID");
		    rowhead.createCell(13).setCellValue("businessTransactionCode");	
		    rowhead.createCell(14).setCellValue("businessTransactionType");
		    rowhead.createCell(15).setCellValue("numberOfInstallment");
		    rowhead.createCell(16).setCellValue("channel");
		    rowhead.createCell(17).setCellValue("paymentAmount");
		    rowhead.createCell(18).setCellValue("currencyCode");
		    rowhead.createCell(19).setCellValue("financingId");
		    rowhead.createCell(20).setCellValue("invoiceId");
		    rowhead.createCell(21).setCellValue("installmentAmount");
		    rowhead.createCell(22).setCellValue("installmentNumber");
		    rowhead.createCell(23).setCellValue("capital");
		    rowhead.createCell(24).setCellValue("interestFix");
		    rowhead.createCell(25).setCellValue("interestVariable");
		    rowhead.createCell(26).setCellValue("adminFee");
		    rowhead.createCell(27).setCellValue("insurance");
		    rowhead.createCell(28).setCellValue("interestAdd");
		    rowhead.createCell(29).setCellValue("adminFeeAdd");
		    rowhead.createCell(30).setCellValue("insuranceAdd");
		    rowhead.createCell(31).setCellValue("interestRemove");
		    rowhead.createCell(32).setCellValue("adminFeeRemove");
		    rowhead.createCell(33).setCellValue("insuranceRemove");
		    rowhead.createCell(34).setCellValue("stampTax");
		    rowhead.createCell(35).setCellValue("stampCreditTax");
		    rowhead.createCell(36).setCellValue("stampCreditTaxAdd");
		    rowhead.createCell(37).setCellValue("stampCreditTaxRemove");
		    rowhead.createCell(38).setCellValue("stampTaxAdd");
		    rowhead.createCell(39).setCellValue("stampTaxRemove");
		    rowhead.createCell(40).setCellValue("settlementAmountPaymentDefault");
		    rowhead.createCell(41).setCellValue("paymentDefaultFee");
		    rowhead.createCell(42).setCellValue("cancellationAmount");
		    rowhead.createCell(43).setCellValue("paymentMethod");
		    rowhead.createCell(44).setCellValue("paymentProcessorName");
		    rowhead.createCell(45).setCellValue("paymentProcessorTransferId");
		    rowhead.createCell(46).setCellValue("merchantFundingMode");
		    rowhead.createCell(47).setCellValue("merchantCommissionFix");
		    rowhead.createCell(48).setCellValue("merchantCommissionVariable");
		    rowhead.createCell(49).setCellValue("merchantPamFix");
		    rowhead.createCell(50).setCellValue("merchantPamVariable");
		    rowhead.createCell(51).setCellValue("vatTaxFee");
		    rowhead.createCell(52).setCellValue("commissionAdd");
		    rowhead.createCell(53).setCellValue("pamAdd");
		    rowhead.createCell(54).setCellValue("commissionRemove");
		    rowhead.createCell(55).setCellValue("pamRemove");
		    rowhead.createCell(56).setCellValue("cancellationFee");
		    rowhead.createCell(57).setCellValue("vatTaxFeeAdd");
		    rowhead.createCell(58).setCellValue("vatTaxFeeRemove");
		    rowhead.createCell(59).setCellValue("refundAmount");
		    rowhead.createCell(60).setCellValue("refundCapital");
		    rowhead.createCell(61).setCellValue("refundAdminFee");
		    rowhead.createCell(62).setCellValue("refundInterest");
		    rowhead.createCell(63).setCellValue("refundInsurance");
		    rowhead.createCell(64).setCellValue("countryCode");
		    Row row = sheet1.createRow(1);
		    row.createCell(0).setCellValue(action);
		    row.createCell(1).setCellValue(externalReference);
		    row.createCell(2).setCellValue(contractNumber);
		    row.createCell(3).setCellValue(amount);
		    row.createCell(4).setCellValue(amountFees);
		    row.createCell(5).setCellValue(merchant);
		    row.createCell(6).setCellValue(bt_code);
		    row.createCell(7).setCellValue(contractStatus);
		    row.createCell(8).setCellValue(merchantGUID);
		    row.createCell(9).setCellValue(merchantName);	
		    row.createCell(10).setCellValue(merchantSUID);
		    row.createCell(11).setCellValue(customerGUID);
		    row.createCell(12).setCellValue(customerSUID);
		    row.createCell(13).setCellValue(businessTransactionCode);	
		    row.createCell(14).setCellValue(businessTransactionType);
		    row.createCell(15).setCellValue(numberOfInstallment);
		    row.createCell(16).setCellValue(channel);
		    row.createCell(17).setCellValue(paymentAmount);
		    row.createCell(18).setCellValue(currencyCode);
		    row.createCell(19).setCellValue(financingId);
		    row.createCell(20).setCellValue(invoiceId);
		    row.createCell(21).setCellValue(installmentAmount);
		    row.createCell(22).setCellValue(installmentNumber);
		    row.createCell(23).setCellValue(capital);
		    row.createCell(24).setCellValue(interestFix);
		    row.createCell(25).setCellValue(interestVariable);
		    row.createCell(26).setCellValue(adminFee);
		    row.createCell(27).setCellValue(insurance);
		    row.createCell(28).setCellValue(interestAdd);
		    row.createCell(29).setCellValue(adminFeeAdd);
		    row.createCell(30).setCellValue(insuranceAdd);
		    row.createCell(31).setCellValue(interestRemove);
		    row.createCell(32).setCellValue(adminFeeRemove);
		    row.createCell(33).setCellValue(insuranceRemove);
		    row.createCell(34).setCellValue(stampTax);
		    row.createCell(35).setCellValue(stampCreditTax);
		    row.createCell(36).setCellValue(stampCreditTaxAdd);
		    row.createCell(37).setCellValue(stampCreditTaxRemove);
		    row.createCell(38).setCellValue(stampTaxAdd);
		    row.createCell(39).setCellValue(stampTaxRemove);
		    row.createCell(40).setCellValue(settlementAmountPaymentDefault);
		    row.createCell(41).setCellValue(paymentDefaultFee);
		    row.createCell(42).setCellValue(cancellationAmount);
		    row.createCell(43).setCellValue(paymentMethod);
		    row.createCell(44).setCellValue(paymentProcessorName);
		    row.createCell(45).setCellValue(paymentProcessorTransferId);
		    row.createCell(46).setCellValue(merchantFundingMode);
		    row.createCell(47).setCellValue(merchantCommissionFix);
		    row.createCell(48).setCellValue(merchantCommissionVariable);
		    row.createCell(49).setCellValue(merchantPamFix);
		    row.createCell(50).setCellValue(merchantPamVariable);
		    row.createCell(51).setCellValue(vatTaxFee);
		    row.createCell(52).setCellValue(commissionAdd);
		    row.createCell(53).setCellValue(pamAdd);
		    row.createCell(54).setCellValue(commissionRemove);
		    row.createCell(55).setCellValue(pamRemove);
		    row.createCell(56).setCellValue(cancellationFee);
		    row.createCell(57).setCellValue(vatTaxFeeAdd);
		    row.createCell(58).setCellValue(vatTaxFeeRemove);
		    row.createCell(59).setCellValue(refundAmount);
		    row.createCell(60).setCellValue(refundCapital);
		    row.createCell(61).setCellValue(refundAdminFee);
		    row.createCell(62).setCellValue(refundInterest);
		    row.createCell(63).setCellValue(refundInsurance);
		    row.createCell(64).setCellValue("");
		    
		} else {
	        try ( 
	            // Make current input to exist file
	            InputStream is = new FileInputStream(file)) {
	                workbook = new XSSFWorkbook(is);
	            }
	        XSSFSheet sheet1 = workbook.getSheet(action);   
			int sheetExist = 0;
	        if (workbook.getNumberOfSheets() != 0) {
	            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
	            	System.out.println(workbook.getSheetName(i));
	               if (workbook.getSheetName(i).equals(action)) {
	            	   sheetExist=1;
	            	   break;
	                }
	            }  
	        }
	        
	        if (sheetExist==1){
	        	sheet1 = workbook.getSheet(action);
	        }
	        else {
	            // Create new sheet to the workbook if empty
	        	sheet1 = workbook.createSheet(action);
	        	Row rowhead = sheet1.createRow(0);
			    rowhead.createCell(0).setCellValue("actions");
			    rowhead.createCell(1).setCellValue("externalReference");
			    rowhead.createCell(2).setCellValue("fundingReference");
			    rowhead.createCell(3).setCellValue("amount");
			    rowhead.createCell(4).setCellValue("amountFees");
			    rowhead.createCell(5).setCellValue("merchant");		
			    rowhead.createCell(6).setCellValue("bt_code");	    
			    rowhead.createCell(7).setCellValue("contractStatus");
			    rowhead.createCell(8).setCellValue("merchantGUID");
			    rowhead.createCell(9).setCellValue("merchantName");	
			    rowhead.createCell(10).setCellValue("merchantSUID");
			    rowhead.createCell(11).setCellValue("customerGUID");
			    rowhead.createCell(12).setCellValue("customerSUID");
			    rowhead.createCell(13).setCellValue("businessTransactionCode");	
			    rowhead.createCell(14).setCellValue("businessTransactionType");
			    rowhead.createCell(15).setCellValue("numberOfInstallment");
			    rowhead.createCell(16).setCellValue("channel");
			    rowhead.createCell(17).setCellValue("paymentAmount");
			    rowhead.createCell(18).setCellValue("currencyCode");
			    rowhead.createCell(19).setCellValue("financingId");
			    rowhead.createCell(20).setCellValue("invoiceId");
			    rowhead.createCell(21).setCellValue("installmentAmount");
			    rowhead.createCell(22).setCellValue("installmentNumber");
			    rowhead.createCell(23).setCellValue("capital");
			    rowhead.createCell(24).setCellValue("interestFix");
			    rowhead.createCell(25).setCellValue("interestVariable");
			    rowhead.createCell(26).setCellValue("adminFee");
			    rowhead.createCell(27).setCellValue("insurance");
			    rowhead.createCell(28).setCellValue("interestAdd");
			    rowhead.createCell(29).setCellValue("adminFeeAdd");
			    rowhead.createCell(30).setCellValue("insuranceAdd");
			    rowhead.createCell(31).setCellValue("interestRemove");
			    rowhead.createCell(32).setCellValue("adminFeeRemove");
			    rowhead.createCell(33).setCellValue("insuranceRemove");
			    rowhead.createCell(34).setCellValue("stampTax");
			    rowhead.createCell(35).setCellValue("stampCreditTax");
			    rowhead.createCell(36).setCellValue("stampCreditTaxAdd");
			    rowhead.createCell(37).setCellValue("stampCreditTaxRemove");
			    rowhead.createCell(38).setCellValue("stampTaxAdd");
			    rowhead.createCell(39).setCellValue("stampTaxRemove");
			    rowhead.createCell(40).setCellValue("settlementAmountPaymentDefault");
			    rowhead.createCell(41).setCellValue("paymentDefaultFee");
			    rowhead.createCell(42).setCellValue("cancellationAmount");
			    rowhead.createCell(43).setCellValue("paymentMethod");
			    rowhead.createCell(44).setCellValue("paymentProcessorName");
			    rowhead.createCell(45).setCellValue("paymentProcessorTransferId");
			    rowhead.createCell(46).setCellValue("merchantFundingMode");
			    rowhead.createCell(47).setCellValue("merchantCommissionFix");
			    rowhead.createCell(48).setCellValue("merchantCommissionVariable");
			    rowhead.createCell(49).setCellValue("merchantPamFix");
			    rowhead.createCell(50).setCellValue("merchantPamVariable");
			    rowhead.createCell(51).setCellValue("vatTaxFee");
			    rowhead.createCell(52).setCellValue("commissionAdd");
			    rowhead.createCell(53).setCellValue("pamAdd");
			    rowhead.createCell(54).setCellValue("commissionRemove");
			    rowhead.createCell(55).setCellValue("pamRemove");
			    rowhead.createCell(56).setCellValue("cancellationFee");
			    rowhead.createCell(57).setCellValue("vatTaxFeeAdd");
			    rowhead.createCell(58).setCellValue("vatTaxFeeRemove");
			    rowhead.createCell(59).setCellValue("refundAmount");
			    rowhead.createCell(60).setCellValue("refundCapital");
			    rowhead.createCell(61).setCellValue("refundAdminFee");
			    rowhead.createCell(62).setCellValue("refundInterest");
			    rowhead.createCell(63).setCellValue("refundInsurance");
			    rowhead.createCell(64).setCellValue("countryCode");
	        } 
	        Row row = sheet1.createRow(sheet1.getLastRowNum()+1);
		    row.createCell(0).setCellValue(action);
		    row.createCell(1).setCellValue(externalReference);
		    row.createCell(2).setCellValue(contractNumber);
		    row.createCell(3).setCellValue(amount);
		    row.createCell(4).setCellValue(amountFees);
		    row.createCell(5).setCellValue(merchant);
		    row.createCell(6).setCellValue(bt_code);
		    row.createCell(7).setCellValue(contractStatus);
		    row.createCell(8).setCellValue(merchantGUID);
		    row.createCell(9).setCellValue(merchantName);	
		    row.createCell(10).setCellValue(merchantSUID);
		    row.createCell(11).setCellValue(customerGUID);
		    row.createCell(12).setCellValue(customerSUID);
		    row.createCell(13).setCellValue(businessTransactionCode);	
		    row.createCell(14).setCellValue(businessTransactionType);
		    row.createCell(15).setCellValue(numberOfInstallment);
		    row.createCell(16).setCellValue(channel);
		    row.createCell(17).setCellValue(paymentAmount);
		    row.createCell(18).setCellValue(currencyCode);
		    row.createCell(19).setCellValue(financingId);
		    row.createCell(20).setCellValue(invoiceId);
		    row.createCell(21).setCellValue(installmentAmount);
		    row.createCell(22).setCellValue(installmentNumber);
		    row.createCell(23).setCellValue(capital);
		    row.createCell(24).setCellValue(interestFix);
		    row.createCell(25).setCellValue(interestVariable);
		    row.createCell(26).setCellValue(adminFee);
		    row.createCell(27).setCellValue(insurance);
		    row.createCell(28).setCellValue(interestAdd);
		    row.createCell(29).setCellValue(adminFeeAdd);
		    row.createCell(30).setCellValue(insuranceAdd);
		    row.createCell(31).setCellValue(interestRemove);
		    row.createCell(32).setCellValue(adminFeeRemove);
		    row.createCell(33).setCellValue(insuranceRemove);
		    row.createCell(34).setCellValue(stampTax);
		    row.createCell(35).setCellValue(stampCreditTax);
		    row.createCell(36).setCellValue(stampCreditTaxAdd);
		    row.createCell(37).setCellValue(stampCreditTaxRemove);
		    row.createCell(38).setCellValue(stampTaxAdd);
		    row.createCell(39).setCellValue(stampTaxRemove);
		    row.createCell(40).setCellValue(settlementAmountPaymentDefault);
		    row.createCell(41).setCellValue(paymentDefaultFee);
		    row.createCell(42).setCellValue(cancellationAmount);
		    row.createCell(43).setCellValue(paymentMethod);
		    row.createCell(44).setCellValue(paymentProcessorName);
		    row.createCell(45).setCellValue(paymentProcessorTransferId);
		    row.createCell(46).setCellValue(merchantFundingMode);
		    row.createCell(47).setCellValue(merchantCommissionFix);
		    row.createCell(48).setCellValue(merchantCommissionVariable);
		    row.createCell(49).setCellValue(merchantPamFix);
		    row.createCell(50).setCellValue(merchantPamVariable);
		    row.createCell(51).setCellValue(vatTaxFee);
		    row.createCell(52).setCellValue(commissionAdd);
		    row.createCell(53).setCellValue(pamAdd);
		    row.createCell(54).setCellValue(commissionRemove);
		    row.createCell(55).setCellValue(pamRemove);
		    row.createCell(56).setCellValue(cancellationFee);
		    row.createCell(57).setCellValue(vatTaxFeeAdd);
		    row.createCell(58).setCellValue(vatTaxFeeRemove);
		    row.createCell(59).setCellValue(refundAmount);
		    row.createCell(60).setCellValue(refundCapital);
		    row.createCell(61).setCellValue(refundAdminFee);
		    row.createCell(62).setCellValue(refundInterest);
		    row.createCell(63).setCellValue(refundInsurance);
		    row.createCell(64).setCellValue("");
	    }	
		
		for (int i = 0; i < 10; i++) {
			Sheet sheet1 = workbook.getSheet(action);
        	sheet1.autoSizeColumn(i);
        } 
        FileOutputStream fileOut = new FileOutputStream(filenamePath);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();	    
		
		System.out.println("Your xls file has been generated!");
		
		
	}
	

}
