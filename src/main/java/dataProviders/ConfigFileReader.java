package dataProviders;

import java.util.Properties;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
 
import enums.DriverType;
import enums.EnvironmentType;

public class ConfigFileReader {

	private Properties properties;
	private final String propertyFilePath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\src\\test\\resources\\configs\\config.properties";
	
	/*
	 * Reads the config.properties
	 */

	public ConfigFileReader() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}
	}
	
	/*
	 * Reads the driverPath from the config.properties
	 */

	public String getDriverPath() {
		
		String driverPath = properties.getProperty("driverPath");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException(
					"Driver Path not specified in the Configuration.properties file for the Key:driverPath");
	}
	
	public String getHead() {
		
		String hd = properties.getProperty("headless");
		if (hd != null)
			return hd;
		else
			throw new RuntimeException(
					"Driver Path not specified in the Configuration.properties file for the Key:driverPath");
	}
	
	/*
	 * Reads the implicityWait from the config.properties, if it can NOT return the value to a Long it returns 30
	 */

	public long getImplicitlyWait() {
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if (implicitlyWait != null) {
			try {
				return Long.parseLong(implicitlyWait);
			} catch (NumberFormatException e) {
				throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
			}
		}
		return 30;
	}
	
	/*
	 * Reads from config.properties and returns a DriverType from the value that is typed
	 */

	public DriverType getBrowser() {
		String browserName = properties.getProperty("browser");
		if (browserName == null || browserName.equals("chrome"))
			return DriverType.chrome;
		else if (browserName.equalsIgnoreCase("firefox"))
			return DriverType.firefox;
		else if (browserName == null || browserName.equals("iexplorer"))
			return DriverType.ie;
		else
			throw new RuntimeException(
					"Browser Name Key value in Configuration.properties is not matched : " + browserName);
	}
	
	/*
	 * Reads from config.properties and returns a EnvironmentType from the value that is typed
	 */

	public EnvironmentType getEnvironment() {
		String environmentName = properties.getProperty("environment");
		if (environmentName == null || environmentName.equalsIgnoreCase("local"))
			return EnvironmentType.local;
		else if (environmentName.equals("remote"))
			return EnvironmentType.remote;
		else
			throw new RuntimeException(
					"Environment Type Key value in Configuration.properties is not matched : " + environmentName);
	}
	
	/*
	 * Reads from config.properties and returns the windowMaximize from the value that is typed
	 */

	public Boolean getBrowserWindowSize() {
		String windowSize = properties.getProperty("windowMaximize");
		if (windowSize != null)
			return Boolean.valueOf(windowSize);
		return true;
	}
}

