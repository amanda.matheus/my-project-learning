package pageObjects.management;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;



public class TenantManagement {

	public WebDriver driver;
	public WebDriverWait wait;
	
	public TenantManagement(WebDriver driver) {

		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
	}
	
	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'UserNameInput')]")
	private WebElement user;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'PasswordInput')]")
	private WebElement password;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'LoginButton')]")
	private WebElement login;

	@FindBy(how = How.XPATH, using = "//a[text()='BE']")
	private WebElement tenantBE;
	
	@FindBy(how = How.XPATH, using = "//a[text()='SP']")
	private WebElement tenantSP;
	
	@FindBy(how = How.XPATH, using = "//a[text()='IT']")
	private WebElement tenantIT;
	
	@FindBy(how = How.XPATH, using = "//a[text()='PT']")
	private WebElement tenantPT;
	
	@FindBy(how = How.XPATH, using = "//a[text()='DE']")
	private WebElement tenantDE;
	
	@FindBy(how = How.XPATH, using = "//a[text()='RO']")
	private WebElement tenantRO;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'SearchInput')]")
	private WebElement searchInput;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Search') and @type='submit']")
	private WebElement searchButton;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'UserTable')]//following::td[contains(@class,'TableRecords')]//a[1]")
	private WebElement userfound;

	@FindBy(how = How.XPATH, using = "//a[@class='ActionEdit']")
	private WebElement editUser;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'wtPasswordInput')]")
	private WebElement passwordUser;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'ConfirmPasswordInput')]")
	private WebElement confirmPasswordUser;

	@FindBy(how = How.XPATH, using = "//input[@type='submit' and @value='Save']")
	private WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//a[text()='Multitenant Management']")
	private WebElement tenantDashboard;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='Mandatory'])[1]")
	private WebElement userDataConservation;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='Mandatory'])[2]")
	private WebElement passwordDataConservation;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Login']")
	private WebElement loginDataConservation;	
	
	public void insertUsernameAndPasswordDataConservation() {
		userDataConservation.click();
		userDataConservation.sendKeys("admin@FacilyPayES");
		passwordDataConservation.sendKeys("admin@FacilyPayES");
		login.click();
	}
	
	public void insertUsernameAndPasswordManagement() {
		user.click();
		user.sendKeys("admin");
		password.sendKeys("Oney2020#");
		login.click();
	}
	
	public void changePassword(String tenant, String userTenant, String passwordTenant) throws InterruptedException {
		
		String userText = userTenant;
		String passwordText = passwordTenant;
		if (tenant.equals("ES")) {
			tenant = "SP";
		}
		tenantDashboard.click();
		searchInput.click();
		searchInput.sendKeys(tenant);
		searchButton.click();
		
		Thread.sleep(1000);
		clickTenant(tenant);
		searchInput.click();
		searchInput.sendKeys(userText);
		searchButton.click();
		Thread.sleep(1000);
		userfound.click();
		Thread.sleep(1000);
		editUser.click();
		Thread.sleep(1000);
		passwordUser.sendKeys(passwordText);
		confirmPasswordUser.sendKeys(passwordText);
		saveButton.click();
		Thread.sleep(1000);
		tenantDashboard.click();
		Thread.sleep(1000);
	
	}
	
	public void clickTenant (String tenant) {
		
		switch (tenant) {

		case "BE":
			tenantBE.click();
			break;
		case "IT":
			tenantIT.click();
			break;
		case "RO":
			tenantRO.click();
			break;
		case "SP":
			tenantSP.click();
			break;
		case "PT":
			tenantPT.click();
			break;
		case "DE":
			tenantDE.click();
			break;
		default:
			System.out.println("This Tenant doesn't Exist");
			break;
		}
		
		
	}
	
	
	public void changeAllPasswords(DataTable arg1) throws InterruptedException {
		
		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String tenantText="";
		String userText = "";
		String passwordText = "";
		
		for (int i = 0; i < list.size(); i++) {
			tenantText = (list.get(i).get("Tenant"));
			userText = (list.get(i).get("User"));
			passwordText = (list.get(i).get("Password"));
			
			System.out.println(tenantText);
			
			clickTenant(tenantText);
			
			searchInput.click();
			searchInput.sendKeys(userText);
			searchButton.click();
			Thread.sleep(1000);
			userfound.click();
			Thread.sleep(1000);
			editUser.click();
			Thread.sleep(1000);
			passwordUser.sendKeys(passwordText);
			confirmPasswordUser.sendKeys(passwordText);
			saveButton.click();
			Thread.sleep(1000);
			tenantDashboard.click();
			Thread.sleep(1000);
		}
		
	}
	
}
