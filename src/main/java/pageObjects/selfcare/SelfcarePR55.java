package pageObjects.selfcare;


import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import pageObjects.backOffice.HomePageBackOffice;
import pageObjects.eCommerce.CheckoutECommerce;
import pageObjects.merchant.CreateContractMerchant;
import pageObjects.subscription.PaymentPageSubscription;
import utils.Utils;

public class SelfcarePR55 {

	public static String contractNumber;
	public WebDriver driver;
	public WebDriverWait wait;
	public static String fileName;
	public Utils utils;

	public SelfcarePR55(WebDriver driver) {

		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		utils = PageFactory.initElements(driver, Utils.class);
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	
	/*
	 * Login Fields
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'UserName')]")
	private static WebElement usernameSelfcare;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Password')]")
	private static WebElement passwordSelfcare;

	@FindBy(how = How.XPATH, using = "//input[contains(@name, 'LoginButton')]")
	private WebElement login;

	/*
	 * SmokeTests and Regression Tests fields
	 */
	@FindBy(how = How.XPATH, using = "(//div[contains(@class, 'MyMenu')])[2]")
	private WebElement myAccount;

	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'Title')]//following::span)[1]")
	private WebElement myAccountTittle;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'My_Account_Responsive Font14')]")
	private WebElement email;

	@FindBy(how = How.XPATH, using = "(//table[@class='OSFillParent']//label)[1]")
	private WebElement registeredEmail;

	@FindBy(how = How.XPATH, using = "(//div[@class='AccordionVertical___icon']//span)[2]")
	private WebElement password;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputConfirmNewPW')]//preceding::label[2]")
	private WebElement currentPassword;

	@FindBy(how = How.XPATH, using = "//div[@class='Paymentinfo_Container']//ancestor::div[contains(@class,'AccordionVertical_item')]")
	private WebElement paymentInfo;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'wtDivTitleDocs')]")
	private WebElement documents;

	@FindBy(how = How.XPATH, using = "(//div[@class='Font14 My_Account_Responsive'])[3]")
	private WebElement phoneNumber;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'DivDescriptionPersInfo')]")
	private WebElement personalInfo;

	@FindBy(how = How.XPATH, using = "(//div[@class='ThemeGrid_Width11 ThemeGrid_MarginGutter']//div)[2]")
	private WebElement preferences;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'wtDivTitleDelAccount')]//following::div[1]")
	private WebElement deleteAccount;

	@FindBy(how = How.XPATH, using = "//div[@class='Heading1 Payment_Info_Responsive']//span[1]")
	private WebElement cards;

	@FindBy(how = How.XPATH, using = "//div[@class='Personal_Data_Document_Type_Responsive']")
	private WebElement documentType;

	@FindBy(how = How.XPATH, using = "//div[@class='Phone_Number_Responsive']//label[1]")
	private WebElement registedPhoneNumber;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'Personal_Data_Modification_Responsive')]//following::tr[2]")
	private WebElement nameBEITRO;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'Personal_Data_Modification_Responsive')]//following::tr[1]")
	private WebElement nameES;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Nome')]")
	private WebElement namePT;

	@FindBy(how = How.XPATH, using = "(//tr[@class='Preferences_Content']//td)[2]")
	private WebElement preferencesCheckBox;

	@FindBy(how = How.XPATH, using = "//div[@class='Delete_Account_Responsive']//span[1]")
	private WebElement accountCheckBox;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'MySubmenu')]/span[contains(text(),'3x4x')]")
	private WebElement my3x4xMenu;

	@FindBy(how = How.XPATH, using = "(//a[@class='Icon_Status'])[1]")
	private WebElement contractDetailsPast;

	@FindBy(how = How.XPATH, using = "(//a[@href='#']//span)[2]")
	private WebElement contractDetailsActive;

	@FindBy(how = How.XPATH, using = "//div[@class='Menu_TopMenu Menu_TopMenuActive']//a")
	private WebElement homeLink;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'Heading1 Title')]//div[1]")
	private WebElement hello;

	@FindBy(how = How.XPATH, using = "//div[@class='ThemeGrid_Container Title']//div[1]")
	private WebElement installment;

	@FindBy(how = How.XPATH, using = "//span[@class='PaidBoxStyle']")
	private WebElement installmentES;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputCurrentEmail')]")
	private WebElement currentEmail;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputNewEmaill')]")
	private WebElement newEmail;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputConfirmNewEmail')]")
	private WebElement newEmailConfirm;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputCurrentPassword')]")
	private WebElement currentPasswordSelfCare;

	@FindBy(how = How.XPATH, using = "(//input[@type='submit'])[1]")
	private WebElement saveEmail;

	@FindBy(how = How.XPATH, using = "(//input[@type='submit'])[2]")
	private WebElement savePassword;

	@FindBy(how = How.XPATH, using = "(//input[@type='password'])[2]")
	private WebElement currentPasswordSelfCarePassword;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputNewPW')]")
	private WebElement newPassword;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputConfirmNewPW')]")
	private WebElement newPasswordConfirm;

	@FindBy(how = How.XPATH, using = "//input[@type='submit'][2]")
	private WebElement yesDeleteAccount;//input[contains(@class,'Button FontGray')]

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'del')]")
	private WebElement deleteAccountBtn;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'wtContracts')]")
	private WebElement totalOfContracts;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'No hay cuotas para mostrar')]")
	private WebElement noIstallmentsToShowActiveContractsES;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/span[1]")
	private WebElement noIstallmentsToShowPastContracts;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'No hay contrato pasado para mostrar ...')]")
	private WebElement noIstallmentsToShowPastContractsES;

	/*
	 * PR38 fields
	 */
	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//preceding::div[1]//child::input[contains(@id,'PhoneNumberInput')]")
	private WebElement inputCurrentNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//preceding::div[1]//child::input[contains(@id,'PrefixInput')]")
	private WebElement inputPrefixCurrentNumber;

	@FindBy(how = How.XPATH, using = "//div[(contains(@class,'Phone_Number_Responsive') and contains(@id,'ChangeMobilePhoneContainer'))]//preceding::span[contains(@id,'PR28')]")
	private WebElement labelCurrentNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//following-sibling::div[1]//child::input[contains(@id,'PhoneNumberInput')]")
	private WebElement inputNewNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//following-sibling::div[1]//child::input[contains(@id,'PrefixInput')]")
	private WebElement inputPrefixNewNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//following-sibling::div[2]//child::input[contains(@id,'PhoneNumberInput')]")
	private WebElement inputConfirmNewNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//following-sibling::div[2]//child::input[contains(@id,'PrefixInput')]")
	private WebElement inputPrefixConfirmNewNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='Phone_Number_Responsive']//following-sibling::div[@class='My_Account_Responsive_Btn_Container']//input[@type='submit']")
	private WebElement customerSave;

	@FindBy(how = How.XPATH, using = "//div[@class='phone-session-title OSAutoMarginTop']//following-sibling::div[1]//child::span[@class='ValidationMessage' and contains(@id,'PhoneNumberInput')]")
	private WebElement mobileMessageError;

	@FindBy(how = How.XPATH, using = "//div[@class='Feedback_Message_Success']")
	private WebElement mobileSaved;

	/*
	 * PR24 and PR30 Fields
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Submit')]")
	private WebElement submitCard;

	@FindBy(how = How.XPATH, using = "(//input[@type='radio'])[last()]")
	private WebElement useAnotherCard;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'CardList_Container')]//following::*[contains(@id,'CardList_')]")
	private WebElement cardList;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Changecard')]")
	private WebElement changeCreditCard;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Description') and contains(@class,'ActiveContract')]//div[2]")
	private WebElement contractNumberChangedCard;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Close')]")
	private WebElement closePopup;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ListCard')]//following::div[contains(@ class,'Card_Information_Container')]")
	private List<WebElement> cardsList;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'535215XXXXXX3404')]//following::input[contains(@id,'ButtonRemove')]")
	private WebElement removeCardButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Yes_Button')]")
	private WebElement yesModifyCard;

	@FindBy(how = How.XPATH, using = "//div[@class='Badge White']//following::td[3]")
	private WebElement paymentMethodNextInstallment;

	@FindBy(how = How.XPATH, using = "//*[text()='424242XXXXXX4242']//preceding::input[@type='radio']")
	private WebElement firstCardList;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Submit')]")
	private WebElement confirmButton;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'TR_Contracts')]//tbody//tr[1]//td[1]//div//span")
	private WebElement firstContractContractList;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'ChangeBtn')]")
	private WebElement modifyButton;

	@FindBy(how = How.XPATH, using = "//input[(not(contains(@id,'ChangeBtn'))) and @class='Button']")
	private WebElement closePopupContractList;

	@FindBy(how = How.XPATH, using = "//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")
	private WebElement iframe;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'My_Account_Responsive_Btn_Container')]//input[2]")
	private WebElement yesDeleteCard;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'AddCardButton')]")
	private WebElement addCardButton;

	@FindBy(how = How.XPATH, using = "//div[@class='MainPopup']//following::input[@class='Button Is_Default ThemeGrid_MarginGutter']")
	private WebElement stayLoggedInButton;

	/*
	 * PR35 - Data Travel field
	 */
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'TravelsTable')]")
	private List<WebElement> travelList;

	/*
	 * New Fields of Portugal.
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'AddressName')]")
	private WebElement address;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'AddressNumber')]")
	private WebElement addressNumber;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'PostalCode')]")
	private WebElement postalCode;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Municipality')]")
	private WebElement municipality;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Occupation')]")
	private WebElement occupation;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Employer')]")
	private WebElement employer;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'GenderRadioButton')]")
	private WebElement gender;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'ParticularCondition')]")
	private WebElement fileDownload;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'ParticularCondition')]//following::td[1]")
	private WebElement fileNameConsent;

	// New Fields for pr36

	@FindBy(how = How.XPATH, using = "(//div[text()='Cancelled'])[1]")
	private WebElement validateCancelledIcon;

	@FindBy(how = How.XPATH, using = "//td[text()='Benjamin R']")
	private WebElement benjamin;

	@FindBy(how = How.XPATH, using = "//td[text()='Cariblanco']")
	private WebElement cariblanco;

	@FindBy(how = How.XPATH, using = "//td[text()='CARM Reser']")
	private WebElement carm;

	@FindBy(how = How.XPATH, using = "//td[text()='Prosecco A']")
	private WebElement prosecco;

	@FindBy(how = How.XPATH, using = "//td[text()='Astica - C']")
	private WebElement astica;

	@FindBy(how = How.XPATH, using = "//td[text()='Greenock C']")
	private WebElement greenock;

	@FindBy(how = How.XPATH, using = "//td[text()='Pesquera R']")
	private WebElement pesquera;

	@FindBy(how = How.XPATH, using = "//td[text()='Chateau Mo']")
	private WebElement chateau;

	@FindBy(how = How.XPATH, using = "//td[text()='basket']")
	private WebElement MarketplaceName;

	@FindBy(how = How.XPATH, using = "//span[@class='fa fa-fw fa-power-off']")
	private WebElement LogoutSelfcare;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='titlecont']//div)[1]")
	private WebElement newSelfcareHeader;
	
	@FindBy(how = How.XPATH, using = "//div[@class='Footer_Copyright']//div[1]")
	private WebElement newSelfcareFooter;
	
	@FindBy(how = How.XPATH, using = " (//div[@class='margintop']//div)[1]")
	private WebElement newSelfcareMenuDashboard;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[3]")
	private WebElement newSelfcareMenuMyAccount;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[4]")
	private WebElement newSelfcareMenuMyProducts;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[5]")
	private WebElement newSelfcareSubmenuMyPayLater;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[6]")
	private WebElement newSelfcareSubmenu3x4x;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[7]")
	private WebElement newSelfcareSubmenuMyInsurance;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[8]")
	private WebElement newSelfcareMenuMyPaymentMethods;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[9]")
	private WebElement newSelfcareMenuMyIdDocuments;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='margintop']//div)[10]")
	private WebElement newSelfcareMenuMyPreferences;

	/**
	 * Login methods
	 */
	public void insertUsernameAndPasswordSelfcarePT() {

		usernameSelfcare.sendKeys("pt@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcarePTPP() {

		usernameSelfcare.sendKeys("qa-pt@automation.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertUsernameAndPasswordSelfcareES() {

		usernameSelfcare.sendKeys("es@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareESPR55() {

		usernameSelfcare.sendKeys("pr33es@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareITPR55() {

		usernameSelfcare.sendKeys("pr33it@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareROPR55() {

		usernameSelfcare.sendKeys("pr33ro@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareBEPR55() {

		usernameSelfcare.sendKeys("pr33be@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcarePTPR55() {

		usernameSelfcare.sendKeys("pr33pt@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertUsernameAndPasswordSelfcareIT() {

		usernameSelfcare.sendKeys("it@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertNewUsernameSelfcareIT() {

		usernameSelfcare.sendKeys("pisanelli1@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertNewUsernameSelfcareBE() {

		usernameSelfcare.sendKeys("newEmailCustomer@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertNewUsernameSelfcarePT() {

		usernameSelfcare.sendKeys("lulu99@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertNewUsernameSelfcareES() {

		usernameSelfcare.sendKeys("qatest00@gmail.com");
		passwordSelfcare.sendKeys("qa_Test12");
	}

	public void insertNewUsernameSelfcareRO() {

		usernameSelfcare.sendKeys("testes99@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertPasswordSelfcare() {

		currentPasswordSelfCare.sendKeys("Oney1testes");
	}

	public void insertNewPasswordSelfcareIT() {

		usernameSelfcare.sendKeys("it@gmail.com");
		passwordSelfcare.sendKeys("Oney2testes");
	}

	public void insertNewPasswordSelfcareES() {

		usernameSelfcare.sendKeys("es@gmail.com");
		passwordSelfcare.sendKeys("Oney2testes");
	}

	public void insertNewPasswordSelfcareRO() {

		usernameSelfcare.sendKeys("ro@gmail.com");
		passwordSelfcare.sendKeys("Oney2testes");
	}

	public void insertNewPasswordSelfcareBE() {

		usernameSelfcare.sendKeys("be@gmail.com");
		passwordSelfcare.sendKeys("Oney2testes");
	}

	public void insertNewPasswordSelfcarePT() {

		usernameSelfcare.sendKeys("pt@gmail.com");
		passwordSelfcare.sendKeys("Oney2testes");
	}

	public void insertPasswordSelfcarePT() {

		currentPasswordSelfCare.sendKeys("Oney1testes");
	}

	public void insertPasswordSelfcareES() {

		currentPasswordSelfCare.sendKeys("qa_Test12");
	}

	public void insertUsernameAndPasswordSelfcareBE() {

		usernameSelfcare.sendKeys("be@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareBEPP() {

		usernameSelfcare.sendKeys("qa-be@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareITPP() {

		usernameSelfcare.sendKeys("qa-it@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareESPP() {

		usernameSelfcare.sendKeys("qa-es@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void insertUsernameAndPasswordSelfcareRO() {

		usernameSelfcare.sendKeys("ro@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcareROPP() {

		usernameSelfcare.sendKeys("matheuvfkoilTU@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}
	
	public void insertUsernameAndPasswordSelfcarePP() {

		usernameSelfcare.sendKeys("ro@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void clickLoginSelfcare() {

		login.click();
	}

	public void insertUsername(String arg1) {
		usernameSelfcare.sendKeys(arg1);
		passwordSelfcare.sendKeys("Oney1testes");
	}

	/**
	 * SmokeTests and Regression Methods.
	 */
	public void clickMyAccount() {

		wait.until(ExpectedConditions.elementToBeClickable(myAccount)).click();
	}

	public void validateMyAccount() {

		if (myAccountTittle.isDisplayed()) {

//			assert(myAccountTittle.getText().contains());
			System.out.println("--- " + myAccountTittle.getText() + " ---");
		}

		else {

			System.out.println("Element is NOT present");
		}
	}

	public void clickEmail() {

		email.click();
	}

	public void validateEmail() {

		System.out.println("--- " + registeredEmail.getText() + " ---");
	}

	public void clickPassword() {

		password.click();
	}

	public void validatePassword() {

		System.out.println("--- " + currentPassword.getText() + " ---");
	}

	public void clickPaymentInfo() {

		System.out.println(paymentInfo.getAttribute("class"));
		if (!paymentInfo.getAttribute("class").equals("AccordionVertical_item open")) {
			wait.until(ExpectedConditions.elementToBeClickable(paymentInfo)).click();
		}
	}

	public void validatePaymentInfo() {

		System.out.println("--- " + cards.getText() + " ---");
	}

	public void clickDocuments() {

		wait.until(ExpectedConditions.elementToBeClickable(documents)).click();
	}

	public void validateDocuments() {

		System.out.println("--- " + documentType.getText() + " ---");
	}

	public void clickPhoneNumber() {

		wait.until(ExpectedConditions.elementToBeClickable(phoneNumber)).click();
	}

	public void validatePhoneNumber() {

		System.out.println("--- " + registedPhoneNumber.getText() + " ---");
	}

	public void clickPersonalInfo() {

		wait.until(ExpectedConditions.elementToBeClickable(personalInfo)).click();
	}

	public void validatePersonalInfo() {

		System.out.println("--- " + nameBEITRO.getText() + " ---");
	}

	public void validatePersonalInfoPT() {

		System.out.println("--- " + namePT.getText() + " ---");
	}

	public void validatePersonalInfoES() {

		System.out.println("--- " + nameES.getText() + " ---");
	}

	public void clickPreferences() {

		preferences.click();
	}

	public void validatePreferences() {

		System.out.println("--- " + preferencesCheckBox.getText() + " ---");
	}

	public void clickDeleteAccount() {

		deleteAccount.click();
	}

	public void validateDeleteAccount() {

		System.out.println("--- " + accountCheckBox.getText() + " ---");
	}

	public void clickMy3x4xMenu() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(my3x4xMenu)).click();
		Thread.sleep(1000);
	}

	public void clickContractDetailsPast() throws InterruptedException {

	Thread.sleep(500);
	contractDetailsPast.click();
	
	}

	public void clickContractDetailsActive() {

		wait.until(ExpectedConditions.elementToBeClickable(contractDetailsActive)).click();
	}

	public void clickHomeLink() throws InterruptedException {

		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(homeLink)).click();
	}

	public void validateGreeting() {

		wait.until(ExpectedConditions.elementToBeClickable(hello)).getText();
	}

	public void validateInstallmentsPlan() {

		wait.until(ExpectedConditions.elementToBeClickable(installment)).getText();
	}

	public void downloadFileConsent(String consent) throws InterruptedException, IOException {

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMMdd");
		String todayfile = now.format(formatter2);

		String downloadPath = System.getProperty("user.home") + "\\Downloads\\";
		String projectPath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\";
		fileDownload.click();

		fileName = fileNameConsent.getText() + ".pdf";

		Thread.sleep(5000);

		String fileNameOld = downloadPath + fileName;
		String fileNameNew = downloadPath + consent.replace(" ", "") + "_" + todayfile + ".pdf";

		utils.renameFile(fileNameOld, fileNameNew);
		Thread.sleep(5000);

		fileName = consent.replace(" ", "") + "_" + todayfile + ".pdf";
		Thread.sleep(1000);
		utils.moveFileToProjectDirectory(downloadPath + fileName, projectPath + fileName);
		Thread.sleep(1000);

	}


	public void validateInstallmentsPlanES() {

		installmentES.getText();
	}

	public void changeEmail(String currentEmail, String newEmail) {

		this.currentEmail.sendKeys(currentEmail);
		this.newEmail.sendKeys(newEmail);
		newEmailConfirm.sendKeys(newEmail);
	}

	public void changePassword(String currentPassword, String newPassword) {

		currentPasswordSelfCarePassword.sendKeys(currentPassword);
		this.newPassword.sendKeys(newPassword);
		newPasswordConfirm.sendKeys(newPassword);
	}

	public void insertNewUsernameAndPassword() {

		System.out.println(CreateContractMerchant.getSaveEmail());
		usernameSelfcare.sendKeys(CreateContractMerchant.getSaveEmail());
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void clickSaveEmail() {

		saveEmail.click();
	}

	public void clickSavePassword() {

		savePassword.click();
	}

	public void confirmDeleteAccount() throws InterruptedException {

		deleteAccountBtn.click();
	}

	public void clickYesDeleteAccount() throws InterruptedException {

		Thread.sleep(3000);
		yesDeleteAccount.click();
		Thread.sleep(30000);
	}

	public static String getExternalReferenceFromJson(String s) throws Exception {

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(System.getProperty("user.home")
				+ "\\git\\oneyablewiseautomation\\OneyAblewiseTesting\\Documents\\helper.json"));
		JSONObject jsonObject = (JSONObject) obj;

		String ref = (String) jsonObject.get("email");

		return ref;
	}

	public void getEmailJson() {

		usernameSelfcare.sendKeys(PaymentPageSubscription.RANDOM_EMAIL);
		passwordSelfcare.sendKeys("Oney1testes");
	}

	public void validateTotalOfContracts() {

		System.out.println(totalOfContracts.getText());
		assert(totalOfContracts.getText().contains("0"));
	}

	public void validateActiveContractsIsEmptyES() {

		System.out.println(noIstallmentsToShowActiveContractsES.getText());
	}

	public void validatePastContractsIsEmpty() {

		System.out.println(noIstallmentsToShowPastContracts.getText());
	}

	public void validatePastContractsIsEmptyES() {

		System.out.println(noIstallmentsToShowPastContractsES.getText());
	}

	public void insertDataNewCustomer() {

		System.out.println(HomePageBackOffice.customerEmail);

		usernameSelfcare.sendKeys(CheckoutECommerce.customerEmail);
		passwordSelfcare.sendKeys("Oney1testes");
		clickLoginSelfcare();
	}

	/**
	 * PR38 - Phone Number method
	 */
	public void changeMobilePhone(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			int count = labelCurrentNumber.getText().length();

			String labelMobileEnd = labelCurrentNumber.getText().substring(count - 4, count);

			System.out.println("Label End:" + labelMobileEnd);
			System.out.println("Current:" + list.get(i).get("Current Mobile Phone"));

			int count1 = list.get(i).get("Current Mobile Phone").length();

			String currentMobile = list.get(i).get("Current Mobile Phone").substring(0, count1 - 4) + labelMobileEnd;
			String currentMobileEnd = list.get(i).get("Current Mobile Phone").substring(count1 - 4, count1);

			System.out.println("Current String: " + currentMobile);

			int count2 = list.get(i).get("New Mobile Phone").length();

			String newMobileEnd = list.get(i).get("New Mobile Phone").substring(count2 - 4, count2);
			String newMobile;

			if (labelMobileEnd.equalsIgnoreCase(newMobileEnd)) {

				newMobile = list.get(i).get("New Mobile Phone").substring(0, count2 - 4) + currentMobileEnd;

			} else {
				newMobile = list.get(i).get("New Mobile Phone");
			}
			System.out.println("New:" + newMobile);

//			inputPrefixCurrentNumber.clear();
//			Thread.sleep(1500);
//			inputCurrentNumber.clear();
			Thread.sleep(1500);
			inputCurrentNumber.sendKeys(currentMobile);
			Thread.sleep(2000);
//			inputNewNumber.click();
//			Thread.sleep(1500);
//			inputPrefixNewNumber.clear();
//			Thread.sleep(1500);
//			inputNewNumber.clear();
//			Thread.sleep(1500);
			inputNewNumber.sendKeys(newMobile);
			Thread.sleep(2000);
			inputConfirmNewNumber.click();
			Thread.sleep(1500);
			inputPrefixConfirmNewNumber.clear();
			Thread.sleep(1500);
			inputConfirmNewNumber.clear();
			Thread.sleep(1500);
			inputConfirmNewNumber.sendKeys(newMobile);
			Thread.sleep(1500);
		}
		customerSave.click();
	}

	public void checkmobileMessageError(DataTable mobile) throws InterruptedException {

		List<Map<String, String>> list = mobile.asMaps(String.class, String.class);
		String message;

		for (int i = 0; i < list.size(); i++) {

			message = list.get(i).get("Message");
			Thread.sleep(2000);

			assert (message.contains(mobileMessageError.getText()));
		}
		System.out.println("Message Error: " + mobileMessageError.getText());
		Thread.sleep(1000);
	}

	public void checkIfMobilephoneIsSaved() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(mobileSaved)).click();
		System.out.println(wait.until(ExpectedConditions.elementToBeClickable(mobileSaved)).getText());
		Thread.sleep(1000);
	}

	/**
	 * PR24 - Customer Card Detection and PR30 - Payment Modification Methods
	 */
	public void clickSubmitCard(String page) {
		if ((page.equalsIgnoreCase("Selfcare")) || (page.equalsIgnoreCase("Payment Info"))) {
			driver.switchTo().frame(iframe);
		}
		wait.until(ExpectedConditions.elementToBeClickable(submitCard)).click();
	}

	public void clickUseAnotherCard(String page) throws InterruptedException {

		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		driver.switchTo().frame(iframe);
		System.out.println("OI");
		System.out.println("Ver:" + cardList.getText());
		if (!cardList.getText().equalsIgnoreCase("")) {
			wait.until(ExpectedConditions.elementToBeClickable(useAnotherCard)).click();
		}
	}

	public void clickContractDetail() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(contractDetailsActive)).click();
		Thread.sleep(1000);
		contractNumber = wait.until(ExpectedConditions.elementToBeClickable(contractNumberChangedCard)).getText()
				.substring((contractNumberChangedCard.getText().length() - 10),
						contractNumberChangedCard.getText().length());
		System.out.println(contractNumber);
		HomePageBackOffice.defineContractNumber(contractNumber);
	}

	public void clickChangeCreditCardButton() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(changeCreditCard)).click();
		Thread.sleep(1000);
	}

	public void clickOnClose() throws InterruptedException {

		String iframeSrc = iframe.getAttribute("src");
		System.out.println(iframeSrc);
		if (iframeSrc.contains("Popup_AddCardCheck")) {
			driver.switchTo().frame(iframe);
			wait.until(ExpectedConditions.elementToBeClickable(closePopup)).click();
			driver.switchTo().defaultContent();
		} else if (iframeSrc.contains("PopUp_ListContracts")) {
			driver.switchTo().frame(iframe);
			wait.until(ExpectedConditions.elementToBeClickable(closePopupContractList)).click();
			driver.switchTo().defaultContent();
		}
		Thread.sleep(10000);
	}

	public void clickAddCard() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(addCardButton)).click();
		Thread.sleep(1000);
	}

	public void clickRemoveCard() throws InterruptedException {

		int count = cardsList.size();
		String iframeSrc;
		System.out.println("Tamanho:" + count);
		clickPaymentInfo();
		for (int i = 0; i < count; i++) {
			System.out.println(cardsList.get(i).getText());
			if (cardsList.get(i).getText().contains("535215XXXXXX3404")) {
				wait.until(ExpectedConditions.elementToBeClickable(removeCardButton)).click();
				Thread.sleep(1000);
				iframeSrc = iframe.getAttribute("src");
				System.out.println(iframeSrc);
				if (iframeSrc.contains("PopUp_Remove")) {
					driver.switchTo().frame(iframe);
					wait.until(ExpectedConditions.elementToBeClickable(yesDeleteCard)).click();
					driver.switchTo().defaultContent();
				} else if (iframeSrc.contains("PopUp_Warning")) {
					clickYesModifyCard();
					Thread.sleep(500);
					selectCardOnTheList();
					Thread.sleep(500);
					clickModifyCardOnContract();
					Thread.sleep(10000);
					clickOnClose();
				}
				Thread.sleep(20000);
				clickPaymentInfo();
			}
		}
	}

	public void clickYesModifyCard() throws InterruptedException {

		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		String iframeSrc = iframe.getAttribute("src");
		System.out.println(iframeSrc);
		if (iframeSrc.contains("PopUp_Warning")) {
			driver.switchTo().frame(iframe);
			wait.until(ExpectedConditions.elementToBeClickable(yesModifyCard)).click();

			driver.switchTo().defaultContent();
			Thread.sleep(1000);
		}
	}

	public void validatePaymentOnNextInstallment() throws InterruptedException {

		assert (paymentMethodNextInstallment.getText().contains("535215XXXXXX3404"));
	}

	public void selectCardOnTheList() throws InterruptedException {

		String iframeSrc = iframe.getAttribute("src");
		System.out.println("a" + iframeSrc);
		if (iframeSrc.contains("AccountManagement")) {
			driver.switchTo().frame(iframe);
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(firstCardList)).click();
			wait.until(ExpectedConditions.elementToBeClickable(confirmButton)).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			Thread.sleep(1000);
		}
	}

	public void clickModifyCardOnContract() throws InterruptedException {

		String iframeSrc = iframe.getAttribute("src");
		System.out.println(iframeSrc);
		if (iframeSrc.contains("PopUp_ListContracts")) {
			driver.switchTo().frame(iframe);
			contractNumber = firstContractContractList.getText();
			HomePageBackOffice.defineContractNumber(contractNumber);
			wait.until(ExpectedConditions.elementToBeClickable(modifyButton)).click();
			Thread.sleep(10000);
			driver.switchTo().defaultContent();
			Thread.sleep(1000);
		}
	}

	public void stayLoggedIn() {

		wait.until(ExpectedConditions.elementToBeClickable(stayLoggedInButton)).click();
	}

	/**
	 * PR35 - Data Travel Methods
	 */
	public void validateItemsListTravelTable() {
		System.out.println(travelList.size());
		assert ((travelList.size() > 0));
	}

	/**
	 * PR39 - Web Contract Form Methods
	 */
	public void validateNewFields() {

		System.out.println("Address " + address.getAttribute("value"));
		System.out.println("Address Number" + addressNumber.getAttribute("value"));
		System.out.println("Postal Code " + postalCode.getAttribute("value"));
		System.out.println("Municipality " + municipality.getAttribute("value"));
		System.out.println("Occupation " + occupation.getAttribute("value"));
		System.out.println("Employer " + employer.getAttribute("value"));

		assert (!((address.getAttribute("value").isEmpty()) && (addressNumber.getAttribute("value").isEmpty())
				&& (postalCode.getAttribute("value").isEmpty()) && (municipality.getAttribute("value").isEmpty())
				&& (occupation.getAttribute("value").isEmpty()) && (employer.getAttribute("value").isEmpty())));

		gender.click();
	}

	public void insertUsernameAndPasswordSelfcarePR36MC() {

		usernameSelfcare.sendKeys("onlymulticapture@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

	// Marketplace

	public void insertUsernameAndPasswordSelfcarePR36MP() {

		usernameSelfcare.sendKeys("onlymarketplace@gmail.com");
		passwordSelfcare.sendKeys("Oney1testes");
	}

//pr36 metodo para clicar no cancelled Icon

	public void clickValidateCancelledIcon() {

		validateCancelledIcon.click();
	}

	public void clickItemBenjamin() {

		benjamin.click();
	}

	public void clickItemCariblanco() {

		cariblanco.click();
	}

	public void clickItemCarm() {

		carm.click();
	}

	public void clickItemProsecco() {

		prosecco.click();
	}

	public void clickItemAstica() {

		astica.click();
	}

	public void clickItemGreenock() {

		greenock.click();
	}

	public void clickItemPesquera() {

		pesquera.click();
	}

	public void clickItemChateau() {

		chateau.click();
	}

	public void clickMarketplaceName() {

		MarketplaceName.click();
	}

	public void clickLogoutSelfcare() {

		LogoutSelfcare.click();
	}
	
	public void validateNewSelfcareComponents() {

		newSelfcareHeader.click();
		newSelfcareFooter.click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareMenuDashboard)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareMenuMyAccount)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareMenuMyProducts)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareSubmenuMyPayLater)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareSubmenu3x4x)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareSubmenuMyInsurance)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareMenuMyPaymentMethods)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareMenuMyIdDocuments)).click();
		wait.until(ExpectedConditions.elementToBeClickable(newSelfcareMenuMyPreferences)).click();
	}
}
