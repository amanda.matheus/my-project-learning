package pageObjects.backOffice;

import java.awt.AWTException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.exceptions.CsvValidationException;

import cucumber.api.DataTable;
import net.bytebuddy.utility.RandomString;
import pageObjects.subscription.PaymentPageSubscription;
import utils.Utils;

public class HomePageBackOffice {
	

	public static String externalRefPR36;
	public static String externalRef;
	public static int countGeneral;
	public static int nrInstallments;
	public static int nrInstallmentsPlanned;
	public static int firstInstallmentPlanned;
	public static String idCollection;
	public static String FPClientCode;
	public static String contractNumber;
	public static String statusOfContract;
	public static String contratoAnonimizado;
	public static String textoId;
	public static String contractTransactionRef;
	public static String url;
	public static String customerEmail;
	public static String merchantSurname;
	public static String customerMobilePhoneNumber;
	public static String priceItem1;
	public static String priceItem2;
	public static String priceItem3;
	public static String fileName;
	public static String currentPassword;
	public static String currentUsername;
	public static String bt_code;
	public static String pr89Name;
	public static String capitalActionsContract;
	public static String amountActionsContract;
	public static String amountFeesActionsContract;
	public static String installmentNumber;
	public static float totalAmount;
	public BackOfficeAccounting acc;
	public WebDriver driver;
	public WebDriverWait wait;
	public Utils utils;
	private XSSFWorkbook workbook;
//	private XSSFWorkbook workbook2;

	public HomePageBackOffice(WebDriver driver) {

		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		utils = PageFactory.initElements(driver, Utils.class);
		acc = PageFactory.initElements(driver, BackOfficeAccounting.class);
	}
	
	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	@FindBy(how = How.XPATH, using = "//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")
	private WebElement iframe;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'UserNameInput')]")
	private WebElement username;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'PasswordInput')]")
	private WebElement password;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Username_wtOldPassword')]")
	private WebElement currentPasswordfield;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Username_wtNewPassword')]")
	private WebElement newPasswordfield;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Password_wtRetryPassword')]")
	private WebElement retryPasswordfield;

	@FindBy(how = How.XPATH, using = "//input[contains(@class,'Button First')]")
	private WebElement loginBackOffice;

//	@FindBy(how = How.XPATH, using = "//input[@value='Login']")
//	private WebElement loginGDPR;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Contracts')]")
	private WebElement contractsMenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Audit')]")
	private WebElement auditMenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'API Logs')]")
	private WebElement apiLogsSubmenu;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Contract Info')]")
	private WebElement contractInfoTab;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Technical')]")
	private WebElement merchantTechnicalTab;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Financial')]")
	private WebElement merchantFinancialTab;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Request Info')]")
	private WebElement requestInfoTab;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Billing Address')]")
	private WebElement billingAddressTab;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Contract Transactions')]")
	private static WebElement contractTransactionsTab;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Customers')]")
	private WebElement customersMenu;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/span[1]/div[1]/div[1]/span[1]/div[4]/div[2]/div[1]/div[1]/span[1]/a[1]")
	private WebElement customersSubMenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Brands')]")
	private WebElement brandsMenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Merchants')]")
	private WebElement merchantsMenu;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search by Merchant name']")
	private WebElement merchantInputName;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Configurations')])[1]")
	private WebElement configurationsMenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Right to be Forgotten')]")
	private WebElement rightToBeForgottenSubmenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Finalized and cancelled contracts')]")
	private WebElement rightForgottenFinalizedCancelledContractsSection;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'ContractRulesRejected')]")
	private WebElement rightForgottenContractRulesRejected;
	
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'ContractRulesFinalized')]")
	private WebElement rightForgottenContractRulesFinalized;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'ContractStatusFinalized')]")
	private WebElement rightForgottenContractStatusFinalized;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Rejected contracts')]")
	private WebElement rightForgottenRejectContractsSection;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Active contracts')]")
	private WebElement rightForgottenActiveContractsSection;
	
	@FindBy(how = How.XPATH, using = "//span[@class='fa fa-fw fa-trash fa-lg']")
	private List<WebElement> rightForgottenTrash;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'Rules_Active')]//child::span[@class='fa fa-fw fa-trash fa-lg']")
	private List<WebElement> rightForgottenTrashActive;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'Rules_Finalized')]//child::span[@class='fa fa-fw fa-trash fa-lg']")
	private List<WebElement> rightForgottenTrashFinalized;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'Rules_Rejected')]//child::span[@class='fa fa-fw fa-trash fa-lg']")
	private List<WebElement> rightForgottenTrashRejected;
	
	@FindBy(how = How.XPATH, using = "//span[@class='fa fa-fw fa-plus-square fa-lg']")
	private List<WebElement> rightForgottenPlus;

	@FindBy(how = How.XPATH, using = "//a[text()='Business Transactions']")
	private WebElement btTab;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'File Name:')]")
	private WebElement btInfo;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Finance')]")
	private WebElement financeSubmenu;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Accepted')]//following::div[1]")
	private WebElement dataConservationAccepted;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Rejected')]//following::div[1]")
	private WebElement dataConservationRejected;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Marketing')]//following::div[1]")
	private WebElement dataConservationMarketing;

	@FindBy(how = How.XPATH, using = "//span[@class='Menu_SubMenuItem'][contains(text(),'Data Conservation')]")  //a[@class='Menu_SubMenuItem Menu_SubMenuItemActive']")
	private WebElement dataConservation;

	@FindBy(how = How.XPATH, using = "//a[@href='/Configurations/PhoneNumbers.aspx']")
	private WebElement phoneNumbers;

	@FindBy(how = How.XPATH, using = "(//*[@class='fa fa-fw fa-pencil-square-o fa-2x' or @class='fa fa-fw fa-edit fa-2x'])[1]")
	private WebElement editButton;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Actions to Contract')]")
	private WebElement actionsContract;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Is Multi-capture')]//following::input[1]")
	private WebElement multiCaptureCheckbox;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Is Marketplace')]//following::input[1]")
	private WebElement marketplaceCheckbox;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'MC & MP Configuration')]")
	private WebElement mcMpSub;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'PSP List')]")
	private WebElement pspList;
	
	@FindBy(how = How.XPATH, using = "//span[@class='Feedback_Message_Text']/following-sibling::a[1]")
	private WebElement closeMessage;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Checkout')]//preceding::td[1]//a")
	private WebElement checkoutPSP;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Altex Files')]")
	private WebElement altexFiles;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'Altex')]//td[1]/a")
	private WebElement altexFileLatest;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'FPI')])[1]")
	private WebElement accountingFileLatest;

	@FindBy(how = How.XPATH, using = "(//table[contains(@id,'AccountingFileTable')])[1]//a[contains(text(),'FPI')]")
	private  List<WebElement> accountingFileAll;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'dtBeginDate')])[1]")
	private WebElement accountingBeginDate;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'dtEndDate')])[1]")
	private WebElement accountingEndDate;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'CRP')]//preceding::td[1]//a")
	private WebElement editCRP;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'CRP Files')]")
	private WebElement crpFiles;

	@FindBy(how = How.XPATH, using = "//th[contains(text(),'Filename')]//following::a[1]")
	private WebElement crpFileLatest;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Marketplaces')]")
	private WebElement marketplaces;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'New Marketplace')]")
	private WebElement newMarketplace;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'General')]")
	private WebElement generalNewMarketplace;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Financial')]")
	private WebElement financialNewMarketplace;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Decision')]")
	private WebElement decisionNewMarketplace;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Vendors list')]")
	private WebElement vendorsListNewMarketplace;

	@FindBy(how = How.XPATH, using = "")
	private WebElement marketplaceOwner;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Multicapture Configuration')]")
	private WebElement multicaptureTab;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'AD_RTD')]")
	private WebElement ad_rtd;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'TSPI')]")
	private WebElement tspi;

	@FindBy(how = How.XPATH, using = "//div[@class='OSInline']//select[1]")
	private WebElement status;

	@FindBy(how = How.XPATH, using = "//select[@class='select OSFillParent']")
	private WebElement productTypeSearch;
	
	//contains(@id,'Product')
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'BrandInput')]")
	private WebElement brandNameSearch;

	@FindBy(how = How.XPATH, using = "//label[text()='Business Transaction Type']//following::div[1]")
	private WebElement btType;

	@FindBy(how = How.XPATH, using = "//a[@class='os-internal-ui-dialog-titlebar-close os-internal-ui-corner-all']")
	private WebElement btFrameExit;

	@FindBy(how = How.XPATH, using = "//li[@class='os-internal-ui-menu-item']//strong[1]")
	private WebElement brandOptionSelect;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"os-internal-ui-active-menuitem\"]")
	private WebElement merchantOptionSelect;
		
	@FindBy(how = How.XPATH, using = "//*[contains(@name,'SuccessCombo')]")
	private WebElement success;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Log Message')]//following::td[4]")
	private WebElement logMessage;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'No sftp files report logs to show...')]")
	private WebElement logMessageNOK;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Session operation has timed out')]")
	private WebElement logMessagePT;

	@FindBy(how = How.XPATH, using = "//input[@value='Search']")
	private WebElement search;

	@FindBy(how = How.XPATH, using = "//input[@value='Search']//preceding::input[1]")
	private WebElement inputSearch;

	@FindBy(how = How.XPATH, using = "//table//tr")
	private List<WebElement> tableResult;

	@FindBy(how = How.XPATH, using = "//input[@value='Cancel']")
	private WebElement cancel;

	@FindBy(how = How.XPATH, using = "//body[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[2]/span[1]/table[1]/tbody[1]/tr[1]/td[7]/div[1]/span[1]")
	private WebElement item1;

	@FindBy(how = How.XPATH, using = "//body[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[2]/span[1]/table[1]/tbody[1]/tr[2]/td[7]/div[1]/span[1]")
	private WebElement item2;

	@FindBy(how = How.XPATH, using = "//body[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[2]/span[1]/table[1]/tbody[1]/tr[3]/td[7]/div[1]/span[1]")
	private WebElement item3;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Automa')]//following::div[4]//input[1]")
	private WebElement amountToRefundMerchant;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Bollinger')]//following::div[4]//input[1]")
	private WebElement amountToRefund1;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Bruno Giac')]//following::div[4]//input[1]")
	private WebElement amountToRefund2;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Barca Velh')]//following::div[4]//input[1]")
	private WebElement amountToRefund2PT;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Saca Rolha')]//following::div[4]//input[1]")
	private WebElement amountToRefund3;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CancelationAmount')]")
	private WebElement cancelationAmount;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'QuantityToCancel_Input')]")
	private WebElement cancelationItem;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CancelAll')]")
	private WebElement cancelAll;

	@FindBy(how = How.XPATH, using = "//script[@type='text/javascript']/following-sibling::input[1]")
	private WebElement applyCancel;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Total Settlement Amount')]//following::input[1]")
	private WebElement applyEarlySettlement;

	@FindBy(how = How.XPATH, using = "//td[text()='Early Settlement']//following::td[1]")
	private WebElement valueEarlySettlement;
	
	@FindBy(how = How.XPATH, using = "//td[text()='Early Settlement Amount']//following::input[1]")
	private WebElement amountPartialEarlySettlement;

	@FindBy(how = How.XPATH, using = "//input[@value='SAVE']")
	private WebElement saveEarlySettlement;

	@FindBy(how = How.XPATH, using = "//input[@value='Change Credit Card']")
	private WebElement changeCreditCard;

	@FindBy(how = How.XPATH, using = "//input[@value='Early Settlement']")
	private WebElement earlySettlement;

	@FindBy(how = How.XPATH, using = "//input[@value='Payment Retry']")
	private WebElement paymentRetry;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Email')]//following::input[1]")
	private WebElement email;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Customer')]//following::input[1]")
	private WebElement customerNameContracsSearch;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Name')]//following::input[1]")
	private WebElement customerNameCustomerSearch;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Reset')]")
	private WebElement reset;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Actions to Contract')]")
	private WebElement actionsToContract;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Actions to Customer')]")
	private WebElement actionsToCustomer;

	@FindBy(how = How.XPATH, using = "//input[@value='Right to be forgotten']")
	private WebElement rightToBeForgottenButton;

	@FindBy(how = How.XPATH, using = "//input[@value='Yes']")
	private WebElement yesButton;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'SectionExpandableArea')]")
	private WebElement actionsToContractComponent;

	@FindBy(how = How.XPATH, using = "//input[@value='New Merchant']")
	private WebElement newMerchant;

	@FindBy(how = How.XPATH, using = "//input[@value='New Brand']")
	private WebElement newBrand;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Import')]")
	private WebElement importButton;
	
	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'Import')]/following::input)[1]")
	private WebElement choseImportFile;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Validate File']")
	private WebElement validateFile;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Import')]")
	private WebElement importFile;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Merchants')]")
	private WebElement merchantsLink;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Brands')]")
	private WebElement brandsLink;

	@FindBy(how = How.XPATH, using = "(//*[@class='fa fa-fw fa-search fa-2x'])[1]")
	private WebElement viewButton;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Ext Ref')]//following::input[1]")
	private static WebElement externalReference;

	@FindBy(how = How.XPATH, using = "(//table[contains(@id,'ContractTable')]//following::td[9])[1]")
	private WebElement contractStatus;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Total Amount Financed')]//following::td[1]")
	private WebElement totalAmountFinanced;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'New amount financed')]//following::td[1]")
	private WebElement newAmountFinanced;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Total Order Cost + Fees')]//following::td[1]")
	private WebElement totalOrderCostFees;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'CAPITAL AMOUNT')]//following::div[27]")
	private WebElement capitalAmount;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ContractPaymentTransactionTable')]//tbody//tr//td[7]")
	private List<WebElement> feesAmountList;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ContractPaymentTransactionTable')]//tbody//tr//td[6]")
	private List<WebElement> capitalAmountList;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ContractPaymentTransactionTable')]//tbody//tr//td[5]")
	private List<WebElement> amountList;


	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ContractPaymentTransactionTable')]//tbody//tr//td[9]//div/span")
	private List<WebElement> installmentStatusList;

	@FindBy(how = How.XPATH, using = "//td[contains(@class,'TableRecords_OddLine')]//a[contains(@href,'Collection')]")
	private WebElement idCollectionLink;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Strategie')]//following::a[contains(text(),'Technical') or contains(text(),'Non-Technical')]")
	private List<WebElement> softcollectionTableList;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Customer Classification')]//following::td[1]")
	private WebElement customerClassificationSoftcollection;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Action')]//following::td[1]")
	private WebElement actionSoftcollection;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Unpaid') or contains(text(),'Hard Collection')]//ancestor::tr//child::td[8]")
	private WebElement delayAmountUnpaid;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Unpaid') or contains(text(),'Hard Collection')]")
	private WebElement statusUnpaid;

	@FindBy(how = How.XPATH, using = "//input[@value='Run Timer Process Strategy']//preceding::input[1]")
	private WebElement softcollectionIdTestPageToComplete;

	@FindBy(how = How.XPATH, using = "//input[@value='Run Timer Process Strategy']")
	private WebElement runTimerProcessStrategyButton;

	@FindBy(how = How.XPATH, using = "//input[@value='Run Timer Process Strategy']//following::input[1]")
	private WebElement softcollectionIdTestPageChangeStrategy;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Update EndDate']")
	private WebElement updateEndDateChangeStrategy;

	@FindBy(how = How.XPATH, using = "//input[@value='Change Strategy']//preceding::input[1]")
	private WebElement amountTestPageChangeStrategy;

	@FindBy(how = How.XPATH, using = "//input[@value='Change Strategy']")
	private WebElement changeStrategyButton;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Unpaid') or contains(text(),'Hard Collection')]//ancestor::tr//child::td[7]")
	private WebElement feesAmountUnpaid;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Unpaid') or contains(text(),'Hard Collection')]//ancestor::tr//child::td[6]")
	private WebElement capitalAmountUnpaid;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Unpaid') or contains(text(),'Hard Collection')]//ancestor::tr//child::td[5]")
	private WebElement amountUnpaid;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Contract')]//following::input[1]")
	private WebElement softcollectionContractInput;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Scoring')]//following::a[1]")
	private WebElement itemListTab;

	@FindBy(how = How.XPATH, using = "(//*[@class='fa fa-fw fa-eye'])[3]")
	private WebElement itemListTabItemsDetails;

	@FindBy(how = How.XPATH, using = "//span[@class='ListRecords']")
	private WebElement itemsDetailsContent;

	@FindBy(how = How.XPATH, using = "//label[text()='From']//following::input[1]")
	private WebElement contractsFrom;

	@FindBy(how = How.XPATH, using = "//label[text()='To']//following::input[1]")
	private WebElement contractsTo;

	@FindBy(how = How.XPATH, using = "//label[text()='Date To:']//following::input[1]")
	private WebElement SFTPTo;

	@FindBy(how = How.XPATH, using = "//label[text()='Date From:']//following::input[1]")
	private WebElement SFTPFrom;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']//tr")
	private List<WebElement> rows;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Planned')]//ancestor::tr//child::td[2]")
	private List<WebElement> rowsPlanned;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Collections')]//ancestor::div[4]")
	private WebElement collectionsMenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Soft Collections')]")
	private WebElement softcollectionsSubmenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'SFTP Files Report Logs')]")
	private WebElement SFTPFilesReportLogsSubmenu;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Contract Nº')]//following::input[1]")
	private WebElement hardcollectionContractInput;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Contract')]")
	private WebElement contractNr;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Reference')]//following::tr[1]//td[3]")
	private WebElement reference;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Reference')]//following::tr[1]//td[4]")
	private WebElement referencePT;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Transaction Reference']")
	private WebElement transactionReference;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Amount']")
	private WebElement amount;

	@FindBy(how = How.XPATH, using = "//input[@value='Charge']")
	private WebElement amountOK;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Contract')]//following::input[1]")
	private WebElement contractInput;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::tr")
	private List<WebElement> contractTransactionsStatusList;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Amount From')]//following::input[1]")
	private WebElement amountFrom;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Amount to')]//following::input[1]")
	private WebElement amountTo;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Customer')]//following::input[@placeholder='Name']")
	private WebElement customerNameSearch;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'MerchantInput')]")
	private WebElement merchantInputSearch;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Brand Info')]")
	private WebElement brandInfo;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Brand Code')]//following::td[1]")
	private WebElement brand;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Decision')]")
	private WebElement decisionTab;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Rejection Type')]")
	private WebElement rejectionType;
	
	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Risk Decision Engine')]//following::td[8]")
	private WebElement rejectionReasonPSP;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Risk Decision Engine')]//following::td[12]")
	private WebElement rejectionReasonEngineDecision;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'SafeWatch')]")
	private WebElement rejectionReasonSafewatch;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'AUTOMATION TESTS_D(DONT CHANGE)')]//following::tr[3]")
	private WebElement customerFinalScoring;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'AUTOMATION TESTS_D(DONT CHANGE)')]//following::tr[2]")
	private WebElement scoringAcceptanceValue;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'AUTOMATION TESTS_D(DONT CHANGE)')]//following::tr[4]")
	private WebElement customerScoringDecision;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Age >= 80')]//ancestor::tr[1]")
	private WebElement scoringAge;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Age >= 80')]//following::tr[1]")
	private WebElement scoringEmail;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Engine Decision')]")
	private WebElement engineDecisionMenu;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Customer Scoring Results')]")
	private WebElement customerScoringResultsTab;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'SearchInput')]")
	private WebElement decisionHistoryContractInput;

	@FindBy(how = How.XPATH, using = "//td[contains(@class,'TableRecords_OddLine')]//a[1]")
	private WebElement contractDecisionHistory;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Decision History')]")
	private WebElement decisionHistorySubmenu;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Decision History')])[3]")
	private WebElement decisionHistorySubmenuES;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Mobile Phone Number')]//following::td[1]")
	private WebElement contractMobilePhoneNumber;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Name')]//following::td[1]//a[contains(@href,'Customer')]")
	private WebElement contractName;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'PhoneNumberInput')]")
	private WebElement customerMobile;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Mobile')]//following::input[1]")
	private WebElement customerMobilePrefix;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Email Address')]//following::td[1]")
	private WebElement customerEmailAddress;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Save')]")
	private WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Mobile_Phone_Number')]//following::span[@class='ValidationMessage' and contains(@id,'Mobile_Phone_Number')]")
	private WebElement mobileMessageError;

	@FindBy(how = How.XPATH, using = "(//span[contains(@class,'fa fa-pencil')]/following-sibling::span)[3]")
	private WebElement mobileMessageErrorBO;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Saved Successfully')]")
	private WebElement mobileSaved;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'CustomerTable')]//following::div[contains(text(),'@')]//following::td[1]")
	private WebElement customerMobileView;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Hard Collections Test Page')]")
	private WebElement hardCollectionLink;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'SoftCollection Id')]//input[1]")
	private WebElement hardCollectionIdSoft;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Hard Collections')]")
	private WebElement hardcollectionsSubmenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Marketplace Rulebook')]")
	private WebElement marketplaceRulebookSubmenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'HardCollections Files')]")
	private WebElement hardcollectionFilesSubmenu;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'HardCollectionFile')]")
	private WebElement hardcollectionEntriesFile;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'dtBeginDate')]")
	private WebElement hardcollectionBeginDate;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'dtEndDate')]")
	private WebElement hardcollectionEndDate;

	@FindBy(how = How.XPATH, using = "//a[text()='Performed Collections']")
	private WebElement performedCollectionsTab;

	@FindBy(how = How.XPATH, using = "//a[text()='Closed Processes']")
	private WebElement closedProcessesTab;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputDate')]")
	private WebElement hardFilesInputDate;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Planned Date Next Transaction')]//input[1]")
	private WebElement hardCollectionPlannedDate;

	@FindBy(how = How.XPATH, using = "//input[@value='Send to HardCollection']")
	private WebElement sentToHardCollectionButton;

	@FindBy(how = How.XPATH, using = "//input[@value='Run Timers']")
	private WebElement runTimersButton;

	@FindBy(how = How.XPATH, using = "//td[contains(@class,'TableRecords_OddLine')]//a[contains(@href,'Collection')]//following::span[1]")
	private WebElement statusSoftcollection;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::td[5]")
	private WebElement statusHardcollection;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Global Debt Amount')]//following::td[7]")
	private WebElement globalDebtAmount;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Hard Collections')]")
	private WebElement harcollectionSubmenu;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'FP Client Code')]//following::td[1]")
	private WebElement fpClientCode;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'External Code')]//following::td[1]")
	private WebElement externalCodeCustomer;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Request Amount')]//following::td[1]")
	private WebElement requestAmount;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Business Transaction')]//following::td[1]/span")
	private WebElement btNew;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'_A')]//ancestor::tr//td[2]//div//div")
	private List<WebElement> earlySettlementInstallmentNumber;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'_A')]//ancestor::tr//td[5]//div")
	private List<WebElement> earlySettlementInstallmentAmount;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Business Transaction')]//following::td[1]/a")
	private WebElement bt;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Merchant Fixed Fee')]//following::td[1]")
	private WebElement merchantFeeFix;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Customer Fixed Fee')]//following::td[1]")
	private WebElement customerFeeFix;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Customer Percentage Fee')]//following::td[1]")
	private WebElement customerFeeVariable;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Merchant Percentage Fee')]//following::td[1]")
	private WebElement merchantFeeVariable;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Merchant Fix')]//following::td[1]")
	private WebElement merchantFix;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Customer Fees Fixed')]//following::td[1]")
	private WebElement customerFix;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Merchant %')]//following::td[1]")
	private WebElement merchantPercentage;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Customer Fees Variable')]//following::td[1]")
	private WebElement customerPercentage;

	@FindBy(how = How.XPATH, using = "//span[@class='fa fa-fw fa-eye']")
	private WebElement btEye;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'GUID')]//following::span)[1]")
	private WebElement merchantGuid;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'MerchantFormFinancial') and contains(@id,'Financing_Type')]//child::input[@origvalue='true']//following::div[1]/div/div[1]")
	private WebElement merchantFinancingType;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'SUID')]//following::span)[1]")
	private WebElement merchantSuid;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Number of Payments')]//following::td[1]")
	private WebElement numberOfInstallment;

	@FindBy(how = How.XPATH, using = "//input[@type='file']")
	private WebElement upload;

	@FindBy(how = How.XPATH, using = "//input[@type='submit' and @value='Upload New File']")
	private WebElement uploadButton;

	@FindBy(how = How.XPATH, using = "//input[@value='Change Credit Card']")
	private WebElement changeCreditCardBO;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Contract')]//following::input[1]")
	private WebElement notificationContratInput;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Notification')]//following::select[1]")
	private WebElement notificationList;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'NotificationDetail')]")
	private WebElement notificationDetail;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'URLChangeCreditCard')]//following::td[contains(text(),'http')]")
	private WebElement getURL;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Payment Data')]")
	private static WebElement paymentDataTab;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ActiveCards_Table')]//td[1]")
	private static WebElement activeCard;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ActiveCards_Table')]//td[2]")
	private static WebElement activeCardExpiration;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'InactiveCards_Table')]//td[1]")
	private static WebElement inactiveCard;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'InactiveCards_Table')]//td[2]")
	private static WebElement inactiveCardExpiration;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Planned')]//ancestor::tr//child::td[4]")
	private WebElement paymentMethodNextInstallment;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),'Status')]//following::*[contains(text(),'Planned')]//ancestor::tr//child::td[3]")
	private WebElement paymentMethodNextInstallmentPT;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'API VALIDATION')]//following::div[2]")
	private WebElement apiValidationRegex;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'API VALIDATION')]//following::div[8]")
	private WebElement editPhoneNumberRegex;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'TravelList')]")
	private List<WebElement> travelList;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Benjamin R')]")
	private WebElement benjaminName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Benjamin R')]//following::td[4]")
	private WebElement benjaminMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Benjamin R')]//following::td[5]")
	private WebElement benjaminQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Benjamin R')]//following::td[8]")
	private WebElement benjaminStatus;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Greenock C')]")
	private WebElement greenockName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Greenock C')]//following::td[4]")
	private WebElement greenockMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Greenock C')]//following::td[5]")
	private WebElement greenockQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Cariblanco')]")
	private WebElement cariblancoName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Cariblanco')]//following::td[4]")
	private WebElement cariblancoMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Cariblanco')]//following::td[5]")
	private WebElement cariblancoQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Cariblanco')]//following::td[8]")
	private WebElement cariblancoStatus;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'CARM')]")
	private WebElement carmName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'CARM')]//following::td[4]")
	private WebElement carmMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'CARM')]//following::td[5]")
	private WebElement carmQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'CARM')]//following::td[8]")
	private WebElement carmStatus;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Pesquera R')]")
	private WebElement pesqueraName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Pesquera R')]//following::td[4]")
	private WebElement pesqueraMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Pesquera R')]//following::td[5]")
	private WebElement pesqueraQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Prosecco')]")
	private WebElement proseccoName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Prosecco')]//following::td[4]")
	private WebElement proseccoMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Prosecco')]//following::td[5]")
	private WebElement proseccoQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Prosecco')]//following::td[8]")
	private WebElement proseccoStatus;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Chateau Mo')]")
	private WebElement chateauName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Chateau Mo')]//following::td[4]")
	private WebElement chateauMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Chateau Mo')]//following::td[5]")
	private WebElement chateauQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Astica')]")
	private WebElement asticaName;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Astica')]//following::td[4]")
	private WebElement asticaMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Astica')]//following::td[5]")
	private WebElement asticaQuantity;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Astica')]//following::td[8]")
	private WebElement asticaStatus;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Astica')]//following::td[3]//input[@checked='checked']")
	private WebElement asticaMarketplace;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Logout')]")
	private WebElement logout;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'LoginInfo_username')]//span[1]")
	private WebElement loginInfo;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Ux Name')]//following::td[1]")
	private WebElement brandInfoUX;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Merchant Name')]//following::td[1]")
	private WebElement merchantName;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Merchant type')]//following::td[1]")
	private WebElement merchantType;

	@FindBy(how = How.XPATH, using = "//a[text()='Scoring']")
	private WebElement scoringTab;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ContractPreScoringShow')]//child::td[text()='Score']//following::td[1]")
	private WebElement score;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Customer ITIN or Customer FP Client Code']")
	private WebElement customerInfo;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'AddressName')]")
	private WebElement address;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CUSTOMER_Address_Number2')]")
	private WebElement addressNumber;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'PostalCode')]")
	private WebElement postalCode;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CUSTOMER_Place2')]")
	private WebElement municipality;

	@FindBy(how = How.XPATH, using = "//label[text()='Occupation']//following::label[1]")
	private WebElement occupation;

	@FindBy(how = How.XPATH, using = "//label[text()='Employer']//following::label[1]")
	private WebElement employer;

	@FindBy(how = How.XPATH, using = "//label[text()='Honorific Code']//following::label[1]")
	private WebElement honorificCode;

	@FindBy(how = How.XPATH, using = "//a[text()='Address']")
	private WebElement addressTab;

	@FindBy(how = How.XPATH, using = "//a[contains(@id,'LinktoGDPRCompliance')]//span[@class='linkToDisclaimer'][contains(text(),'aqui')]")
	private WebElement cookieDisclaimer;

	@FindBy(how = How.XPATH, using = "//img[contains(@src,'Icon_Check_Green')]//preceding::td[7]")
	private List<WebElement> btInstallments;

	@FindBy(how = How.XPATH, using = "//img[contains(@src,'Icon_Check_Green')]//preceding::td[3]")
	private List<WebElement> btBasketMin;

	@FindBy(how = How.XPATH, using = "//img[contains(@src,'Icon_Check_Green')]//preceding::td[2]")
	private List<WebElement> btBasketMax;

	@FindBy(how = How.XPATH, using = "//div[text()='Filter By:']/select[1]")
	private WebElement filterBy;

	@FindBy(how = How.XPATH, using = "//label[text()='Collect Fee Mode']//following::select[1]")
	private WebElement collectFeeMode;

	@FindBy(how = How.XPATH, using = "//input[@type='file']")
	private WebElement uploadFileFee;

	@FindBy(how = How.XPATH, using = "//input[@type='submit' and @value='Import']")
	private WebElement importFileFee;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'csv was uploaded with success.')]")
	private WebElement sucessMessage;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Merchant Fee File applied successfully')]")
	private WebElement sucessMessageMerchant;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Your file contains errors')]")
	private WebElement unsucessMessage;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Active')]")
	private WebElement collumnActive;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Active')]//a[1]")
	private WebElement collumnActive2;

	@FindBy(how = How.XPATH, using = "//label[contains(@id,'CustomerFeeFileTable') and contains(@id,'Checkbox')]")
	private List<WebElement> toggledButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'ActivationDate')]")
	private List<WebElement> activationDateValue;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'EndDate')]")
	private List<WebElement> endDate;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'MerchantFeeFile') and contains(@class,'select2')]")
	private WebElement selectMerchantFile;

	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'MerchantFeeFile') and contains(@class,'select2')]//following::input[@class='select2-input'])[last()]")
	private WebElement inputMerchantFile;

	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'MerchantFeeFile') and contains(@class,'select2')]//following::div[contains(@id,'select2-result-label')]input[@class='select2-input'])[last()]")
	private WebElement chooseMerchantFile;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Quality Assurance Automation Tests')]//preceding::span[@class='fa fa-fw fa-plus-square-o fa-lg'][1]")
	private WebElement selectBrandQAA;

	@FindBy(how = How.XPATH, using = "//input[@value='Confirm']")
	private WebElement confirMerchantFee;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'IsCommercialOffers')]//following::div[contains(@class,'ToggleButton')][1]/label")
	private WebElement consentOney;

	@FindBy(how = How.XPATH, using = "(//label[contains(@for,'')]//following::div[contains(@class,'ToggleButton')][1]/label)[1]")
	private WebElement consentOney1;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'IsPartnerCommercialOffers')]//following::div[contains(@class,'ToggleButton')][1]/label")
	private WebElement consentPartner;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']/tbody/tr")
	private List<WebElement> contractsTableRows;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']/tbody/tr[1]/td[3]")
	private WebElement contractsTableRowContractNumber;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']/tbody/tr[1]/td[6]")
	private WebElement contractsTableRowAmount;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']/tbody/tr[1]/td[7]")
	private WebElement contractsTableRowMerchant;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']/tbody/tr[1]/td[8]")
	private WebElement contractsTableRowCode;

	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']/tbody/tr[1]/td[9]")
	private WebElement contractsTableRowStatus;

	@FindBy(how = How.LINK_TEXT, using = "Import Rulebook")
	private WebElement importRulebookMarketplace;

	@FindBy(how = How.XPATH, using = "//input[@value='New RuleBook']")
	private WebElement newRulebook;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Code')]")
	private WebElement codeMarketplace;

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Select the XML')]")
	private WebElement importRulebookMarketplaceFile;

	@FindBy(how = How.XPATH, using = "//input[@type='search']")
	private WebElement searchMarketplaceRulebook;

//	@FindBy(how = How.XPATH, using = "//input[@value='Search']")
//	private WebElement searchMarketplaceRulebookButton;

	@FindBy(how = How.LINK_TEXT, using = "basket01")
	private WebElement searchedLabelMarketplace;

	@FindBy(how = How.XPATH, using = "//a[@href='/Audit/SellSecure_Logs.aspx']//span[1]")
	private WebElement sellSecureSubmenu;

	@FindBy(how = How.XPATH, using = "(//td[@class='TableRecords_OddLine']//a)[2]")
	private WebElement inputXMLsellsecurelog;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search by identifier']")
	private WebElement sellSecureSearchBar;

//	@FindBy(how = How.XPATH, using = "//input[@value='Search']")
//	private WebElement searchButtonSellSecureLogs;

	@FindBy(how = How.XPATH, using = "//table[@class='DocumentPopup']//td")
	private WebElement oneyTrustfields;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[3]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/span[2]")
	private WebElement payLaterAcceptedContracts;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[3]/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[3]/span[2]")
	private WebElement payLaterRejectedContracts;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Merchant_IBAN')]")
	private WebElement merchantIBAN;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Merchant_BIC')]")
	private WebElement merchantBIC;

	@FindBy(how = How.XPATH, using = "//label[contains(@id,'MerchantTable') and contains(@id,'Checkbox')]")
	private WebElement merchantActive;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Accounting')]")
	private WebElement accountingMenu;
	
	@FindBy(how = How.XPATH, using = "//span[@class='Feedback_Message_Text']/following-sibling::a[1]")
	private WebElement closeFeedbackMessage;

	@FindBy(how = How.XPATH, using = "//span[text()='Billing Files']")
	private WebElement billingFilesSubmenu;
	
	@FindBy(how = How.XPATH, using = "(//a[@class='Tabs_TabOff'])[2]")
	private WebElement invoicingFileTab;
	
	@FindBy(how = How.XPATH, using = "(//td[@class='TableRecords_OddLine']//div)[1]")
	private WebElement financialFileDate;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'InvoicingTable')]/tbody[1]/tr[1]/td[1]/div[1]")
	private WebElement invoicingFileDate;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'FinancingTable')]/tbody[1]/tr[1]/td[4]/div[1]")
	private WebElement financialFileXlsx;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'InvoicingTable')]/tbody[1]/tr[1]/td[5]/div[1]")
	private WebElement invoicingFileXlsx;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Accounting Files')]")
	private WebElement accountingFilesSubmenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'JSON Files')]")
	private WebElement jsonFilesSubmenu;

	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'Accounting')]/div[contains(@id,'Items')]//div[contains(@id,'Title')])[1]")
	private WebElement accountingFilesSection;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'IBAN Validation')]")
	private WebElement ibanValidationSubmenu;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'User Actions BO Logs')]")
	private WebElement userActionsBOLogsMenu;

	@FindBy(how = How.XPATH, using = "//select[1]")
	private WebElement screenboid;

	@FindBy(how = How.XPATH, using = "//td[3]")
	private WebElement merchantLog;

	@FindBy(how = How.XPATH, using = "//input[@class='Button GreenButton']")
	private WebElement approveIBAN;

	@FindBy(how = How.XPATH, using = "//input[@class='Button GrayButton']")
	private WebElement rejectIBAN;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'MerchantNameInput')]")
	private WebElement searchMerchantInput;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'DateFrom')]")
	private WebElement logFrom;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'EndDate')]")
	private WebElement logDateTo;

	@FindBy(how = How.XPATH, using = "//input[@value='Cancel']//following::input[1]")
	private WebElement saveButtonMerchant;
	
	@FindBy(how = How.XPATH, using = "(//label[text()=' Surname']/following::input)[1]")
	private WebElement merchantMerchantSurname;
	
	@FindBy(how = How.XPATH, using = "//a[@href='/Audit/MFS_Logs.aspx']")
	private WebElement mfsLogsSubmenu;
	
	@FindBy(how = How.XPATH, using = "//a[@href='/Audit/IP_Logs.aspx']")
	private WebElement ipLogsSubmenu;
	
	@FindBy(how = How.XPATH, using = "//td[@class='TableRecords_EvenLine']//a")
	private WebElement downloadInputJson;
	
	@FindBy(how = How.XPATH, using = "(//a[@role='presentation'])[2]")
	private WebElement dataTab;
	
	@FindBy(how = How.XPATH, using = "(//label[text()='Date From:']/following::input)[1]")
	private WebElement dateFrom;

	@FindBy(how = How.XPATH, using = "(//label[text()='Date From:']/following::input)[2]")
	private WebElement dateTo;
	
	@FindBy(how = How.XPATH, using = "(//label[text()='Method:']/following::input)[1]")
	private WebElement searchMfsLogs;
	
	@FindBy(how = How.XPATH, using = "(//td[text()='Create Financial Order']//following::*)[3]")
	private WebElement downloadxlsx;

	@FindBy(how = How.XPATH, using = "//option[contains(text(),'Create Financial Order')]")
	private WebElement createFinancialOrderFile;
	
	@FindBy(how = How.XPATH, using = "(//td[text()='Update Merchant'])[1]")
	private WebElement methodName;
	
	@FindBy(how = How.XPATH, using = "(//td[@class='TableRecords_EvenLine']/following-sibling::td)[3]")
	private WebElement createdOn;
	
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'method')]")
	private WebElement methodNameMFSSearch;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@id,'linkinputJSON')]")
	private List<WebElement> fileMFSInputJson;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'MerchantFinanceServiceLogsTablePR69')]//td[1]")
	private List<WebElement> methodString;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'MerchantFinanceServiceLogsTablePR69')]//td[4]")
	private List<WebElement> methodCreationOn;
	
	 //select[@class='OSFillParent']
	 //option[contains(text(),'Create Financial Order')]

	@FindBy(how = How.XPATH, using = "//td[text()='No contracts to show...']")
	private WebElement searchResultNoContractToShow;

	@FindBy(how = How.XPATH, using = "//td[text()='No customers to show...']")
	private WebElement searchResultNoCustomersToShow;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'rule') and contains(text(),'configured')]")
	private WebElement rightForgottenMessageError;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Users & Profiles']")
	private WebElement userAndProfileMenu;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Successfully Logged in']/following-sibling::a")
	private WebElement closeSuccessfullMessage;
	
	@FindBy(how = How.XPATH, using = "//a[@href='/BackOffice/Users.aspx']")
	private WebElement userAndProfileSubmenu;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Occupation']/following-sibling::select[1]")
	private WebElement occupationfieldCustomer;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Employer']/following::input")
	private WebElement employerfieldCustomer;
	
	@FindBy(how = How.XPATH, using = "(//label[text()='Contract Number for Anonymize']/following::input)[1]")
	private WebElement pasteToAnonymize;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Contract Number for Delete']/following::input")
	private WebElement pasteToDelete;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Test Anonymize Contract']")
	private WebElement submitAnonymize;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Test Delete Contract']")
	private WebElement submitDelete;
	
	@FindBy(how = How.XPATH, using = "(//label[text()='Contract']/following::input)[1]")
	private WebElement contractSearch;
	
	@FindBy(how = How.XPATH, using = "//td[text()='No contracts to show...']")
	private WebElement noContractFoundResult;
	
	@FindBy(how = How.XPATH, using = "(//td[@class='TableRecords_OddLine']//div)[2]")
	private WebElement getExternalReferencePR36;
	
	@FindBy(how = How.XPATH, using = "(//a[contains(@id,'MerchantTable')]//span)[2]")
	private WebElement clickOnMerchantDetails;
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[3]/div[1]/div[2]/div[1]/ul[1]/li[8]/a[1]")
	private WebElement clickonCRPconfigurationsTab;
	
	@FindBy(how = How.XPATH, using = "(//label[text()='CRP Sendings']/following::input)[1]")
	private WebElement crpmerchantButton;
	
	@FindBy(how = How.XPATH, using = "(//label[text()='CRP Sendings']/following::input)[2]")
	private WebElement crpPspButton;
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[3]/div[1]/div[2]/div[1]/ul[1]/li[9]/a[1]")
	private WebElement merchantFilesTab;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='AccordionVertical_item']//div)[1]")
	private WebElement merchantfilespopup;
	
	@FindBy(how = How.XPATH, using = "//div[text()='Sent On']//following::a[1]")
	private WebElement validatefirstmerchantFile;
	
	@FindBy(how = How.XPATH, using = "//td[text()='Checkout']//preceding::span[1]")
	private WebElement validatecheckoutPSP;
	
	@FindBy(how = How.XPATH, using = "//a[@href='/Configurations/SubscriptionPageConfig.aspx?isPayLater=False']")
	private WebElement subscriptionpageconfigssplit;
	
	@FindBy(how = How.XPATH, using = "//a[@href='/Configurations/SubscriptionPageConfig.aspx?isPayLater=True']")
	private WebElement subscriptionpageconfigsPL;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Before-you-go pop-in')]")
	private WebElement beforeyougopopinexpand;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@id,'Items_WebPatterns_wtBeforeGo')]")
	private WebElement beforeyougopopinisdisplayed;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Before_Go')]//label[contains(@id,'Checkbox')]")
	private WebElement togglebuttonbeforeyougo;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Merchant Category']/following-sibling::select")
	private List<WebElement> merchantCategoryOptions;
	
	public void insertUsernameAndPasswordBackOffice(String tenant) {

		username.sendKeys("QualityAutomationTests@"+tenant);
		password.sendKeys("QATests@"+tenant);
		currentUsername = "QualityAutomationTests@"+tenant;
		currentPassword = "QATests@"+tenant;
	}
	
	public void changePassword() {
		String pass = RandomString.make(2);
		currentPasswordfield.sendKeys(currentPassword);
		newPasswordfield.sendKeys(currentPassword + pass);
		retryPasswordfield.sendKeys(currentPassword + pass);
		saveButton.click();
	}
	
	public String getCurrentUsername() {
		return currentUsername;
	}
	
	public String getCurrentPassword() {
		return currentPassword;
	}

	public void clickLoginBackOffice() {

		loginBackOffice.click();
	}

	public void clickContractsMenu() throws InterruptedException{
		
		wait.until(ExpectedConditions.elementToBeClickable(contractsMenu)).click();

	}

	public void clickCustomersMenu() {

		wait.until(ExpectedConditions.elementToBeClickable(customersMenu)).click();
	}

	public void clickCustomersSubMenu() {

		wait.until(ExpectedConditions.elementToBeClickable(customersSubMenu)).click();
	}

	public void clickBrandsMenu() throws InterruptedException {

		brandsMenu.click();
		Thread.sleep(1000);
	}

	public void clickMerchantsMenu() {

		merchantsMenu.click();
	}

	public void clickConfigurationsMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(configurationsMenu)).click();
	}
	
	public void clickRightToBeForgottenSubmenu() {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(false);", rightToBeForgottenSubmenu);
		rightToBeForgottenSubmenu.click();
	}
	
	public void clickRightForgottenSection(DataTable rule) throws InterruptedException {
		
		List<Map<String, String>> list = rule.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			
			String section = list.get(i).get("Section");
			System.out.println(section);
			if(section.contains("Active")) {
				System.out.println("hi");
				wait.until(ExpectedConditions.elementToBeClickable(rightForgottenActiveContractsSection)).click();
				
				By  rightForgottenTrash = By.xpath("//table[contains(@id,'Rules_Active')]//child::span[@class='fa fa-fw fa-trash fa-lg']");
				if (driver.findElements(rightForgottenTrash).size() != 0) {
					System.out.println("tem trash");
					for (int t = 0; t < rightForgottenTrashActive.size(); t++) {
						System.out.println("clica em trash");
						rightForgottenTrashActive.get(t).click();
						Thread.sleep(3000);
						driver.switchTo().alert().accept();
					}
					
					
				}
				
				Thread.sleep(3000);
				rightForgottenPlus.get(0).click();
				Thread.sleep(1000);
				saveButton.click();
				Thread.sleep(3000);
				
			} else if (section.contains("Finalized")) {
				
				wait.until(ExpectedConditions.elementToBeClickable(rightForgottenFinalizedCancelledContractsSection)).click();
				
				By  rightForgottenTrash = By.xpath("//table[contains(@id,'Rules_Finalized')]//child::span[@class='fa fa-fw fa-trash fa-lg']");
				if (driver.findElements(rightForgottenTrash).size() != 0) {
					for (int t = 0; t < rightForgottenTrashFinalized.size(); t++) {
						rightForgottenTrashFinalized.get(t).click();
						Thread.sleep(3000);
						driver.switchTo().alert().accept();
					}
				}
				
				Thread.sleep(3000);
				rightForgottenPlus.get(0).click();
				Thread.sleep(1000);
				rightForgottenContractStatusFinalized.sendKeys(list.get(i).get("Status"));
				Thread.sleep(1000);
				rightForgottenContractRulesFinalized.sendKeys(list.get(i).get("Option"));
				Thread.sleep(1000);
				saveButton.click();
				Thread.sleep(3000);
			} else if (section.contains("Rejected contracts")) {
				
				wait.until(ExpectedConditions.elementToBeClickable(rightForgottenRejectContractsSection)).click();
				By  rightForgottenTrash = By.xpath("//table[contains(@id,'Rules_Rejected')]//child::span[@class='fa fa-fw fa-trash fa-lg']");
				if (driver.findElements(rightForgottenTrash).size() != 0) {
					for (int t = 0; t < rightForgottenTrashRejected.size(); t++) {
						rightForgottenTrashRejected.get(t).click();
						Thread.sleep(3000);
						driver.switchTo().alert().accept();
					}
				}
				
				Thread.sleep(3000);
				rightForgottenPlus.get(1).click();
				Thread.sleep(1000);
				rightForgottenContractRulesRejected.sendKeys(list.get(i).get("Option"));
				Thread.sleep(1000);
				saveButton.click();
				Thread.sleep(3000);
			}
			
		}

					
	}

	public void clickFinanceSubmenu() {

		wait.until(ExpectedConditions.elementToBeClickable(financeSubmenu)).click();
	}

	public void clickAuditMenu() {

		wait.until(ExpectedConditions.elementToBeClickable(auditMenu)).click();
	}
	
	public void clickAPILogsSubmenu() {

		wait.until(ExpectedConditions.elementToBeClickable(apiLogsSubmenu)).click();
	}

	public void clickUserActionsBOLogsMenu() {

		wait.until(ExpectedConditions.elementToBeClickable(userActionsBOLogsMenu)).click();
	}

	public void clickContractInfoTab() {

		contractInfoTab.click();
	}

	public void clickContractTransactionsTab() {

		wait.until(ExpectedConditions.elementToBeClickable(contractTransactionsTab)).click();
	}

	public void clickScoringTab() {

		wait.until(ExpectedConditions.elementToBeClickable(scoringTab)).click();
	}

	public void clickOnTheTab(String tab) {

		WebElement clickTab = driver.findElement(By.xpath("//a[text()='" + tab + "']"));
		wait.until(ExpectedConditions.elementToBeClickable(clickTab)).click();

	}

	public void clickOnTheSection(String section) {

		WebElement clickSection = driver.findElement(By.xpath("//span[contains(text(),'" + section
				+ "')]//ancestor::div[contains(@class,'AccordionVertical_item')]"));

		if (!clickSection.getAttribute("class").contains("open")) {
			wait.until(ExpectedConditions.elementToBeClickable(clickSection)).click();
		}
	}

	public void unmarkConsentsAndSave() {

		if (consentOney.getAttribute("class").contains("changed")) {
			consentOney.click();
		}
		if (consentOney1.getAttribute("class").contains("changed")) {
			consentOney1.click();
		}
		if (consentPartner.getAttribute("class").contains("changed")) {
			consentPartner.click();
		}
	}

	public void clickCollectionsMenu() throws InterruptedException {

		System.out.println(collectionsMenu.getAttribute("class"));

		if (!collectionsMenu.getAttribute("class").equalsIgnoreCase("Menu_DropDownButton OSInline open")) {
			collectionsMenu.click();
		}
		Thread.sleep(300);

	}

	public void clickSoftCollectionsSubmenu() {

		softcollectionsSubmenu.click();
	}

	public void clickSFTPFilesReportLogsSubmenu() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", SFTPFilesReportLogsSubmenu);
		SFTPFilesReportLogsSubmenu.click();
	}

	public void clickHardCollectionsSubmenu() {

		wait.until(ExpectedConditions.elementToBeClickable(hardcollectionsSubmenu)).click();
	}

	public void clickMarketplaceRulebookSubmenu() {

		wait.until(ExpectedConditions.elementToBeClickable(marketplaceRulebookSubmenu)).click();
	}

	public void clickHardCollectionFilesSubmenu() {

		hardcollectionFilesSubmenu.click();
	}


	public void clickPSPList() throws InterruptedException {
		
		Thread.sleep(11000);
		wait.until(ExpectedConditions.elementToBeClickable(pspList)).click();

	}

	public void clickCheckoutPSP() {

		wait.until(ExpectedConditions.elementToBeClickable(checkoutPSP)).click();
	}

	public void clickAltexFiles() {

		wait.until(ExpectedConditions.elementToBeClickable(altexFiles)).click();
	}

	public void validateLastAltexFileGenerated() throws InterruptedException{
		
		Thread.sleep(1000);

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);


		LocalDate today = LocalDate.now();
		LocalDate yesterday = LocalDate.now().minusDays(1);
		LocalDate date2 = LocalDate.now().minusDays(3);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMYYYY");
		String todayDate = (today.format(formatter));
		String yesterdayDate = (yesterday.format(formatter));
		String lastfriday = (date2.format(formatter));
		String altexFile = altexFileLatest.getText();
		System.out.println(altexFile);

		switch (day) {

		case Calendar.MONDAY:
			assert (altexFile.contains(lastfriday) || altexFile.contains(todayDate) );
			break;

		case Calendar.TUESDAY:
			assert (altexFile.contains(yesterdayDate) || altexFile.contains(todayDate));
			break;

		case Calendar.WEDNESDAY:
			assert (altexFile.contains(yesterdayDate) || altexFile.contains(todayDate));
			break;

		case Calendar.THURSDAY:
			assert (altexFile.contains(yesterdayDate) || altexFile.contains(todayDate));
			break;

		case Calendar.FRIDAY:
			assert (altexFile.contains(yesterdayDate) || altexFile.contains(todayDate));
			break;

		default:
			System.out.println("No Altex file generated over the weekend");
		}
	}

	public void downloadAltexFile() throws InterruptedException, IOException  {

		wait.until(ExpectedConditions.elementToBeClickable(altexFileLatest)).click();
		fileName = (altexFileLatest.getText());
		System.out.println("FileName:" + fileName);
		Thread.sleep(5000);

		String downloadPath = System.getProperty("user.home") + "\\Downloads\\";
		String projectPath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\";

		utils.moveFileToProjectDirectory(downloadPath + fileName, projectPath + fileName);
	}
	
	public void getNameAccountingFile() {
		
		fileName = accountingFileLatest.getText();
	}
	
	public int searchAccountingFile(String beginDate, String endDate ) throws InterruptedException {
		
		accountingBeginDate.clear();
		accountingBeginDate.sendKeys(beginDate);
		Thread.sleep(5000);
		accountingEndDate.clear();
		accountingEndDate.sendKeys(endDate);
		Thread.sleep(5000);
		search.click();
		Thread.sleep(20000);
		
		System.out.println(accountingFileAll.size());
		return accountingFileAll.size();
		
	}
	
	public void downloadAccountingSpecificFile(int i) throws InterruptedException, IOException {
		
		fileName = accountingFileAll.get(i).getText();
		System.out.println("FileName:" + fileName);
		Thread.sleep(5000);

	
		String downloadPath = System.getProperty("user.home") + "\\Downloads\\";
		String projectPath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\";
		String filenamePath = projectPath +fileName;
		
		File file = new File(filenamePath);
		
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			System.out.println(i);
			wait.until(ExpectedConditions.elementToBeClickable(accountingFileAll.get(i))).click();

			Thread.sleep(5000);
			utils.moveFileToProjectDirectory(downloadPath + fileName, filenamePath);
		}
		
	}
	
	public void downloadAccountingFile() throws InterruptedException, IOException  {

		
		fileName = accountingFileLatest.getText();
		
		System.out.println("FileName:" + fileName);
		Thread.sleep(5000);

		String downloadPath = System.getProperty("user.home") + "\\Downloads\\";
		String projectPath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\";
		String filenamePath = projectPath +fileName;
		
		File file = new File(filenamePath);
		
		if (!file.exists() && !file.isDirectory()){ //checking file availability
			
			wait.until(ExpectedConditions.elementToBeClickable(accountingFileLatest)).click();

			Thread.sleep(5000);
			utils.moveFileToProjectDirectory(downloadPath + fileName, filenamePath);
		}
		
	}
	
	

	public void clickMerchantPSPList() {

		wait.until(ExpectedConditions.elementToBeClickable(editCRP)).click();
	}

	public void clickCRPFiles() {

		wait.until(ExpectedConditions.elementToBeClickable(crpFiles)).click();
	}

	public void downloadCRPFile() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(crpFileLatest)).click();
		fileName = (crpFileLatest.getText());
		System.out.println("FileName:" + fileName);
		Thread.sleep(5000);

		String downloadPath = System.getProperty("user.home") + "\\Downloads\\";
		String projectPath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\";

		utils.moveFileToProjectDirectory(downloadPath + fileName, projectPath + fileName);

	}

	public void clickMCMPTab() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", mcMpSub);
	}






	public void clickMulticaptureTab() {

		wait.until(ExpectedConditions.elementToBeClickable(multicaptureTab)).click();
	}

	public void clickAD_RTD() {

		wait.until(ExpectedConditions.elementToBeClickable(ad_rtd)).click();
	}

	public void clickTSPI() {

		wait.until(ExpectedConditions.elementToBeClickable(tspi)).click();
	}

	public void valida() {

		wait.until(ExpectedConditions.elementToBeClickable(mcMpSub)).click();
	}

	public void clickPhoneNumbersTab() {

		wait.until(ExpectedConditions.elementToBeClickable(phoneNumbers)).click();
	}

	public void clickEditButton() {

		wait.until(ExpectedConditions.elementToBeClickable(editButton)).click();
	}

	public void validateStringContractPage() {

		actionsContract.click();
	}

	public void validateMulticaptureMarketplace() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scroll(0,850)");
		Thread.sleep(5000);

		wait.until(ExpectedConditions.elementToBeClickable(marketplaceCheckbox)).click();

		Thread.sleep(1000);

		assert (multiCaptureCheckbox.isSelected());
		System.out.println("Is Multi-Capture field is selected");

		multiCaptureCheckbox.click();

		Thread.sleep(1000);

		assert (multiCaptureCheckbox.isSelected() && marketplaceCheckbox.isSelected());
		System.out.println("Is Multi-Capture and Is Marketplace fields are both selected");

	}

	public void searchByContractStatus(String statusToSearch) throws InterruptedException {

		Thread.sleep(1000);
		statusToSearch = statusToSearch.toLowerCase();
//		wait.until(ExpectedConditions.elementToBeClickable(status)).click();
		wait.until(ExpectedConditions.elementToBeClickable(status)).sendKeys(statusToSearch);
		wait.until(ExpectedConditions.elementToBeClickable(status)).sendKeys(Keys.ENTER);
		Thread.sleep(700);
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();
		Thread.sleep(3000);
	}

	public void clickOnSearch() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();
	}

	public void searchbySuccessStatus(String statusToSearch) throws InterruptedException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(success)).click();
		wait.until(ExpectedConditions.elementToBeClickable(success)).sendKeys(statusToSearch);
		wait.until(ExpectedConditions.elementToBeClickable(success)).sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();
	}

	public void validateLogMessage(String logMessage) throws InterruptedException {

		Thread.sleep(1000);

		switch (logMessage) {
		case "File sent successfully":
			System.out.println("The SFTP Log Message is: " + logMessage);
			wait.until(ExpectedConditions.elementToBeClickable(this.logMessage)).click();
			break;

		case "No sftp files report logs to show...":
			System.out.println("The SFTP Log Message is: " + logMessage);
			wait.until(ExpectedConditions.elementToBeClickable(this.logMessageNOK)).click();
			break;

		case "Session operation has timed out":
			System.out.println("The SFTP Log Message is: " + logMessage);
			wait.until(ExpectedConditions.elementToBeClickable(this.logMessagePT)).click();
			break;

		default:
			System.out.println("no match");
		}
	}

	public void searchOnGoingContractXdaysAmountXCustomerX(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		brandNameSearch.clear();
		merchantInputSearch.clear();
		status.sendKeys("-");
//		status.sendKeys(Keys.ENTER);
		amountFrom.clear();
		amountTo.clear();
		contractsFrom.clear();
		contractsTo.clear();
		customerNameSearch.clear();
		Thread.sleep(3000);
		
		for (int i = 0; i < list.size(); i++) {

			String brand = list.get(i).get("Brand");
			if (!(brand.equals(null) || brand.equals(""))) {
				brandNameSearch.sendKeys(brand);
//				brandNameSearch.sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				brandOptionSelect.click();
				Thread.sleep(1000);
			}
			
			String merchant = list.get(i).get("Merchant");
			if (!(merchant.equals(null)|| merchant.equals(""))) {
				Thread.sleep(1000);
				merchantInputSearch.sendKeys(merchant);
				merchantSurname = merchant;
				merchantOptionSelect.click();
				Thread.sleep(1000);
//				Thread.sleep(3000);
//				merchantInputSearch.sendKeys(merchant);
//				Thread.sleep(10000);
			}
			
			String statusSearch = list.get(i).get("Status");
			if (!(statusSearch.equals(null)|| statusSearch.equals(""))) {
				status.sendKeys(statusSearch);
				status.sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				
			}
			
			String amountFrom = list.get(i).get("Amount");
			if (!(amountFrom.equals(null)|| amountFrom.equals(""))) {
				this.amountFrom.sendKeys(list.get(i).get("Amount"));
				Thread.sleep(1000);
				
			}

			String amountTo = list.get(i).get("Amount");
			if (!(amountTo.equals(null)|| amountTo.equals(""))) {
				this.amountTo.sendKeys(list.get(i).get("Amount"));
				Thread.sleep(1000);
			
			}
			
		
			String productType = list.get(i).get("Product Type");
			System.out.println(productType);
			if (!(productType.equals(null)|| productType.equals(""))) {
				productTypeSearch.click();
				productTypeSearch.sendKeys(productType);
				productTypeSearch.sendKeys(Keys.ENTER);
			}
			
			LocalDateTime now = LocalDateTime.now().plusDays(1);
			int days = Integer.parseInt(list.get(i).get("Days"));
			LocalDateTime dayToSearch = LocalDateTime.now().minusDays(days);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
			String today = now.format(formatter);
			String yesterday = dayToSearch.format(formatter);

			if (days == 0) {

			} else {
				contractsFrom.sendKeys(yesterday);
				contractsTo.sendKeys(today);
				Thread.sleep(1000);
			}

			String customerName = list.get(i).get("Customer Name");
			if (!(customerName.equals(null) || customerName.equals(""))) {
				customerNameSearch.sendKeys(list.get(i).get("Customer Name"));
				Thread.sleep(1000);
			}		
		}

		search.click();
		Thread.sleep(10000);
	}

	public void searchForSFTPToday() {

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime now1 = LocalDateTime.now().minusDays(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00");
		String today = now.format(formatter);
		String yesterday = now1.format(formatter);

		SFTPFrom.sendKeys(yesterday);
		SFTPTo.sendKeys(today);
	}

	public void searchContractReference(String referenceToSerch) throws InterruptedException {

		externalReference.clear();
		externalReference.sendKeys(referenceToSerch);
	}
	
	public void searchContractNumber(String contractNumber) throws InterruptedException {

		contractInput.clear();
		contractInput.sendKeys(contractNumber);
	}

	public void searchOnGoingContractXdays(String amountToSearch, int days) {

		status.click();
		status.sendKeys("O");
		status.sendKeys(Keys.ENTER);

		LocalDateTime now = LocalDateTime.now();
		LocalDateTime dayToSearch = LocalDateTime.now().minusDays(days);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String today = now.format(formatter);
		String yesterday = dayToSearch.format(formatter);

		contractsFrom.sendKeys(yesterday);
		contractsTo.sendKeys(today);
		amountFrom.sendKeys(amountToSearch);
		amountTo.sendKeys(amountToSearch);

		search.click();
	}

	public void checkOCRDecision(DataTable arg1) {

	}

	public void checkActionsOnGoing() {

		validateStringContractPage();

		if (cancel.isDisplayed() && changeCreditCard.isDisplayed() && earlySettlement.isDisplayed()) {

			changeCreditCard.click();
		}
	}

	public void checkActionsUnpaid() throws InterruptedException {

		validateStringContractPage();
		Thread.sleep(5000);

		if (earlySettlement.isDisplayed()) {

			actionsContract.click();
		}
	}

	public void searchCustomer() throws InterruptedException{
		Thread.sleep(1000);
		email.click();
		Thread.sleep(1000);
		email.sendKeys("testing");
		Thread.sleep(1000);
		search.click();
		Thread.sleep(1000);
		
	}

	public void searchContractsByCustomerName(String customer) {

		customerNameContracsSearch.click();
		customerNameContracsSearch.sendKeys(customer);
		customerNameContracsSearch.sendKeys(Keys.ENTER);
	}

	public void searchCustomer(DataTable customer) throws InterruptedException{

		List<Map<String, String>> list = customer.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			String emailCustomer = list.get(i).get("Email");
			if (!emailCustomer.equalsIgnoreCase("")) {
				email.sendKeys(emailCustomer);
			}

			String nameCustomer = list.get(i).get("Name");
			if (!nameCustomer.equalsIgnoreCase("")) {
				customerNameCustomerSearch.click();
				customerNameCustomerSearch.sendKeys(nameCustomer);
			}
			Thread.sleep(2000);
			search.sendKeys(Keys.ENTER);
		}
	}

//	public void searchContract(DataTable customContractSearch) throws InterruptedException {
//
//		List<Map<String, String>> list = customContractSearch.asMaps(String.class, String.class);
//
//		for (int i = 0; i < list.size(); i++) {
//
//			status.sendKeys(list.get(i).get("Status"));
//			customerName.sendKeys(list.get(i).get("Name"));
//			amountFrom.sendKeys(list.get(i).get("Amount From"));
//			amountTo.sendKeys(list.get(i).get("Amount To"));
//			amountTo.sendKeys(Keys.ENTER);
//		}
//	}

	public void clickReset() throws InterruptedException{
		Thread.sleep(4000);
		reset.click();
	}

	public void clickNewMerchant() {

		newMerchant.click();
	}

	public void clickNewBrand() {

		newBrand.click();
	}

	public void clickMerchantsLink() {

		merchantsLink.click();
	}

	public void clickBrandsLink() {

		brandsLink.click();
	}

	public void clickViewButton() {

		viewButton.click();
	}

	public void searchExternalReference() {

		externalReference.sendKeys(PaymentPageSubscription.CONTRACT_REF);
	}

	public void clickSearch() {

		search.click();
	}

	public void checkContractStatus(String contractStatus) throws InterruptedException {

		Thread.sleep(3000);
		assert(this.contractStatus.getText().equalsIgnoreCase(contractStatus));
		System.out.println("Contract Status is: " + this.contractStatus.getText());
	}

	public void validateTotalAmountFinanced(String totalAmountFinanced) {

		String total = this.totalAmountFinanced.getText().replace("€", "").replace("Lei", "").replace(" ", "");
		System.out.println(totalAmountFinanced.replace(" ", ""));
		System.out.println(total);
		assert (total.contains(totalAmountFinanced.replace(" ", "")));

		System.out.println("Total amount financed is " + total);

	}
	
	public void validateNewAmountFinanced(String newAmountFinanced) {

		String newA = this.newAmountFinanced.getText().replace("€", "").replace("Lei", "").replace(" ", "");
		System.out.println(newAmountFinanced.replace(" ", ""));
		System.out.println(newA);
		assert (newA.contains(newAmountFinanced.replace(" ", "")));

		System.out.println("New amount financed is " + newA);

	}

	public void validateTotalOrderCostFees(String amount) {

		String total = this.totalOrderCostFees.getText().replace("€", "").replace("Lei", "").replace(" ", "");
		System.out.println(amount.replace(" ", ""));
		System.out.println(total);
		assert (total.contains(amount.replace(" ", "")));

		System.out.println("Total Order Cost + Fees " + total);

	}

	public void validateCapitalAmount(String capitalAmount) {

		String capital = this.capitalAmount.getText().replace("€", "").replace("Lei", "");

		System.out.println("A mount is " + capital);
		
		assert (capital.contains(capitalAmount));

	}

	public void validateNumberOfInstallments() {

		int count = rows.size();
		int count2 = rowsPlanned.size();
		getReferenceNumber();

		nrInstallments = count;

		nrInstallmentsPlanned = count2;
		firstInstallmentPlanned = Integer.parseInt(rowsPlanned.get(0).getText());
		System.out.println("First installment: " + String.valueOf(firstInstallmentPlanned));
		capitalActionsContract = capitalAmountList.get(count-count2).getText();
		amountFeesActionsContract = feesAmountList.get(count-count2).getText();
		amountActionsContract = amountList.get(count-count2).getText();
	}

	public void getContractNumber() {

		contractNumber = contractNr.getText().substring(9, 19);
		System.out.println(contractNumber);
	}
	
	
	public void getReferenceNumber() {

		contractNumber = contractNr.getText().substring(9, 19);

		contractTransactionRef = reference.getText().substring(0, (reference.getText().length() - 1));
		System.out.println("NR DA REF: " + contractTransactionRef);

		System.out.println("NR DO CONTRATO: " + contractNumber);

	}

	public void searchContractNumber() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(contractInput)).sendKeys(contractNumber);
	}

	public void clickActionsToContract() throws InterruptedException {

		Thread.sleep(1500);
		actionsToContract.click();
		Thread.sleep(1500);
	}
	
	public void clickActionsToCustomer() throws InterruptedException {

		Thread.sleep(1500);
		actionsToCustomer.click();
		Thread.sleep(1500);
	}

	public void clickCancelButton() throws InterruptedException {

		cancel.click();
		Thread.sleep(5000);
	}
	
	public void clickRightForgotten() throws InterruptedException {

		rightToBeForgottenButton.click();
		Thread.sleep(5000);
		
		
	}
	
	public void confirmRightForgotten() throws InterruptedException {

		driver.switchTo().frame(iframe);
		yesButton.click();
		Thread.sleep(120000);
		
	}


	public void clickEarlySettlement() throws InterruptedException {

		earlySettlement.click();
		Thread.sleep(5000);
	}

	public void getItemsLastPrice() {

		priceItem1 = item1.getText().replace("€", "").replace("Lei", "").replace(".", "");
		System.out.println("ESTE É O PREÇO DO ARTIGO 1: " + priceItem1);
		By itemExist2 = By.xpath("//body[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[2]/span[1]/table[1]/tbody[1]/tr[2]/td[7]/div[1]/span[1]");
		By itemExist3 =  By.xpath("//body[1]/form[1]/div[3]/div[1]/div[1]/div[1]/div[2]/span[1]/table[1]/tbody[1]/tr[3]/td[7]/div[1]/span[1]");
	
		if (driver.findElements(itemExist2).size() != 0) {
			priceItem2 = item2.getText().replace("€", "").replace("Lei", "").replace(".", "");
			System.out.println("ESTE É O PREÇO DO ARTIGO 2: " + priceItem2);
		}

		if (driver.findElements(itemExist3).size() != 0) {
			priceItem3 = item3.getText().replace("€", "").replace("Lei", "").replace(".", "");
			System.out.println("ESTE É O PREÇO DO ARTIGO 3: " + priceItem3);
		}
		
	}
	
	public void clickCancelAll() throws InterruptedException {
		cancelAll.click();
		Thread.sleep(700);;
		amountActionsContract = cancelationAmount.getAttribute("value");
		amountFeesActionsContract = "0.00";
		System.out.println(cancelationAmount.getAttribute("value"));
	}
	
	public void cancelPartial(String amount) {
		
		cancelationAmount.click();
		cancelationAmount.sendKeys(amount);
		amountActionsContract = amount;
		amountFeesActionsContract = "0.00";
		System.out.println(amount);
	}
	
	public void cancelItems(String item) {
		
		cancelationItem.click();
		cancelationItem.sendKeys(item);
		cancelationAmount.click();
		amountActionsContract = cancelationAmount.getAttribute("value");
		amountFeesActionsContract = "0.00";
		System.out.println(cancelationAmount.getAttribute("value"));
	}

	public void confirmCancel() {

		applyCancel.click();
	}

	public void confirmEarlySettlement(String arg1) throws InterruptedException {

		if (arg1.equalsIgnoreCase("total")) {
			System.out.println(valueEarlySettlement.getText());

			if ((valueEarlySettlement.getText().contains(",05")) || (valueEarlySettlement.getText().contains(",12"))
					|| (valueEarlySettlement.getText().contains(",14"))
					|| (valueEarlySettlement.getText().contains(",51"))
					|| (valueEarlySettlement.getText().contains(",54"))
					|| (valueEarlySettlement.getText().contains(",63"))
					|| (valueEarlySettlement.getText().contains(",62"))
					|| (valueEarlySettlement.getText().contains(",33"))) {

				amountPartialEarlySettlement.sendKeys(valueEarlySettlement.getText().replace(".", "")
						.replace(",05", ".04").replace(",12", ".11").replace(",14", ".13").replace(",51", ".50")
						.replace(",54", ".53").replace(",62", ".61").replace(",63", ".61").replace(",33", ".32"));
				saveEarlySettlement.click();
				Thread.sleep(15000);
				clickEarlySettlement();

				driver.switchTo().frame(iframe);
				applyEarlySettlement.click();
				Thread.sleep(800);
				amountActionsContract = amountPartialEarlySettlement.getAttribute("value");
				amountFeesActionsContract = "0.00";

				System.out.println(amountPartialEarlySettlement.getAttribute("value"));

			} else {
				applyEarlySettlement.click();
				Thread.sleep(800);
				amountActionsContract = amountPartialEarlySettlement.getAttribute("value");
				amountFeesActionsContract = "0.00";

				System.out.println(amountPartialEarlySettlement.getAttribute("value"));
			}

		} else if (arg1.equalsIgnoreCase("partial")) {

			amountPartialEarlySettlement.sendKeys("25.10");
			amountActionsContract = "25.10";
			amountFeesActionsContract = "0.00";
		}
		saveEarlySettlement.click();
	}
	
	public void refusingEarlySettlement() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(amountPartialEarlySettlement)).click();
		wait.until(ExpectedConditions.elementToBeClickable(amountPartialEarlySettlement)).sendKeys("50,14");
		amountActionsContract = "50,14";
		amountFeesActionsContract = "0.00";
//		applyEarlySettlement.click();
		wait.until(ExpectedConditions.elementToBeClickable(saveEarlySettlement)).click();
//		saveEarlySettlement.click();
	}

	public void payNextInstallmentPlanned(String amountToCharge) throws InterruptedException {
		
		installmentNumber = String.valueOf(firstInstallmentPlanned);
		chargeInstallment(contractTransactionRef + firstInstallmentPlanned, amountToCharge);
		Thread.sleep(5000);
		driver.navigate().refresh();
	}

	public void payInstallments(String amountToCharge) throws InterruptedException, IOException {

		int count = (firstInstallmentPlanned + nrInstallmentsPlanned);
		installmentNumber = String.valueOf(firstInstallmentPlanned);
		for (int i = firstInstallmentPlanned; i < count; i++) {

			chargeInstallment(contractTransactionRef + i, amountToCharge);
			Thread.sleep(50000);
			driver.navigate().refresh();
				
		}
	}

	public void validateStatusContractTransactions(String contractTransactionsStatus) {

		int count = contractTransactionsStatusList.size() - 2;
		System.out.println(contractTransactionsStatusList.get(count).getText());

		assert (contractTransactionsStatusList.get(count).getText().indexOf(contractTransactionsStatus) > 0);
		System.out.println("Contract Transaction Status is: " + contractTransactionsStatus);

	}

	public void chargeInstallment(String reference, String chargeAmount) {

		wait.until(ExpectedConditions.elementToBeClickable(transactionReference)).clear();
		wait.until(ExpectedConditions.elementToBeClickable(transactionReference)).sendKeys(reference);
		wait.until(ExpectedConditions.elementToBeClickable(amount)).sendKeys(chargeAmount);
		wait.until(ExpectedConditions.elementToBeClickable(amountOK)).click();
	}

	public void clickBrandInfo() {

		wait.until(ExpectedConditions.elementToBeClickable(brandInfo)).click();
		BackOfficeAccounting.merchantName = merchantName.getText();
		String type =  merchantType.getText();
		BackOfficeAccounting.channel = type;
	}

	public void getFPClientCode() {

		FPClientCode = wait.until(ExpectedConditions.elementToBeClickable(fpClientCode)).getText();
		BackOfficeAccounting.customerSUID = FPClientCode;
		BackOfficeAccounting.customerGUID = externalCodeCustomer.getText();
		System.out.println("This is the FP Client Code: " + FPClientCode + " ExternalCode " + externalCodeCustomer.getText());
	}

	public void clickPaymentRetry() throws InterruptedException {

		for (int i = 0; i < nrInstallmentsPlanned; i++) {

			if (actionsToContractComponent.getAttribute("class").equals("SectionExpandable expanded")) {

				paymentRetry.click();
				Thread.sleep(10000);
			} else {
				actionsToContract.click();
				Thread.sleep(1000);
				paymentRetry.click();
				Thread.sleep(10000);
			}
			Thread.sleep(20000);
		}
	}

	public void clickDecisionTab() {

		decisionTab.click();
	}

	public void validateRejectionReasonPSP(String rejectionReason) {

		assert (this.rejectionReasonPSP.getText().contains(rejectionReason));

		System.out.println("The Rejection Reason is: " + rejectionReason);

	}
	
	public void validateRejectionReasonEngineDecision(String rejectionReason) {

		assert (this.rejectionReasonEngineDecision.getText().contains(rejectionReason));

		System.out.println("The Rejection Reason is: " + rejectionReason);

	}
	
	public void validateRejectionType(String rejectionType) {

		assert (this.rejectionType.getText().contains(rejectionType));

		System.out.println("The Rejection Type is: " + rejectionType);

	}

	public void validateRejectionReasonSafewatch(String rejectionReason) {

		assert (this.rejectionReasonSafewatch.getText().contains(rejectionReason));

		System.out.println("The Rejection Reason is: " + rejectionReason);
	}

	public void clickItemListTab() {

		wait.until(ExpectedConditions.elementToBeClickable(itemListTab)).click();
	}

	public void clickItemListTabItemsDetails() {

		wait.until(ExpectedConditions.elementToBeClickable(itemListTabItemsDetails)).click();
	}

	public void validateItemsDetailsContent() {

		driver.switchTo().frame(iframe);
		wait.until(ExpectedConditions.textToBePresentInElement(itemsDetailsContent, "560022010444"));
		wait.until(ExpectedConditions.textToBePresentInElement(itemsDetailsContent, "560022688485"));
	}

	public void checkCustomerScoringTable(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			assert (scoringAcceptanceValue.getText().equalsIgnoreCase(list.get(i).get("Scoring Acceptance Value")));
			assert (customerFinalScoring.getText().equalsIgnoreCase(list.get(i).get("Customer Final Scoring")));
			assert (customerScoringDecision.getText().equalsIgnoreCase(list.get(i).get("Decision")));
		}
	}

	public void checkCustomerScoringDetails(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			assert (scoringAge.getText().contains(list.get(i).get("Age")));
			assert (scoringEmail.getText().contains(list.get(i).get("Email")));
		}
	}

	public void clickEngineDecisionMenu() throws InterruptedException {

		engineDecisionMenu.click();
	}

	public void clickDecisionHistorySubmenu() {

		decisionHistorySubmenu.click();

	}
	
	public void clickDecisionHistorySubmenuES() {

		decisionHistorySubmenuES.click();

	}

	public void clickAccountingMenu() {
		closeFeedbackMessage.click();
		wait.until(ExpectedConditions.elementToBeClickable(accountingMenu)).click();

	}
	
	public void clickBillingFiles() {

		wait.until(ExpectedConditions.elementToBeClickable(billingFilesSubmenu)).click();

	}
	
	public void clickInvoicingFilesTab() {

		wait.until(ExpectedConditions.elementToBeClickable(invoicingFileTab)).click();

	}
	
	public void validateFinancingFileGeneration() {
		//Billing files are only generated when the amount of created'contracts are higher than cancellations.
		assert financialFileXlsx.isDisplayed();
	}
	
	public void validateInvoicingFileGeneration() {
		
		assert invoicingFileXlsx.isDisplayed();

	}
	
	public void clickAccountingFilesSection() {

		accountingFilesSection.click();

	}
	
	public void clickAccountingFilesSubmenu() {

		accountingFilesSubmenu.click();

	}
	
	public void clickJSONFilesSubmenu() {

		jsonFilesSubmenu.click();

	}

	public void clickIBANValidationSubmenu() {

		ibanValidationSubmenu.click();

	}

	public void searchDecisionHistoryByContract() throws InterruptedException {

		Thread.sleep(1500);
		decisionHistoryContractInput.sendKeys(contractNumber);
		Thread.sleep(1500);
		search.click();
		Thread.sleep(3000);
		contractDecisionHistory.click();
	}

	public void clickCustomerScoringRseultsTab() throws InterruptedException {

		Thread.sleep(1500);
		customerScoringResultsTab.click();
	}

	public void insertAmountToRefund() throws InterruptedException {

		amountToRefund1.click();
		amountToRefund1.sendKeys(priceItem1);
		System.out.println("ESTE É O PREÇO DO REFUND 1: " + amountToRefund1.getText());

		Thread.sleep(1000);
		
		if (driver.getCurrentUrl().contains("oney.pt")) {
			By amountItemPT2 = By.xpath("//div[contains(text(),'Barca Velh')]//following::div[4]//input[1]");
			if (driver.findElements(amountItemPT2).size() != 0) {
				amountToRefund2PT.click();
				amountToRefund2PT.sendKeys(priceItem2);
				System.out.println("ESTE É O PREÇO DO REFUND 2: " + amountToRefund2PT.getText());
			}
		} else {
			By amountItem2 = By.xpath("//div[contains(text(),'Bruno Giac')]//following::div[4]//input[1]");
			if (driver.findElements(amountItem2).size() != 0) {
				amountToRefund2.click();
				amountToRefund2.sendKeys(priceItem2);
				System.out.println("ESTE É O PREÇO DO REFUND 2: " + amountToRefund2.getText());
			}
		}

		By amountItem3 =  By.xpath("//div[contains(text(),'Saca Rolha')]//following::div[4]//input[1]");
		if (driver.findElements(amountItem3).size() != 0) {
			amountToRefund3.click();
			amountToRefund3.sendKeys(priceItem3);
			System.out.println("ESTE É O PREÇO DO REFUND 3: " + amountToRefund3.getText());
		}
		
		

	}

	public void insertAmountToRefundFirstProduct() throws InterruptedException {

		System.out.println(priceItem1);
		if (priceItem1.equals("")) {
			amountToRefund2.sendKeys(priceItem2);

		} else {
			amountToRefund1.click();
			amountToRefund1.sendKeys(priceItem1);
		}
	}

	public void insertAmountToRefundTaCompesation(String amountToCancel) throws InterruptedException {

		amountToRefundMerchant.sendKeys(amountToCancel);
	}

	public void checkMobilePhone() throws InterruptedException {

		String customerMobilePhone = contractMobilePhoneNumber.getText();
		customerEmail = customerEmailAddress.getText();
		System.out.println(customerEmailAddress.getText());
		System.out.println(contractMobilePhoneNumber.getText());
		contractName.click();
		Thread.sleep(1000);
		assert (customerMobilePhone.contains(customerMobile.getAttribute("value")));

		System.out.println(customerMobile.getAttribute("value"));
	}
	
	public void clickContractName() {
		customerEmail = customerEmailAddress.getText();
		System.out.println(customerEmail);
		contractName.click();
	}

	public void checkIfMobilephoneIsSaved() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(mobileSaved)).click();
		Thread.sleep(1000);
	}

	public void changeMobilePhone(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			employerfieldCustomer.sendKeys("aeuhaeuaehu");
			occupationfieldCustomer.sendKeys("QATest");
			customerMobilePrefix.clear();
			customerMobile.clear();
			Thread.sleep(2000);
			customerMobilePhoneNumber = list.get(i).get("Mobile Phone");
			customerMobile.sendKeys(customerMobilePhoneNumber.replace(" ", ""));
			Thread.sleep(1000);
			System.out.println(customerMobilePhoneNumber);
		}
		saveButton.click();
	}

	public void checkmobileMessageError(DataTable mobile) throws InterruptedException {

		List<Map<String, String>> list = mobile.asMaps(String.class, String.class);
		String message;

		for (int i = 0; i < list.size(); i++) {

			message = list.get(i).get("Message");
			Thread.sleep(2000);
			System.out.println(message);

			System.out.println(mobileMessageErrorBO.getText());
			assert (message.contains(mobileMessageErrorBO.getText()));
		}
		System.out.println("Message Error: " + mobileMessageErrorBO.getText());
		Thread.sleep(2000);
	}

	public void checkMobilePhoneView() throws InterruptedException {

		String customerMobilePhoneSubscription = PaymentPageSubscription.customerMobilePhoneNumber;
		String customerMobilePhoneCustomer = customerMobileView.getText();

		System.out.println("Customer: " + customerMobilePhoneCustomer);
		System.out.println("Subscription: " + customerMobilePhoneSubscription);

		assert (customerMobilePhoneCustomer.equalsIgnoreCase(customerMobilePhoneSubscription));
		Thread.sleep(2000);
	}

	public void searchCustomerbyEmail(String emailCustomer) throws InterruptedException {

		email.click();
		email.sendKeys(emailCustomer);
		Thread.sleep(200);
		search.click();
		Thread.sleep(1000);
	}

	public void validateBillingAddress() throws InterruptedException {

		billingAddressTab.click();
		Thread.sleep(1000);

	}
	
	public void getIDSoftcollections() {
		System.out.println("//td[text()='" + contractTransactionRef + firstInstallmentPlanned + "']//following::div[1]");
		WebElement id = driver.findElement(By.xpath("//td[text()='" + contractTransactionRef + firstInstallmentPlanned + "']//following::div[1]"));
		idCollection = id.getText();
		System.out.println(idCollection);
	}

	public void clickLinkHardCollection() {

		hardCollectionLink.click();

	}

	public void putOnHardCollection() throws InterruptedException {
		LocalDateTime dayToSearch = LocalDateTime.now().plusDays(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		String today = dayToSearch.format(formatter);
		wait.until(ExpectedConditions.elementToBeClickable(hardCollectionIdSoft)).clear();
		wait.until(ExpectedConditions.elementToBeClickable(hardCollectionIdSoft)).sendKeys(idCollection);
		wait.until(ExpectedConditions.elementToBeClickable(hardCollectionPlannedDate)).clear();
		Thread.sleep(1000);
		hardCollectionPlannedDate.sendKeys(today);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(sentToHardCollectionButton)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(runTimersButton)).click();
		Thread.sleep(7000);
	}

	public void validateStatusHardCollection(String status) {

		float globalDebt;

		if (status.equalsIgnoreCase("Settled")) {
			globalDebt = 0.00f;
		} else {
			globalDebt = totalAmount;
		}
		assert (statusHardcollection.getText().equalsIgnoreCase(status));
		System.out.println(Float.parseFloat(
				globalDebtAmount.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".")));
		System.out.println(globalDebt);
		assert ((Float.parseFloat(globalDebtAmount.getText().replace("€", "").replace("Lei", "").replace(".", "")
				.replace(",", ".")) == globalDebt));

	}

	public void searchHardcollectionByContract() throws InterruptedException {

		reset.click();
		Thread.sleep(500);
		hardcollectionContractInput.sendKeys(contractNumber);
		Thread.sleep(1500);
		search.click();
		Thread.sleep(1500);

	}

	public void downloadHardCollectionEntries() throws InterruptedException {

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String today = now.format(formatter);
		wait.until(ExpectedConditions.elementToBeClickable(hardcollectionBeginDate)).clear();
		hardcollectionBeginDate.sendKeys(today);
		wait.until(ExpectedConditions.elementToBeClickable(hardcollectionEndDate)).clear();
		hardcollectionEndDate.sendKeys(today);
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();
		Thread.sleep(1000);
		hardcollectionEntriesFile.click();

	}

	public void clickPerformedCollectionTab() {

		wait.until(ExpectedConditions.elementToBeClickable(performedCollectionsTab)).click();
	}

	public void clickClosedProcessesTab() {

		wait.until(ExpectedConditions.elementToBeClickable(closedProcessesTab)).click();
	}
	
	public String getClientIJ (String tenant) {
		String clientij = "";
		
		switch (tenant) {
			case "RO":
				clientij= "2C0799";
				break;
			case "BE":
				clientij= "2T4270";
				break;
			case "IT":
				clientij= "2T4269";
				break;
			case "ES":
				clientij= "2C0206";
				break;
			case "PT":
				clientij= "2C0937";
				break;
		}
		return clientij;
	}
	
	public String getFileRecollectionName(String file, String tenant) {
		String fileNameRec = "";
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyyMMdd");
		DateTimeFormatter formatterPerformed = DateTimeFormatter.ofPattern("MMyyyydd");
		DateTimeFormatter formatterClosed = DateTimeFormatter.ofPattern("HH");
		
		String filePerformed = now.format(formatterPerformed) + "_Retour_Facturation_" + tenant + "_" + getClientIJ(tenant);
		String fileClosed = "FICHIER_TERMINES_" + tenant + "_" + now.format(formatter2) + "_" + now.format(formatterClosed)+ now.format(formatterClosed)+ now.format(formatterClosed);

		if (file.contains("Performed")) {
			fileNameRec = filePerformed;
		} else {
			fileNameRec = fileClosed;
		}
		
		return fileNameRec;
	}

	public void uploadFileHardCollection(String file, String tenant) throws InterruptedException, IOException {

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String today = now.format(formatter);
		String fileNameRec = getFileRecollectionName(file,tenant);
		
		
		hardFilesInputDate.sendKeys(today);
		System.out.println(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\"
				+ fileNameRec + ".csv");

		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\"
				+ fileNameRec + ".csv");
		uploadButton.click();
		Thread.sleep(6000);
		System.out.println("ok");
		utils.deleteFile(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\"
				+ fileNameRec + ".csv");

	}

	public void performedCollectionsExcel(String tenant) throws IOException {

		LocalDateTime now = LocalDateTime.now();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/mm/yyyy");
		String today = now.format(formatter);
		String fileNameRec = getFileRecollectionName("Performed",tenant);
		String filenamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\" + fileNameRec + ".csv";
		
		String header = "RefClientIJ;Nos_Ref;Vos_References;VosReference_2;VosReference_3;Mont_Principal;Mont_Recouvre;"
				+ "Date_Mouvement;Act_Faite;Sold_Du_Client";

		String totalAmountString = String.valueOf(totalAmount);
		String line = getClientIJ(tenant) + ";ONEY BANK " + tenant +";"+ FPClientCode + ";"+
		contractTransactionRef + firstInstallmentPlanned+";"+ 
		"QA Automation Web"+";"+totalAmountString+";"+ totalAmountString+";"+today+";ACG;0";
		
		 FileWriter performedFile = new FileWriter(filenamePath);
		 System.out.println(header + "\r\n" + line);
		 performedFile.write(header + "\r\n" + line);
		 performedFile.close();
		
        
		System.out.println("Your csv file has been generated!");

	}

	public void closedProcessesExcel(String tenant, String status) throws IOException {

		LocalDateTime now = LocalDateTime.now();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String today = now.format(formatter);
		
		String fileNameRec = getFileRecollectionName("Closed",tenant);
		String filenamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\" + fileNameRec + ".csv";
		
		String totalAmountString = String.valueOf(totalAmount);
		
		String soldDossier = "";
		String montRecouvre = "";
		
		if (status.equals("Unpaid")) {
			
			soldDossier = totalAmountString;
			montRecouvre = "0";
		} else {
			
			soldDossier = "0";
			montRecouvre = totalAmountString;
		}
		
		String header = "RefClientIJ;Nom_Client;Nos_Ref;Vos_References;VosReference_2;VosReference_3;Date_Ouverture_Doss"
				+ ";Mont_Principal;Mont_Recouvre;Sold_Dossier;Scn_Dossier;Date_Faite_Deb;Act_Faite_Deb;Libelle_Action_IJ";
		
		String line = getClientIJ(tenant)+";ONEY BANK " + tenant+ ";1057702682;"+ FPClientCode+";"+contractNumber+";QA Automation Web;"
				+today+";"+ totalAmountString+";"+ montRecouvre + ";" + soldDossier + ";PM;"+ today+ ";ACG;Dossier classe en raison dun litige";
        
		 FileWriter closedFile = new FileWriter(filenamePath);
		 System.out.println(header + "\r\n" + line);
		 closedFile.write(header + "\r\n" + line);
		 closedFile.close();

		System.out.println("Your csv file has been generated!");
		
	}

	public void clickChangeCreditCardButton() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(changeCreditCardBO)).click();
		Thread.sleep(3000);
	}

	public void getLink() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(notificationContratInput)).clear();
		wait.until(ExpectedConditions.elementToBeClickable(notificationContratInput)).sendKeys(contractNumber);
		wait.until(ExpectedConditions.elementToBeClickable(notificationList)).click();
		wait.until(ExpectedConditions.elementToBeClickable(notificationList)).sendKeys("CHG_CARD_BO");
		wait.until(ExpectedConditions.elementToBeClickable(notificationList)).sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();

		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(notificationDetail)).click();
		url = getURL.getText();

	}
	
	public String getURL() {
		return url;
	}

	public void navigateToNotificationURL() throws AWTException, InterruptedException {

		driver.navigate().to(url);
	}

	public void clickPaymentDataTab() {

		wait.until(ExpectedConditions.elementToBeClickable(paymentDataTab)).click();

	}

	public void validateChangeCreditCardRequest() {

		assert (!inactiveCard.getText().equalsIgnoreCase("No cards to show..."));
	}

	public void validatePaymentOnNextInstallment(String arg1) {
		if (!arg1.equals("PT")) {
			assert (paymentMethodNextInstallment.getText().contains("535215XXXXXX3404"));
		} else {
			assert (paymentMethodNextInstallmentPT.getText().contains("535215XXXXXX3404"));
		}
	}

	public static void defineContractNumber(String contractnum) {

		contractNumber = contractnum;
		System.out.println(contractNumber);
	}

	public void validatePhoneNumberRegex(String regex) {

		assert (apiValidationRegex.getText().contains(regex) && editPhoneNumberRegex.getText().contains(regex));

		apiValidationRegex.click();
		System.out.println(apiValidationRegex.getText());
		System.out.println(editPhoneNumberRegex.getText());
	}

	public void validateItemsListTravelTable() {
		System.out.println(travelList.size());
		assert ((travelList.size() > 0));
	}

	public void clickLogout() {
		loginInfo.click();
		wait.until(ExpectedConditions.elementToBeClickable(logout)).click();
	}

	public void readCSVFile() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(System.getProperty("user.home")
					+ "\\git\\oney-ablewise-chrome\\Documents\\" + fileName));
		String line;
		int c = 0;
		while ( (line = br.readLine()) != null ) {
		        c++;
		        System.out.println(c + " - " + line); 
		}
		br.close();
		assert(c>=2);

		utils.deleteFile(System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\" + fileName);
	}

	public void readTxtFile() throws IOException {

		try {
//			File myObj = new File("filename.txt");
			File myObj = new File(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\" + fileName);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				System.out.println(data);
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		
		
		utils.deleteFile(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\" + fileName);
	}

	public void validateUX(String ux) {

		String uX = wait.until(ExpectedConditions.elementToBeClickable(brandInfoUX)).getText();
		assert (uX.equalsIgnoreCase(ux));
	}

	public void validateScore() {

		String scorePoint = wait.until(ExpectedConditions.elementToBeClickable(score)).getText();
		System.out.println(scorePoint);
		assert (!scorePoint.equalsIgnoreCase(""));
	}

	public void validateNewFields(String tenant) throws InterruptedException {

		
		Thread.sleep(2000);
		if (tenant.equals("PT")) {
			assert (!((occupation.getText().isEmpty()) && (employer.getText().isEmpty())));

			addressTab.click();
			Thread.sleep(2000);

			assert (!((address.getAttribute("value").isEmpty()) && (addressNumber.getAttribute("value").isEmpty())
					&& (postalCode.getAttribute("value").isEmpty()) && (municipality.getAttribute("value").isEmpty())));
		} else {
		
		System.out.println("Honorofic Code " + honorificCode.getText());
		System.out.println("Occupation " + occupation.getText());
		System.out.println("Employer " + employer.getText());
		System.out.println(((honorificCode.getText().isEmpty()) && (occupation.getText().isEmpty())
				&& (employer.getText().isEmpty())));
		assert (!((honorificCode.getText().isEmpty()) && (occupation.getText().isEmpty())
				&& (employer.getText().isEmpty())));

		addressTab.click();
		Thread.sleep(2000);
		System.out.println("Address " + address.getAttribute("value"));
		System.out.println("Address Number " + addressNumber.getAttribute("value"));
		System.out.println("Postal Code " + postalCode.getAttribute("value"));
		System.out.println("Municipality " + municipality.getAttribute("value"));

		assert (!((address.getAttribute("value").isEmpty()) && (addressNumber.getAttribute("value").isEmpty())
				&& (postalCode.getAttribute("value").isEmpty()) && (municipality.getAttribute("value").isEmpty())));
		
		}

	}

	public void clickCookieDisclaimer() {

		cookieDisclaimer.click();
	}

	public void searchMerchantByName(String merchantName) {

		wait.until(ExpectedConditions.elementToBeClickable(merchantInputName)).sendKeys(merchantName);
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();

	}

	public void searchSoftcollectionByContract() throws InterruptedException {

		softcollectionContractInput.clear();
		Thread.sleep(1500);
		softcollectionContractInput.sendKeys(contractNumber);
		Thread.sleep(2000);
		search.click();
		Thread.sleep(2000);

	}

	public void validateStatusSoftCollection(String status) {
		System.out.println("Status:" + statusSoftcollection.getText());
		assert (statusSoftcollection.getText().equalsIgnoreCase(status));
	}
	
	public void validateDelayAmount(String amount) {
		String delayAmount = delayAmountUnpaid.getText().replace("€", "").replace("Lei", "").replace(".", "")
				.replace(",", ".");
		String feesAmount = feesAmountUnpaid.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",",
				".");
		String capitalAmount = capitalAmountUnpaid.getText().replace("€", "").replace("Lei", "").replace(".", "")
				.replace(",", ".");
		String amountInstallment = amountUnpaid.getText().replace("€", "").replace("Lei", "").replace(".", "")
				.replace(",", ".");
		System.out.println("Delay: " + delayAmount);
		System.out.println("Fees: " + feesAmount);
		System.out.println("Capital: " + capitalAmount);
		System.out.println("Total: " + amountInstallment);

		float delayAmountf = Float.parseFloat(delayAmount);
		float feesAmountf = Float.parseFloat(feesAmount);
		float capitalAmountf = Float.parseFloat(capitalAmount);
		float amountInstallmentf = Float.parseFloat(amountInstallment);
		totalAmount = delayAmountf + feesAmountf + capitalAmountf;

		assert (delayAmount.contains(amount));
		assert (amountInstallmentf == totalAmount);
		amountFeesActionsContract = delayAmount;
		capitalActionsContract	= capitalAmount;
		amountActionsContract = String.valueOf(totalAmount).replace("f", "");
		
	}

	public void changeStrategy(String amountToCharge) throws InterruptedException {

		LocalDateTime dayToSearch = LocalDateTime.now().plusDays(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		String today = dayToSearch.format(formatter);
		wait.until(ExpectedConditions.elementToBeClickable(softcollectionIdTestPageChangeStrategy))
				.sendKeys(idCollection);
		System.out.println("id: " + idCollection);
		wait.until(ExpectedConditions.elementToBeClickable(updateEndDateChangeStrategy)).sendKeys(today);
		wait.until(ExpectedConditions.elementToBeClickable(amountTestPageChangeStrategy)).sendKeys(amountToCharge);
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(changeStrategyButton)).click();
		Thread.sleep(2000);

	}

	public void chargeCompleteProcess() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(softcollectionIdTestPageToComplete)).sendKeys(idCollection);
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(runTimerProcessStrategyButton)).click();
	}

	public void validateInstallmentStatus(String status) {

		WebElement nextStatus = driver
				.findElement(By.xpath("//*[starts-with(text(),'Reference')]//following::*[contains(text(),'"
						+ contractTransactionRef + String.valueOf(firstInstallmentPlanned) + "')]//following::td[6]"));
		System.out.println("//*[starts-with(text(),'Reference')]//following::*[contains(text(),'"
				+ contractTransactionRef + String.valueOf(firstInstallmentPlanned) + "')]//following::td[6]");
		System.out.println("Instal:" + nextStatus.getText());
		assert (nextStatus.getText().equalsIgnoreCase(status));
		System.out.println("Contract Status is: " + status);
	}

	public void clickIdSoftcollection() {
		idCollection = idCollectionLink.getText();
		idCollectionLink.click();

	}

	public void validateStrategy(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			assert (softcollectionTableList.get(0).getText().equalsIgnoreCase(list.get(i).get("Strategy")));

			System.out.println("Strategy: " + list.get(i).get("Strategy"));

		}
	}

	public void clickStrategy() {

		softcollectionTableList.get(0).click();
	}

	public void validateClassificationAction(DataTable arg1) throws InterruptedException {
		Thread.sleep(1000);

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		System.out.println("Customer Classification: " + customerClassificationSoftcollection.getText());
		System.out.println("Action: " + actionSoftcollection.getText());

		for (int i = 0; i < list.size(); i++) {

			assert (customerClassificationSoftcollection.getText()
					.contains(list.get(i).get("Customer Classification")));
			assert (actionSoftcollection.getText().contains(list.get(i).get("Action")));
		}
	}

	public void validateBusinessTransaction() {

		wait.until(ExpectedConditions.elementToBeClickable(btInfo)).click();
	}

	public void getNumberOfContracts(DataTable arg1) throws InterruptedException {

		String type = "total";

		while (contractsTableRows.size() != 0) {

			clickEditButton();
			clickActionsToContract();
			clickEarlySettlement();

			driver.switchTo().frame(driver.findElement(
					By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

			confirmEarlySettlement(type);
			clickContractsMenu();
		}
	}

	public void clickImportRulebook() {

		wait.until(ExpectedConditions.elementToBeClickable(importRulebookMarketplace)).click();
	}

	public void clickNewRulebook() {

		wait.until(ExpectedConditions.elementToBeClickable(newRulebook)).click();
	}

	public void validateMarketplaceNewRulebook() {

		assert (codeMarketplace.getText().equalsIgnoreCase("Code"));
	}

	public void validateImportRulebookMarketplace() {

		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'Rulebook')]")));

		wait.until(ExpectedConditions.elementToBeClickable(importRulebookMarketplaceFile)).isDisplayed();
	}

	public void searchMarketplaceRulebook(String label) {

		wait.until(ExpectedConditions.elementToBeClickable(searchMarketplaceRulebook)).sendKeys(label);
		wait.until(ExpectedConditions.elementToBeClickable(search)).click();
	}

	public void validateSearchedLabelMarketplace(String label) throws InterruptedException {

		Thread.sleep(1000);

		wait.until(ExpectedConditions.elementToBeClickable(searchedLabelMarketplace));
		String labelTableValue = searchedLabelMarketplace.getText();

		assert (labelTableValue.equalsIgnoreCase(label));
	}

	public void clicksellSecureSubmenu() {

		sellSecureSubmenu.click();
	}

	public void clickSellSecureSearchBar() {

		sellSecureSearchBar.click();
		sellSecureSearchBar.sendKeys(contractNumber);
	}

	public void clickSearchButtonSellSecureLogs() {

		search.click();
	}

	public void validatePayLaterContracts() {

		assert (payLaterAcceptedContracts.getText().equalsIgnoreCase("Pay Later "));
		assert (payLaterRejectedContracts.getText().equalsIgnoreCase("Pay Later "));
	}

	public void changeIBANBIC() {

		String iban = merchantIBAN.getAttribute("origvalue");
		String bic = merchantBIC.getAttribute("origvalue");

		if (iban.substring(iban.length() - 2, iban.length()).equals("00")) {
			iban = iban.substring(0, iban.length() - 2) + "11";
		} else {
			iban = iban.substring(0, iban.length() - 2) + "00";
		}

		System.out.println(iban);

		if (bic.substring(bic.length() - 2, bic.length()).equals("XX")) {
			bic = bic.substring(0, bic.length() - 2) + "SP";
		} else {
			bic = bic.substring(0, bic.length() - 2) + "XX";
		}
		System.out.println(bic);

		wait.until(ExpectedConditions.elementToBeClickable(merchantIBAN)).clear();
		merchantIBAN.sendKeys(iban);
		wait.until(ExpectedConditions.elementToBeClickable(merchantBIC)).clear();
		merchantBIC.sendKeys(bic);
		saveButtonMerchant.click();
	}

	public void validateMerchantStatus(String status) {

		if (status.equalsIgnoreCase("inactive")) {
			assert (!(wait.until(ExpectedConditions.elementToBeClickable(merchantActive)).getAttribute("class").contentEquals("PH ToggleButton_label changed toggled")));
		} else {
			assert (wait.until(ExpectedConditions.elementToBeClickable(merchantActive)).getAttribute("class").contentEquals("PH ToggleButton_label changed toggled"));
		}

	}

	public void validateIBAN(String approval) throws InterruptedException {

		searchMerchantInput.sendKeys("QA Automation PR60");
		search.click();

		Thread.sleep(3000);

		if (approval.equalsIgnoreCase("Approve")) {
			wait.until(ExpectedConditions.elementToBeClickable(approveIBAN)).click();
		} else {
			wait.until(ExpectedConditions.elementToBeClickable(rejectIBAN)).click();
		}
		Thread.sleep(30000);

	}

	public void existIBAN(String merchant) throws InterruptedException {

		searchMerchantInput.sendKeys(merchant);
		search.click();

		Thread.sleep(3000);

		By noMerchant = By.xpath("//td[contains(text(),'No merchants to show...')]");
				
		if (driver.findElements(noMerchant).size() == 0) {
			wait.until(ExpectedConditions.elementToBeClickable(approveIBAN)).click();
			Thread.sleep(10000);
		}

		

	}
	
	public void searchScreenBOID(String log) throws InterruptedException {
		System.out.println(logDateTo.getAttribute("value"));
		logFrom.clear();
		logFrom.sendKeys(logDateTo.getAttribute("value"));

		Thread.sleep(200);
		screenboid.click();
		screenboid.sendKeys(log);
		search.click();
		Thread.sleep(200);
	}

	public void validateIBANValidationLogDay() throws InterruptedException {

		Thread.sleep(4000);
		System.out.println(merchantLog.getText());
		assert (merchantLog.getText().equalsIgnoreCase("QA Automation PR60"));
	}

	public void validateMerchantSurname() {
		
		merchantMerchantSurname.click();
		merchantMerchantSurname.clear();
		String randomSurname= "teste" + RandomString.make(1);
		merchantMerchantSurname.sendKeys(randomSurname);
		merchantSurname = randomSurname;
		saveButtonMerchant.click();
	}
	
	public void clickMFSlogsSubmenu() {
		
		mfsLogsSubmenu.click();
		
	}
	
	public void clickIPlogsSubmenu() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", ipLogsSubmenu);
		
	}
	
	public void validatenotePadInfo(String tenant) throws InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String merchantFormat = dateFormat.format(date);
		wait.until(ExpectedConditions.elementToBeClickable(downloadInputJson)).click();
		Thread.sleep(4000);
		String rename1 = tenant + "_" + methodName.getText() + "_" + merchantFormat + "_Request.txt";
		String rename2 = rename1.replace(" ", "");
		String patholdString = (System.getProperty("user.home") + "\\Downloads\\");
		String pathnewString = (System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\");
		System.out.println(rename2);
		
		
		JSONParser parser = new JSONParser();
		
		try {

			utils.renameFile(patholdString + rename1, patholdString + rename2);
            utils.moveFileToProjectDirectory(patholdString + rename2, pathnewString + rename2);
            Thread.sleep(1000);
     
            Object obj = parser.parse(new FileReader(pathnewString + rename2));
            JSONObject jsonObject = (JSONObject) obj;
            System.out.println(jsonObject.get("invoice_name"));
            Object var = jsonObject.get("invoice_name");
            System.out.println(var);
            assert (var.equals(merchantSurname));
            utils.deleteFile(pathnewString + rename2);
        } catch (Exception e) {
        	
            e.printStackTrace();
            
        }
    }
				
	public void validatecontractID() throws InterruptedException {
	
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime todayNow = LocalDateTime.now();
		LocalDateTime lastWeek = LocalDateTime.now().minusDays(7);
		System.out.println(lastWeek);
		String dateFromString = lastWeek.format(dateFormat);
		String dateToString = todayNow.format(dateFormat);
		dataTab.click();
		Thread.sleep(2000);
		
		dateFrom.sendKeys(dateFromString);
		dateTo.sendKeys(dateToString);
		wait.until(ExpectedConditions.elementToBeClickable(createFinancialOrderFile)).click();
		wait.until(ExpectedConditions.elementToBeClickable(searchMfsLogs)).click();
		wait.until(ExpectedConditions.elementToBeClickable(downloadxlsx)).click();
		Thread.sleep(10000);
   
	}
		
	public void readXlsxFromMfsLogs(String tenant) throws IOException, InterruptedException {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date2 = new Date();
		String date3 = dateFormat.format(date2);
		
		
		String pathDownload = (System.getProperty("user.home") + "\\Downloads\\BO_MFS_OrderCreate.xlsx");
		String pathDownloadNew = (System.getProperty("user.home") + "\\Downloads\\"+date3+"_" + tenant +"_BO_MFS_OrderCreate.xlsx");
		utils.renameFile(pathDownload,pathDownloadNew);
//			String pathOldString = (System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\BO_MFS_OrderCreate.xlsx");
		
		
		String pathNewString = (System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\"+date3+"_" + tenant +"_BO_MFS_OrderCreate.xlsx");
		utils.moveFileToProjectDirectory(pathDownloadNew, pathNewString);
		
		
		File excelFile = new File(pathNewString);  
		FileInputStream fis = new FileInputStream(excelFile);
		// we create an XSSF Workbook object for our XLSX Excel File
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		// we get first sheet
		XSSFSheet sheet = workbook.getSheetAt(0);
        Thread.sleep(1000);
		//we iterate on rows

		int colIndex = 0;
		Row row = null;
		Cell cell = null;
		boolean found = false;
		boolean contractFound = false;
		
		for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			row = sheet.getRow(rowIndex);
			
			if (row != null) {
				for (colIndex = 0; colIndex <= row.getLastCellNum(); colIndex++) {
					cell = row.getCell(colIndex);
										
					if (cell.getStringCellValue().equals("Contract")) {
						found = true;
						break;
					}
				}
			}
			if (found) { 
				break;
			}

		}
		
		
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			row = sheet.getRow(rowIndex);
			
			if (row != null) {
				cell = row.getCell(colIndex);
				if (cell != null) {
					if (cell.getStringCellValue().equals(contractNumber)) {
						contractFound = true;
						break;
					}
				}
			}
		}
		assert(contractFound==true);
		
		workbook.close();
		fis.close();
		utils.deleteFile(pathNewString);
	}

	public void validateSearchResultNoContractsToShow() {
		
		searchResultNoContractToShow.click();
	}

	public String getCustomerEmail() {
		
		return customerEmail;
	}

	public void validateContract() {
		
		
		System.out.println(contractNumber);
	}
	
	public void validateRightForgottenMessageError() {
		
		rightForgottenMessageError.click();
	}
	
	public void validateSearchResultNoCustomersToShow() {
		
		searchResultNoCustomersToShow.click();
	}
	
	public void creatingAnExcelforBrandImport(String filename) throws IOException {
		
		Date date3 = new Date();
		String random = RandomString.make(5);
		pr89Name ="QA Automation Tests - PR89 "+random;
		
		Workbook workbook1 = new XSSFWorkbook();
        Sheet sheet1 = workbook1.createSheet("Sheet1"); 
        CellStyle cellStyle = workbook1.createCellStyle();
        CreationHelper createHelper = workbook1.getCreationHelper();
        cellStyle.setDataFormat(
        	    createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
        Row rowhead = sheet1.createRow(0);
        rowhead.createCell(0).setCellValue("Company_Name");
        rowhead.createCell(1).setCellValue("Code");
        rowhead.createCell(2).setCellValue("Brand_Sector");
        rowhead.createCell(3).setCellValue("Zip_Code");
        rowhead.createCell(4).setCellValue("Town");
        rowhead.createCell(5).setCellValue("Phone");
        rowhead.createCell(6).setCellValue("Email");
        rowhead.createCell(7).setCellValue("Registration_Number");
        rowhead.createCell(8).setCellValue("Priority");
        rowhead.createCell(9).setCellValue("Contracts_Start_Date");
        rowhead.createCell(10).setCellValue("Contracts_End_Date");
        rowhead.createCell(11).setCellValue("Decision_Library");
        rowhead.createCell(12).setCellValue("Customer_Scoring_Rulebook");
        rowhead.createCell(13).setCellValue("Additional Fields Decision");
        rowhead.createCell(14).setCellValue("Mobile Validation Rulebook");
        
        
        Row row = sheet1.createRow(1);
        Cell cell1 = row.createCell(9);
        cell1.setCellValue(date3);
        cell1.setCellStyle(cellStyle);
        Cell cell2 = row.createCell(10);
        cell2.setCellValue(date3);
        cell2.setCellStyle(cellStyle);
        row.createCell(0).setCellValue(pr89Name);
        row.createCell(1).setCellValue("TPR89");
        row.createCell(2).setCellValue("Electronics");
        row.createCell(3).setCellValue("1000");
        row.createCell(4).setCellValue("Bruxells");
        row.createCell(5).setCellValue("32046895228");
        row.createCell(6).setCellValue("oneytester@gmail.com");
        row.createCell(7).setCellValue("17");
        row.createCell(8).setCellValue("small");
        
        for (int i = 0; i < 10; i++) {
        	sheet1.autoSizeColumn(i);
        } 
        FileOutputStream fileOut = new FileOutputStream(filename);
        workbook1.write(fileOut);
        fileOut.close();
        workbook1.close();
        System.out.println("Your excel file has been generated!");
	}
	
	public void creatingAnExcelforMerchantImport(String filename) throws IOException {
		
		String random = RandomString.make(5);
		pr89Name = "QA Automation PR89 "+random;
        
		workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Sheet1");
        

        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        if (filename.contains("MerchantPT")) {
        	data.put("1", new Object[] {"Brand","Merchant Type","Merchant Category","Choose Ux","Merchant Name","Registration Number","Street","Building Number","Floor","Postal Code","City","Country","Web Page Url","Priority","Is Travel","Customer Address Input","Is Multi-capture","Is Marketplace","Use Pre Scorefor Decision","Show In Store Contract Notes","Is Number Of Instalments Editable","Is New Business Transactions","Financing Type","Financing Moment","Invoicing Moment","Debiting Moment","Direct Debiting Info","Transfers Info","Collected Customer fee mode","Financial Contact Name","Financial Contact Surname","Financial Contact Contact","Financial Contact Email","IBAN","BIC","Tax Type","External Reference","Vat Number","CRP Config Type","PS Pname"});
		    data.put("2", new Object[] {"PR89 - QA Automation Tests", "WEB","PSP","","QA Automation PR89 "+random,"AEZ2131"+random,"rua teste", "2","1","1000","Sintra","PORTUGAL","","Small","No","No","No","","No","","Yes","No","TRANSFER_DIRECTDEBIT","DAILY","DAILY","1","1","1","All Installments","Thomas", "Dupont ","351914907787","oneytester@gmail.com","PT50000201231234567890154","MPIOPTPLXXX","VAT Madeira","","VT"+random,"PSP","Checkout"});
		    
        } else if (filename.contains("MerchantDE")) {
        	data.put("1", new Object[] {"Brand","Merchant Type","Merchant Category","Choose Ux","Merchant Name","Registration Number","Street","Building Number","Floor","Postal Code","City","Country","Web Page Url","Priority","Is Travel","Customer Address Input","Is Multi-capture","Is Marketplace","Use Pre Scorefor Decision","Show In Store Contract Notes","Is Number Of Instalments Editable","Is New Business Transactions","Financing Type","Financing Moment","Invoicing Moment","Debiting Moment","Direct Debiting Info","Transfers Info","Collected Customer fee mode","Financial Contact Name","Financial Contact Surname","Financial Contact Contact","Financial Contact Email","IBAN","BIC","Tax Type","External Reference","Vat Number","CRP Config Type","PS Pname"});
		    data.put("2", new Object[] {"PR89 - QA Automation Tests", "WEB","","","QA Automation PR89 "+random,"AEZ2131"+random,"rua teste", "2","1","1000","Sintra","BELGIUM","https://qalogin.oneygmbh.de/BackOffice","Small","No","No","No","","No","","Yes","No","TRANSFER_DIRECTDEBIT","DAILY","DAILY","1","1","1","All Installments","Thomas", "Dupont ","351914907787","oneytester@gmail.com","PT50000201231234567890154","MPIOPTPLXXX","VAT Madeira","","VT"+random,"PSP","PSPL"});
 
	        
			} else {
	        	data.put("1", new Object[] {"Brand","Merchant Type","Merchant Category","Choose Ux","Merchant Name","Registration Number","Street","Building Number","Floor","Postal Code","City","Country","Web Page Url","Priority","Is Travel","Is Multi-capture","Is Marketplace","Customer Address Input","Use Pre Scorefor Decision","Show In Store Contract Notes","Is Number Of Instalments Editable","Is New Business Transactions","Financing Type","Financing Moment","Invoicing Moment","Collected Customerfeemode","Financial Contact Name","Financial Contact Surname","Financial Contact Phone","Financial Contact Email","IBAN","BIC","External Reference","Vat Number","CRP Config Type","PS Pname"});
	        data.put("2", new Object[] {"PR89 - QA Automation Tests","WEB","PSP","",pr89Name,"AEZ2131"+random,"Rua Teste","2","1","1000","Antwerp","BELGIUM","","Small","No","No","No","","No","","Yes","No","TRANSFER_DIRECTDEBIT","DAILY","DAILY","All Installments","Oney","Oney","351916111777","oneytester@gmail.com","PT50002700000001234567833","BNPAFRPPXXX","","JU45","PSP","Checkout"});
	              
	      
		}
        
      //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File(filename));
            workbook.write(out);
            out.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
	}

	public void creatingAnExcelforUserImport(String filename) throws InterruptedException, IOException {
		
		String random = RandomString.make(5);
		pr89Name = "testing-pr89"+random;
			workbook = new XSSFWorkbook(); 		         
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet("Sheet1");          
	        //This data needs to be written (Object[])
	        Map<String, Object[]> data = new TreeMap<String, Object[]>();
	        data.put("1", new Object[] {"Username","First And Last Name","Email","Prefix Phone Number","Mobile Phone","Application Type","Brand","Is Generic","Is Active","Profile","Merchant"});

	        data.put("3", new Object[] {pr89Name,"Automation Tests PR89","testing-"+random+"pr89@gmail.com","","","Backoffice","","0","1","Administrator"});
	        //Iterate over data and write to sheet
	        Set<String> keyset = data.keySet();
	        int rownum = 0;
	        for (String key : keyset)
	        {
	            Row row = sheet.createRow(rownum++);
	            Object [] objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	               if(obj instanceof String)
	                    cell.setCellValue((String)obj);
	                else if(obj instanceof Integer)
	                    cell.setCellValue((Integer)obj);
	            }
	        }
	        try
	        {
	            //Write the workbook in file system
	            FileOutputStream out = new FileOutputStream(new File(filename));
	            workbook.write(out);
	            out.close();
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	}
	
	public void createExcelFileImport(String file) throws IOException, InterruptedException, ParseException {
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date3 = new Date();
		String date4 = dateFormat.format(date3);
		

		String filename = System.getProperty("user.home")+ "\\git\\oney-ablewise-chrome\\Documents\\" +date4+ file + "Import.xlsx";
	        
		if (file.contains("Brand")) {
			creatingAnExcelforBrandImport(filename);
			
		} else if (file.contains("Merchant")) {
			creatingAnExcelforMerchantImport(filename);
		}else if (file.contains("Users")) {
			creatingAnExcelforUserImport(filename);
		}
	        

	   }
	
	public void validateTheCreation() throws InterruptedException {
		
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		Thread.sleep(800);
		inputSearch.sendKeys(pr89Name);
		search.click();
		Thread.sleep(4000);
		int countResult = tableResult.size();

		assert(countResult==2);
		By noItemToShow =  By.xpath("//td[contains(text(),'No') and contains(text(),'to show...')]");
		assert(driver.findElements(noItemToShow).size()== 0);
		
		
		
	}
			
	public void uploadFileImport(String file) throws InterruptedException, IOException{
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date3 = new Date();
		String date4 = dateFormat.format(date3);
		importButton.click();
		driver.switchTo().frame(iframe);
		choseImportFile.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\"+date4+ file +"Import.xlsx");
		validateFile.click();
		Thread.sleep(1000);
		importFile.click();
		Thread.sleep(2000);
		utils.deleteFile(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\"+date4+ file +"Import.xlsx");
	}
				
	public void clickonUsersProfilemenu() {
		wait.until(ExpectedConditions.elementToBeClickable(closeSuccessfullMessage)).click();
		wait.until(ExpectedConditions.elementToBeClickable(userAndProfileMenu)).click();
		wait.until(ExpectedConditions.elementToBeClickable(userAndProfileSubmenu)).click();
	}
	
	public void anonymizeContract() {
		pasteToAnonymize.click();
		pasteToAnonymize.sendKeys(contractNumber);
		submitAnonymize.click();
		
	}
	
	public void deleteContract() {
		pasteToDelete.click();
		pasteToDelete.sendKeys(contractNumber);
		submitDelete.click();
		
	}
	
	public void validateIfcontractNumberStillAvailable() throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(contractSearch)).click();
		contractSearch.sendKeys(contractNumber);
		search.click();
		Thread.sleep(1500);
		noContractFoundResult.getText();
		assert (noContractFoundResult.isDisplayed());
	}
			
	public void writeActionsToContract(String tenant, String action) throws IOException {
		
		if (action.equals("Soft") || action.equals("Hard") || action.equals("Paid")) {
			utils.writeContract(tenant, action, null, contractNumber,amountActionsContract, amountFeesActionsContract,
					null, null, "ACCEPTED", null, null, null, null,null, null, null, null, null, null, null, null, null,
					amountActionsContract, installmentNumber, capitalActionsContract, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					null, null, null, amountFeesActionsContract, null, null, null, null, null, null, null, null, null, null, null, null, null, 
					null, null, null, null, null, null, null, null, null);
		}else {
			utils.writeContract(tenant, action, null, contractNumber, amountActionsContract, amountFeesActionsContract,
				null, null, "ACCEPTED", null, merchantSurname, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 
				null, null, null, null, null, null, null, null, null);		
		}
	}
				
	public String getActionContractPath(String tenant, int days) {
		
		LocalDateTime yesterday = LocalDateTime.now().minusDays(days);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String yesterdayString = yesterday.format(formatter);
		
		String fileNameS = yesterdayString + "_" + tenant;
		String filenamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\" + fileNameS + ".xlsx";
		System.out.println(filenamePath);
		return filenamePath;
	}
	
	public String getErrorFiletPath(String tenant, int days) {
		
		LocalDateTime yesterday = LocalDateTime.now().minusDays(days);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String yesterdayString = yesterday.format(formatter);
		
		String fileNameS = yesterdayString + "_" + tenant + "_Error";
		String filenamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\" + fileNameS + ".xlsx";
		System.out.println(filenamePath);
		return filenamePath;
	}
	
	public String getErrorFiletPathOfTheWeek(String tenant) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		String dateBegin = utils.getLastWeekBegin().format(formatter);
		String dateEnd = utils.getLastWeekEnd().format(formatter);
					
		String fileNameAcc = dateBegin + "_" + dateEnd + "_" + tenant + "_Error";
		String fileNamePath = System.getProperty("user.home")
				+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\" + fileNameAcc + ".xlsx";
		
		System.out.println(fileNamePath);
		return fileNamePath;
	}
			
	public void writeContractDetails(String tenant, String sheet) throws InterruptedException, CsvValidationException, IOException {
		
		
		String filenamePath =getActionContractPath(tenant,1);

//					System.getProperty("user.home")
//					+ "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\20211130_DE.xlsx";
				//getActionContractPath(tenant,1);
					
		System.out.println(filenamePath);
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
            workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(sheet);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	        while (rowIterator.hasNext()) {
	        	count++;
	        	Row row = rowIterator.next();
                if (count==0) {
                	System.out.println("hi");
                	continue;
                	
                }
                Iterator<Cell> cellIterator = row.cellIterator();
                String cellValue = "";
                while (cellIterator.hasNext()) {

                	Cell cell = cellIterator.next();
                	if(cell.getColumnIndex()==1) {
                		cellValue = cell.getStringCellValue();	
                		externalRef = cellValue;
    		            System.out.println("extref:" + cellValue );
    		            Thread.sleep(100);
    		            if (cellValue.isEmpty() || cellValue.equals("")) {
    		            	continue;
    		            } else {
    		            	searchContractReference(cellValue);
    		            }
                	 } else if(cell.getColumnIndex()==2) {
                		 cellValue = cell.getStringCellValue();	 
                		 externalRef = null;
	    		         System.out.println("contractnumber:" + cellValue );
                		 searchContractNumber(cellValue);
                	 }
   
                }
	            Thread.sleep(100);
	            clickSearch();
	            Thread.sleep(1000);
	            getInfoContractSearched();
	            

                row.createCell(1).setCellValue(externalRef);		            
                row.createCell(2).setCellValue(contractNumber);
                row.createCell(3).setCellValue(amountActionsContract);
                row.createCell(4).setCellValue("");
                row.createCell(5).setCellValue(merchantSurname);
                row.createCell(6).setCellValue(bt_code);
                row.createCell(7).setCellValue(statusOfContract);	
    			
	            
	        }
		    
		    

			for (int i = 0; i < 10; i++) {

	        	sheet1.autoSizeColumn(i);
	        } 
	        FileOutputStream fileOut = new FileOutputStream(filenamePath);
	        workbook.write(fileOut);
	        fileOut.close();
	        workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	            
                           
	        }
	        
      
	}
	
	public void writeDetails(String tenant, String sheet) throws InterruptedException, CsvValidationException, IOException {
		
		String filenamePath = getActionContractPath(tenant,1);
		
		System.out.println(filenamePath);
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
            workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(sheet);
			Row rowhead = sheet1.getRow(0);
		    rowhead.createCell(2).setCellValue("contractNumber");
		    rowhead.createCell(3).setCellValue("amount");
		    rowhead.createCell(4).setCellValue("amountFees");
		    rowhead.createCell(5).setCellValue("merchant");
		    rowhead.createCell(6).setCellValue("bt_code");
		    rowhead.createCell(7).setCellValue("contractStatus");
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	        while (rowIterator.hasNext()) {
	        	count++;
	        	Row row = rowIterator.next();
                if (count==0) {
                	System.out.println("hi");
                	continue;
                	
                }
                Iterator<Cell> cellIterator = row.cellIterator();
                String cellValue = "";
                while (cellIterator.hasNext()) {

                	Cell cell = cellIterator.next();
                	if(cell.getColumnIndex()==1) {
                		cellValue = cell.getStringCellValue();	            
    		            System.out.println("extref:" + cellValue );
    		            Thread.sleep(1000);
    		            if (cellValue.isEmpty()  || cellValue.equals("")) {
    		            	continue;
    		            } else {
    		            	searchContractReference(cellValue);
    		            }
                	 } else if(cell.getColumnIndex()==2) {
                		 cellValue = cell.getStringCellValue();	 
	    		         System.out.println("contractnumber:" + cellValue );
                		 searchContractNumber(cellValue);
                	 }
   
                }
	            Thread.sleep(1000);
	            clickSearch();
	            Thread.sleep(10000);
	            getInfoContractSearched();
	            
                row.createCell(2).setCellValue(contractNumber);
                row.createCell(3).setCellValue(amountActionsContract);
                row.createCell(4).setCellValue("");
                row.createCell(5).setCellValue(merchantSurname);
                row.createCell(6).setCellValue(bt_code);
                row.createCell(7).setCellValue(statusOfContract);
	            
	        }
		    
		    

			for (int i = 0; i < 10; i++) {

	        	sheet1.autoSizeColumn(i);
	        } 
	        FileOutputStream fileOut = new FileOutputStream(filenamePath);
	        workbook.write(fileOut);
	        fileOut.close();
	        workbook.close();	    
			
			System.out.println("Your xls file has been generated!");
	            
                           
	        }		        
	        
      
	}
				
	public void getInfoContractSearched() {
		
		
		By  noContractToShow = By.xpath("//td[text()='No contracts to show...']");
		if (driver.findElements(noContractToShow).size() == 0) {
						
			contractNumber= contractsTableRowContractNumber.getText();
			amountActionsContract = contractsTableRowAmount.getText().replace("€", "").replace("Lei", "").replace(" ", "");
			merchantSurname = contractsTableRowMerchant.getText() ;
			bt_code = contractsTableRowCode.getText();
			statusOfContract = contractsTableRowStatus.getText();
			
			System.out.println(contractNumber);
			System.out.println(amountActionsContract);
			System.out.println(merchantSurname);
			System.out.println(bt_code);
			System.out.println(statusOfContract);
		} else {
			
			contractNumber= "";
			amountActionsContract =  "";
			merchantSurname =  "";
			bt_code =  "";
			statusOfContract =  "DELETED";
		}
	}
	
	public void addExtraaccountingSoftCollectioninfo(String tenant)  throws InterruptedException, CsvValidationException, IOException {
		
		
	}
	
	public void getExternalreference()  throws InterruptedException{
		
		Thread.sleep(5000);
		externalRefPR36 = getExternalReferencePR36.getText();
		System.out.println(externalRefPR36);
	}

	public void searchByMethod(String method, String tenant) throws InterruptedException {
		
		methodNameMFSSearch.sendKeys(method);
		ZonedDateTime now = utils.dateTimeTenant(tenant);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy 00:00:00");
		String todayFrom = now.format(formatter2);
		String todayTo = now.format(formatter);
		dateFrom.sendKeys(todayFrom);
		dateTo.sendKeys(todayTo);
		search.click();
		Thread.sleep(500);
	}
	
	public void downloadMFSFile(String tenant) throws InterruptedException, IOException {

		int countFile=fileMFSInputJson.size();
		countGeneral = countFile;
		for (int i = 0;i <countFile;i++) {
			wait.until(ExpectedConditions.elementToBeClickable(fileMFSInputJson.get(i))).click();
		
			LocalDateTime now = LocalDateTime.now();
	
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String today = now.format(formatter);
			String fileNameOriginal = methodString.get(i).getText() + "_" + today + "_Request.txt";
			String method  = methodString.get(i).getText();
			method = method.replace(" ", "");
			fileName = method + "_" + tenant + "_" + today + "_Request" + i + ".txt";
			System.out.println("FileName:" + fileName);
			Thread.sleep(5000);

			String downloadPath = System.getProperty("user.home") + "\\Downloads\\";
			String projectPath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\MFS\\";

			utils.renameFile(downloadPath+fileNameOriginal, downloadPath+fileName);
			File file = new File(projectPath+fileName);
			
			if (!file.exists() && !file.isDirectory()){ //checking file availability
				utils.moveFileToProjectDirectory( downloadPath+fileName, projectPath + fileName);
			} 			
		}

	}
	
	public void accessMerchantDetails()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(clickOnMerchantDetails)).click();
	}	
	
	
	public void validateMerchantCategoryOptions()  throws InterruptedException{
	(merchantCategoryOptions).get(1).getText();
		System.out.println(merchantCategoryOptions);
	}
	
	public void accesscrpConfigurationsDetails()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(clickonCRPconfigurationsTab)).click();
	}	
	
	public void validateMerchantAndPSPButtons()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(crpmerchantButton)).isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(crpPspButton)).isDisplayed();
	}
			
	public void clickonFilesTab()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(merchantFilesTab)).click();
	}
	
	public void validateMerchantFiles()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(merchantfilespopup)).click();
		wait.until(ExpectedConditions.elementToBeClickable(validatefirstmerchantFile)).isDisplayed();
	}
	
	public void clickPSPconfEditing()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(validatecheckoutPSP)).click();
	}
	
	public void validateCRPfiles()  throws InterruptedException{
		wait.until(ExpectedConditions.elementToBeClickable(crpFiles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(crpFileLatest)).isDisplayed();
	}
	
	public void validateBeforeYouGoSection()  throws InterruptedException{
		Thread.sleep(3000);
		subscriptionpageconfigssplit.click();
		Thread.sleep(2000);
		beforeyougopopinexpand.click();
	}
	
	public void validateBeforeYouGoSectionForPL()  throws InterruptedException{
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(subscriptionpageconfigsPL)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(beforeyougopopinexpand)).click();
	}
	
	public void validateBeforeYouGoSectioninfo()  throws InterruptedException{
		assert (beforeyougopopinisdisplayed.isDisplayed());
	}
	
	public void checkthatthetogglebuttonisactive()  throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scroll(0,350)");
		Thread.sleep(1000);		
		if (togglebuttonbeforeyougo.getAttribute("class").contains("ToggleButton OSInline changed")) {	
			}
		else if (togglebuttonbeforeyougo.getAttribute("class").contains("ToggleButton OSInline")) { 
			togglebuttonbeforeyougo.click();
		}
	}
	
	public void saveDataEventType(String action, String contractNumber, String tenant) throws IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, InterruptedException {
				    	
	    	switch (action) {
			case "ContractCreated":
				
				acc.saveDataEventTypeContractCreated(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;				
			case "Cancel":
			
				acc.saveDataEventTypeCancel(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;	
			case "CancelPartial":
				
				acc.saveDataEventTypeCancel(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "CancelTotal":
				
				acc.saveDataEventTypeCancel(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "EarlySettlement":
				
				acc.saveDataEventTypeEarlySettlement(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "Soft":
				
				acc.saveDataEventTypeSoft(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "SoftPaid":
				
				acc.saveDataEventTypeSoftPaid(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "Hard":
				
				acc.saveDataEventTypeHard(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "Litigation":
				
				acc.saveDataEventTypeLitigation(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "Paid":
					
				acc.saveDataEventTypePaid(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			case "MFS":
					
				acc.saveDataEventTypeMFS(action, getActionContractPath(tenant,1), contractNumber, tenant);
				break;
			default:
				System.out.println("Please insert correct eventType");
			}
	    
	 }

	public void clickRequestInfoTab() {

		requestInfoTab.click();
		BackOfficeAccounting.paymentAmount =  requestAmount.getText().replace(".","").replace("€", "").replace("Lei", "").replace(" ", "").replace(",", ".");
	}
	
	public void getDataEventTypeContractCreated(String tenant, String contractNumber) throws InterruptedException {
		
		DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.HALF_UP);
		String customerFeeF;
		String customerFeeV;
		BackOfficeAccounting.clearFields();
		clickContractsMenu();
		BackOfficeAccounting.countryCode = tenant;
		BackOfficeAccounting.fundingReference = contractNumber;
		searchContractNumber(contractNumber);
		search.click();
		Thread.sleep(1000);
		clickEditButton();
		getFPClientCode();

		Thread.sleep(1000);
		clickRequestInfoTab();
		Thread.sleep(1000);
		
		clickContractInfoTab();
		Thread.sleep(5000);
		
		BackOfficeAccounting.numberOfInstallment = numberOfInstallment.getText().replace("X", "").replace("x", "");
		if (btNew.getText().contains("X")) {
			if (Integer.parseInt(BackOfficeAccounting.numberOfInstallment)>4) {
				BackOfficeAccounting.businessTransactionType = "Long";
			}else {
				BackOfficeAccounting.businessTransactionType = "Short";
			}
			BackOfficeAccounting.businessTransactionCode = btNew.getText();
			if (merchantFix.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "").equals("0.00")) {
				BackOfficeAccounting.merchantCommissionFix = "0.0";
			}else {
				BackOfficeAccounting.merchantCommissionFix = merchantFix.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
			}			
			BackOfficeAccounting.merchantCommissionVariable = merchantPercentage.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");

			customerFeeF = customerFix.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
			customerFeeV = customerPercentage.getText().replace("%", "").replace(".", "").replace(",", ".").replace(" ", "");
		} else {
			
			if (Integer.parseInt(BackOfficeAccounting.numberOfInstallment)>4) {
				BackOfficeAccounting.businessTransactionType = "LongCredit";
			}else {
				BackOfficeAccounting.businessTransactionType = "ShortCredit";
			}
			BackOfficeAccounting.businessTransactionCode = bt.getText();
			if (merchantFeeFix.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "").equals("0.00")) {
				BackOfficeAccounting.merchantCommissionFix = "0.0";
			}else {
				BackOfficeAccounting.merchantCommissionFix = merchantFeeFix.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
			}
			BackOfficeAccounting.merchantCommissionVariable = merchantFeeVariable.getText().replace("%", "").replace(".", "").replace(",", ".").replace(" ", "");
			if (BackOfficeAccounting.merchantCommissionVariable.equals("0")) {
				BackOfficeAccounting.merchantCommissionVariable = "0.0";
			}else {
				BackOfficeAccounting.merchantCommissionVariable = String.valueOf(df.format(((Double.parseDouble(BackOfficeAccounting.merchantCommissionVariable) * Double.parseDouble(BackOfficeAccounting.paymentAmount))/100))).replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
				System.out.println(BackOfficeAccounting.merchantCommissionVariable);
			}
			customerFeeF = customerFeeFix.getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
			customerFeeV = customerFeeVariable.getText().replace("%", "").replace(".", "").replace(",", ".").replace(" ", "");
		}
	
		BackOfficeAccounting.merchantPamFix = "0.0";
		BackOfficeAccounting.merchantPamVariable = "0.0";
		BackOfficeAccounting.vatTaxFee = "0.0";
		
		if (tenant.equals("RO")) {
			BackOfficeAccounting.currencyCode = "RON";
		}else {
			BackOfficeAccounting.currencyCode = "EUR";
		}
		clickContractTransactionsTab();
		Thread.sleep(1000);
		BackOfficeAccounting.installmentAmount=amountList.get(0).getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
		BackOfficeAccounting.installmentNumber="1"; //sempre 1 aqui pois é da criação do contrato
		BackOfficeAccounting.capital= capitalAmountList.get(0).getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
		
		if ((!customerFeeF.equals("0.00")) & (customerFeeV.equals("0"))) {
			if (feesAmountList.get(0).getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "").equals("0.00")) {
				BackOfficeAccounting.interestFix= "0.0";
				BackOfficeAccounting.interestVariable= "0.0";
			} else {
				BackOfficeAccounting.interestFix= feesAmountList.get(0).getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
				BackOfficeAccounting.interestVariable= "0.0";
			}
		}else {
				if (feesAmountList.get(0).getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "").equals("0.00")) {
					BackOfficeAccounting.interestVariable= "0.0";
					BackOfficeAccounting.interestFix= "0.0";
				} else {
					BackOfficeAccounting.interestVariable= feesAmountList.get(0).getText().replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
					BackOfficeAccounting.interestFix= "0.0";
				}
			}
		
		
		BackOfficeAccounting.adminFee= "0.0";
		BackOfficeAccounting.insurance= "0.0";
		BackOfficeAccounting.interestAdd= "0.0";
		BackOfficeAccounting.adminFeeAdd= "0.0";
		BackOfficeAccounting.insuranceAdd= "0.0";
		BackOfficeAccounting.interestRemove= "0.0";
		BackOfficeAccounting.adminFeeRemove= "0.0";
		BackOfficeAccounting.insuranceRemove= "0.0";
		BackOfficeAccounting.stampTax= "0.0";
		BackOfficeAccounting.stampCreditTax= "0.0";
		BackOfficeAccounting.stampCreditTaxAdd= "0.0";
		BackOfficeAccounting.stampCreditTaxRemove= "0.0";
		BackOfficeAccounting.stampTaxAdd= "0.0";
		BackOfficeAccounting.stampTaxRemove= "0.0";
		BackOfficeAccounting.paymentMethod="CARD";
		BackOfficeAccounting.paymentProcessorName = "CHECKOUT";
		
		Thread.sleep(1000);
		
		clickBrandInfo();
		Thread.sleep(1000);
		clickMerchantsMenu();
		Thread.sleep(1000);
		searchMerchantByName(BackOfficeAccounting.merchantName);

		Thread.sleep(5000);
		accessMerchantDetails();
		
		Thread.sleep(100);
		merchantFinancialTab.click();
		Thread.sleep(100);
		BackOfficeAccounting.merchantFundingMode = merchantFinancingType.getText() ;

		Thread.sleep(100);
		merchantTechnicalTab.click();
		BackOfficeAccounting.merchantGUID = merchantGuid.getText();
		BackOfficeAccounting.merchantSUID = merchantSuid.getText();
		
		System.out.println("All variables");
		BackOfficeAccounting.printFields();
	}
	
	public void getDataEventTypePaid(String tenant, String contractNumber, String amount, String amountFees) throws InterruptedException {
		
		BackOfficeAccounting.clearFields();
		clickContractsMenu();
		BackOfficeAccounting.countryCode = tenant;
		BackOfficeAccounting.fundingReference = contractNumber;
		searchContractNumber(contractNumber);
		search.click();
		Thread.sleep(1000);
		clickEditButton();
		getFPClientCode();
		
		clickContractInfoTab();
		Thread.sleep(5000);
		
	
		if (btNew.getText().contains("X")) {
			
			BackOfficeAccounting.businessTransactionCode = btNew.getText();
		} else {
		
			BackOfficeAccounting.businessTransactionCode = bt.getText();
		}
		
		if (tenant.equals("RO")) {
			BackOfficeAccounting.currencyCode = "RON";
		}else {
			BackOfficeAccounting.currencyCode = "EUR";
		}
		BackOfficeAccounting.installmentAmount=amount.replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
		if (amountFees.replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "").equals("0.00")) {
			BackOfficeAccounting.interestFix= "0.0";
		} else {
			BackOfficeAccounting.interestFix= amountFees.replace("€", "").replace("Lei", "").replace(".", "").replace(",", ".").replace(" ", "");
		}
		BackOfficeAccounting.capital= String.valueOf(Double.parseDouble(BackOfficeAccounting.installmentAmount)-Double.parseDouble(BackOfficeAccounting.interestFix));
		
		
		BackOfficeAccounting.interestVariable= "0.0";
		BackOfficeAccounting.adminFee= "0.0";
		BackOfficeAccounting.insurance= "0.0";
		BackOfficeAccounting.interestAdd= "0.0";
		BackOfficeAccounting.adminFeeAdd= "0.0";
		BackOfficeAccounting.insuranceAdd= "0.0";
		BackOfficeAccounting.interestRemove= "0.0";
		BackOfficeAccounting.adminFeeRemove= "0.0";
		BackOfficeAccounting.insuranceRemove= "0.0";
		BackOfficeAccounting.stampTax= "0.0";
		BackOfficeAccounting.stampCreditTax= "0.0";
		BackOfficeAccounting.stampCreditTaxAdd= "0.0";
		BackOfficeAccounting.stampCreditTaxRemove= "0.0";
		BackOfficeAccounting.stampTaxAdd= "0.0";
		BackOfficeAccounting.stampTaxRemove= "0.0";
		BackOfficeAccounting.paymentMethod="CARD";
		BackOfficeAccounting.paymentProcessorName = "CHECKOUT";
		
		System.out.println("All variables");
		BackOfficeAccounting.printFields();

		
		
	}
	
	public void getDataEventTypeEarlySettlement(String tenant, String contractNumber, String amount) throws InterruptedException {
		
		BackOfficeAccounting.clearFields();
		clickContractsMenu();
		BackOfficeAccounting.countryCode = tenant;
		BackOfficeAccounting.fundingReference = contractNumber;
		searchContractNumber(contractNumber);
		search.click();
		Thread.sleep(1000);
		clickEditButton();
		getFPClientCode();
		
		clickContractInfoTab();
		Thread.sleep(5000);
		
		if (bt.getText().contains("X")) {

			BackOfficeAccounting.businessTransactionCode = btNew.getText();
			
		} else {
			

			BackOfficeAccounting.businessTransactionCode = bt.getText();
			
		}
	
		if (tenant.equals("RO")) {
			BackOfficeAccounting.currencyCode = "RON";
		}else {
			BackOfficeAccounting.currencyCode = "EUR";
		}
		clickContractTransactionsTab();
		Thread.sleep(1000);
		BackOfficeAccounting.installmentAmount=amount.replace(",", ".").replace("€", "").replace("Lei", "").replace(" ", "");
		int count = earlySettlementInstallmentNumber.size();
		if (count==1) {
			BackOfficeAccounting.installmentNumber=earlySettlementInstallmentNumber.get(0).getText();
		} else {
			for (int i=0;i<count;i++) {
				String val = earlySettlementInstallmentAmount.get(i).getText().replace(".", "").replace(",", ".").replace("€", "").replace("Lei", "").replace(" ", "");
				if(val.contains(amount.replace(",", ".").replace("€", "").replace("Lei", "").replace(" ", ""))){
					BackOfficeAccounting.installmentNumber=earlySettlementInstallmentNumber.get(i).getText();
					break;
				}
			}
		}
		BackOfficeAccounting.capital= amount.replace(",", ".").replace("€", "").replace("Lei", "").replace(" ", "");
		BackOfficeAccounting.interestFix=  "0.0";
		BackOfficeAccounting.interestVariable= "0.0";
		BackOfficeAccounting.adminFee= "0.0";
		BackOfficeAccounting.insurance= "0.0";
		BackOfficeAccounting.stampTax= "0.0";
		BackOfficeAccounting.stampCreditTax= "0.0";
		BackOfficeAccounting.stampCreditTaxAdd= "0.0";
		BackOfficeAccounting.stampCreditTaxRemove= "0.0";
		BackOfficeAccounting.stampTaxAdd= "0.0";
		BackOfficeAccounting.stampTaxRemove= "0.0";
		BackOfficeAccounting.paymentMethod="CARD";
		BackOfficeAccounting.paymentProcessorName = "CHECKOUT";

		
		System.out.println("All variables");
		BackOfficeAccounting.printFields();

		
		
	}
	
	public void getDataEventTypeCancel(String tenant, String contractNumber, String action, String amount) throws InterruptedException {
		
		BackOfficeAccounting.clearFields();
		clickContractsMenu();
		BackOfficeAccounting.countryCode = tenant;
		BackOfficeAccounting.fundingReference = contractNumber;
		searchContractNumber(contractNumber);
		search.click();
		Thread.sleep(1000);
		clickEditButton();
		getFPClientCode();
		

		if (tenant.equals("RO")) {
			BackOfficeAccounting.currencyCode = "RON";
		}else {
			BackOfficeAccounting.currencyCode = "EUR";
		}
		clickContractTransactionsTab();
		Thread.sleep(1000);
		BackOfficeAccounting.cancellationAmount=amount.replace(".", "").replace(",", ".").replace("€", "").replace("Lei", "").replace(" ", "");
	
		BackOfficeAccounting.interestAdd=  "0.0";
		BackOfficeAccounting.adminFeeAdd= "0.0";
		BackOfficeAccounting.insuranceAdd= "0.0";
//			BackOfficeAccounting.interestRemove= "0.0"; // qual a fórmula deste campo?? customer fee * valor cancelado
		BackOfficeAccounting.adminFeeRemove= "0.0";
		BackOfficeAccounting.insuranceRemove= "0.0";
		BackOfficeAccounting.stampCreditTaxAdd= "0.0";
		BackOfficeAccounting.stampCreditTaxRemove= "0.0";
		BackOfficeAccounting.stampTaxAdd= "0.0";
		BackOfficeAccounting.stampTaxRemove= "0.0";
		if (action.contains("Total")) {
//				BackOfficeAccounting.refundAmount = 
//				BackOfficeAccounting.refundCapital = 
			BackOfficeAccounting.refundAdminFee = "0.0";
//				BackOfficeAccounting.refundInterest =
			BackOfficeAccounting.refundInsurance = "0.0";
		}

		clickBrandInfo();
		Thread.sleep(1000);
		clickMerchantsMenu();
		Thread.sleep(1000);
		searchMerchantByName(BackOfficeAccounting.merchantName);

		Thread.sleep(5000);
		accessMerchantDetails();
		
		Thread.sleep(100);
		merchantFinancialTab.click();
		Thread.sleep(100);
		BackOfficeAccounting.merchantFundingMode = merchantFinancingType.getText() ;
		BackOfficeAccounting.commissionAdd = "0.0";
		BackOfficeAccounting.pamAdd = "0.0";
//			BackOfficeAccounting.commissionRemove// qual a fórmula deste campo?? amount cancelado * merchant fee
		BackOfficeAccounting.pamRemove = "0.0";
//			BackOfficeAccounting.cancellationFee // qual a fórmula deste campo?? amount cancelado * fee cancelamento
		BackOfficeAccounting.vatTaxFeeAdd  = "0.0";
		BackOfficeAccounting.vatTaxFeeRemove = "0.0";
		
		
		Thread.sleep(100);
		merchantTechnicalTab.click();
		BackOfficeAccounting.merchantSUID = merchantSuid.getText();
		
		System.out.println("All variables");
		BackOfficeAccounting.printFields();

		
		
	}
	
	public void getDataEventTypeSoft(String tenant, String contractNumber, String amount, String amountFees) throws InterruptedException {
			
			BackOfficeAccounting.clearFields();
			clickContractsMenu();
			BackOfficeAccounting.countryCode = tenant;
			BackOfficeAccounting.fundingReference = contractNumber;
			searchContractNumber(contractNumber);
			search.click();
			Thread.sleep(1000);
			clickEditButton();
			getFPClientCode();
			
			clickContractInfoTab();
			Thread.sleep(5000);
			
			
			if (bt.getText().contains("X")) {
				BackOfficeAccounting.businessTransactionCode = btNew.getText();
				
			} else {
				
				BackOfficeAccounting.businessTransactionCode = bt.getText();
			}
	
			if (tenant.equals("RO")) {
				BackOfficeAccounting.currencyCode = "RON";
			}else {
				BackOfficeAccounting.currencyCode = "EUR";
			}
			clickContractTransactionsTab();
			Thread.sleep(1000);
			BackOfficeAccounting.interestFix= "0.0";
			BackOfficeAccounting.interestVariable= "0.0";
			BackOfficeAccounting.adminFee= "0.0";
			BackOfficeAccounting.insurance= "0.0";
			BackOfficeAccounting.interestAdd= "0.0";
			BackOfficeAccounting.adminFeeAdd= "0.0";
			BackOfficeAccounting.insuranceAdd= "0.0";
			BackOfficeAccounting.interestRemove= "0.0";
			BackOfficeAccounting.adminFeeRemove= "0.0";
			BackOfficeAccounting.insuranceRemove= "0.0";
			BackOfficeAccounting.stampTax= "0.0";
			BackOfficeAccounting.stampCreditTax= "0.0";
			BackOfficeAccounting.stampCreditTaxAdd= "0.0";
			BackOfficeAccounting.stampCreditTaxRemove= "0.0";
			BackOfficeAccounting.stampTaxAdd= "0.0";
			BackOfficeAccounting.stampTaxRemove= "0.0";
			BackOfficeAccounting.paymentMethod="CARD";
			BackOfficeAccounting.paymentProcessorName = "CHECKOUT";
			
			Thread.sleep(1000);
			
			clickBrandInfo();
			Thread.sleep(1000);
			clickMerchantsMenu();
			Thread.sleep(1000);
			searchMerchantByName(BackOfficeAccounting.merchantName);

			Thread.sleep(5000);
			accessMerchantDetails();
			
			Thread.sleep(100);
			merchantFinancialTab.click();
			Thread.sleep(100);
			BackOfficeAccounting.merchantFundingMode = merchantFinancingType.getText() ;

			Thread.sleep(100);
			merchantTechnicalTab.click();
			BackOfficeAccounting.merchantGUID = merchantGuid.getText();
			BackOfficeAccounting.merchantSUID = merchantSuid.getText();
			
			System.out.println("All variables");
			BackOfficeAccounting.printFields();

			
			
		}
		
		
}
