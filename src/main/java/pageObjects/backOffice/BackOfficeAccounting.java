package pageObjects.backOffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class BackOfficeAccounting {
	
    public static String countryCode;
    public static String fundingReference;
    public static String merchantGUID;
    public static String merchantName;
    public static String merchantSUID;
    public static String customerGUID;
    public static String customerSUID;
    public static String businessTransactionCode;
    public static String businessTransactionType;
    public static String numberOfInstallment;
    public static String channel;
    public static String paymentAmount;
    public static String currencyCode;
    public static String financingId;
    public static String invoiceId;
    public static String installmentAmount;
    public static String installmentNumber;
    public static String capital;
    public static String interestFix;
    public static String interestVariable;
    public static String adminFee;
    public static String insurance;
    public static String interestAdd;
    public static String adminFeeAdd;
    public static String insuranceAdd;
    public static String interestRemove;
    public static String adminFeeRemove;
    public static String insuranceRemove;
    public static String stampTax;
    public static String stampCreditTax;
    public static String stampCreditTaxAdd;
    public static String stampCreditTaxRemove;
    public static String stampTaxAdd;
    public static String stampTaxRemove;
    public static String settlementAmountPaymentDefault;
    public static String paymentDefaultFee;
    public static String cancellationAmount;
    public static String paymentMethod;
    public static String paymentProcessorName;
    public static String paymentProcessorTransferId;
    public static String merchantFundingMode;
    public static String merchantCommissionFix;
    public static String merchantCommissionVariable;
    public static String merchantPamFix;
    public static String merchantPamVariable;
    public static String vatTaxFee;
    public static String commissionAdd;
    public static String pamAdd;
    public static String commissionRemove;
    public static String pamRemove;
    public static String cancellationFee;
    public static String vatTaxFeeAdd;
    public static String vatTaxFeeRemove;
    public static String refundAmount;
    public static String refundCapital;
    public static String refundAdminFee;
    public static String refundInterest;
    public static String refundInsurance;
    public static String period_start;
    public static String period_end;
    public static String payouts_currency;
    public static String payout_carried_forward_amount;
    public static String current_period_amount;
    public static String net_amount;
    public static String status;
    public static String payout_fee;
    public static String processed_amount;
    public static String refund_amount;
    public static String chargeback_amount;
    public static String payouts_to_card_amount;
    public static String processing_fees;
    public static String interchange_fees;
    public static String scheme_and_other_network_fees;
    public static String premium_and_apm_fees;
    public static String chargeback_fees;
    public static String payout_to_card_fees;
    public static String payment_gateway_fees;
    public static String rolling_reserve_amount;
    public static String tax;
    public static String admin_fees;
    public static String general_adjustments;
    
    public void saveDataEventTypeModel(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook1;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook1 = new XSSFWorkbook(is);
			Sheet sheet1 = workbook1.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	System.out.println("hi");
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			            System.out.println("contractNumber:" + celContractNumber );
			            Thread.sleep(1000);
			            if (celContractNumber.contains(contractNumber)) {
			            	break;
			            }
	         	 } 
	
	         }
	                
	         	
			    row.createCell(8).setCellValue(merchantGUID);
			    row.createCell(9).setCellValue(merchantName);	
			    row.createCell(10).setCellValue(merchantSUID);
			    row.createCell(11).setCellValue(customerGUID);
			    row.createCell(12).setCellValue(customerSUID);
			    row.createCell(13).setCellValue(businessTransactionCode);	
			    row.createCell(14).setCellValue(businessTransactionType);
			    row.createCell(15).setCellValue(numberOfInstallment);
			    row.createCell(16).setCellValue(channel);
			    row.createCell(17).setCellValue(paymentAmount);
			    row.createCell(18).setCellValue(currencyCode);
			    row.createCell(19).setCellValue(financingId);
			    row.createCell(20).setCellValue(invoiceId);
			    row.createCell(21).setCellValue(installmentAmount);
			    row.createCell(22).setCellValue(installmentNumber);
			    row.createCell(23).setCellValue(capital);
			    row.createCell(24).setCellValue(interestFix);
			    row.createCell(25).setCellValue(interestVariable);
			    row.createCell(26).setCellValue(adminFee);
			    row.createCell(27).setCellValue(insurance);
			    row.createCell(28).setCellValue(interestAdd);
			    row.createCell(29).setCellValue(adminFeeAdd);
			    row.createCell(30).setCellValue(insuranceAdd);
			    row.createCell(31).setCellValue(interestRemove);
			    row.createCell(32).setCellValue(adminFeeRemove);
			    row.createCell(33).setCellValue(insuranceRemove);
			    row.createCell(34).setCellValue(stampTax);
			    row.createCell(35).setCellValue(stampCreditTax);
			    row.createCell(36).setCellValue(stampCreditTaxAdd);
			    row.createCell(37).setCellValue(stampCreditTaxRemove);
			    row.createCell(38).setCellValue(stampTaxAdd);
			    row.createCell(39).setCellValue(stampTaxRemove);
			    row.createCell(40).setCellValue(settlementAmountPaymentDefault);
			    row.createCell(41).setCellValue(paymentDefaultFee);
			    row.createCell(42).setCellValue(cancellationAmount);
			    row.createCell(43).setCellValue(paymentMethod);
			    row.createCell(44).setCellValue(paymentProcessorName);
			    row.createCell(45).setCellValue(paymentProcessorTransferId);
			    row.createCell(46).setCellValue(merchantFundingMode);
			    row.createCell(47).setCellValue(merchantCommissionFix);
			    row.createCell(48).setCellValue(merchantCommissionVariable);
			    row.createCell(49).setCellValue(merchantPamFix);
			    row.createCell(50).setCellValue(merchantPamVariable);
			    row.createCell(51).setCellValue(vatTaxFee);
			    row.createCell(52).setCellValue(pamAdd);
			    row.createCell(53).setCellValue(commissionRemove);
			    row.createCell(54).setCellValue(pamRemove);
			    row.createCell(55).setCellValue(cancellationFee);
			    row.createCell(56).setCellValue(vatTaxFeeRemove);
			    row.createCell(57).setCellValue(refundAmount);
			    row.createCell(58).setCellValue(refundCapital);
			    row.createCell(59).setCellValue(refundAdminFee);
			    row.createCell(60).setCellValue(refundInterest);
			    row.createCell(61).setCellValue(refundInsurance);	
				
	         
	     }
		    
		    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	     workbook1.write(fileOut);
	     fileOut.close();
	     workbook1.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
   }
    
    
    public void saveDataEventTypeContractCreated(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 01, 14, 03, 19, 20
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			            	 row.createCell(8).setCellValue(merchantGUID);
			 			    row.createCell(9).setCellValue(merchantName);	
			 			    row.createCell(10).setCellValue(merchantSUID);
			 			    row.createCell(11).setCellValue(customerGUID);
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(13).setCellValue(businessTransactionCode);	
			 			    row.createCell(14).setCellValue(businessTransactionType);
			 			    row.createCell(15).setCellValue(numberOfInstallment);
			 			    row.createCell(16).setCellValue(channel);
			 			    row.createCell(17).setCellValue(paymentAmount);
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(21).setCellValue(installmentAmount);
			 			    row.createCell(22).setCellValue(installmentNumber);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
			 			    row.createCell(28).setCellValue(interestAdd);
							row.createCell(29).setCellValue(adminFeeAdd);
							row.createCell(30).setCellValue(insuranceAdd);
			 				row.createCell(31).setCellValue(interestRemove);
							row.createCell(32).setCellValue(adminFeeRemove);
							row.createCell(33).setCellValue(insuranceRemove);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(35).setCellValue(stampCreditTax);
							row.createCell(36).setCellValue(stampCreditTaxAdd);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);
							row.createCell(43).setCellValue(paymentMethod);
							row.createCell(44).setCellValue(paymentProcessorName);
							row.createCell(46).setCellValue(merchantFundingMode);
							row.createCell(47).setCellValue(merchantCommissionFix);
							row.createCell(48).setCellValue(merchantCommissionVariable);
							row.createCell(49).setCellValue(merchantPamFix);
							row.createCell(50).setCellValue(merchantPamVariable);
							row.createCell(51).setCellValue(vatTaxFee);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 
	
	         }
	                
	           
				
	     }
		    
		    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public static void clearFields() {
    
    	countryCode = null;
    	fundingReference = null;
    	merchantGUID = null;
    	merchantName = null;
    	merchantSUID = null;
    	customerGUID = null;
    	customerSUID = null;
    	businessTransactionCode = null;
    	businessTransactionType = null;
        numberOfInstallment = null;
        channel = null;
        paymentAmount = null;
        currencyCode = null;
        financingId = null;
        invoiceId = null;
        installmentAmount = null;
        installmentNumber = null;
        capital = null;
        interestFix = null;
        interestVariable = null;
        adminFee = null;
        insurance = null;
        interestAdd = null;
        adminFeeAdd = null;
        insuranceAdd = null;
        interestRemove = null;
        adminFeeRemove = null;
        insuranceRemove = null;
        stampTax = null;
        stampCreditTax = null;
        stampCreditTaxAdd = null;
        stampCreditTaxRemove = null;
        stampTaxAdd = null;
        stampTaxRemove = null;
        settlementAmountPaymentDefault = null;
        paymentDefaultFee = null;
        cancellationAmount = null;
        paymentMethod = null;
        paymentProcessorName = null;
        paymentProcessorTransferId = null;
        merchantFundingMode = null;
        merchantCommissionFix = null;
        merchantCommissionVariable = null;
        merchantPamFix = null;
        merchantPamVariable = null;
        vatTaxFee = null;
        commissionAdd = null;
        pamAdd = null;
        commissionRemove = null;
        pamRemove = null;
        cancellationFee = null;
        vatTaxFeeAdd = null;
        vatTaxFeeRemove = null;
        refundAmount = null;
        refundCapital = null;
        refundAdminFee = null;
        refundInterest = null;
        refundInsurance = null;
        period_start = null;
        period_end = null;
        payouts_currency = null;
        payout_carried_forward_amount = null;
        current_period_amount = null;
        net_amount = null;
        status = null;
        payout_fee = null;
        processed_amount = null;
        refund_amount = null;
        chargeback_amount = null;
        payouts_to_card_amount = null;
        processing_fees = null;
        interchange_fees = null;
        scheme_and_other_network_fees = null;
        premium_and_apm_fees = null;
        chargeback_fees = null;
        payout_to_card_fees = null;
        payment_gateway_fees = null;
        rolling_reserve_amount = null;
        tax = null;
        admin_fees = null;
        general_adjustments = null;							
    	
    }
    
    public static void printFields() {

    	
    	System.out.println(countryCode);
    	System.out.println(fundingReference);
    	System.out.println(merchantGUID);
    	System.out.println(merchantName);
    	System.out.println(merchantSUID);
    	System.out.println(customerGUID);
    	System.out.println(customerSUID);
    	System.out.println(businessTransactionCode);
    	System.out.println(businessTransactionType);
    	System.out.println(numberOfInstallment);
    	System.out.println(channel);
    	System.out.println(paymentAmount);
    	System.out.println(currencyCode);
    	System.out.println(financingId);
    	System.out.println(invoiceId);
        System.out.println(installmentAmount);
        System.out.println(installmentNumber);
        System.out.println(capital);
        System.out.println(interestFix);
        System.out.println(interestVariable);
        System.out.println(adminFee);
        System.out.println(insurance);
        System.out.println(interestAdd);
        System.out.println(adminFeeAdd);
        System.out.println(insuranceAdd);
        System.out.println(interestRemove);
        System.out.println(adminFeeRemove);
        System.out.println(insuranceRemove);
        System.out.println(stampTax);
        System.out.println(stampCreditTax);
        System.out.println(stampCreditTaxAdd);
        System.out.println(stampCreditTaxRemove);
        System.out.println(stampTaxAdd);
        System.out.println(stampTaxRemove);
        System.out.println(settlementAmountPaymentDefault);
        System.out.println(paymentDefaultFee);
        System.out.println(cancellationAmount);
        System.out.println(paymentMethod);
        System.out.println(paymentProcessorName);
        System.out.println(paymentProcessorTransferId);
        System.out.println(merchantFundingMode);
        System.out.println(merchantCommissionFix);
        System.out.println(merchantCommissionVariable);
        System.out.println(merchantPamFix);
        System.out.println(merchantPamVariable);
        System.out.println(vatTaxFee);
        System.out.println(commissionAdd);
        System.out.println(pamAdd);
        System.out.println(commissionRemove);
        System.out.println(pamRemove);
        System.out.println(cancellationFee);
        System.out.println(vatTaxFeeAdd);
        System.out.println(vatTaxFeeRemove);
        System.out.println(refundAmount);
        System.out.println(refundCapital);
        System.out.println(refundAdminFee);
        System.out.println(refundInterest);
        System.out.println(refundInsurance);
        System.out.println(period_start);
        System.out.println(period_end);
        System.out.println(payouts_currency);
        System.out.println(payout_carried_forward_amount);
        System.out.println(current_period_amount);
        System.out.println(net_amount);
        System.out.println(status);
        System.out.println(payout_fee);
        System.out.println(processed_amount);
        System.out.println(refund_amount);
        System.out.println(chargeback_amount);
        System.out.println(payouts_to_card_amount);
        System.out.println(processing_fees);
        System.out.println(interchange_fees);
        System.out.println(scheme_and_other_network_fees);
        System.out.println(premium_and_apm_fees);
        System.out.println(chargeback_fees);
        System.out.println(payout_to_card_fees);
        System.out.println(payment_gateway_fees);
        System.out.println(rolling_reserve_amount);
        System.out.println(tax);
        System.out.println(admin_fees);
        System.out.println(general_adjustments);	
    }
    
    public void saveDataEventTypeEarlySettlement(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 05 and 07
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			            	
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(13).setCellValue(businessTransactionCode);
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(21).setCellValue(installmentAmount);
			 			    row.createCell(22).setCellValue(installmentNumber);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(35).setCellValue(stampCreditTax);
							row.createCell(36).setCellValue(stampCreditTaxAdd);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);
							row.createCell(43).setCellValue(paymentMethod);
							row.createCell(44).setCellValue(paymentProcessorName);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 
	         }			
	     }    
		    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");	        
	                    
	  }
    }
    
    public void saveDataEventTypeCancel(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 06, 10, 18, 21, 22, 23, 24
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			            	row.createCell(10).setCellValue(merchantSUID);
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(42).setCellValue(cancellationAmount);
			 			    row.createCell(28).setCellValue(interestAdd);
			 			    row.createCell(29).setCellValue(adminFeeAdd);
			 			    row.createCell(30).setCellValue(insuranceAdd);
			 			    row.createCell(31).setCellValue(interestRemove);
			 			    row.createCell(32).setCellValue(adminFeeRemove);
			 			    row.createCell(33).setCellValue(insuranceRemove);
							row.createCell(36).setCellValue(stampCreditTaxAdd);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);
							row.createCell(43).setCellValue(paymentMethod);
							row.createCell(44).setCellValue(paymentProcessorName);
							row.createCell(46).setCellValue(merchantFundingMode);
							row.createCell(52).setCellValue(commissionAdd);
							row.createCell(53).setCellValue(pamAdd);
							row.createCell(54).setCellValue(commissionRemove);
							row.createCell(56).setCellValue(cancellationFee);
							row.createCell(57).setCellValue(vatTaxFeeAdd);
							row.createCell(58).setCellValue(vatTaxFeeRemove);
							row.createCell(59).setCellValue(refundAmount);
							row.createCell(60).setCellValue(refundCapital);
							row.createCell(61).setCellValue(refundAdminFee);
							row.createCell(62).setCellValue(refundInterest);
							row.createCell(63).setCellValue(refundInsurance);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 
	        }			
	     }  
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public void saveDataEventTypeSoft(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 03, 04 and 15
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			            	row.createCell(10).setCellValue(merchantSUID);
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(13).setCellValue(businessTransactionCode);
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(21).setCellValue(installmentAmount);
			 			    row.createCell(22).setCellValue(installmentNumber);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
			 			    row.createCell(28).setCellValue(interestAdd);
							row.createCell(29).setCellValue(adminFeeAdd);
							row.createCell(30).setCellValue(insuranceAdd);
			 				row.createCell(31).setCellValue(interestRemove);
							row.createCell(32).setCellValue(adminFeeRemove);
							row.createCell(33).setCellValue(insuranceRemove);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(35).setCellValue(stampCreditTax);
							row.createCell(36).setCellValue(stampCreditTaxAdd);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);
							row.createCell(41).setCellValue(paymentDefaultFee);
							row.createCell(43).setCellValue(paymentMethod);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 	
	         }       				
	     }	    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public void saveDataEventTypeSoftPaid(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 25 and 26
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
			 			    row.createCell(28).setCellValue(interestAdd);
							row.createCell(29).setCellValue(adminFeeAdd);
							row.createCell(30).setCellValue(insuranceAdd);
			 				row.createCell(31).setCellValue(interestRemove);
							row.createCell(32).setCellValue(adminFeeRemove);
							row.createCell(33).setCellValue(insuranceRemove);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(40).setCellValue(settlementAmountPaymentDefault);
							row.createCell(41).setCellValue(paymentDefaultFee);
							row.createCell(43).setCellValue(paymentMethod);
						    row.createCell(44).setCellValue(paymentProcessorName);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 	
	         }       				
	     }	    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public void saveDataEventTypePaid(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 03 and 20
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(13).setCellValue(businessTransactionCode);;
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(21).setCellValue(installmentAmount);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
			 			    row.createCell(28).setCellValue(interestAdd);
							row.createCell(29).setCellValue(adminFeeAdd);
							row.createCell(30).setCellValue(insuranceAdd);
			 				row.createCell(31).setCellValue(interestRemove);
							row.createCell(32).setCellValue(adminFeeRemove);
							row.createCell(33).setCellValue(insuranceRemove);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(35).setCellValue(stampCreditTax);
							row.createCell(36).setCellValue(stampCreditTaxAdd);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);
							row.createCell(43).setCellValue(paymentMethod);
						    row.createCell(44).setCellValue(paymentProcessorName);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 	
	         }       				
	     }	    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public void saveDataEventTypeMFS(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 02 and 13
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			 			    row.createCell(10).setCellValue(merchantSUID);
			 			    row.createCell(17).setCellValue(paymentAmount);
			 			    row.createCell(18).setCellValue(currencyCode);
							row.createCell(46).setCellValue(merchantFundingMode);
							row.createCell(47).setCellValue(merchantCommissionFix);
							row.createCell(48).setCellValue(merchantCommissionVariable);
							row.createCell(49).setCellValue(merchantPamFix);
							row.createCell(50).setCellValue(merchantPamVariable);
							row.createCell(51).setCellValue(vatTaxFee);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 	
	         }       				
	     }	    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public void saveDataEventTypeHard(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 08 and 09
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(13).setCellValue(businessTransactionCode);;
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(21).setCellValue(installmentAmount);
			 			    row.createCell(22).setCellValue(installmentNumber);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(35).setCellValue(stampCreditTax);
							row.createCell(36).setCellValue(stampCreditTaxAdd);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);							
							row.createCell(41).setCellValue(paymentDefaultFee);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 	
	         }       				
	     }	    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
    
    public void saveDataEventTypeLitigation(String action, String fileName, String contractNumber, String tenant) throws IOException, InterruptedException {
		//events 16
		String filenamePath = fileName;
				
		File file = new File(filenamePath);
		XSSFWorkbook workbook;
		
		
		if (file.exists()){ 
			System.out.println("entrou");
			InputStream is = new FileInputStream(file);
			
			workbook = new XSSFWorkbook(is);
			Sheet sheet1 = workbook.getSheet(action);
			
		    Iterator<Row> rowIterator = sheet1.rowIterator();
		    int count = -1;
	     while (rowIterator.hasNext()) {
	     	count++;
	     	Row row = rowIterator.next();
	         if (count==0) {
	         	continue;
	         	
	         }
	         Iterator<Cell> cellIterator = row.cellIterator();
	         String celContractNumber = "";
	         while (cellIterator.hasNext()) {
	
	         	Cell cell = cellIterator.next();
	         	if(cell.getColumnIndex()==2) {
	         		celContractNumber = cell.getStringCellValue();	
			        
			            if (celContractNumber.contains(contractNumber)) {
			            	
			 			    row.createCell(12).setCellValue(customerSUID);
			 			    row.createCell(18).setCellValue(currencyCode);
			 			    row.createCell(23).setCellValue(capital);
			 			    row.createCell(24).setCellValue(interestFix);
			 			    row.createCell(25).setCellValue(interestVariable);
			 			    row.createCell(26).setCellValue(adminFee);
			 			    row.createCell(27).setCellValue(insurance);
			 			    row.createCell(28).setCellValue(interestAdd);
							row.createCell(29).setCellValue(adminFeeAdd);
							row.createCell(30).setCellValue(insuranceAdd);
			 				row.createCell(31).setCellValue(interestRemove);
							row.createCell(32).setCellValue(adminFeeRemove);
							row.createCell(33).setCellValue(insuranceRemove);
							row.createCell(34).setCellValue(stampTax);
							row.createCell(35).setCellValue(stampCreditTax);
							row.createCell(37).setCellValue(stampCreditTaxRemove);
							row.createCell(38).setCellValue(stampTaxAdd);
							row.createCell(39).setCellValue(stampTaxRemove);
			 			    row.createCell(64).setCellValue(tenant);
			 			    break;
			            }
	         	 } 	
	         }       				
	     }	    
	
		for (int i = 0; i < 10; i++) {
	
	     	sheet1.autoSizeColumn(i);
	     } 
	     
		FileOutputStream fileOut = new FileOutputStream(filenamePath);
	    workbook.write(fileOut);
	    fileOut.close();
	    workbook.close();	    
			
			System.out.println("Your xls file has been updated!");
	         
	                    
	  }
    }
}
