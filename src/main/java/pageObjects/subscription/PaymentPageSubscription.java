package pageObjects.subscription;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import utils.Utils;

public class PaymentPageSubscription {

	public WebDriverWait wait;
	public WebDriverWait longWait;
	public WebDriver driver;
	private Actions action;
	private static Utils utils;
	public RemoteWebDriver remoteDriver;
	public static String CONTRACT_REF = "";
	public static String RANDOM_EMAIL = "";
	public static String customerMobilePhoneNumber;

	public PaymentPageSubscription(WebDriver driver) {

		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 60);
		longWait = new WebDriverWait(driver, 300);
		action = new Actions(driver);
		utils = PageFactory.initElements(driver, Utils.class);
		this.driver = driver;
	}
	
	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	
	
	/*
	 * The field Nationality (list) to IT, RO and ES.
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'NationalityInput')]")
	private WebElement nationality;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'NationalityInput')]/a/span")
	private WebElement nationalityFilled;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'NationalityInput')]/option[@selected='selected']")
	private WebElement nationalityFilledIT;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'NationalityInput')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement nationalityList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'NationalityInput')]//div//ul[contains(@class,'chosen-results')]//li[1]//em")
	private WebElement nationalityResult;

	/*
	 * The field Nationality to BE.
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'NacionalityInput')]")
	private WebElement nationalityInput;

	/*
	 * The field Birth Place to BE and ES. The field Birth Place (list) to IT.
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceComboBox')]")
	private WebElement placeOfBirth;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceComboBox')]/a/span")
	private WebElement placeOfBirthFilled;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'BirthPlaceInput')]")
	private WebElement placeOfBirthInput;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceComboBox')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement placeOfBirthList;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Clicca per accedere all’elenco']")
	private WebElement placeOfBirthListIT;
	
	@FindBy(how = How.XPATH, using = "//ul[contains(@class,'autocomplete')]//ui-autocomplete-item")
	private WebElement placeOfBirthOptionIT;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceComboBox')]//div//ul[contains(@class,'chosen-results')]//li[1]//em")
	private WebElement placeOfBirthResult;

	/*
	 * Province of IT and ES.
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceCombo')]")
	private WebElement provinceIt;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceCombo')]/a/span")
	private WebElement provinceItFilled;

	
	@FindBy(how = How.XPATH, using = "//*[contains(@id,'ProvinceCombo')]/option[@selected='selected']")
	private WebElement provinceItFilledSelect;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceCombo')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement provinceItList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceCombo')]//ul[contains(@class,'chosen-results')]//li[contains(@class,'active-result')]//em[1]")
	private WebElement provinceItResult;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceText')]")
	private WebElement provinceEs;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceText')]/a/span")
	private WebElement provinceEsFilled;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceText')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement provinceEsList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ProvinceText')]//ul[contains(@class,'chosen-results')]//li[contains(@class,'active-result')]//em[1]")
	private WebElement provinceEsResult;

	/*
	 * The 1º checkbox (TermsAndConditions) is valid to New Customer and Existing
	 * Customer for IT, BE and RO.
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'TermsAndConditions')]")
	private WebElement checkBoxTermsAndConditions;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'TermsAndConditions')]//input")
	private WebElement checkBoxTermsAndConditionsES;

	/*
	 * The 2º checkbox (PrivacyPolice) is valid to NC and EC for RO
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'PrivacyPolicy')]")
	private WebElement checkBoxPrivacyPolicy;

	/*
	 * The 2º checkbox (NewDisclaimer) is valid to NC and EC for IT and is valid to
	 * NC for BE. There is no checkbox EC for BE.
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'NewDisclaimer')]")
	private WebElement checkBoxNcNewDisclaimer;

	/*
	 * The 3º checkbox (CommercialOffersOney) is valid to NC for RO, IT, ES and BE
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CommercialOffersOney')]")
	private WebElement checkBoxNcCommercialOffersOney;

	/*
	 * The 3º checkbox (CommercialOffersOney) is valid to EC for RO, IT and BE
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CommOney')]")
	private WebElement checkBoxEcCommercialOffersOney;
	
	/*
	 * It's the click here to display all consents in ES
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Download')]/div[2]/a[1]")
	private WebElement consentClickHere;
	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'CommercialOffersOney')])[1]")
	private WebElement checkBoxEcCommercialOffersOneySiminarES;
	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'CommercialOffersOney')])[2]")
	private WebElement checkBoxEcCommercialOffersOneyNoSiminarES;
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CommercialOffersPartners')]")
	private WebElement checkBoxEcCommercialOffersPartnersES;

	/*
	 * The 4º checkbox (CommercialOffersPartners) is valid to NC for RO, IT and
	 * BE
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CommercialOffersPartn')]")
	private WebElement checkBoxNcCommercialOffersPartners;

	/*
	 * The 4º checkbox (CommercialOffersPartners) is valid to EC for RO, IT, ES and
	 * BE
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CommPartn')]")
	private WebElement checkBoxEcCommercialOffersPartners;

	/*
	 * The submit button is valid to NC and EC for RO, IT, ES and BE.
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@class,'Button graybtn')]")
	private WebElement submit;

	/*
	 * Fields from PT.
	 */

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[1]")
	private WebElement checkBoxPT1;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[2]")
	private WebElement checkBoxPT2;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[3]")
	private WebElement checkBoxPT3;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[4]")
	private WebElement checkBoxPT4;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[5]")
	private WebElement checkBoxPT5;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[7]")
	private WebElement checkBoxPT7;

	@FindBy(how = How.XPATH, using = "(//input[@type='radio'])[1]")
	private WebElement cardPT;
	
	@FindBy(how = How.XPATH, using = "//div[@class='TermsAndConditionsCheckboxContainer OSInline']//input")
	private WebElement checkboxDataConfirmation;

	@FindBy(how = How.XPATH, using = "//div[@class='MainButtonPT']//label[contains(text(),'Submeter pedido')]")
	private WebElement submitPT;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'Declare_DataInformation')]//descendant::input[1]")
	private WebElement checkBoxDataInformation;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Product_Service_Offer')]")
	private WebElement checkBoxOneyBank;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'MarketingOffersOney')]")
	private WebElement checkBoxMarketingOffersOney;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'PaymentMethodTitle')]")
	private WebElement paymentMethodLogos;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'AuthorizeConsultBD')]//descendant::input[1]")
	private WebElement checkBoxConsultBD;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'ParticularCondition')]//descendant::input[1]")
	private WebElement checkBoxParticularCondition;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'ReceiveInvoiceEmail')]//descendant::input[1]")
	private WebElement checkBoxReceiveInvoiceEmail;

	@FindBy(how = How.XPATH, using = "//div[@class='MainButtonPT']//div//label[text()='Avançar']")
	private WebElement avancar;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Nº de Documento de Identificação')]//following::input[1]")
	private WebElement nrDocumento;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'NIF')]//following::input[1]")
	private WebElement NIF;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'Upload') and (@type='file')]")
	private WebElement upload;

	/*
	 * Fields from ES, IT and RO.
	 */
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Número DNI/NIE')]//following::input[1]")
	private WebElement IdCardSpain;

	/*
	 * Subscription Fields
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id , 'CustomerData_Adress')]")
	private WebElement changeAddress;

	@FindBy(how = How.XPATH, using = "//input[contains(@id , 'CustomerData_Zipcode')]")
	private WebElement postalCode;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CustomerData_Email')]")
	private WebElement emailAddress;

	@FindBy(how = How.XPATH, using = "/html")
	private WebElement form;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'CNP')]//following::input[1]")
	private WebElement CNP;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Codice fiscale')]//following::input[1]")
	private WebElement personalNumber;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'CreditCardPaymentMethod')]")
	private WebElement VISA;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'fBirth_chosen')]/a[1]/span[1]")
	private WebElement countryOfBirth;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'CountryOfBirth')]/a/span")
	private WebElement countryOfBirthFilled;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'CountryOfBirth')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement countryOfBirthList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'CountryOfBirth_chosen')]//ul[contains(@class,'chosen-results')]//li[1]//em")
	private WebElement countryOfBirthResult;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-municipality-without-account-fpi')]")
	private WebElement municipality;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'ThemeGrid_MarginGutter')]")
	private WebElement ref2;

	@FindBy(how = How.XPATH, using = "(//span[@class='Heading2']//following::span[1])[1]")
	private WebElement ref;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Encomenda')]//following::div[1]")
	private WebElement refPT;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'WithAccount_rb2')]")
	private WebElement iHaveAccountPT;

	/*
	 * Fields to login on subscription
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Password_Input')]")
	private WebElement password;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'btn_Login')]")
	private WebElement login;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'Username_Input')])[2]")
	private WebElement emailPT;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'CustomerLoginDesktop') and contains(@id,'RightSide_wtbuttons_container')]//div[@class='PTNormalButton']//span[@class='PTButtonLabel'][contains(text(),'Iniciar sessão')]")
	private WebElement loginPT;

	@FindBy(how = How.XPATH, using = "//div[@class='RightSide_PT']/div[contains(@id,'CustomerLoginDesktop_wt32_wtRightSide')]")
	private WebElement clickPT;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'Password_Input')])[2]")
	private WebElement passwordPT;

	/*
	 * 3ds authentication
	 */
	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	private WebElement secureAuthentication;

	@FindBy(how = How.XPATH, using = "//input[@id='txtButton']")
	private WebElement secureAuthenticationContinue;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'Alert Warning')]")
	private WebElement validationRejectedContract;

	@FindBy(how = How.XPATH, using = "//div[@class='ContainerCellphoneSorry']")
	private WebElement validationRejectedContractPT;

	@FindBy(how = How.XPATH, using = "//div[@class='Feedback_Message_Error']/span[ contains(text(),'Infelizmente não foi possível realizar o pagamento com o cartão.')]")
	private WebElement validationRejectedCardPT;

//	@FindBy(how = How.XPATH, using = "//div[@class='BoxContNew']")
//	private WebElement validationCreatedContract;

//	@FindBy(how = How.XPATH, using = "//div[@class='LandingPageSuccess']")
//	private WebElement validationCreatedContractPT;

	@FindBy(how = How.XPATH, using = "(//div[@class='Title'])[2]")
	private WebElement validationCreatedContractDE;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'PW5')]")
	private WebElement newAccountPassword;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'PW6')]")
	private WebElement newAccountPassword1;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Input_PW_Active')]")
	private WebElement newAccountPasswordPT;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'InputConfirmPW_Active')]")
	private WebElement newAccountPassword1PT;

	@FindBy(how = How.XPATH, using = "//label[contains(@class,'ButtonNew OSFillParent')]")
	private WebElement activate;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Quero ativar')]")
	private WebElement activatePT;

	/*
	 * PR38 fields
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'CustomerData_Phonenumber')]")
	private WebElement mobilePhone;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CustomerData_Phonenumber')]//following::span[@class='ValidationMessage' and contains(@id,'CustomerData_Phonenumber')]")
	private WebElement mobileMessageError;

	@FindBy(how = How.XPATH, using = "//span//div[@class='ProgressBar_Container OSInline']")
	private WebElement loading;

	/*
	 * Existing Customer card
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Card_Cont') and not (contains(@id,'Card_Container'))]//span[contains(@id,'CardPaymentList')]/span[1]")
	private WebElement creditCardList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'CardInfo_Cont')]//a[contains(@href,'Structure')]")
	private WebElement newCard;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'Alert Error')] //div[contains(@id,'Checkout')]")
	private WebElement unauthorizedCard; //div[contains(text(),'Your card is not authorized')]

	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'CardInfo')]//following::span[1])[1]")
	private WebElement addNewPaymentCard;// html[1]/body[1]/form[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/a[1]/span[2]

	/*
	 * PR39 - Web Contract Form fields. 
	 * PR63 - Additional fields.
	 */
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'CustomerData_Occupation')]")
	private WebElement occupation;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Company_Input')]")
	private WebElement employer;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CustomerData_Employer')]")
	private WebElement employerPT;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'MonthlyIncome_Input')]")
	private WebElement monthlyIncome;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'MonthlyExpenses_Input')]")
	private WebElement monthlyExpenses;
	
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'CustomerData_MainSourceofIncome')]")
	private WebElement mainSourceIncome;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Validar']")
	private WebElement validateButtonSubscription;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'CardData_Container')]//span[@class='redText']")
	private WebElement cardAutenticationFailedWarning;
	
	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'TAE')]//span)[1]")
	private WebElement tae;

	@FindBy(how = How.XPATH, using = "(//div[contains(@id,'TAEG')]//div)[4]")
	private WebElement taegPortugal;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Authentication failed, please try again.')]")
	private WebElement secondTryPaymentMessage;
	
	@FindBy(how = How.XPATH, using = "//a[@class='cookies-oney__close']//span[1]")
	private WebElement closingCookiesMessage;
	
	@FindBy(how = How.XPATH, using = "//a[@class='Text_Note cancel-link']//span[1]")
	private WebElement cancelAndComeBackToTheMerchantWebsite;
	
	@FindBy(how = How.XPATH, using = "/html/body/form/div[3]/div[1]/div[1]/div")
	private WebElement popinBeforeYouGo;
	
	/**
	 * Accepting Terms and Conditions to NC for PT.
	 */
	public void clickAllCheckBoxes() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", checkBoxMarketingOffersOney);
		Thread.sleep(1500);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxMarketingOffersOney)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxOneyBank)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxDataInformation)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxConsultBD)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxParticularCondition)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxReceiveInvoiceEmail)).click();
		Thread.sleep(1000);
	}
	
	public void scrollIntoViewPaymentArea() {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", paymentMethodLogos);
	}

	/**
	 * Method to New Customer for PT to click on the "Avançar" button after click
	 * all checkboxes.
	 */
	public void clickContinue() throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(avancar)).click();
	}

	/**
	 * Upload of IDCard from Portugal.
	 */
	public void clickUploadPersonalDocument() throws InterruptedException {

		Thread.sleep(3000);
		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\cc1.png");
		Thread.sleep(3000);
		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\cc2.png");
		Thread.sleep(3000);
	}
	
	public void clickUploadPersonalDocument2() throws InterruptedException {

		Thread.sleep(3000);
		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\aa1.jpg");
		Thread.sleep(3000);
		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\aa2.jpg");
		Thread.sleep(3000);
	}

	public void clickUploadIDCardES() throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		Thread.sleep(10000);
		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\es1.png");
		Thread.sleep(5000);
		upload.sendKeys(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\es2.png");
		Thread.sleep(10000);
		
		js.executeScript("window.scroll(0,800)");

		
		
	}

	/**
	 * Method to New Customer for PT to click on the "Avançar" button after upload
	 * the IDCard.
	 */
	public void clickContinueWaiting() throws InterruptedException {

//		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(avancar)).click();
//		Thread.sleep(30000);
	}
	
	public void clickcheckboxDataConfirmation() throws InterruptedException {
		
		//clica no checkbox de confirmação dos dados
		wait.until(ExpectedConditions.elementToBeClickable(checkboxDataConfirmation)).click();
	}
	
	

	/**
	 * Submit the contract to PT.
	 */
	public void clicksubmitPT() throws InterruptedException {

		
		wait.until(ExpectedConditions.elementToBeClickable(submitPT)).click();
		System.out.println("cliquei submit");
		Thread.sleep(18000);
	}
	
	public void closeCookies() {
		
		By cookiesOpen = By.xpath("//a[@class='cookies-oney__close']//span[1]");
		
		if (driver.findElements(cookiesOpen).size()!=0) {
			closingCookiesMessage.click();
		}
	}

	/**
	 * Accepting Terms and Conditions to NC and EC for BE, IT, ES and RO.
	 */
	public void acceptTermsAndConditions() throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", submit);
		
		String url = driver.getCurrentUrl();
		if (url.contains("qalogin.oney.es")) {
			
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(submit)).click();
		} else if (url.contains("qalogin.oney.ro")) { 
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(submit)).click();
		}else {
			checkBoxTermsAndConditions.click();
			Thread.sleep(2000);
			submit.click();
		}
		
		System.out.println("cliquei submit");
		
		Thread.sleep(20000);

		
	}

	public void clickSubmit() throws InterruptedException {

		submit.click();

		By nextButton = By.xpath("//*[contains(@id,'SubmitBtn')]");
		int i = 0;
		while ((i < 10) & (driver.findElements(nextButton).size() != 0)) {
			Thread.sleep(100);
			System.out.println("submit");
			i++;

		}
	}

	/**
	 * Accepting Terms and Conditions to EC for PT.
	 */
	public void acceptTermsAndConditionsExistingCustomerPT() throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		js.executeScript("arguments[0].scrollIntoView(true);", checkBoxPT1);
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxPT1)).click();
		Thread.sleep(1000);
		js.executeScript("arguments[0].scrollIntoView(true);", checkBoxPT2);
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxPT2)).click();
		Thread.sleep(500);
		js.executeScript("arguments[0].scrollIntoView(true);", checkBoxPT3);
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxPT3)).click();
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxPT4)).click();
		Thread.sleep(500);
		wait.until(ExpectedConditions.elementToBeClickable(checkBoxPT5)).click();
		Thread.sleep(500);


		wait.until(ExpectedConditions.elementToBeClickable(submitPT)).click();

	}
	
	public void insertBrithPlaceIT(String birthPlaceIT,String birthPlace) throws InterruptedException {
		
				String birthPlaceString = birthPlace;
				placeOfBirthInput.clear();
				placeOfBirthInput.sendKeys(birthPlaceString);
				Thread.sleep(2500);
				wait.until(ExpectedConditions.elementToBeClickable(placeOfBirthOptionIT)).click();
				Thread.sleep(2500);
					
				System.out.println(placeOfBirthListIT.getAttribute("value"));
	}

	/**
	 * Insert the data missing on Subscription to all countries.
	 */
	public void insertMissingInformation(DataTable arg1, String arg2) throws InterruptedException {
		//Aqui pega o Datatable da feature e preenche os campos do subscription.
		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		try {
			emailAddress.click();

		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			WebElement emailAddress1 = driver.findElement(By.xpath("//input[contains(@id,'CustomerData_Email')]"));

			emailAddress1.click();
		}
		
		Thread.sleep(500);

		//o IF é pq cada tenant tem seus campos diferentes
		if (arg2 == "BE") {
			//string que vai guardar os dados do DataTable
			String place="";
			String nationality="";
			
			for (int i = 0; i < list.size(); i++) {
				
				place = list.get(i).get("BirthPlace");
				nationality = list.get(i).get("Nationality");
			}

			placeOfBirthInput.sendKeys(place);
			Thread.sleep(2500);
			nationalityInput.sendKeys(nationality);
			Thread.sleep(2500);
			System.out.println(placeOfBirthInput.getAttribute("origvalue"));
			
			//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
			//Webelement.getAttribute é para pegar este atributo que guarda o valor inputado. Se estiver vazio, incluirá o valor novamente.
			if (!placeOfBirthInput.getAttribute("origvalue").equalsIgnoreCase(place)) {
				System.out.println(placeOfBirthInput.getAttribute("origvalue"));
				placeOfBirthInput.sendKeys(place);
				Thread.sleep(500);
			}
			System.out.println(nationalityInput.getAttribute("origvalue"));
			//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
			if (!nationalityInput.getAttribute("origvalue").equalsIgnoreCase(nationality)) {
				System.out.println(nationalityInput.getAttribute("origvalue"));
				nationalityInput.sendKeys(nationality);
				Thread.sleep(500);
			}
			
			
		} else if (arg2 == "ES") {
			//string que vai guardar os dados do DataTable
            String birthPlace = "";
            String province = "";
            String national = "";
            String dni = "";
            
            for (int i = 0; i < list.size(); i++) {

 
                birthPlace = list.get(i).get("BirthPlace");
                province = list.get(i).get("Province");
                national = list.get(i).get("Nationality");
                dni = list.get(i).get("DNI");
            }
                
                Thread.sleep(3500);
                wait.until(ExpectedConditions.elementToBeClickable(placeOfBirth)).click();
                Thread.sleep(2500);
                placeOfBirthList.sendKeys(birthPlace+Keys.ENTER);
                Thread.sleep(6000);
                wait.until(ExpectedConditions.elementToBeClickable(provinceEs)).click();
                Thread.sleep(2500);
                provinceEsList.sendKeys(province+Keys.ENTER);
                Thread.sleep(2500);
                wait.until(ExpectedConditions.elementToBeClickable(nationality)).click();
                nationalityList.sendKeys(national+Keys.ENTER);
                Thread.sleep(5000);
                IdCardSpain.sendKeys(dni);
                Thread.sleep(2500);
                form.click();
				
				
				Thread.sleep(2500);
				System.out.println("Nascido " + placeOfBirthFilled.getText());
				System.out.println("Provincia " + provinceEsFilled.getText());
				System.out.println("Nascionalidade " + nationalityFilled.getText());
				String placeOfBirthFilledText = placeOfBirthFilled.getText();
				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!placeOfBirthFilledText.equalsIgnoreCase(birthPlace)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!placeOfBirthFilledText.equalsIgnoreCase(birthPlace))); i++) {
						wait.until(ExpectedConditions.elementToBeClickable(placeOfBirth)).click();
						Thread.sleep(3500);
						placeOfBirthList.sendKeys(birthPlace);
						Thread.sleep(2500);
						placeOfBirthResult.click();
						Thread.sleep(2500);
						placeOfBirthFilledText =  placeOfBirthFilled.getText();
						System.out.println("Place of Birth: " +placeOfBirthFilledText);
					}
				}
				String provinceEsFilledText = provinceEsFilled.getText();
				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!provinceEsFilledText.equalsIgnoreCase(province)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!provinceEsFilledText.equalsIgnoreCase(province))); i++) {
						provinceEs.click();
						Thread.sleep(2500);
						provinceEsList.sendKeys(province);
						Thread.sleep(2500);
						provinceEsResult.click();
						Thread.sleep(2500);
						provinceEsFilledText = provinceEsFilled.getText();
						System.out.println("Province: " + provinceEsFilledText);
					}
				}
				String nationalityFilledText = nationalityFilled.getText();

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!nationalityFilledText.equalsIgnoreCase(national)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!nationalityFilledText.equalsIgnoreCase(national))); i++) {
						nationality.click();
						Thread.sleep(2500);
						nationalityList.sendKeys(national);
						Thread.sleep(2500);
						nationalityResult.click();
						Thread.sleep(2500);
						nationalityFilledText = nationalityFilled.getText();
						System.out.println("Nationality: " + nationalityFilledText);
					}
				}
				String IdCardSpainText = "";
				IdCardSpainText = IdCardSpain.getAttribute("value");
				System.out.println(IdCardSpainText);

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!IdCardSpainText.equalsIgnoreCase(dni)) {
					System.out.println("1"+IdCardSpainText);
					for (int i=0;((i>10) || (!IdCardSpainText.equalsIgnoreCase(dni))); i++) {
						IdCardSpain.sendKeys(dni);
						Thread.sleep(2500);
						IdCardSpainText =  IdCardSpain.getAttribute("value");
						System.out.println(IdCardSpainText);
					}
				}
					

			
		} else if (arg2 == "IT") {
			//string que vai guardar os dados do DataTable
			String nationalityIT = "";
			String birthPlace = "";
			String codiceFiscale = "";
			String birthPlaceProvince = "";
			
			for (int i = 0; i < list.size(); i++) {
				nationalityIT = list.get(i).get("Nationality");
				birthPlace = list.get(i).get("BirthPlace"); // + "-"+ list.get(i).get("Province");
				codiceFiscale = list.get(i).get("Codice Fiscale");
				birthPlaceProvince = list.get(i).get("BirthPlace") + "-"+ list.get(i).get("Province");

			}
			
				Thread.sleep(2500);
				
				nationality.click();
				Thread.sleep(2500);
				nationalityList.sendKeys(nationalityIT + Keys.ENTER);
				Thread.sleep(2500);
				placeOfBirthInput.clear();
				placeOfBirthInput.sendKeys(birthPlace);
				Thread.sleep(2500);
			
				wait.until(ExpectedConditions.elementToBeClickable(placeOfBirthOptionIT)).click();
				Thread.sleep(2500);
				
				System.out.println(placeOfBirthListIT.getAttribute("value"));
				System.out.println(nationalityFilledIT.getText());
				String birthPlaceFilledText = placeOfBirthListIT.getAttribute("value");

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!birthPlaceFilledText.equalsIgnoreCase(birthPlaceProvince)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!birthPlaceFilledText.equalsIgnoreCase(birthPlaceProvince))); i++) {
						placeOfBirthListIT.sendKeys(birthPlace);
						Thread.sleep(2500);
						birthPlaceFilledText = placeOfBirthListIT.getAttribute("value");
						System.out.println(birthPlaceFilledText);
					}
				}
				String nationalityFilledText = nationalityFilledIT.getAttribute("value");

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!nationalityFilledText.equalsIgnoreCase(nationalityIT)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!nationalityFilledText.equalsIgnoreCase(nationalityIT))); i++) {
						nationality.click();
						Thread.sleep(2500);
						nationalityList.sendKeys(nationalityIT + Keys.ENTER);
						Thread.sleep(2500);
						nationalityFilledText =  nationalityFilledIT.getAttribute("value");
						System.out.println(nationalityFilledText);
					}
				}
				
				
				personalNumber.sendKeys(codiceFiscale);
				String personalNumberFilledText = personalNumber.getAttribute("value");

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!personalNumberFilledText.equalsIgnoreCase(codiceFiscale)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!personalNumberFilledText.equalsIgnoreCase(codiceFiscale))); i++) {
						personalNumber.sendKeys(codiceFiscale);
						Thread.sleep(2500);
						personalNumberFilledText = personalNumber.getAttribute("value");
						System.out.println(personalNumberFilledText);
					}
				}
				
			
		} else if (arg2 == "PT") {
			

			for (int i = 0; i < list.size(); i++) {
				nrDocumento.sendKeys(list.get(i).get("NrDocument"));
				Thread.sleep(1500);
				NIF.sendKeys(list.get(i).get("NIF"));
			}

		} else if (arg2 == "RO") {
			//string que vai guardar os dados do DataTable
			String country = "";
			String nationalityRO = "";
			String cnp = "";
			
			for (int i = 0; i < list.size(); i++) {

				country = list.get(i).get("Country");
				nationalityRO = list.get(i).get("Nationality");
				cnp = list.get(i).get("CNP");
			}
			
			wait.until(ExpectedConditions.elementToBeClickable(countryOfBirth)).click();

				countryOfBirthList.sendKeys(country);
				countryOfBirthList.sendKeys(Keys.ENTER);
				Thread.sleep(1500);
				nationality.click();
				Thread.sleep(100);
				nationalityList.sendKeys(nationalityRO);
				nationalityList.sendKeys(Keys.ENTER);
				Thread.sleep(2500);
			
				
				System.out.println(countryOfBirthFilled.getText());
				System.out.println(nationalityFilled.getText());
				String countryOfBirthFilledText = countryOfBirthFilled.getText();
				System.out.println(countryOfBirthFilledText);
				if (!countryOfBirthFilledText.equalsIgnoreCase(country)) {
					System.out.println(countryOfBirthFilledText);
					for (int i=0;((i>10) || (!countryOfBirthFilledText.equalsIgnoreCase(country))); i++) {
						countryOfBirth.click();
						Thread.sleep(2500);
						countryOfBirthList.click();
						countryOfBirthList.sendKeys(country);
						countryOfBirthList.sendKeys(Keys.ENTER);
						Thread.sleep(2500);
						countryOfBirthFilledText =  countryOfBirthFilled.getText();
						System.out.println(countryOfBirthFilledText);
					}
				}
				String nationalityFilledText = nationalityFilled.getText();

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!nationalityFilledText.equalsIgnoreCase(nationalityRO)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!nationalityFilledText.equalsIgnoreCase(nationalityRO))); i++) {
						nationality.click();
						Thread.sleep(2500);
						nationalityList.sendKeys(nationalityRO);
						Thread.sleep(2500);
						nationalityList.sendKeys(Keys.ENTER);
						Thread.sleep(2500);
						nationalityFilledText =  nationalityFilled.getText();
						System.out.println(nationalityFilledText);
					}
				}
				
				CNP.click();
				Thread.sleep(1500);
				CNP.sendKeys(cnp);
				Thread.sleep(1500);
				CNP.sendKeys(Keys.ENTER);
				Thread.sleep(1500);
				
				System.out.println(CNP.getAttribute("value"));
				
				String CNPText = CNP.getAttribute("value");

				//Aqui verifica se o elemento foi preenchido, senão preenche de novo. 
				if (!CNPText.equalsIgnoreCase(cnp)) {
					System.out.println("1");
					for (int i=0;((i>10) || (!CNPText.equalsIgnoreCase(cnp))); i++) {
						CNP.click();
						Thread.sleep(1500);
						CNP.sendKeys(cnp);
						Thread.sleep(1500);
						CNP.sendKeys(Keys.ENTER);
						Thread.sleep(1500);
						CNPText =  CNP.getAttribute("value");
						System.out.println(CNPText);
					}
				}
				
				
		}
	}
	
	public void insertAdditionalFields(DataTable arg1, String arg2) throws InterruptedException {
		
		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String occupationText = "";
		String employerText = "";
		String monthlyIncomeText = "";
		String monthlyExpensesText = "";
		String mainSourceIncomeText = "";
			
		Thread.sleep(2000);
		for (int i = 0; i < list.size(); i++) {
			occupationText = list.get(i).get("Occupation");
			employerText = list.get(i).get("Employer");
			monthlyIncomeText = list.get(i).get("Monthly Income");
			monthlyExpensesText = list.get(i).get("Monthly Expenses");
			mainSourceIncomeText = list.get(i).get("Main Source Income");
		}
						
		if (arg2.contains("PT")) {
			System.out.println(driver.getCurrentUrl());
			if (!driver.getCurrentUrl().contains("CustomerID")) {
				try {
					occupation.sendKeys(occupationText);
					occupation.sendKeys(Keys.ENTER);
				} catch (org.openqa.selenium.StaleElementReferenceException ex) {
					
					WebElement occupation1 = driver.findElement(By.xpath("//select[contains(@id,'CustomerData_Occupation')]"));
					occupation1.sendKeys(occupationText);
					occupation1.sendKeys(Keys.ENTER);
				}
				
				Thread.sleep(20000);
				if (!monthlyIncomeText.equalsIgnoreCase("")) {
					employer.sendKeys(employerText);
				} else {
					employerPT.sendKeys(employerText);
				}
			}	
	
		} else if (arg2.contains("ES")) { 
			wait.until(ExpectedConditions.elementToBeClickable(occupation)).click();
			wait.until(ExpectedConditions.elementToBeClickable(occupation)).sendKeys(occupationText + Keys.ENTER);
			Thread.sleep(20000);
			wait.until(ExpectedConditions.elementToBeClickable(employer)).sendKeys(employerText);
			
			Thread.sleep(5000);
			String oc = wait.until(ExpectedConditions.elementToBeClickable(occupation)).getAttribute("origvalue");
			if (oc.contains("ossli")) {
				System.out.println(oc);
				wait.until(ExpectedConditions.elementToBeClickable(occupation)).click();
				wait.until(ExpectedConditions.elementToBeClickable(occupation)).sendKeys(occupationText + Keys.ENTER);
				Thread.sleep(20000);
			}
			if (wait.until(ExpectedConditions.elementToBeClickable(employer)).getText().equalsIgnoreCase("")) {
				wait.until(ExpectedConditions.elementToBeClickable(employer)).sendKeys(employerText);
				Thread.sleep(5000);
			}
		} else {
			Thread.sleep(8000);
			occupation.click();
			occupation.sendKeys(occupationText);
			occupation.sendKeys(Keys.ENTER);
			Thread.sleep(20000);
			employer.sendKeys(employerText);
			System.out.println("2");
			Thread.sleep(5000);
			if (occupation.getAttribute("origvalue").contains("ossli")) {
				System.out.println(occupation.getAttribute("origvalue"));
				occupation.click();
				occupation.sendKeys(occupationText);
				occupation.sendKeys(Keys.ENTER);
				Thread.sleep(20000);
			}
			if (employer.getText().equalsIgnoreCase("")) {
				employer.sendKeys(employerText);
				Thread.sleep(5000);
			}
		}
		
		Thread.sleep(3000);
		if (!monthlyIncomeText.equalsIgnoreCase("")) {
			monthlyIncome.sendKeys(monthlyIncomeText);
		}
		if (!monthlyExpensesText.equalsIgnoreCase("")) {
			monthlyExpenses.sendKeys(monthlyExpensesText);
		}
		if (!mainSourceIncomeText.equalsIgnoreCase("")) {
			mainSourceIncome.sendKeys(mainSourceIncomeText);
		}
						
			
	}

	public void choseCardPT() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", cardPT);
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(cardPT)).click();
	}

	public void clickVISA() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", VISA);
	
		Thread.sleep(1000);
		VISA.click();
	}
	
	public void payWithAuthentication() throws InterruptedException {

		
//		if (!driver.getCurrentUrl().contains("https://api.sandbox.checkout.com/")) {
			WebElement iframe = longWait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//iframe[@name='cko-3ds2-iframe']")));
			System.out.println("paywithautentication");
			driver.switchTo().frame(iframe);
//		}
			
			
		
			System.out.println("entrou 3ds");
			wait.until(ExpectedConditions.elementToBeClickable(secureAuthentication)).click();
			Thread.sleep(1500);
			secureAuthentication.sendKeys("Checkout1!");
			Thread.sleep(1500);
			secureAuthenticationContinue.click();
			System.out.println("saiu 3ds");
			
		
	}
	
	public void payWithAuthentication2() throws InterruptedException {

		
	if (!driver.getCurrentUrl().contains("https://api.sandbox.checkout.com/")) {
			WebElement iframe = longWait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//iframe[@name='cko-3ds2-iframe']")));
			System.out.println("paywithautentication");
			driver.switchTo().frame(iframe);
	}
		
			
		
			System.out.println("entrou 3ds");
			wait.until(ExpectedConditions.elementToBeClickable(secureAuthentication)).click();
			Thread.sleep(1500);
			secureAuthentication.sendKeys("Checkout1!");
			Thread.sleep(1500);
			secureAuthenticationContinue.click();
			System.out.println("saiu 3ds");
			
		
	}

	/**
	 * Write the contract reference in json file.
	 */
	public void saveContractReference() {

		CONTRACT_REF = ref.getText();
	}

	public void saveContractReferencePT() {

		CONTRACT_REF = refPT.getText();
	}
	
	public void saveContractReferenceAPIDE(String extReference) {

		CONTRACT_REF = extReference;
	}

	/**
	 * Subscription login of existing customer for BE, RO, ES and PT.
	 */

	public void loginExistingCustomer() throws InterruptedException {

//		Thread.sleep(20000);
		
		password = longWait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@id,'Password_Input')]")));
		password.sendKeys("Oney1testes");
		wait.until(ExpectedConditions.elementToBeClickable(login)).click();
		Thread.sleep(12000);
	}

	public void loginExistingCustomerPT(String email) throws InterruptedException {

		wait.until(ExpectedConditions.elementToBeClickable(iHaveAccountPT)).click();
		Thread.sleep(19000);

		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(
				"//div[contains(@id,'CustomerLoginDesktop')]//div[@class='H3_PT'][contains(text(),'Inicie sessão na sua conta 3x 4x Oney')]"))
				)).click();
//		Thread.sleep(300);
		wait.until(ExpectedConditions.elementToBeClickable(emailPT)).sendKeys(email);
//		Thread.sleep(300);
		wait.until(ExpectedConditions.elementToBeClickable(passwordPT)).sendKeys("Oney1testes");

		wait.until(ExpectedConditions.elementToBeClickable(iHaveAccountPT)).click();
//		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(clickPT)).click();
//		Thread.sleep(1000);
		action.moveToElement(wait.until(ExpectedConditions.elementToBeClickable(loginPT))).click().build().perform();


		Thread.sleep(900);
		Thread.sleep(9000);
	}

	public void insertPassword() {

		newAccountPassword.sendKeys("Oney1testes");
		newAccountPassword1.sendKeys("Oney1testes");
	}

	public void insertPasswordPT() {

		newAccountPasswordPT.sendKeys("Oney1testes");
		newAccountPassword1PT.sendKeys("Oney1testes");
	}

	public void activateSelfcareAccount() {

		activate.getLocation();
		activate.click();
	}

	public void activateSelfcareAccountPT() {

		activatePT.getLocation();
		activatePT.click();
	}

	public void checkCreatedContract(String tenant) throws IOException {
		
		WebElement validationCreatedContract = longWait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='BoxContNew']")));
		validationCreatedContract.getText();
		//clica no método para entender melhor
		writeContractCreated(tenant);
		
	}
	
	
	
	public void writeContractCreated(String tenant) throws IOException {
		//aqui gravamos informações do contrato num ficheiro para validar depois o accounting	
		if (tenant.equals("BE") || tenant.equals("IT") || tenant.equals("RO") ||  tenant.equals("ES")) {
			saveContractReference();
		} 
		//sempre que gravar informações de contrato, usar o método do utils com todos os campos do ficheiro.
		//sempre usamos este método para gravar informação no ficheiro, devido ao grande número de campos.
		utils.writeContract(tenant, "ContractCreated", CONTRACT_REF, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 
				null, null, null, null, null, null, null, null, null);		
		
	}
	
	
	
	public void checkAutomaticRedirection() throws InterruptedException {
		
		String url1 = driver.getCurrentUrl();
		Thread.sleep(70000);
		String url2 = driver.getCurrentUrl();
		
		assert(!url1.equals(url2));
	
	}

	public void checkCreatedContractPT() throws IOException {

		WebElement validationCreatedContractPT = longWait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='LandingPageSuccess']")));
		validationCreatedContractPT.getText();
		writeContractCreated("PT");
	}
	
	public void checkCreatedContractDE() {

		wait.until(ExpectedConditions.elementToBeClickable(validationCreatedContractDE)).click();
	}

	public void checkUnauthorizedCard() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", unauthorizedCard);
		wait.until(ExpectedConditions.elementToBeClickable(unauthorizedCard)).isDisplayed();
	}

	public void insertNewInvalidPaymentCard() {

		addNewPaymentCard.click();
	}

	public void checkRejectedContract() {

		wait.until(ExpectedConditions.elementToBeClickable(validationRejectedContract)).getText();
	}
	
	public void checkCardAutenticationFailded() {

		wait.until(ExpectedConditions.elementToBeClickable(cardAutenticationFailedWarning)).getText();
	}
	
	public void checkRejectedCard() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='Feedback_Message_Error']/span[ contains(text(),'Infelizmente não foi possível realizar o pagamento com o cartão.')]")));
	}

	public void checkRejectedContractPT() {

		wait.until(ExpectedConditions.elementToBeClickable(validationRejectedContractPT)).getText();
	}

	public void changeMobilePhone(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			mobilePhone.clear();
			Thread.sleep(3000);
			mobilePhone.sendKeys(list.get(i).get("Mobile Phone"));
			Thread.sleep(3000);
			emailAddress.click();

		}
	}

	public void getMobilePhone() throws InterruptedException {

		customerMobilePhoneNumber = mobilePhone.getAttribute("value");

		System.out.println("This is the current number: " + customerMobilePhoneNumber);

	}

	public void checkmobileMessageError(DataTable mobile) throws InterruptedException {

		List<Map<String, String>> list = mobile.asMaps(String.class, String.class);
		String message;

		for (int i = 0; i < list.size(); i++) {

			message = list.get(i).get("Message");
			Thread.sleep(9000);
			System.out.println("Message Error: " + mobileMessageError.getText());
			assert (message.contains(mobileMessageError.getText()));
		}
		Thread.sleep(1000);
	}

	public void changeBillingAddress() {

		changeAddress.clear();
		changeAddress.sendKeys("Calle 1 Sur");
		postalCode.clear();
		postalCode.sendKeys("29001");
	}

	public boolean verifyCreditCardListIsEmpty() {

		System.out.println(creditCardList.getAttribute("id"));
		if (creditCardList.getAttribute("id").contains("CardPaymentList_EmptyMsg")) {
			newCard.click();
			return true;
		} else {
			return false;
		}
	}

	public void clickConsentEC(String consent) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scroll(0,850)");
		consentClickHere.click();
		Thread.sleep(1000);
		checkBoxTermsAndConditions.click();
		Thread.sleep(1000);
		if (consent.contains("Commercial Offers Oney")) {
			js.executeScript("window.scroll(0,850)");
			checkBoxEcCommercialOffersPartnersES.click();
		} else if (consent.contains("Commercial Offers Partners")) {
			js.executeScript("window.scroll(0,850)");
			checkBoxEcCommercialOffersPartnersES.click();
		} else {
			js.executeScript("window.scroll(0,850)");
			checkBoxEcCommercialOffersOneySiminarES.click();
			checkBoxEcCommercialOffersOneyNoSiminarES.click();
			checkBoxEcCommercialOffersPartnersES.click();
		}
	}

	public WebElement validateLoadingPopUp() {

		return loading.isDisplayed() ? loading : null;

	}
	
	public void validateButton() throws InterruptedException{
		
		wait.until(ExpectedConditions.elementToBeClickable(validateButtonSubscription)).click();
		Thread.sleep(25000);
	}
	
	public void validateTae(String btsType) throws InterruptedException{
		Thread.sleep(15000);
		String taeString = tae.getText();
		if (btsType.equals("3x (with fees)")) {
			
			assert (taeString.equals("294,68 %"));			
		} else if (btsType.equals("4x (with fees)")) {
			
			assert (taeString.equals("52,54 %"));			
		} else if (btsType.equals("6x (with fees)")) {
			
			assert (taeString.equals("84,57 %"));		
		} else if (btsType.equals("8x (with fees")) {
		
			assert (taeString.equals("18,7 %"));
		}

	}
	
	
	public void validateTaegPortugal(String btsType) throws InterruptedException{
		
		Thread.sleep(15000);
		String taeString = taegPortugal.getText();
		System.out.println(taeString);
		if (btsType.equals("3x (with fees)")) {
			
			assert (taeString.equals("294,68%"));			
		} else if (btsType.equals("4x (with fees)")) {
			
			assert (taeString.equals("52,54%"));			
		} else if (btsType.equals("6x (with fees)")) {
			
		assert (taeString.equals("84,57%"));
	}
		
}
	public void validateSecondTry() throws InterruptedException{
		
		Thread.sleep(20000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", secondTryPaymentMessage);
		assert (secondTryPaymentMessage.isDisplayed());
}
	public void clicktoleavethesubscriptionform() throws InterruptedException{
		Thread.sleep(20000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scroll(0,1850)");
		Thread.sleep(1000);
		cancelAndComeBackToTheMerchantWebsite.click();
		}
	
	public void confirmthatthepopinisdisplayed() throws InterruptedException{
		Thread.sleep(2000);
		assert (popinBeforeYouGo.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(popinBeforeYouGo)).isDisplayed();
		}
}
