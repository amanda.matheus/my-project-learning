package pageObjects.postman;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import dto.mfs.CreateInvoice;
import dto.mfs.CreateTransfer;
import pageObjects.backOffice.HomePageBackOffice;
import utils.Utils;


public class Postman {

	private WebElement folder;
	public static String preRequest;
	private WebElement folderOpen;
//	private WebElement file;
	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action; 
	public String extRef;
	public Utils utils;
	private String link;
	public Postman(WebDriver driver) {

		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
		action = new Actions(driver);
		utils = PageFactory.initElements(driver, Utils.class);
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	
	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	private WebElement username;
	
	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	private WebElement password;
	
	@FindBy(how = How.XPATH, using = "//button[@id='sign-in-btn']")
	private WebElement signIn;

	@FindBy(how = How.XPATH, using = "//div[@class='homepage-sidebar-tab-title__label' and text()='Workspaces']")
	private WebElement workspace;
	
	@FindBy(how = How.XPATH, using = "//*[ text()='My Workspace']")
	private WebElement myWorkspace;
		
	@FindBy(how = How.XPATH, using = "//*[ text()='QA Workspace']")
	private WebElement qaWorkspace;
	
	@FindBy(how = How.XPATH, using = "//div[@class='btn']//span[contains(text(),'Send')]")
	private WebElement send;
	
	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'tab__close')])[2]")
	private WebElement closeTab;
	
	@FindBy(how = How.XPATH, using = "(//span[@class='wf__tk'])[1]")
	private WebElement extReference;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Console')]")
	private WebElement console;
	
	@FindBy(how = How.XPATH, using = "//div[@class='sb__item__icon']/i[@class='pm-icon pm-icon-xs pm-icon-normal']/*[1]")
	private WebElement paneView;
	
	@FindBy(how = How.XPATH, using = "//div[@class='console-actions']//following::div[@class='btn btn-tertiary actions__close-icon']")
	private WebElement exitConsole;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'collection-sidebar-list-item__head__name') and @title='FacilyPayURL_QA' ]//ancestor::div[contains(@class,'collection-sidebar-list-item__head')]")
	private WebElement facilyPayUrlQAFolderOpen;

	@FindBy(how = How.XPATH, using = "//span[text()='Body']")
	private WebElement bodytab;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'dropdown-menu-item dropdown-menu-item--edit')]//span[contains(@class,'collection-action-item')]//div[@class='dropdown-menu-item-label']")
	private WebElement folderEdit;

	@FindBy(how = How.XPATH, using = "//div[@class='tab tab-primary collection-modal-tab tab--variables']")
	private WebElement variablestab;
	
	@FindBy(how = How.XPATH, using = "//i[@class='pm-icon pm-icon-sm pm-icon-normal modal-header-close-button']//*[local-name()='svg']")
	private WebElement exitEdition;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'btn btn-primary collection-modal-footer__submit')]")
	private WebElement updateEdition;

	@FindBy(how = How.XPATH, using = "//div[@class='auto-suggest-cell key-value-form-row__input_value reference__0-2']//div[@class='notranslate public-DraftEditor-content']")
	private WebElement variablesCurrentValue;

	@FindBy(how = How.XPATH, using = "//div[@class='key-value-form-editor']//div[@class='auto-suggest-cell key-value-form-row__input_value reference__0-1']//div[@class='notranslate public-DraftEditor-content']")
	private WebElement variablesInitialValue; 

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'request-body-raw-editor')]//div[contains(@class,'text-editor-wrapper')]")
	private WebElement ref; 

	@FindBy(how = How.XPATH, using = "//span[@class='sync-status-badge__text sync-status-badge__text-insync']")
	private WebElement sync;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'collection-sidebar-list-item__head__name') and contains(text(),'PR47_QA')]//following::div[@class='dropdown collection-sidebar-actions-dropdown']")
	private WebElement folderOptionsPR47;

	@FindBy(how = How.XPATH, using = "//div[@class='collection-modal-name-container']")
	private WebElement popup;

	@FindBy(how = How.XPATH, using = "//span[@class='response-meta-status-code']")
	private WebElement statusCode;

	@FindBy(how = How.XPATH, using = "//span[@class='response-meta-status-code-desc']")
	private WebElement statusDesc;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'response-body-text-viewer')]//div[contains(@class,'text-editor')]//div//div//div//i[contains(@class,'pm-icon pm-icon-sm pm-icon-normal text-editor__copy-button')]//*[local-name()='svg']")
	private WebElement copyResponse;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'response-body-text-viewer')]//div[contains(@class,'text-editor')]//div//div[contains(@class,'view-lines')]")
	private WebElement clickLink;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'request-url-editor')]//div[2]//div[1]//div[1]//div[1]//div[1]")
	private WebElement linkOnField;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'alert alert-info')]")
	private WebElement anyClick;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='input input-search' and @type='search'])[2]")
	private WebElement inputSearch;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='input input-search' and @type='search'])[2]//following::i[1]")
	private WebElement inputSearchClear;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'collection-sidebar-list-item__entity__name collection-sidebar-list-item__request__name')]/ancestor::div[1] ")
	private WebElement fileAll;
	
	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'btn btn-icon')])[3]")
	private WebElement clickOnSettings;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Type to filter']")
	private WebElement enviromentInput;
	
	@FindBy(how = How.XPATH, using = "//div[@class='input-search-group is-searching is-blurred']//div[@class='input-search-group__search-cancel-wrapper']")
	private WebElement enviromentCancel;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-menu fluid positioned']")
	private WebElement enviromentList;	
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown-menu-item dropdown-menu-item--settings']")
	private WebElement clickOnSettingsOption;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='toggle-switch-container']//div)[3]")
	private WebElement clickOnSSLButton;	
	
	@FindBy(how = How.XPATH, using = "//div[@class='btn btn-tertiary']")
	private WebElement closeSettingsWindow;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Pre-request Script')]")
	private WebElement preRequestScriptTab;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'request-prscript-editor text-editor-wrapper')]//div[contains(@class,'text-editor')]//div//div[contains(@class,'view-lines')]//div[contains(@class,'view-line')]")
	private WebElement preRequestEditor;
	
	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'requester-tab__close')])[2]")
	private WebElement closeTabRequest;
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),\"Don't save\")]")
	private WebElement dontSave;
	
	
	
	
	/*All methods*/
	public void insertUsernameAndPassword() {
		
		username.sendKeys("oneyAblewise");
		password.sendKeys("Oney1testes@");
	}
	
	public void clickSignIn() {
		
		signIn.click();
	}
	
	public void clickOnWorkspace() throws InterruptedException{
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(workspace)).click();
	}
	
	public void clickOnMyWorkspace() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(myWorkspace)).click();
		Thread.sleep(2000);
	}
	
	public void clickOnQAWorkspace() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(qaWorkspace)).click();
		System.out.println("clicou");
		Thread.sleep(9000);
	}
	
	public void clickOnFolder(String folder) {
		
		folderOpen = driver.findElement(By.xpath("//div[contains(@class,'collection-sidebar-list-item__head__name') and @title='"
				+ folder + "']/preceding::div[1]/ancestor::div[1]"));//ancestor::div[contains(@class,'collection-sidebar-list-item__head')]
		
		this.folder = driver.findElement(By.xpath("//div[@title='" + folder + "']/preceding::div[1]"));		
		
		if (!folderOpen.getAttribute("class").contains("open")) {
			System.out.println();
			wait.until(ExpectedConditions.elementToBeClickable(this.folder)).click();
		}
	}
	
	public void searchFolder(String folder) {
		
		inputSearch.click();
		inputSearch.sendKeys(folder);
	}
	
	public void clickOnFile(String fileDescription) throws InterruptedException {
		
//		action.moveToElement(inputSearch).click().sendKeys(filePostman).build().perform();
		Thread.sleep(1200);
		inputSearch.click();
		Thread.sleep(100);
		inputSearch.sendKeys("a");
		Thread.sleep(100);
		inputSearchClear.click();
		Thread.sleep(100);
		inputSearch.click();
		inputSearch.sendKeys(fileDescription);
				
//		file = driver.findElement(By.xpath("//div[contains(@class,'collection-sidebar-list-item__entity__name collection-sidebar-list-item__request__name') and text()='"
//				+ fileDescription +"']/ancestor::div[1]"));
		
		Thread.sleep(2000);
		fileAll.click();
//		action.moveToElement(file).doubleClick().perform();
		
	
		Thread.sleep(500);
	}
	
	public void clickOnSend() throws InterruptedException {
		
		wait.until(ExpectedConditions.elementToBeClickable(send)).click();
		Thread.sleep(15000);
	}
	
	public void clickOnCloseTab() throws InterruptedException {
	
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(closeTab)).click();
		Thread.sleep(500);
	}
	
	public void clickConsole() {
		console.click();
	}
	
	public void clickPaneView() {
		paneView.click();
	}
	
	public void exitConsole() {
		exitConsole.click();
	}
	
	public String getExternalReference() {
		
		extRef = extReference.getText();
		System.out.println("extRef " + extRef);
		return extRef;
	}
	
	public void writeActionsToContract(String tenant, String action) throws IOException {
		
		utils.writeContract(tenant, action, extRef, null, null, null, null, null, "ACCEPTED", null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 
				null, null, null, null, null, null, null, null, null);	
		
	}
	
	public void clickOnBodyTab() {
		
		wait.until(ExpectedConditions.elementToBeClickable(bodytab)).click();
		
	}

//	public void changeExternalReference(String arg) throws InterruptedException {
//		
//		String random = "";
//		if (arg.equalsIgnoreCase("to the same")) {
//			random = "gvx4fuz5l9o";
//		} else {
//			random = RandomString.make(9);
//		}
//		 
//		Thread.sleep(500);
////		ref.clear();
////		System.out.println(ref.getText());
////		ref.sendKeys(random);
////		System.out.println("Oi2");
//		action.moveToElement(folderOptionsPR47).click().build().perform();
////		wait.until(ExpectedConditions.elementToBeClickable(getFolderOpen())).();
//		Thread.sleep(500);
////		wait.until(ExpectedConditions.elementToBeClickable(folderOptionsPR47)).click();
//		Thread.sleep(100);
//		wait.until(ExpectedConditions.elementToBeClickable(folderEdit)).click();
//		Thread.sleep(300);
//		wait.until(ExpectedConditions.elementToBeClickable(popup)).click();
//		Thread.sleep(300);
//		wait.until(ExpectedConditions.elementToBeClickable(variablestab)).click();
//		Thread.sleep(300);
//		action.moveToElement(variablesInitialValue).click().build().perform();
//		action.click(variablesInitialValue).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).build().perform();
//		Thread.sleep(1000);
//		action.moveToElement(variablesInitialValue).sendKeys(random).build().perform();
//		Thread.sleep(300);
//		action.moveToElement(variablesCurrentValue).click().build().perform();
//		action.click(variablesCurrentValue).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).build().perform();
//		Thread.sleep(1000);
//		action.moveToElement(variablesCurrentValue).sendKeys(random).build().perform();
//		anyClick.click();
//		Thread.sleep(300);
//		Thread.sleep(300);
//		Thread.sleep(300);
//		anyClick.click();
//		Thread.sleep(2000);
//		wait.until(ExpectedConditions.elementToBeClickable(updateEdition)).click();
////		Thread.sleep(500);
////		wait.until(ExpectedConditions.elementToBeClickable(exitEdition)).click();
//
//		Thread.sleep(800);
//		
//	}
	
	
	public void clickPersistAll() throws InterruptedException {
		
//		
//		 
//		Thread.sleep(500);
//
//		action.moveToElement(folderOptionsPR47).click().build().perform();
////		wait.until(ExpectedConditions.elementToBeClickable(getFolderOpen())).();
//		Thread.sleep(500);
////		wait.until(ExpectedConditions.elementToBeClickable(folderOptionsPR47)).click();
//		Thread.sleep(100);
//		wait.until(ExpectedConditions.elementToBeClickable(folderEdit)).click();
//		Thread.sleep(300);
//		wait.until(ExpectedConditions.elementToBeClickable(popup)).click();
//		Thread.sleep(300);
//		wait.until(ExpectedConditions.elementToBeClickable(variablestab)).click();
//		Thread.sleep(300);
//		action.moveToElement(variablesInitialValue).click().build().perform();
//		action.click(variablesInitialValue).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).build().perform();
//		Thread.sleep(1000);
//		action.moveToElement(variablesInitialValue).sendKeys(random).build().perform();
//		Thread.sleep(300);
//		action.moveToElement(variablesCurrentValue).click().build().perform();
//		action.click(variablesCurrentValue).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).build().perform();
//		Thread.sleep(1000);
//		action.moveToElement(variablesCurrentValue).sendKeys(random).build().perform();
//		anyClick.click();
//		Thread.sleep(300);
//		Thread.sleep(300);
//		Thread.sleep(300);
//		anyClick.click();
//		Thread.sleep(2000);
//		wait.until(ExpectedConditions.elementToBeClickable(updateEdition)).click();
////		Thread.sleep(500);
////		wait.until(ExpectedConditions.elementToBeClickable(exitEdition)).click();
//
//		Thread.sleep(800);
//		
	}
	
	public void verificarsync() throws InterruptedException {
		
		if (!sync.getText().equalsIgnoreCase("Online")) {
			Thread.sleep(1000);
			
		}
		sync.getText();
	}
	
	public void validateResponse(String status) throws InterruptedException{
		Thread.sleep(2000);
		System.out.println(status.substring(0,3)+statusCode.getText());
		assert (statusCode.getText().equals(status.substring(0,3)));
		System.out.println(status.substring(4,status.length()) + statusDesc.getText());
		assert (statusDesc.getText().equals(status.substring(4,status.length())));
	}
	
	public void getLinkSubscription() throws InterruptedException {
		

		link = clickLink.getText();
		System.out.println(link);
		link = link.replace("\n", "").replace(" ", "").replace("\"", "").replace("}", "");
		System.out.println(link);
		int var = link.indexOf("http");
		link = link.substring(var, link.length());
		System.out.println(var);
		System.out.println(link);
		Thread.sleep(2500);
	}
	
	public String getResponse() {
		String response = clickLink.getText();
		System.out.println("response " + response);
		return response;
	}
	
	
	public String getLink() {
		
		return link;
	}
	
	public void deactivate_SSL_Certificate() throws InterruptedException {
		
		Thread.sleep(2000);
		clickOnSettings.click();
		Thread.sleep(2000);
		clickOnSettingsOption.click();
		Thread.sleep(2000);
		
		System.out.println(clickOnSSLButton.getAttribute("class"));
		if (clickOnSSLButton.getAttribute("class").contains("toggle-switch is-active")) {
			clickOnSSLButton.click();
		}
		Thread.sleep(3000);
		action.moveToElement(closeSettingsWindow).click().build().perform();

	}
	
	public void changeEnvironment(String environment) throws InterruptedException {
		
		Thread.sleep(2000);
		enviromentInput.click();
		Thread.sleep(2000);
		enviromentInput.sendKeys(environment);
		Thread.sleep(2000);
		enviromentList.click();
		
	}
	
	
	
	public void fillPreRequestScript(String fileNamePath) throws InterruptedException, JsonSyntaxException, JsonIOException, IOException {
		
		String merchant_guid = "";
		String ref = "";
		Random random = new Random();
		String messageId = String.valueOf(random.nextInt()).replace("-", "0"); 
		Gson gson = new Gson();	
		
		preRequestScriptTab.click();
		
		FileReader reader = new FileReader(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\MFS\\"+fileNamePath);
		
		Thread.sleep(3000);
		if (fileNamePath.contains("Transfer")) {
			
		    CreateTransfer transfer = gson.fromJson(reader, CreateTransfer.class);
		    System.out.println(transfer.reference);
		    ref = transfer.reference;
		    String labelString = transfer.label;
		    int labelLength = labelString.indexOf("SCT");
		    merchant_guid = labelString.substring(0,labelLength);
		} else {
			CreateInvoice inv = gson.fromJson(reader, CreateInvoice.class);

			ref = inv.customers.get(0).invoices.get(0).externalReference;
			merchant_guid = inv.customers.get(0).contractref;
			
			
		}
		
	    reader.close();
		
	    System.out.println("pm.variables.set(\"Merchant_GUID\", \""+merchant_guid+"\");" + "\r\n" +
				"pm.variables.set(\"reference_id\", \"" + ref +"\");"+ "\r\n" +
				"pm.variables.set(\"message_id\", \""+ messageId + ref + messageId +"\");");
		preRequest = "pm.variables.set(\"Merchant_GUID\", \""+merchant_guid+"\");" + "\r\n" +
							"pm.variables.set(\"reference_id\", \"" + ref +"\");"+ "\r\n" +
									"pm.variables.set(\"message_id\", \""+messageId+ ref + messageId +"\");";
		
		preRequestEditor.click();
		action.moveToElement(preRequestEditor).sendKeys(preRequest).build().perform();
		
		
	}
	
	public void fillPreRequestScript() throws InterruptedException, JsonSyntaxException, JsonIOException, FileNotFoundException {
		
		preRequestScriptTab.click();

		Thread.sleep(3000);
		
	    String preRequest = "pm.globals.set(\"external_reference_multicapture\", \""+HomePageBackOffice.externalRefPR36+"\");";
		
		preRequestEditor.click();
		action.moveToElement(preRequestEditor).sendKeys(preRequest).build().perform();
		
		
	}
	
	public void closeTab() throws InterruptedException {
		
		closeTabRequest.click();
		Thread.sleep(3000);
		dontSave.click();
	}
}
