package pageObjects.merchant;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import net.bytebuddy.utility.RandomString;

public class HomePageMerchant {
	
	public static String currentPassword;
	public static String currentUsername;

	public HomePageMerchant(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'UserName')]")
	private WebElement usernameMerchant;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Password')]")
	private WebElement passwordMerchant;

	@FindBy(how = How.XPATH, using = "//span[@class='caretButton']")
	private WebElement loginMerchant;

	@FindBy(how = How.XPATH, using = "//span[@class='caretButton']//following::a[1]")
	private WebElement forgotPasswordMerchant;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'ResetPassword_Email')]")
	private WebElement emailRecoveryPasswordMerchant;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'ResetPassword_Email')]//following::input[1]")
	private WebElement recoverPasswordMerchant;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Username_wtOldPassword')]")
	private WebElement currentPasswordfield;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Username_wtNewPassword')]")
	private WebElement newPasswordfield;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Password_wtRetryPassword')]")
	private WebElement retryPasswordfield;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Save')]")
	private WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@type,'submit')]")
	private WebElement submitButton;


	/* Logins para o merchant space da Brand Quality Assurance Automation Tests */
	public void insertUsernameAndPasswordMerchant(String tenant) {

		usernameMerchant.sendKeys("QAAutomation" + tenant);
		passwordMerchant.sendKeys("Oney1testes");
		currentUsername = "QAAutomation" + tenant;
		currentPassword = "Oney1testes";
	}
		
	public void clickLoginMerchant() {

		loginMerchant.click();
	}

	public void clickForgotPassword() {

		forgotPasswordMerchant.click();
	}

	public void insertEmailToRecoverPassword() {

		emailRecoveryPasswordMerchant.sendKeys("testing.test123@test.com");
	}

	public void clickRecoverPassword() {

		recoverPasswordMerchant.click();
	}
	
	public void changePassword() {
		String pass = RandomString.make(2);
		System.out.println(currentPassword);
		System.out.println(currentPassword + pass);
		System.out.println(currentUsername);
		currentPasswordfield.sendKeys(currentPassword);
		newPasswordfield.sendKeys(currentPassword + pass);
		retryPasswordfield.sendKeys(currentPassword + pass);
		submitButton.click();
	}
	

	public String getCurrentUsername() {
		return currentUsername;
	}
	
	public String getCurrentPassword() {
		return currentPassword;
	}

}
