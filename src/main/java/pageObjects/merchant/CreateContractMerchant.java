package pageObjects.merchant;

import java.awt.AWTException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import net.bytebuddy.utility.RandomString;

public class CreateContractMerchant {

	public WebDriver driver;
	public static String saveEmail;
	public WebDriverWait wait;
	public static String birthPlaceString;

	public CreateContractMerchant(WebDriver driver) {

		PageFactory.initElements(driver, this);
		this.driver = driver;
		wait = new WebDriverWait(driver, 60);
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	
	
	/*
	 * Step 1 to Create a Contract
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'MerchantCombo')]")
	private WebElement comboMerchantName;
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'MerchantCombo')]//span[@class='select2-arrow']")
	private WebElement comboMerchantNameClick;
	@FindBy(how = How.XPATH, using = "//input[@id='s2id_autogen1_search']")
	private WebElement comboMerchantNameSearch;
	
	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'GenerateNumber')])")
	private WebElement generateContractNumber;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'ItemDescription')])")
	private WebElement itemDescription;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'ProductCategory')])")
	private WebElement itemCategory;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'Quantity')])")
	private WebElement quantity;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'Price')])")
	private WebElement price;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'AddProduct')]")
	private WebElement addProducts;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '4x002')]")
	private WebElement oney4xWithoutFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '4x001')]")
	private WebElement oney4xWithFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '3x002')]")
	private WebElement oney3xWithoutFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '3x001')]")
	private WebElement oney3xWithFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '6x001')]")
	private WebElement oney6xWithFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '8x001')]")
	private WebElement oney8xWithFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '8x002')]")
	private WebElement oney8xWithoutFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '10x01')]")
	private WebElement oney10xWithFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '12x01')]")
	private WebElement oney12xWithFees;

	@FindBy(how = How.XPATH, using = "//img[contains(@src, '12x02')]")
	private WebElement oney12xWithoutFees;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'Confirm')])")
	private WebElement confirm;

	
	/*
	 * Step 2 to Create a Contract
	 */
	@FindBy(how = How.XPATH, using = "//*[contains(@id,'_Gender')]")
	private WebElement gender;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'_Gender')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement genderList;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-firstname-without-account-fpi')]")
	private WebElement firstName;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'GivenName')]")
	private WebElement segundoApelido;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-lastname-without-account-fpi')]")
	private WebElement lastName;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-email-without-account-fpi')]")
	private WebElement email;

	@FindBy(how = How.XPATH, using = "//*[contains(@name, 'Phone')]")
	private WebElement mobile;
	
	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-address-without-account-fpi')]")
	private WebElement address;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'CustomerData_Adress2')]")
	private WebElement addressNumber;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-zip-code-without-account-fpi')]")
	private WebElement zipCode;

	@FindBy(how = How.XPATH, using = "//select[contains(@id-oney, 'select-subscription-day-birthdate-without-account-fpi')]")
	private WebElement birthDay;

	@FindBy(how = How.XPATH, using = "//select[contains(@id-oney, 'select-subscription-month-birthdate-without-account-fpi')]")
	private WebElement birthMonth;

	@FindBy(how = How.XPATH, using = "//select[contains(@id-oney, 'select-subscription-year-birthdate-without-account-fpi')]")
	private WebElement birthYear;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Data_Dateofbirth')]")
	private WebElement birthPT;
	
	/*
	 * The field Nationality (list) to IT, RO and ES.
	 */
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'NationalityInput')]")
	private WebElement nationality;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'NationalityInput')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement nationalityList;

	/*
	 * The field Nationality to BE.
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'NacionalityInput')]")
	private WebElement nationalityInput;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'BirthPlace')]")
	private WebElement placeOfBirth;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceInput')]")
	private WebElement birthPlace;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceInput')]//div[1]//div[1]//input[1]")
	private WebElement birthPlaceList;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceCB')]")
	private WebElement birthPlaceES;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'BirthPlaceCB')]//div[1]//div[1]//input[1]")
	private WebElement birthPlaceListES;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'BillingMunicipality2')]//input[1]")
	private WebElement municipalityBE;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-municipality-without-account-fpi')]")
	private WebElement municipality;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Province_chosen')]")
	private WebElement provincia;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Province_chosen')]//div[contains(@class,'chosen-search')]//input[1]")
	private WebElement provinciaList;

	@FindBy(how = How.XPATH, using = "//*[contains(@id-oney, 'input-subscription-fiscal-code-without-account-fpi')]")
	private WebElement personalNumber;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'ContractData_IdentityInformation')]")
	private WebElement documentType;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'IDNumber')]")
	private WebElement documentNumber;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, 'ContractData_IssuingPlace')]")
	private WebElement issuingPlace;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'ContractData_IssuingDate')]//following::select[contains(@id, 'ShowDay')]")
	private WebElement issuingDay;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'ContractData_IssuingDate')]//following::select[contains(@id, 'ShowMonth')]")
	private WebElement issuingMonth;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'ContractData_IssuingDate')]//following::select[contains(@id, 'ShowYear')]")
	private WebElement issuingYear;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'ContractData_ExpirationDate')]//following::select[contains(@id, 'ShowDay')][1]")
	private WebElement expiricyDay;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'ContractData_ExpirationDate')]//following::select[contains(@id, 'ShowMonth')][1]")
	private WebElement expiricyMonth;

	@FindBy(how = How.XPATH, using = "//label[contains(@for,'ContractData_ExpirationDate')]//following::select[contains(@id, 'ShowYear')][1]")
	private WebElement expiricyYear;

	@FindBy(how = How.XPATH, using = "(//input[contains(@id,'ConfirmContract')])")
	private WebElement confirmContract;

	/*
	 * Link Subscription
	 */
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'http')]")
	private WebElement confirmationURL;


	/*
	 * PR35 - Data Travel fields
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'AddProduct')]//following::input[1]")
	private WebElement addTravels;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Traveller_NumberOfTickets') and @origvalue='0']")
	private WebElement numberOfTickets;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Traveller_JourneyTypeId') and @origvalue='__ossli_0']")
	private WebElement typeOfJourney;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Traveller_IncludesStayId') and @origvalue='__ossli_0']")
	private WebElement accommodationVehicle;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Traveller_PricingType') and (@origvalue='__ossli_0' or @origvalue='2')]")
	private WebElement pricingType;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'SaveTravellerButton')]")
	private WebElement nextTravellerInfo;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Departure_MeanOfTransportId')]")
	private WebElement departureMeanOfTransport;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Departure_DepartureCity')]")
	private WebElement departureDepartureCity;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Departure_DepartureDate')]")
	private WebElement departureDepartureDate;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Departure_ArrivalCity')]")
	private WebElement departureArrivalCity;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Departure_TicketCategoryId')]")
	private WebElement departureTicketCategory;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Departure_IncludesInsuranceId')]")
	private WebElement departureTravelWithInsurance;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Departure_ExchangeableId')]")
	private WebElement departureExchangeable;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Departure_Price')]")
	private WebElement departurePrice;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Return_MeanOfTransportId')]")
	private WebElement returnMeanOfTransport;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Return_DepartureDate')]")
	private WebElement returnDepartureDate;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Return_ArrivalCity')]")
	private WebElement returnArrivalCity;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Return_TicketCategoryId')]")
	private WebElement returnTicketCategory;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Return_IncludesInsuranceId')]")
	private WebElement returnTravelWithInsurance;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Return_ExchangeableId')]")
	private WebElement returnExchangeable;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Return_Price')]")
	private WebElement returnPrice;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Return_DepartureCity')]")
	private WebElement returnDepartureCity;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Departure_ExchangeableId')]//following::input[contains(@id,'TravelListRecords') and contains(@id,'Text') and @type='submit']")
	private WebElement saveDepartureInfo;
	
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Return_ExchangeableId')]//following::input[contains(@id,'TravelListRecords') and contains(@id,'Text') and @type='submit']")
	private WebElement saveReturnInfo;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Stay_StayTypeId')]")
	private WebElement accommodationTypeOfStay;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Stay_City')]")
	private WebElement accommodationCity;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Stay_ArrivalDate')]")
	private WebElement accommodationArrivalDate;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Stay_DepartureDate')]")
	private WebElement accommodationDepartureDate;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Stay_NumberOfRooms')]")
	private WebElement accommodationNumberOfRooms;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Stay_IncludesInsuranceId')]")
	private WebElement accommodationStayWithInsurance;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Stay_IncludesRentalId')]")
	private WebElement accommodationVehicleRental;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Stay_Price')]")
	private WebElement accommodationPrice;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'Stay_IncludesRentalId')]//following::input[contains(@id,'TravelListRecords') and contains(@id,'Text') and @type='submit']")
	private WebElement saveAccommodationsInfo;
	
	@FindBy(how = How.XPATH, using = "//div[@class='Cell2']//input[1]")
	private WebElement travelBasketPrice;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Travel_TotalPrice')]")
	private WebElement travelTotalPrice;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Actions_wtDepartureAddWrapper')]//child::input[@type='submit']")
	private WebElement addAnotherDestination;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'TotalBasketPrice')]")
	private WebElement totalBasketPrice;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'Actions_wtStayAddWrapper')]//child::input[@type='submit']")
	private WebElement addStayOrVehicle;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ActionsFooter_wtTravelForm')]//following::input[@type='submit']")
	private WebElement addAnotherTravel;
	

	@FindBy(how = How.XPATH, using = "//span[@class='Feedback_Message_Text']")
	private WebElement messageAPIError;
	
	
	/*
	 * PR38 - Phone number message
	 */
	@FindBy(how = How.XPATH, using = "//*[contains(@name, 'Phone')]//following::span[@class='ValidationMessage' and @role='alert' and contains(@id,'PhoneNumberInput') and not(@style='display: none;')]")
	private WebElement mobileMessageError;
		
	
	
	/**
	 * Step 1 to Create a Contract - Methods
	 */
	public void clickGenerateContractNumber() {

		wait.until(ExpectedConditions.elementToBeClickable(generateContractNumber)).click();
	}
	
	public void insertProductInformation(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String priceValue;
		
		Thread.sleep(500);
		for (int i = 0; i < list.size(); i++) {

			String description = list.get(i).get("Description");
			wait.until(ExpectedConditions.elementToBeClickable(itemDescription)).sendKeys(description);
			
			wait.until(ExpectedConditions.elementToBeClickable(itemCategory)).sendKeys(list.get(i).get("Category"));
			wait.until(ExpectedConditions.elementToBeClickable(quantity)).sendKeys(list.get(i).get("Quantity"));
			priceValue = list.get(i).get("Price");
			wait.until(ExpectedConditions.elementToBeClickable(price)).clear();
//			Thread.sleep(1500);
			wait.until(ExpectedConditions.elementToBeClickable(price)).sendKeys(priceValue);
//			Thread.sleep(1500);
			System.out.println(price.getAttribute("value"));
			if (!price.getAttribute("value").contains(priceValue)) {
				price.clear();
				price.sendKeys(priceValue);
				System.out.println(price.getAttribute("value"));
			}
			wait.until(ExpectedConditions.elementToBeClickable(addProducts)).click();
			Thread.sleep(100);

			By itemAdded = By.xpath("//table[contains(@id,'TableRecordsPurchase')]//td//span[text()='" + description + "']");
	
		    int z = 0;
			while ((z<10) & (driver.findElements(itemAdded).size() == 0)) { 
			      Thread.sleep(100);
			      z++;
			  }
			 
			Thread.sleep(1000);
		}
	
	}

	public void click4xOneyWithoutFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney4xWithoutFees)).click();
	}

	public void click4xOneyWithFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney4xWithFees)).click();
	}

	public void click3xOneyWithoutFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney3xWithoutFees)).click();
	}

	public void click3xOneyWithFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney3xWithFees)).click();
	}
	
	public void click6xOneyWithFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney6xWithFees)).click();
	}
	
	public void click8xOneyWithFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney8xWithFees)).click();
	}
	
	public void click8xOneyWithoutFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney8xWithoutFees)).click();
	}
	
	public void click10xOneyWithFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney10xWithFees)).click();
	}
	
	public void click12xOneyWithFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney12xWithFees)).click();
	}
	
	public void click12xOneyWithoutFees() {

		wait.until(ExpectedConditions.elementToBeClickable(oney12xWithoutFees)).click();
	}


	public void clickConfirm() {

		wait.until(ExpectedConditions.elementToBeClickable(confirm)).click();
	}

	
	/**
	 * Step 2 to Create a Contract - Methods
	 */
	public void insertGender(DataTable arg1, String arg2) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			if (arg2 == "IT") {
				gender.click();
				gender.sendKeys(list.get(i).get("Gender"));
				gender.sendKeys(Keys.ENTER);
			} else if (arg2 == "RO") {
				gender.click();
				genderList.sendKeys(list.get(i).get("Gender"));
				genderList.sendKeys(Keys.ENTER);
			}
		}
	}

	public void insertPersonalInfo(DataTable arg1, String tenant) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String fn;
		String ln;

		for (int i = 0; i < list.size(); i++) {
			
			fn=list.get(i).get("First name");
			ln=list.get(i).get("Last name");
			
			if (tenant.equalsIgnoreCase("PT")) {
				firstName.sendKeys(fn + " " + ln);
			} else {
				
			firstName.sendKeys(fn);
			lastName.sendKeys(ln);
			}
			Thread.sleep(1500);
			mobile.sendKeys(list.get(i).get("Mobile Phone"));
			
		}
	}

	public void insertPersonalInfo_2(DataTable arg1, String tenant) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			address.sendKeys(list.get(i).get("Address"));
			Thread.sleep(1500);
			zipCode.sendKeys(list.get(i).get("Postal Code"));
			Thread.sleep(500);
			if (tenant.equals("PT")) {
				addressNumber.sendKeys(list.get(i).get("Address Number"));
			}
		}
	}

	public void insertMunicipality(DataTable arg1, String arg2) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		if (arg2 == "BE") {

			for (int i = 0; i < list.size(); i++) {

				municipalityBE.sendKeys(list.get(i).get("Municipality"));
				Thread.sleep(500);
			}
		} else if ((arg2 == "ES") || (arg2 == "RO") || (arg2 == "PT") || (arg2 == "IT") ){

			for (int i = 0; i < list.size(); i++) {
				municipality.sendKeys(list.get(i).get("Municipality"));
			}
		} 
	}

	public void insertEmail(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String emailarg = "";
		String randomEmail;

		for (int i = 0; i < list.size(); i++) {
			emailarg = list.get(i).get("Email");
		}

		if (emailarg.equalsIgnoreCase("")) {

			randomEmail = "testing-" + RandomString.make(10) + "@gmail.com";
			email.sendKeys(randomEmail);
			saveEmail = randomEmail;
			System.out.println(saveEmail);
		} else {
			email.sendKeys(emailarg);
		}
		
		
	}
	
	public static String getSaveEmail() {
		System.out.println(saveEmail);
		return saveEmail;
	}

	public void insertNacionality(DataTable arg1, String arg2) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		if (arg2 != "BE") {
			for (int i = 0; i < list.size(); i++) {
				nationality.click();
				Thread.sleep(500);
				nationalityList.sendKeys(list.get(i).get("Nationality"));
				nationalityList.sendKeys(Keys.ENTER);
			}
		} else if (arg2 == "BE") {
			for (int i = 0; i < list.size(); i++) {
				nationalityInput.sendKeys(list.get(i).get("Nationality"));
			}
		}
	}

	public void insertProvincia(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			wait.until(ExpectedConditions.elementToBeClickable(provincia)).click();
			wait.until(ExpectedConditions.elementToBeClickable(provinciaList)).sendKeys(list.get(i).get("Province"));
			wait.until(ExpectedConditions.elementToBeClickable(provinciaList)).sendKeys(Keys.ENTER);
		}
	}
	
	public String getBirthPlace() {
		return birthPlaceString;
	}
	
	public void insertPlaceOfBirth(DataTable arg1, String arg2) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			if (arg2 == "IT" || arg2 == "RO") {
				birthPlace.click();
				Thread.sleep(500);
				birthPlaceString = list.get(i).get("Birth place");
				birthPlaceList.sendKeys(list.get(i).get("Birth place"));
				birthPlaceList.sendKeys(Keys.ENTER);
				Thread.sleep(500);
			} else if (arg2 == "ES") {
				Thread.sleep(500);
				birthPlaceES.click();
				Thread.sleep(500);
				birthPlaceListES.sendKeys(list.get(i).get("Birth place"));
				birthPlaceListES.sendKeys(Keys.ENTER);
				Thread.sleep(500);

			} else if (arg2 == "BE") {
				Thread.sleep(500);
				placeOfBirth.sendKeys(list.get(i).get("Birth place"));
				
			}
		}
	}

	public void insertSegundoApelido(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			segundoApelido.sendKeys(list.get(i).get("Apelido"));
		}
	}

	public void insertBirthDatePT() throws InterruptedException {
		Thread.sleep(100);
		birthPT.sendKeys("10-11-1982");
		Thread.sleep(100);
	}

	public void insertBirthDate() throws InterruptedException {


		for (int i = 0; i < 15; i++) {

			try {
				Select bDay = new Select(birthDay);
				Thread.sleep(100);
				bDay.selectByValue("10");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}

			try {
				Select bMonth = new Select(birthMonth);
				Thread.sleep(100);
				bMonth.selectByValue("8");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}

			try {
				Select bYear = new Select(birthYear);
				Thread.sleep(100);
				bYear.selectByVisibleText("1982");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}
		}

	}

	public void insertEmissionDate() throws InterruptedException {

		for (int i = 0; i < 15; i++) {

			try {
				Select iDay = new Select(issuingDay);
				Thread.sleep(500);
				iDay.selectByValue("1");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}

			try {
				Select iMonth = new Select(issuingMonth);
				Thread.sleep(500);
				iMonth.selectByValue("1");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}

			try {
				Select iYear = new Select(issuingYear);
				Thread.sleep(500);
				iYear.selectByValue("2006");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}
		}

	}

	public void insertExpiracyDate() throws InterruptedException {

		for (int i = 0; i < 15; i++) {

			try {
				Select eDay = new Select(expiricyDay);
				Thread.sleep(500);
				eDay.selectByValue("3");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}

			try {
				Select eMonth = new Select(expiricyMonth);
				Thread.sleep(500);
				eMonth.selectByValue("4");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());
			}

			try {
				Select eYear = new Select(expiricyYear);
				Thread.sleep(500);
				eYear.selectByVisibleText("2050");

			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ex.toString();
				System.out.println("Stale element error, trying ::  " + ex.getMessage());

			}
		}

	}

	public void insertDocumentType(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		documentType.sendKeys("Citizen");
		Thread.sleep(3500);
		documentType.sendKeys(Keys.ENTER);
		Thread.sleep(1000);

		for (int i = 0; i < list.size(); i++) {

			documentNumber.sendKeys(list.get(i).get("Document Number"));
			issuingPlace.sendKeys(list.get(i).get("Issuing Place"));
			Thread.sleep(500);
		}
	}

//	public void insertDocumentTypePT(DataTable arg1) throws InterruptedException {
//
//		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
//
//		documentType.sendKeys("Cartão de Cidadão");
//		Thread.sleep(500);
//		documentType.sendKeys(Keys.ENTER);
//		for (int i = 0; i < list.size(); i++) {
//
//			documentNumber.sendKeys(list.get(i).get("Document Number"));
//			issuingPlace.sendKeys(list.get(i).get("Issuing Place"));
//			Thread.sleep(500);
//		}
//	}

	public void insertIdCard(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			personalNumber.sendKeys(list.get(i).get("ID Card"));
			Thread.sleep(500);
		}
	}

	public void clickConfirmContract() throws InterruptedException {

		Thread.sleep(5000);
		confirmContract.click();
	}

	public void navigateToConfirmationURL(String arg1) throws AWTException, InterruptedException {

		int count1 = confirmationURL.getText().length();

		String url = confirmationURL.getText().substring(count1 - 38, count1);
		driver.navigate().to(url);
	}

	/**
	 * PR35 - Data Travel fields Methods
	 * @throws InterruptedException 
	 */
	public void clickAddTravels() throws InterruptedException {

		addTravels.click();
		Thread.sleep(1000);
	}
	
	public void clickAddProducts() {

		addProducts.click();
	}

	public void fillTravellerInfo(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			wait.until(ExpectedConditions.elementToBeClickable(numberOfTickets));
			numberOfTickets.clear();
			numberOfTickets.sendKeys(list.get(i).get("Number of Tickets"));

			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(typeOfJourney));
			Select typeOfJourney1 = new Select(typeOfJourney);
			typeOfJourney1.selectByVisibleText(list.get(i).get("Type of Journey"));
			
			if (!list.get(i).get("Accommodation and Vehicle").equalsIgnoreCase("")) {
				Thread.sleep(2000);
				wait.until(ExpectedConditions.elementToBeClickable(accommodationVehicle));
				
				Select accommodationVehicle1 = new Select(accommodationVehicle);
				accommodationVehicle1.selectByVisibleText(list.get(i).get("Accommodation and Vehicle"));
			}
			if (!list.get(i).get("Pricing Type").equalsIgnoreCase("")) {
				Thread.sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(pricingType));
				Select pricingType1 = new Select(pricingType);
				pricingType1.selectByVisibleText(list.get(i).get("Pricing Type"));
			}
			Thread.sleep(1000);	
		}
		nextTravellerInfo.click();
		Thread.sleep(1000);	
		
		By nextButton = By.xpath("//input[contains(@id,'SaveTravellerButton')]");
		int i = 0;
		while ( (i<100) & (driver.findElements(nextButton).size() != 0) ) {
			Thread.sleep(100);
			i++;
		}
	}

	public void fillDepartureInfo(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			wait.until(ExpectedConditions.elementToBeClickable(departureMeanOfTransport));
			wait.until(ExpectedConditions.elementToBeClickable(departureMeanOfTransport));
			Select departureMeanOfTransport1 = new Select(departureMeanOfTransport);
			departureMeanOfTransport1.selectByVisibleText(list.get(i).get("Mean of Transport"));
			
			wait.until(ExpectedConditions.elementToBeClickable(departureDepartureCity)).sendKeys(list.get(i).get("Departure City"));
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(departureDepartureDate)).sendKeys(list.get(i).get("Departure Date"));
			wait.until(ExpectedConditions.elementToBeClickable(departureArrivalCity)).sendKeys(list.get(i).get("Arrival City"));
			Thread.sleep(1000);

			wait.until(ExpectedConditions.elementToBeClickable(departureTicketCategory));
			Select departureTicketCategory1 = new Select(departureTicketCategory);
			departureTicketCategory1.selectByVisibleText(list.get(i).get("Ticket Category"));
			
			wait.until(ExpectedConditions.elementToBeClickable(departureTravelWithInsurance));
			Select departureTravelWithInsurance1 = new Select(departureTravelWithInsurance);
			departureTravelWithInsurance1.selectByVisibleText(list.get(i).get("Travel with insurance"));
			
			wait.until(ExpectedConditions.elementToBeClickable(departureExchangeable));
			Select departureExchangeable1 = new Select(departureExchangeable);
			departureExchangeable1.selectByVisibleText(list.get(i).get("Exchangeable"));
			
			if (!list.get(i).get("Price").equalsIgnoreCase("")) {
				departurePrice.clear();
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(departurePrice)).sendKeys(list.get(i).get("Price"));
			}
		}
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(saveDepartureInfo)).click();
		Thread.sleep(1000);	
		

		By saveButton = By.xpath("//select[contains(@id,'Departure_ExchangeableId')]//following::input[contains(@id,'TravelListRecords') and contains(@id,'Text') and @type='submit']");
		int i = 0;
		while ( (i<100) & (driver.findElements(saveButton).size() != 0) ) {
			Thread.sleep(100);
			i++;
		}
	}

	public void fillReturnInfo(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			Thread.sleep(1000);	
			wait.until(ExpectedConditions.elementToBeClickable(returnMeanOfTransport));
			Select returnMeanOfTransport1 = new Select(returnMeanOfTransport);
			returnMeanOfTransport1.selectByVisibleText(list.get(i).get("Mean of Transport"));
			
			wait.until(ExpectedConditions.elementToBeClickable(returnDepartureCity)).sendKeys(list.get(i).get("Departure City"));
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(returnDepartureDate)).sendKeys(list.get(i).get("Departure Date"));
			wait.until(ExpectedConditions.elementToBeClickable(returnArrivalCity)).sendKeys(list.get(i).get("Arrival City"));
			Thread.sleep(1000);
			
			wait.until(ExpectedConditions.elementToBeClickable(returnTicketCategory));
			Select returnTicketCategory1 = new Select(returnTicketCategory);
			returnTicketCategory1.selectByVisibleText(list.get(i).get("Ticket Category"));
			
			wait.until(ExpectedConditions.elementToBeClickable(returnTravelWithInsurance));
			Select returnTravelWithInsurance1 = new Select(returnTravelWithInsurance);
			returnTravelWithInsurance1.selectByVisibleText(list.get(i).get("Travel with insurance"));
			
			wait.until(ExpectedConditions.elementToBeClickable(returnExchangeable));
			Select returnExchangeable1 = new Select(returnExchangeable);
			returnExchangeable1.selectByVisibleText(list.get(i).get("Exchangeable"));
			if (!list.get(i).get("Price").equalsIgnoreCase("")) {
			
				returnPrice.clear();
				Thread.sleep(1000);	
				wait.until(ExpectedConditions.elementToBeClickable(returnPrice)).sendKeys(list.get(i).get("Price"));
			}
		}

		wait.until(ExpectedConditions.elementToBeClickable(saveReturnInfo)).click();
		Thread.sleep(1000);	
	}
	
	public void fillTravelTotalPrice(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		travelTotalPrice.click();
		for (int i = 0; i < list.size(); i++) {
			Thread.sleep(1000);
			travelTotalPrice.sendKeys(list.get(i).get("Price"));
			Thread.sleep(1000);
			while (!travelTotalPrice.getAttribute("value").contains(list.get(i).get("Price"))) {
				travelTotalPrice.clear();
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(travelTotalPrice)).sendKeys(list.get(i).get("Price"));
			}
			
			travelBasketPrice.click();
			Thread.sleep(1000);
		}
	}

	public void fillStayAccommodationsVehicle(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			Thread.sleep(1000);
			
			wait.until(ExpectedConditions.elementToBeClickable(accommodationTypeOfStay));
			Select accommodationTypeOfStay1 = new Select(accommodationTypeOfStay);
			accommodationTypeOfStay1.selectByVisibleText(list.get(i).get("Type of Stay"));
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(accommodationCity));
			wait.until(ExpectedConditions.elementToBeClickable(accommodationCity)).sendKeys(list.get(i).get("City"));
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(accommodationArrivalDate)).sendKeys(list.get(i).get("Arrival Date"));
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(accommodationDepartureDate)).sendKeys(list.get(i).get("Departure Date"));
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(accommodationNumberOfRooms)).clear();
			Thread.sleep(500);
			accommodationNumberOfRooms.sendKeys(list.get(i).get("Number of rooms"));
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(accommodationStayWithInsurance));
			Select accommodationStayWithInsurance1 = new Select(accommodationStayWithInsurance);
			accommodationStayWithInsurance1.selectByVisibleText(list.get(i).get("Stay with insurance"));
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(accommodationVehicleRental));
			Select accommodationVehicleRental1 = new Select(accommodationVehicleRental);
			accommodationVehicleRental1.selectByVisibleText(list.get(i).get("Vehicle Rental"));
			Thread.sleep(500);
			
			if (!list.get(i).get("Price").equalsIgnoreCase("")) {
				accommodationPrice.clear();
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(accommodationPrice)).sendKeys(list.get(i).get("Price"));
			}
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(saveAccommodationsInfo)).click();
		Thread.sleep(1000);	
		
		By saveButton = By.xpath("//select[contains(@id,'Stay_IncludesRentalId')]//following::input[contains(@id,'TravelListRecords') and contains(@id,'Text') and @type='submit']");
		int i = 0;
		while ((i<100) & (driver.findElements(saveButton).size() != 0)) {
			Thread.sleep(100);
			i++;
		}
	}
	
	public void clickAddAnotherDestination() throws InterruptedException {		
		
		Thread.sleep(2000);	
		wait.until(ExpectedConditions.elementToBeClickable(addAnotherDestination)).click();
	}
	
	public void checkIfAddAnotherDestinationIsDisable() throws InterruptedException {		
		
		Thread.sleep(1000);	
		
		assert (addAnotherDestination.getAttribute("disabled").equalsIgnoreCase("true"));
	}
	
	public void checkTotalBasketPrice(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String totalPrice;
		
		for (int i = 0; i < list.size(); i++) {
			
			totalPrice = list.get(i).get("Price");
			Thread.sleep(2000);
			System.out.println("Total Basket Price: " + totalBasketPrice.getAttribute("value"));
			System.out.println(totalPrice.contains(totalBasketPrice.getAttribute("value")));
			Thread.sleep(1500);
			assert (totalPrice.contains(totalBasketPrice.getAttribute("value")));
		}
	}
	
	public void clickAddStayOrVehicle() throws InterruptedException {		
		
		Thread.sleep(2000);	
		wait.until(ExpectedConditions.elementToBeClickable(addStayOrVehicle)).click();
	}
	
    public void clickAddAnotherTravel() throws InterruptedException {		
		
		Thread.sleep(2000);	
		wait.until(ExpectedConditions.elementToBeClickable(addAnotherTravel)).click();
		Thread.sleep(1000);	
	}

	public void checkIfAddStayOrVehicleIsDisable() throws InterruptedException {		
		
		Thread.sleep(1000);	
		assert (addStayOrVehicle.getAttribute("disabled").equalsIgnoreCase("true"));
	
	}
	
	
	/**
	 * PR38 - Phone Number Methods
	 */
	public void checkmobileMessageError(DataTable mobile) throws InterruptedException {

		
		List<Map<String, String>> list = mobile.asMaps(String.class, String.class);
		String message = "";
		
		for (int i = 0; i < list.size(); i++) {
			
			message = list.get(i).get("Message");			
		}		
		
		Thread.sleep(2000);
		
		assert (message.contains(mobileMessageError.getText()));
		System.out.println("Message Error: " + mobileMessageError.getText());
		Thread.sleep(2000);
		
	}
	
	public void changeMobilePhone(DataTable arg1) throws InterruptedException {
		
		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			
			mobile.clear();
			Thread.sleep(1000);
			mobile.sendKeys(list.get(i).get("Mobile Phone"));
			Thread.sleep(1000);
			email.click();
		}		
	}
	
	/**
	 * PR47 - Instore V2
	 */
	public void validateMerchant(String merchant) {
		
		System.out.println(comboMerchantName.getText());
//		System.out.println(merchant);
		assert (!comboMerchantName.getText().contains(merchant));
	}
	
	public void changeMerchant(String merchant) throws InterruptedException {
		comboMerchantNameClick.click();
		Thread.sleep(1000);
		comboMerchantNameSearch.sendKeys(merchant + Keys.ENTER);
	}
	
	public void validateAPIError() {
		wait.until(ExpectedConditions.elementToBeClickable(messageAPIError)).click();
	}
	
}
