package pageObjects.merchant;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardMerchant {
	
	private String externalRef = null;
	public WebDriverWait wait;
	public WebDriver driver;

	public DashboardMerchant(WebDriver driver) {

		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 60);
		this.driver = driver;
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	
	@FindBy(how = How.XPATH, using = "//div[@class='Logout_Icon OSInline']//a")
	private WebElement logoutMerchant;

	@FindBy(how = How.XPATH, using = "//a[@href='/Merchant/CreateContract.aspx']")
	private WebElement createContract;

	@FindBy(how = How.XPATH, using = "//a[@href='/Merchant/Contracts.aspx']")
	private WebElement contracts;

	@FindBy(how = How.XPATH, using = "//a[@href='/Merchant/Files.aspx']")
	private WebElement files;

	@FindBy(how = How.XPATH, using = "//a[@href='/Merchant/UserList.aspx']")
	private WebElement users;

	@FindBy(how = How.XPATH, using = "//span[@class='LinkDPhover']")
	private WebElement last30Days;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'AdvanceSearch') and contains(@id,'Link4')]")
	private WebElement lastYear;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'OneyMerchant_Theme') and contains(@href,'MainContent')]")
	private WebElement exportToExcel;

	@FindBy(how = How.XPATH, using = "//div[@class='Search_wrapper']//input")
	private WebElement searchCustomerName;
//	
//	@FindBy(how = How.XPATH, using = "//table[@class='TableRecords OSFillParent']//td[1]")
//	private WebElement matchUser;

	@FindBy(how = How.XPATH, using = "//table[contains(@id,'ContractTable')]//td[2]")
	private WebElement clientName;
	
	@FindBy(how = How.XPATH, using = "//table[contains(@id,'UserList')]//td[1]")
	private WebElement userName;

	@FindBy(how = How.XPATH, using = "//table[1]/thead[1]/tr[1]")
	private WebElement tableHeader;

	@FindBy(how = How.XPATH, using = "//span[contains(@class,'download')]")
	private WebElement fileDownload;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'Empty')]")
	private WebElement noFile;
	
	@FindBy(how = How.XPATH, using = "//a[@class='select2-choice']")
	private WebElement statusDropDown;
	
	@FindBy(how = How.XPATH, using = "//input[@id='s2id_autogen1_search']")
	private WebElement statusSearch;
	
	@FindBy(how = How.XPATH, using = "//div[@class='BreadcrumbsContainer PH']//following-sibling::div[2]//input")
	private WebElement newUser;
	
	@FindBy(how = How.XPATH, using = "//div[@class='Btn_Content']//div[3]//input")
	private WebElement cancelNewUser;
	
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'MerchantCombo')]")
	private WebElement merchantList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'ComboStores2')]")
	private WebElement merchantListContract;

	@FindBy(how = How.XPATH, using = "//input[@id='s2id_autogen1_search']")
	private WebElement searchCombo1;

	@FindBy(how = How.XPATH, using = "//input[@id='s2id_autogen2_search']")
	private WebElement searchCombo2;

	@FindBy(how = How.XPATH, using = "//span[@class='select2-match']")
	private WebElement resultCombo;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'RTDButton')]")
	private WebElement rtdButton;

	@FindBy(how = How.XPATH, using = "//div[not (contains(@id,'ContractStatusCombo2')) and contains(@id,'ContractStatusCombo')]")
	private WebElement statusListContract;

	@FindBy(how = How.XPATH, using = "//tr[1]//td[10]")
	private WebElement statusContract;

	@FindBy(how = How.XPATH, using = "//tr[1]//td[9]//preceding::td[4]")
	private WebElement externalReference;
	
	@FindBy(how = How.XPATH, using = "(//input[@class='Button'])[1]")
	private WebElement merchantContractDetails;
	
	@FindBy(how = How.XPATH, using = "(//td[@class='TableRecords_OddLine']//div)[2]")
	private WebElement newCodeField;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='WordNoSpace'])[2]")
	private WebElement newInitialBasketField;

	@FindBy(how = How.XPATH, using = "(//div[@class='WordNoSpace']//span)[1]")
	private WebElement newRemainingBasketField;
	
	@FindBy(how = How.XPATH, using = "(//input[not (contains(@disabled,'disabled')) and @type='submit'])[2]")
	private WebElement proceedToCancel;
	
	@FindBy(how = How.XPATH, using = "(//div[@align='right']//input)[1]")
	private WebElement addAmountToCancel;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'select2-container select')]//a[1]")
	private WebElement merchantReturnCombobox;
	
	public void clickLogoutMerchant() {

		logoutMerchant.click();
	}

	public void clickCreateContract() {

		createContract.click();
	}
	
//	/* Select the merchant in Create a Contract*/
//	public void selectMerchant(String merchantName) {
//		
//		Select drpMerchant = new Select(merchantList);
//		
//		drpMerchant.selectByVisibleText(merchantName);
//	}
	
	/*Select the merchant of dropdown in Contract menu*/
	public void selectMerchantCombo(String merchantName) {
		
		merchantListContract.click();
		searchCombo1.sendKeys(merchantName);
		resultCombo.click();
	}
	
	/*Select the status of dropdown in Contract menu*/
	public void selectStatusCombo(String statusName) {
		
		statusListContract.click();
		searchCombo2.sendKeys(statusName);
		resultCombo.click();

	}

	public void clickContracts() {

		contracts.click();
	}


	public void clickLast30Days() throws InterruptedException{
		Thread.sleep(1000);
		last30Days.click();
		lastYear.click();
	}

	public void clickExportToExcel() throws InterruptedException{
		Thread.sleep(1000);
		try{
			exportToExcel.click();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			WebElement exportToExcel1 = driver.findElement(By.xpath("//a[contains(@href,'OneyMerchant_Theme') and contains(@href,'MainContent')]"));
 		
			exportToExcel1.click();
		}
	}
		
	public void searchCustomer (String arg) throws InterruptedException {
			
		searchCustomerName.click();
		searchCustomerName.sendKeys(arg);
		
		Thread.sleep(20000);
		System.out.println("Client Name: " + clientName.getText());
	}
	
	public void searchUser (String arg) throws InterruptedException {
		
		searchCustomerName.click();
		searchCustomerName.sendKeys(arg);
		
		Thread.sleep(20000);
		System.out.println("User Name: " + userName.getText());	
		assert(userName.getText().contains(arg));
	}

	
	public void clickFiles() {

		files.click();
	}

	public void checkFilesTable() {
		assert(tableHeader.isDisplayed());

			tableHeader.click();
			System.out.println("File table: " + tableHeader.getLocation());
		
	}

	public void downloadTableFile() {

		try {
			
			fileDownload.click();

		} catch (Exception e) {

			System.out.println(noFile.getText());
		}
	}
	
	public void clickUsers() {

		users.click();
	}

	public void changeStatus(String status) throws InterruptedException {
		
		statusDropDown.click();
		Thread.sleep(500);
		statusSearch.sendKeys(status);
		Thread.sleep(500);
		statusSearch.sendKeys(Keys.ENTER);
		
	}

	public void clickNewUser() {
		
		newUser.click();
	}

	public void clickCancelNewUser() {
		
		cancelNewUser.click();
	}
	
	public void clickRTDButton() throws InterruptedException {
		
		Thread.sleep(3000);
		rtdButton.click();
		By nextButton = By.xpath("(//input[contains(@id,'RTDButton')])[1]"); 
	    int i=0;
		while ((i<120) &(driver.findElements(nextButton).size() != 0)) { 
		      Thread.sleep(100);
		      i++;		     
		  }
	}
	
	public void validateContractStatus(String status) throws InterruptedException {
		
		Thread.sleep(5000);
		System.out.println(statusContract.getText());
		assert (statusContract.getText().equalsIgnoreCase(status));
	}
	
	public void getExternalReference() {
		
		externalRef = externalReference.getText();
		System.out.println(externalRef);
		
	}
	
	public String getexternalRef() {
		return externalRef;
	}

	public void validateNewRemainingBasketField() throws InterruptedException {

		Thread.sleep(3000);
		newRemainingBasketField.click();
	}

	public void validateNewInitialBasketField() throws InterruptedException {

		newInitialBasketField.click();
	}

	public void validateNewCodeField() throws InterruptedException {
		
		newCodeField.click();
	}

	public void validateMerchantContractDetails() throws InterruptedException {

		Thread.sleep(12000);
		merchantContractDetails.click();
	}

	public void validatecancelPartially() {

		addAmountToCancel.click();
		addAmountToCancel.sendKeys("1");
	}
	
	public void validateAnMerchantCancellationReturn() throws InterruptedException {

		merchantReturnCombobox.click();
		merchantReturnCombobox.sendKeys("Qa automation");
		merchantReturnCombobox.sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		proceedToCancel.click();
	}
	
	public void submitPartialCancellation() throws InterruptedException {

		proceedToCancel.click();
		Thread.sleep(3000);
		
	}
}
