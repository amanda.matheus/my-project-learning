package pageObjects.eCommerce;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePageECommerce {

	public WebDriverWait wait;
	public WebDriver driver;

	public HomePageECommerce(WebDriver driver) {

		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 60);
		this.driver = driver;
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Bollinger RD')]//following::div[3]")
	private WebElement bollingerRD;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Bollinger RD 1995')]//following::input[1]")
	private WebElement bollingerRDQuantity;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Bruno Giacosa Santo Stefano')]//following::div[3]")
	private WebElement brunoGiacosa;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Altos de Luzon')]//following::div[3]")
	private WebElement altosDeLuzon;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Bruno Giacosa Santo Stefano 2004')]//following::input[1]")
	private WebElement brunoGiacosaQuantity;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Altos de Luzon')]//following::input[1]")
	private WebElement altosDeLuzonQuantity;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Saca Rolhas 3')]//following::div[3]")
	private WebElement sacaRolhas3;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Saca Rolhas 3')]//following::input[1]")
	private WebElement sacaRolhas3Quantity;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Saca Rolhas 2')]//following::div[3]")
	private WebElement sacaRolhas2;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Saca Rolhas 2')]//following::input[1]")
	private WebElement sacaRolhas2Quantity;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Saca Rolhas 1')]//following::div[3]")
	private WebElement sacaRolhas1;
	
	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Saca Rolhas 1')]//following::input[1]")
	private WebElement sacaRolhas1Quantity;

	@FindBy(how = How.XPATH, using = "//input[@value='Search']//preceding::input[1]")
	private WebElement search;

	@FindBy(how = How.XPATH, using = "//input[@value='Search']")
	private WebElement searchButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'CheckoutButton')]")
	private WebElement checkoutCart;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Barca Velha')]//following::div[3]")
	private WebElement barcaVelha;

	@FindBy(how = How.XPATH, using = "//td[contains(text(),'Barca Velha 2000')]//following::input[1]")
	private WebElement barcaVelhaQuantity;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Greenock')]//following::div[contains(@class,'AddToCart')]/input)[1]")
	private WebElement greenock;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'Chateau Monbousquet')]//following::div[contains(@class,'AddToCart')]/input)[1]")
	private WebElement chateauMonbousquet;
	
	public void addGreenock() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", greenock);
		Thread.sleep(2500);
		greenock.click();
		Thread.sleep(3500);
	}
	
	public void addChateauMonbousquet() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", chateauMonbousquet);
		Thread.sleep(2500);
		chateauMonbousquet.click();
	}
	
	public void addBollingerRdOnClick() throws InterruptedException {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", search);
		Thread.sleep(2500);
//		TOTAL = 399,75 
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Bollinger RD");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(bollingerRD)).click();
		Thread.sleep(4000);
	}
	


	public void addBollingerRd(String nrItems) throws InterruptedException {
		
//		TOTAL = 399,75 * nrItems
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Bollinger RD");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(bollingerRD)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(bollingerRDQuantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(bollingerRDQuantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(bollingerRDQuantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(bollingerRDQuantity)).click();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();
	}

	public void addBarcaVelha(String nrItems) throws InterruptedException {
		
//		TOTAL = 335,61 * nrItems
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Barca Velha");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(barcaVelha)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(barcaVelhaQuantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(barcaVelhaQuantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(barcaVelhaQuantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(barcaVelhaQuantity)).click();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();
	}
	
	public void addBrunoGiacosaSantoStefano(String nrItems) throws InterruptedException {
		
//		TOTAL = 299,00 * nrItems
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Bruno Giacosa Santo Stefano");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(brunoGiacosa)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(brunoGiacosaQuantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(brunoGiacosaQuantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(brunoGiacosaQuantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(brunoGiacosaQuantity)).click();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();	
	}
	
	public void addAltosDeLuzon(String nrItems) throws InterruptedException {
		
//		TOTAL = 1013,61
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Altos de Luzon");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(altosDeLuzon)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(brunoGiacosaQuantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(altosDeLuzonQuantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(altosDeLuzonQuantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(altosDeLuzonQuantity)).click();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();	
	}
	
	public void addSacaRolha3(String nrItems) throws InterruptedException {
		
//		TOTAL = 0,50 * nrItems
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Saca Rolhas 3");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas3)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas3Quantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas3Quantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas3Quantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas3Quantity)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();
	}
	
	public void addSacaRolhas1(String nrItems) throws InterruptedException {
		
		// TOTAL = 0,01 * nrItems
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Saca Rolhas 1");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas1)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas1Quantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas1Quantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas1Quantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas1Quantity)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();
	}
	
	public void addSacaRolhas2(String nrItems) throws InterruptedException {
		
		// TOTAL = 0,05 * nrItems
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).sendKeys("Saca Rolhas 2");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(searchButton)).click();
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas2)).click();
		Thread.sleep(2000);
		clickCheckoutItems();
		Thread.sleep(2000);
		
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas2Quantity)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas2Quantity)).sendKeys(Keys.CONTROL, Keys.LEFT_SHIFT, Keys.ARROW_LEFT);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas2Quantity)).sendKeys(nrItems);
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sacaRolhas2Quantity)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(search)).clear();
	}
			
	public void clickCheckoutItems() throws InterruptedException {
	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", checkoutCart);
		Thread.sleep(4000);
//		searchButton.click();
		checkoutCart.click();
	}
}
