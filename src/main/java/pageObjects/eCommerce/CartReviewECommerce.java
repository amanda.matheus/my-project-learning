package pageObjects.eCommerce;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CartReviewECommerce {

	public CartReviewECommerce(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
	
	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'Proceed')]")
	private WebElement proceedShipping;

	@FindBy(how = How.XPATH, using = "//th[contains(text(),'Product')]//following::div[10]")
	private WebElement item1;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/table[1]/tbody[1]/tr[2]/td[1]/div[2]/div[5]/span[1]/table[1]/tbody[1]/tr[1]/td[4]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]")
	private WebElement item1Quantity;

	@FindBy(how = How.XPATH, using = "//th[contains(text(),'Product')]//following::div[15]")
	private WebElement item2;

	@FindBy(how = How.XPATH, using = "//th[contains(text(),'Product')]//following::div[20]")
	private WebElement item3;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/table[1]/tbody[1]/tr[2]/td[1]/div[2]/div[5]/span[1]/table[1]/tbody[1]/tr[4]/td[4]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]")
	private WebElement item4;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/table[1]/tbody[1]/tr[2]/td[1]/div[2]/div[5]/span[1]/table[1]/tbody[1]/tr[2]/td[4]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]")
	private WebElement item2Quantity;
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/table[1]/tbody[1]/tr[2]/td[1]/div[2]/div[5]/span[1]/table[1]/tbody[1]/tr[3]/td[4]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]")
	private WebElement item3Quantity;	
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/form[1]/div[3]/div[2]/table[1]/tbody[1]/tr[2]/td[1]/div[2]/div[5]/span[1]/table[1]/tbody[1]/tr[4]/td[4]/div[1]/table[1]/tbody[1]/tr[1]/td[2]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/img[1]")
	private WebElement item4Quantity;
	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Total Price')]//following::div[1]")
	private WebElement amountCart;

	public void clickProceedShipping() {

		proceedShipping.click();
	}

//	public void validate1stItemQuantity() {
//
//		if (!item1.getText().contains("€975.00")) {
//
//			item1Quantity.sendKeys("3");
//		}
//	}
//
//	public void validate2ndItemQuantity() {
//
//		if (!item2.getText().contains("€897.00")) {
//
//			item2Quantity.sendKeys("3");
//		}
//	}

	
//	public void validate4ndItemQuantity(String arg1) {
//
//		System.out.println(item4.getAttribute("value"));
//		if (!item4.getAttribute("value").equals(arg1)) {
//
//			item4Quantity.click();
//		}
//	}
	
//	public void validateCartAmount(String arg1) {
//		
//		String amountCartNew = amountCart.getText().replace(",", "").replace("€", "").replace(".", ",");
//		System.out.println("Amount: " + arg1);
//
//		assert (amountCartNew.equals(arg1));
//
//		System.out.println("Cart Amount: " + amountCartNew);
//			
//	}
}
