package pageObjects.eCommerce;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import net.bytebuddy.utility.RandomString;
import pageObjects.subscription.PaymentPageSubscription;

public class CheckoutECommerce {

	public WebDriverWait wait;
	public WebDriver driver;
	public static String customerEmail;

	public CheckoutECommerce(WebDriver driver) {

		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 60);
		this.driver = driver;
	}

	/* 
	 * Defines WebElement so they can be called in the function
	 * 
	 */
	
	@FindBy(how = How.XPATH, using = "//td[text() = 'Address']//following::input[1]")
	private WebElement address;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Postal Code']//following::input[1]")
	private WebElement postalCode;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Municipality']//following::input[1]")
	private WebElement municipality;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Country']//following::select[1]")
	private WebElement country;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Street Number']//following::input[1]")
	private WebElement streetNumber;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Floor']//following::input[1]")
	private WebElement floor;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Save and Review Order')]")
	private WebElement saveAndReviewOrder;

	@FindBy(how = How.XPATH, using = "//input[contains(@value,'Proceed to Checkout')]")
	private WebElement proceedCheckout;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Honorific']//following::select[1]")
	private WebElement honorific;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Person type']//following::select[1]")
	private WebElement personType;

	@FindBy(how = How.XPATH, using = "//td[text() = 'First Name']//following::input[1]")
	private WebElement firstName;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Last Name']//following::input[1]")
	private WebElement lastName;

	@FindBy(how = How.XPATH, using = "//td[text() = 'BirthDate']//following::input[1]")
	private WebElement birthDate;

	@FindBy(how = How.XPATH, using = "//td[text() = 'eMail']//following::input[1]")
	private WebElement email;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Place']//following::input[1]")
	private WebElement place;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Mobile Phone']//following::input[1]")
	private WebElement mobilePhone;

	@FindBy(how = How.XPATH, using = "//td[text() = 'Tenant To Submit']//following::select[1]")
	private WebElement TenantSubmit;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'3x (with fees)')]")
	private WebElement withFees3x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'PL14F')]")
	private WebElement PL14F;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'3x (without fees)')]")
	private WebElement withoutFees3x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'4x (with fees)')]")
	private WebElement withFees4x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'4x (without fees)')]")
	private WebElement withoutFees4x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'5x (with fees)')]")
	private WebElement withFees5x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'6x (with fees)')]")
	private WebElement withFees6x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'8x (with fees)')]")
	private WebElement withFees8x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'8x (without fees)')]")
	private WebElement withoutFees8x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'10x (with fees)')]")
	private WebElement withFees10x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'12x (with fees)')]")
	private WebElement withFees12x;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'12x (without fees)')]")
	private WebElement withoutFees12x;

	@FindBy(how = How.XPATH, using = "//div[@class='Feedback_Message_Error']")
	private WebElement mobileMessageError;
//
//	@FindBy(how = How.XPATH, using = "//span[contains(@id,'ValidationMessage') and contains(@id,'wtPrefixPhoneNumber_block_wtPhoneNumberInput')]")
//	private WebElement mobileMessageErrorPT;

	@FindBy(how = How.XPATH, using = "//div[@class='Feedback_Message_Error']//img[contains(@src,'Close.png')]")
	private WebElement mobileMessageErrorX;

	@FindBy(how = How.XPATH, using = "//span[text() = 'Holder Name']//following::input[1]")
	private WebElement holderName;

	@FindBy(how = How.XPATH, using = "//span[text() = 'BIC Code']//following::input[1]")
	private WebElement bic;

	@FindBy(how = How.XPATH, using = "//span[text() = 'IBAN Code']//following::input[1]")
	private WebElement iban;

	public void insertShippingAddress(DataTable arg1) throws InterruptedException {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			address.sendKeys(list.get(i).get("Address"));
			municipality.sendKeys(list.get(i).get("Municipality"));
			postalCode.sendKeys(list.get(i).get("Postal Code"));

			Select drpCountry = new Select(country);

			country.click();
			Thread.sleep(500);
			drpCountry.selectByVisibleText(list.get(i).get("Country"));

			streetNumber.sendKeys(list.get(i).get("StreetNumber"));
			floor.sendKeys(list.get(i).get("Floor"));
		}
	}

	public void clickSaveReviewOrder() {

		saveAndReviewOrder.click();
	}

	public void clickProceedCheckout() {

		proceedCheckout.click();
	}

	public void insertPaymentInformationData(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			Select drpHonorific = new Select(honorific);
			honorific.click();
			drpHonorific.selectByVisibleText(list.get(i).get("Honorific"));

			Select drpPersonType = new Select(personType);
			personType.click();
			drpPersonType.selectByVisibleText(list.get(i).get("PersonType"));

			firstName.sendKeys(list.get(i).get("First name"));
			lastName.sendKeys(list.get(i).get("Last name"));
			birthDate.sendKeys(list.get(i).get("Birth Date"));
			place.sendKeys(list.get(i).get("Place"));
			mobilePhone.sendKeys(list.get(i).get("Mobile Phone"));

			Select drpTenant = new Select(TenantSubmit);
			drpTenant.selectByVisibleText(list.get(i).get("Tenant"));
		}
	}

	public void insertEmail(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String emailarg = "";

		for (int i = 0; i < list.size(); i++) {
			emailarg = list.get(i).get("Email");
		}

		if (emailarg.equalsIgnoreCase("")) {

			String randomEmail = "testing-" + RandomString.make(10) + "@gmail.com";
			email.sendKeys(randomEmail);
			customerEmail = randomEmail;
		} else {

			email.sendKeys(emailarg);
		}
	}

//	public void insertEmailExistingCustomer(DataTable arg1) {
//
//		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
//		String emailarg = null;
//
//		for (int i = 0; i < list.size(); i++) {
//
//			emailarg = list.get(i).get("Email");
//		}
//		if (emailarg == null) {
//
//			String randomEmail = "testing-" + RandomString.make(10) + "@gmail.com";
//			email.sendKeys(randomEmail);
//		} else {
//			this.email.sendKeys(emailarg);
//		}
//	}

	public void insertAndSaveEmail(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String emailarg = "";

		for (int i = 0; i < list.size(); i++) {

			emailarg = list.get(i).get("Email");
		}
		if (emailarg.equalsIgnoreCase("")) {

			PaymentPageSubscription.RANDOM_EMAIL = "testing-" + RandomString.make(10) + "@gmail.com";
			email.sendKeys(PaymentPageSubscription.RANDOM_EMAIL);
		} else {

			email.sendKeys(emailarg);
		}
	}

	public void click3xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees3x.click();
	}
	
	public void clickPL14F() throws InterruptedException {

		Thread.sleep(500);
		PL14F.click();
	}


	public void click3xWithoutFees() throws InterruptedException {

		Thread.sleep(500);
		withoutFees3x.click();
	}

	public void click4xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees4x.click();
	}

	public void click4xWithoutFees() throws InterruptedException {

		Thread.sleep(500);
		withoutFees4x.click();
	}

	public void click5xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees5x.click();
	}

	public void click6xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees6x.click();
	}

	public void click8xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees8x.click();
	}

	public void click8xWithoutFees() throws InterruptedException {

		Thread.sleep(500);
		withoutFees8x.click();
	}

	public void click10xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees10x.click();
	}

	public void click12xWithFees() throws InterruptedException {

		Thread.sleep(500);
		withFees12x.click();
	}

	public void click12xWithoutFees() throws InterruptedException {

		Thread.sleep(500);
		withoutFees12x.click();
	}

	public void checkmobileMessageError(DataTable mobile) throws InterruptedException {

		List<Map<String, String>> list = mobile.asMaps(String.class, String.class);
		String message = "";
		
//		if (driver.getCurrentUrl().contains("oney.pt")) {
//
//			for (int i = 0; i < list.size(); i++) {
//
//				message = list.get(i).get("Message");
//
//				assert (message.contains(wait.until(ExpectedConditions.visibilityOf(mobileMessageErrorPT)).getText()));
//			}

//			if (!driver.getCurrentUrl().contains("oney.pt")) {

				for (int i = 0; i < list.size(); i++) {

					message = list.get(i).get("Message");
			
				}
				
				assert (message.contains(wait.until(ExpectedConditions.visibilityOf(mobileMessageError)).getText()));
				System.out.println("Message Error: " + mobileMessageError.getText());
				wait.until(ExpectedConditions.elementToBeClickable(mobileMessageErrorX)).click();
//			}
//		}
	}

	public void changeMobilePhone(DataTable arg1) {

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {

			mobilePhone.clear();
			mobilePhone.sendKeys(list.get(i).get("Mobile Phone"));
		}
	}
	
	public void insertBankingInfo(DataTable arg1) {
		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);


        for (int i = 0; i < list.size(); i++) {


            holderName.sendKeys(list.get(i).get("Holder Name"));
            bic.sendKeys(list.get(i).get("BIC Code"));
            iban.sendKeys(list.get(i).get("IBAN Code"));

        }
	}

	public void validateAPIError() {
		wait.until(ExpectedConditions.elementToBeClickable(mobileMessageErrorX)).click();
	}
	
}
