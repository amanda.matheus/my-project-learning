@changePassword
Feature: Change Password

  @ChangeAllPassword
  Scenario: Change Password BackOffice
    Given navigate to "Tenant Management"
    When enter a valid username and password
    Then change all passwords
      | Tenant | User              | Password          |
      | RO     | QAAutomationRO    | Oney1testes       |
      | BE     | admin@FacilyPayBE | admin@FacilyPayBE |
      | IT     | admin@FacilyPayIT | admin@FacilyPayIT |
      | PT     | admin@FacilyPayPT | admin@FacilyPayPT |
      | RO     | admin@FacilyPayRO | admin@FacilyPayRO |
      | SP     | admin@FacilyPayES | admin@FacilyPayES |
      | DE     | admin@FacilyPayDE | admin@FacilyPayDE |
      | BE     | QAAutomationBE    | Oney1testes       |
      | IT     | QAAutomationIT    | Oney1testes       |
      | PT     | QAAutomationPT    | Oney1testes       |
      | SP     | QAAutomationES    | Oney1testes       |
