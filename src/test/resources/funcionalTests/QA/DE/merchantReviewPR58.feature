#Requirements - LT1 - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/1812463617/PR58+LT1+FATA+-+Merchant+Space+Review
#Author: Willian Arruda
@all @PR58 @PR58DE @newComponentsContractPage @DE
Feature: Merchant Review (PR58 LT1) - Germany
  Add more information into contract details and the cancellation process will be modified:
  -Cancellation process made by item quantity and total amount 
  -New fields in contract detail


  @newComponentsContractPagePR58DE @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant DE"
    And enter a valid username and password
    When click Contracts
    And select the "Financed" status on the list
    And validate New Contract Tab Fields
    Then cancel The Contract Partially
