@all @createContracteCommerce @createContracteCommerceDE @DE @criticalFeatures
Feature: Create Contract eCommerce - Germany
  Create contract with QA Automation business transactions inside eCommerce

  @createContractNewUsereCommerceDE3xFees 
  Scenario: eCommerce: 3x with fees with 11 items in the cart
    Given navigate to "eCommerce BE"
    # quando existir um url para DE alterar o step acima
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address     | Municipality      | Postal Code | Country | StreetNumber | Floor |
      | Gotenstraße | Nordrhein-Westfalen |       53937 | Germany |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email                     | Birth Date | Place     | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | carlos.xavier@ablewise.pt | 1995-03-26 | Schleiden | +49692193140 | DE - QA Automation Web |
    And insert the banking information
      | Holder Name    | BIC Code | IBAN Code    |
      | Willian Arruda | BICCGD   | 123654789654 |
    And choose the payment type "3x (with fees)"
    Then validate Contract Creation DE

  @createContractNewUsereCommerceDEPL14DF 
  Scenario: eCommerce: 3x with fees with 11 items in the cart
    Given navigate to "eCommerce BE"
    # quando existir um url para DE alterar o step acima
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address     | Municipality      | Postal Code | Country | StreetNumber | Floor |
      | Langenhorner Chaussee | Freistaat Bayern |       83416 | Germany |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email                     | Birth Date | Place     | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | carlos.xavier@ablewise.pt | 1995-03-26 | Saaldorf | +49692193140 | DE - EDENLY |
    And insert the banking information
      | Holder Name    | BIC Code | IBAN Code    |
      | Willian Arruda | BICCGD   | 123654789654 |
    And choose the payment type "PL14F"
    Then validate Contract Creation DE
    
    