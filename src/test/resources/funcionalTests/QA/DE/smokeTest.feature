@all @smokeTest @smokeTestDE @criticalFeatures @DE @critical
Feature: Smoke tests - Germany

  ##----------------------------------------------------------------------------------
  ##														BackOffice
  ##----------------------------------------------------------------------------------
  @payLaterContractBackOfficeDE @smokeTestBackOfficeDE
  Scenario: Validate the Pay Later contracts
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    Then validate the Pay Later contracts

  @paymentMethodBackOfficeDE @smokeTestBackOfficeDE
  Scenario: Validate the Payment Method
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    Then validate the Pay Later contracts

  @contractBackOfficeDE @smokeTestBackOfficeDE
  Scenario: Search on going contract
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 |          |       |              |
    And click on the edit button
    Then validate if the page contains the correct information

  @customersBackOfficeDE @smokeTestBackOfficeDE
  Scenario: Search for a costumer by email address
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    Then click the reset button

  @merchantBackOfficeDE @smokeTestBackOfficeDE
  Scenario: Check the Merchants menu
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Merchants menu
    * click on New Merchant
    * return to the previous page by clicking on the Merchants link
    * click on the view icon of any merchant
    * return to the previous page by clicking on the Merchants link
    Then click on the edit icon of any merchant

  @brandsBackOfficeDE @smokeTestBackOfficeDE
  Scenario: Check the Brands menu
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    When click on the Brands menu
    * click on New Brand
    * return to the previous page by clicking on the Brands link
    * click on the view icon of any brand
    * return to the previous page by clicking on the Brands link
    Then click on the edit icon of any brand

  ##----------------------------------------------------------------------------------
  ##														Merchant
  ##----------------------------------------------------------------------------------
  @logoutMerchantDE @smokeTestMerchantDE
  Scenario: Login and Logout with valid credentials
    Given navigate to "Merchant DE"
    And enter a valid username and password
    * should be able to logout

  @forgotPassworMerchantDE @smokeTestMerchantDE
  Scenario: Request a new password
    Given navigate to "Merchant DE"
    And enter a valid username and password
    * should be able to logout
    * click on the forgot password
    * insert my email
    * should receive a new password

  @contractsMerchantDE @smokeTestMerchantDE
  Scenario: Search for cancelled contracts
    Given navigate to "Merchant DE"
    And enter a valid username and password
    * click Contracts
    * verify cancelled contracts
    * click export to excel
    * click on the last 30 days link

  @contractsSearchMerchantDE @smokeTestMerchantDE
  Scenario: Search for a customer name
    Given navigate to "Merchant DE"
    And enter a valid username and password
    * click Contracts
    * search for the customer
      | Name   | Email | Webpage  |
      | Carlos |       | Merchant |

  @filesMerchantDE @smokeTestMerchantDE
  Scenario: Check for files
    Given navigate to "Merchant DE"
    And enter a valid username and password
    * click Files
    * check if contract table is displayed
    * click download if any file exists

  @usersMerchantDE @smokeTestMerchantDE
  Scenario: Validate existing user and create a new one
    Given navigate to "Merchant DE"
    And enter a valid username and password
    * click Users
    * change status to "Inactive"
    * change status to "Status"
    * search for the user "QAAutomationDE"
    * click on New User
    * click on Cancel on the new user page
