#@all @createContracteCommerce @createContracteCommerceDE @DE @criticalFeatures
#Feature: Create Contract API - Germany
  #Create contract with QA Automation business transactions with Postman
#
  #@CreateContractDEPL30DF @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_QAW_PL30DF_Invoice" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_PL30DF" file
    #Then click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
#
  #@CreateContractDEPL14DF @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_QAW_PL14DF_Invoice" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_PL14DF" file
    #Then click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
#
  #@CreateContractDEPL14DFCancel @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_QAW_PL14DF_Invoice" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_PL14DF" file
    #Then click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
#
  #@CreateContractDE3XM @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_LoanSimulation_QAW_3XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Installments_QAW_3XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_3XM" file
    #And click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
#
  #@CreateContractDE4XM @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_LoanSimulation_QAW_4XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Installments_QAW_4XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_4XM" file
    #And click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
#
  #@CreateContractDE3XM @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_LoanSimulation_QAW_3XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Installments_QAW_3XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_3XM" file
    #And click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
    #
  #@CreateContractDE4XM @createContracteCommerce
  #Scenario: Create DE contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_LoanSimulation_QAW_4XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Installments_QAW_4XM" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_4XM" file
    #And click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
