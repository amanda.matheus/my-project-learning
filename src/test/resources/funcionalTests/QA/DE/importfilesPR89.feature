#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2611150896/PR89+Brand+Merchant+user+creation+by+import+files
#Author: Willian Arruda
@all @PR89 @projectFeatures  @PR89DE 
Feature: Creation by import files (PR89) - Germany
  This feature aims to import and create Brand, Merchant and Users through Excel.


  @brandImportPR89DE
  Scenario: to Import and crate a brand
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    When click on the Brands menu
    And creating An Excel for "Brand" Import
    And import "Brand" file
    And validate the creation
    

  @merchantImportPR89DE 
  Scenario: to Import and crate a Merchant
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    When click on the Merchants menu
    And creating An Excel for "MerchantDE" Import
    Then import "MerchantDE" file
    And validate the creation

  @userImportPR89DE
  Scenario: to Import and crate a user
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    When click on userprofile Menu and Submenu
    And creating An Excel for "Users" Import
    Then import "Users" file
    And validate the creation