#@all @cancel @cancelDE @actionsToContract @projectFeatures @DE
#Feature: Cancel - Germany
  #Perform a total and partial cancelation on products in the contract
#
  #@totalCancelDE @createContracteCommerce
  #Scenario: DE - Total Cancelation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_QAW_PL30DF_Invoice" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_PL30DF" file
    #And click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
    #Given click on close tab
    #When click on the "QA_DE_CancelTotal_QAW_PL30DF" file
    #And click on the body tab
    #And click on two pane view
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action |
      #| DE     | CANCELLED | Cancel |
    #Given navigate to "BackOffice DE"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* check if the status is "CANCELLED"
    #When click on the edit button
    #* validate the Total Amount Financed on the Contract Info is "399,75"
    #* validate the New Amount Financed on the Contract Info is "0,00"
#
  #And click on Contract Transactions tab
  #Then validate the Contract Transactions status is "Cancelled"
  #@partialCancelDE @createContracteCommerce
  #Scenario: DE - Partial Cancelation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_DE_QAW_PL14DF_Invoice" file
    #And click on the body tab
    #And click on the send button
    #* click on close tab
    #When click on the "QA_DE_Confirm_QAW_PL14DF" file
    #And click on the body tab
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action          |
      #| DE     | FUNDED   | ContractCreated |
    #* click on close tab
    #When click on the "QA_DE_CancelPartial_QAW_PL14DF" file
    #And click on the body tab
    #And click on two pane view
    #And click on the send button
    #Then validate if the response
      #| Tenant | Response | Action |
      #| DE     | FUNDED   | Cancel |
    #Given navigate to "BackOffice DE"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* check if the status is "ONGOING"
    #When click on the edit button
    #* validate the Total Amount Financed on the Contract Info is "399,75"
    #* validate the New Amount Financed on the Contract Info is "299,75"
    #And click on Contract Transactions tab
    #Then validate the Contract Transactions status is "Pending"
