#@apiQARO @createContracteCommerce @apiQA
#Feature: Create Contracs by API - Romania
#
  ##----------------------------------------------------------------------------------
  ##                   eCommerce
  ##----------------------------------------------------------------------------------
  #@QA_RO_3x001_194.36NC
  #Scenario: 3x001 | 194.36 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_3x001_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_RO_3x002_194.36NC
  #Scenario: 3x002 | 194.36 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_3x002_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_RO_4x001_399.75NC
  #Scenario: 4x001 | 399.75 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x001_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_RO_4x002_399.75NC
  #Scenario: 4x002 | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x002_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_RO_4x001_399.75NC_GDPR
  #Scenario: 4x001 | 399.75 | NC GDPR
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x001_399.75NC_GDPR" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_RO_3x001_399.75EC
  #Scenario: 3x001 | 399.75 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_3x001_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #* login as "ro@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_RO_3x002_399.75EC
  #Scenario: 3x002 | 399.75 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_3x002_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #* login as "ro@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_RO_4x001_194.36EC
  #Scenario: 4x001 | 194.36 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x001_194.36EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #* login as "ro@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_RO_4x002_194.36EC
  #Scenario: 4x002 | 194.36 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x002_194.36EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #* login as "ro@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Recollections
  ##----------------------------------------------------------------------------------
  #@QA_RO_4x001_194.36NC
  #Scenario: 4x001 | 194.36 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x001_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_RO_4x001_194.36NCH
  #Scenario: 4x001 | 194.36 | NCH
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4x001_194.36NCH" file
    #And click on the body tab
    #And click on the send button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
 ##----------------------------------------------------------------------------------
 ##                   eCommerce - PR33
 ##----------------------------------------------------------------------------------
  #
  #@QA_RO_3XP_399.75NC
  #Scenario: 3XP | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_3XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
  #
  #@QA_RO_4XP_399.75NC
  #Scenario: 4XP | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_4XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
    #
    #@QA_RO_6XP_399.75NC
  #Scenario: 6XP | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_6XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill the additional fields
      #| Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      #| Tester     | Automation |           1000 |              300 | Employer           |
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
    