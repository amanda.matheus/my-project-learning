#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2088075265/PR63+FATA+-+New+Fields+3x4x+Contract+Form
#Author: Amanda Matheus
@all @PR63 @PR63RO @RO @criticalFeatures
Feature: New Fields 3x4x Contract Form (PR63) - Romania
  Additional fields in Subscription. The additional fields are displayed only to purchase amount greater that 1.000,00 €

  @webNCPR63RO
  Scenario: Web contract with the additional fields for new customers.
    Given navigate to "eCommerce RO"
    And add products to a total amount of "1870,97" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 1821110014039 |
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | Tester     | Automation |           1000 |              300 | Employer           |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @instoreECPR63RO
  Scenario: Instore contract with the additional fields for existing customers.
    Given navigate to "Merchant RO"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price   |
      | Bollinger RD | Cars & motorbikes |        1 | 1870.97 |
    When choose "6x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    And follow the "link" to finish my subscription
    And login as "ro@gmail.com" in the subscription page
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | Tester     | Automation |           1000 |              300 | Employer           |
    And accept the contract terms
    Then validate that the contract was successfully created
