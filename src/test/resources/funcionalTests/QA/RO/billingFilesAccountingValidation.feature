@billingFilesValidation @billingFilesValidationRO  @projectFeatures
Feature: Billing Files Daily Validation - Romania
  Verify if the files are being generated daily

  ##----------------------------------------------------------------------------------
  ##                   Billing files validation
  ##----------------------------------------------------------------------------------
  
  @financingFileValidationRO
  Scenario: Validate the generation of yesterday s financing file	
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Billing files submenu
    Then validate if financing file was generated
    
    
 @invoicingFileValidationRO 
  Scenario: Validate the generation of yesterday s invoicing file
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Billing files submenu
    And click on the invoicing file tab
    Then validate if invoicing file was generated