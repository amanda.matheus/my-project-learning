@all @createContractInstore @createContractInstoreRO @RO
Feature: Create Contract Instore - Romania
  Create contract with QA Automation business transactions inside Merchant Space

  ##----------------------------------------------------------------------------------
  ##Instore - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUserMerchantRO3xWithFees @criticalFeatures
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address                  | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 |       | Strada Ștefan cel Mare 1 |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Constanța     | Constanța   | Constanța    | Romanian    |          | 2821110138053 |          123456 |
    * follow the "link" to finish my subscription
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantRO3xWithoutFees @criticalFeatures
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 |       | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110016253 |          123456 |
    * follow the "link" to finish my subscription
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantRO4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 |       | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110017626 |          123456 |
    * follow the "link" to finish my subscription
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantRO4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 |       | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110015761 |          123456 |
    * follow the "link" to finish my subscription
    Then insert a valid payment card
    Then validate that the contract was successfully created

  ##----------------------------------------------------------------------------------
  ##       Instore - Create Contract Existing Customer
  ## ----------------------------------------------------------------------------------
  @createContractExistingUserMerchantRO3xWithFees @criticalFeatures
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 194.36 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    * follow the "link" to finish my subscription
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantRO3xWithoutFees @criticalFeatures
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    * follow the "link" to finish my subscription
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantRO4xFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    * follow the "link" to finish my subscription
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantRO4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    * follow the "link" to finish my subscription
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantRO4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    * follow the "link" to finish my subscription
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantRO4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 194.36 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Amanda     | Matheus   |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    * follow the "link" to finish my subscription
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created
