#Author: Amanda Matheus
@all @engineDecision @engineDecisionRO @criticalFeatures @RO @critical
Feature: Engine Decision - Romania
  Verify if the contract is rejected by Engine Decision rules.

  @rulebookAgeContractRO
  Scenario: Validate the engine decision rulebook rejection
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Engine     | Decision  |       | 2002-11-10 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 5021110019456 |
    Then insert a valid payment card
    Then validate that the contract was rejected

  @rulebookAgeBackOfficeRO
  Scenario: Validate the rejection reason in BackOffice
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name   | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Engine Decision | 194.36 |    7 |          |       |              |
    And click on the edit button
    When click on Decision tab
    Then validate if the rejection type is "Motor de decizie"
    And validate if the rejection reason is "Age between 18 and 21"
