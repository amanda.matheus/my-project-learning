@all @cancelEarlySettlement @cancelEarlySettlementRO @actionsToContract @projectFeatures @RO
Feature: Cancel & Early Settlement - Romania
  Perform a cancel and early settlement on products in the contract

  ##----------------------------------------------------------------------------------
  ##                               Cancellation
  ##----------------------------------------------------------------------------------
  @totalCancelRO
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelRO
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR33 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR33RO
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelPR33RO
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing | Amanda Matheus | 399.75 |    2 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR36
  ##----------------------------------------------------------------------------------
  #@multicaptureContractCancellationPR36RO
  #Scenario: Multicapture Contract RO Cancellation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_MULTICAPTURE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-multicaptureRO@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_RO_MULTICAPTURE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
    #Given click on close tab
    #When click on the "QA_RO_MULTICAPTURE-CANCEL" file
    #And click on the body tab
    #Then click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action      |
      #| RO     | CANCELLED | CancelTotal |

  #@marketplaceContractCancellationPR36RO
  #Scenario: Marketplace Contract RO Cancellation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_MARKETPLACE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-marketplaceRO@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_RO_MARKETPLACE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
    #Given click on close tab
    #When click on the "QA_RO_MARKETPLACE-CANCEL" file
    #And click on the body tab
    #Then click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action      |
      #| RO     | CANCELLED | CancelTotal |

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR47 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR47RO
  Scenario: Total Cancel amount of all the products in this contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When cancel the item
    And click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    And validate the contract status is "ONGOING"
    
  ##----------------------------------------------------------------------------------
  ##                               Early Settlement
  ##----------------------------------------------------------------------------------
  @totalEarlySettlementRO
  Scenario: Total Early Settlement
    Perform a early settlement to the total contract amount

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Total" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "FINALIZED"

  @partialEarlySettlementRO
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "ONGOING"

  @partialEarlySettlementRefusedRO
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When refusing an early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement PR33
  ##----------------------------------------------------------------------------------
  @partialEarlySettlementPR33RO
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement PR36
  ##----------------------------------------------------------------------------------
  @multicaptureContractEarlySettlementPR36RO
  Scenario: confirm multicapture contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name     | Amount | Days | Merchant                   | Brand | Product Type |
      | OnGoing | Will Multicapture |        |    0 | QA Automation Multicapture |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"

  @marketplaceContractEarlySettlementPR36RO
  Scenario: confirm multicapture contract
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing | Will Marketplace |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
