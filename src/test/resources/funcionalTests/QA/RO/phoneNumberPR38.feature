@all @PR38 @PR38RO @criticalFeatures @RO
Feature: Phone Number Changes (PR38) - Romania

  #----------------------------------------------------------------------------------
  #                        Ecommerce Validation
  #----------------------------------------------------------------------------------
  @ecommerceValidPrefixPR38RO
  Scenario: Valid prefix and invalid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    Current regex expression: ^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$
    Prefix: 0040 or +40
    Nr of digits: 10
    Must begin with: 07
    - "Is Editable" Toggle button must be DEACTIVATED inside: Backoffice > Configurations > Subscription Page Configurations

    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Rua Testes XXI | +400698765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  @ecommerceInvalidPrefixPR38RO
  Scenario: Invalid prefix and valid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    
    Current regex expression: ^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$
    Prefix: 0040 or +40
    Nr of digits: 10
    Must begin with: 07

    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Rua Testes XXI | +410798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  #----------------------------------------------------------------------------------
  #                        BackOffice Validation
  #----------------------------------------------------------------------------------
  @backOfficePR38RO
  Scenario: Validate the regex expression
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Configurations menu
    * click on Phone Numbers
    * validate the regrex expression matches the "^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$" string

  @PR38BackOfficeCustomerValidRO
  Scenario: Validate and insert a valid phone number in BackOffice
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +400798765432 |
    Then validate the "BackOffice" mobile phone

  @backOfficeCustomerInvalidPR38RO
  Scenario: Validate and insert a invalid phone number in BackOffice
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +407798765432 |
    * validate the "BackOffice" message error
      | Message              |
      | Phone Number Invalid |

  #----------------------------------------------------------------------------------
  #                        Selfcare Validation
  #----------------------------------------------------------------------------------
  #@selfcareValidPR38RO
  #Scenario: Validate a valid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$
  #Prefix: 0040 or +40
  #Nr of digits: 10
  #Must begin with: 07
  #
  #Given navigate to "Selfcare RO"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +400798765432        | +400798762231    |
  #Then validate the "Selfcare" mobile phone
  #
  #@selfcareInvalidPR38RO
  #Scenario: Validate an invalid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$
  #Prefix: 0040 or +40
  #Nr of digits: 10
  #Must begin with: 07
  #
  #Given navigate to "Selfcare RO"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +400798765432        | +402455559008    |
  #* validate the "Selfcare" message error
  #| Message              |
  #| Phone Number Invalid |
  #----------------------------------------------------------------------------------
  #                        Merchant Validation
  #----------------------------------------------------------------------------------
  @merchantValidPR38RO
  Scenario: Valid phone number in Merchant
    Current regex expression: ^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$
    Prefix: 0040 or +40
    Nr of digits: 10
    Must begin with: 07

    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price |
      | Bollinger RD | Cars & motorbikes |        3 |   300 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address          | Postal Code |
      | Miss     | Amanda    | Matheus    |         | +400798765432 |       | Bulevardul Dacia |      013822 |

  @merchantInvalidPR38RO
  Scenario: Valid phone number in Merchant
    Current regex expression: ^(?:(?:\+|00)40|)[0]{1}[7]{1}[0-9]{8}$
    Prefix: 0040 or +40
    Nr of digits: 10
    Must begin with: 07

    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price |
      | Bollinger RD | Cars & motorbikes |        3 |   300 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone   | Email | Address          | Postal Code |
      | Miss     | Amanda    | Matheus    |         | +4010798765432 |       | Bulevardul Dacia |      013822 |
    * validate the "Merchant" message error
      | Message              |
      | Phone Number Invalid |
