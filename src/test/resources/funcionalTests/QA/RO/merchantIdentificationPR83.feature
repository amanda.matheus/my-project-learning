#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2421686283/PR83+Merchant+Identification+for+cancellations+Decathlon+ESP
#Author: Willian Arruda
@all @PR83 @PR83RO @RO
Feature: Merchant Identification (PR83) - Romania
  Each country will parameterize in Backoffice, at the Brand level, the new field “Follow the in-store cancellations“.
  Displays in the detail screen of Normal Contracts in Merchant Space the new field “Store to Refund“.
  The Multicapture and Marketplace Contracts is out of scope.

  @creationContractPR83RO @createContractInstore
  Scenario: Create a instore contract to cancel in merchant space.
    Given navigate to "Merchant RO"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 194.36 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name       | Apelido | Mobile Phone  | Email        | Address          | Postal Code |
      | Miss   | Merchant   | PR Eighty Three |         | +400798765432 | ro@gmail.com | Bulevardul Dacia |      013822 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110018716 |          123456 |
    And follow the "link" to finish my subscription
    And login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @cancellationAndReturnPR83RO @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click Contracts
    And search for the customer
      | Name                     | Email | Webpage  |
      | Merchant PR Eighty Three |       | Merchant |
    And select the "QA Automation Instore UX1" Merchant on the list
    And select the "Finanțat" status on the list
    And click on last contract details
    Then cancel An Amount An Return Partially
