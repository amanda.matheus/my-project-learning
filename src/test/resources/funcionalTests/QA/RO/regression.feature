@all @criticalFeatures @regression @regressionRO @RO
Feature: Regression - Romania
  This feature creates a contract for a new user with QA Automation business transactions, validates the contract values on the Back Office and Self Care

  @regressionRO3xFees @createContracteCommerce
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name      | Email | Birth Date | Place        | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Regression | Threexwithfees |       | 1982-11-10 | Calea Turzii | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110018142 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionRO3xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status | Customer Name             | Amount | Days | Merchant          | Brand                     | Product Type |
      |        | Regression Threexwithfees | 399.75 |    7 | QA Automation Web | QA Automation Tests - Web | 3X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "419,75"

  #* validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionRO3xFees2
  #Scenario: Selfcare: Change email
    #Given navigate to "Selfcare RO"
    #* enter the new username
    #When click on My Account menu
    #* click on Email
    #* change email to "newEmailCustomerRO@gmail.com"

  @regressionRO3xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name         | Email | Birth Date | Place        | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Regression | Threexwithoutfees |       | 1982-11-10 | Calea Turzii | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110018142 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionRO3xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status | Customer Name                | Amount | Days | Merchant          | Brand                     | Product Type |
      |        | Regression Threexwithoutfees | 399.75 |    7 | QA Automation Web | QA Automation Tests - Web | 3X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "399,75"

  #* validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionRO3xWithoutFees2
  #Scenario: Change password
    #Given navigate to "Selfcare RO"
    #* enter the new username
    #* click on Password
    #* change password from "Oney1testes" to "Oney2testes"
    #Then enter the new password of "oldPasswordCustomerRO@gmail.com"

  @regressionRO4xFees @createContracteCommerce
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name     | Email | Birth Date | Place        | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Regression | Fourxwithfees |       | 1982-11-10 | Calea Turzii | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110018142 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionRO4xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status | Customer Name            | Amount | Days | Merchant          | Brand                     | Product Type |
      |        | Regression Fourxwithfees | 594.11 |    7 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "623,82"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionRO4xFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare RO"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @regressionRO4xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name        | Email | Birth Date | Place        | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Regression | Fourxwithoutfees |       | 1982-11-10 | Calea Turzii | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110018142 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionRO4xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status | Customer Name               | Amount | Days | Merchant          | Brand                     | Product Type |
      |        | Regression Fourxwithoutfees | 594.11 |    7 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "594,11"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionRO4xWithoutFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare RO"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @onGoingToFinalizedRO
  Scenario: On Going to Finalized
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 399.75 |    7 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Romania" test payment page
    * pay all the installments
    Given navigate to "BackOffice RO"
    And click on the Contracts menu
    * search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR33RO
  Scenario: On Going to Finalized PR33
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Romania" test payment page
    And pay all the installments
    Given navigate to "BackOffice RO"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR36RO
  Scenario: On Going to Finalized PR36
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Romania" test payment page
    And pay all the installments
    Given navigate to "BackOffice RO"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR47RO
  Scenario: On Going to Finalized PR47
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Romania" test payment page
    And pay all the installments
    Given navigate to "BackOffice RO"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  #@endToEndMerchantRO @smokeTestMerchantRO
  #Scenario: Create a contract, perform smoke tests and delete account on Selfcare
    #Instore: 3x with fess and with 1 product
#
    #Given navigate to "Merchant RO"
    #And enter a valid username and password
    #When click on create a contract
    #* select the "QA Automation Instore UX1" Merchant
    #* click on Generate Contract Number
    #* fill in the product information
      #| Description | Category          | Quantity | Price  |
      #| Automa Item | Cars & motorbikes |        1 | 399.75 |
    #* choose "3x (with fees)" Oney transaction type
    #* fill in the client personal information
      #| Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address          | Postal Code |
      #| Miss   | Amanda     | Matheus   |         | +400798765432 |       | Bulevardul Dacia |      013822 |
    #* fill in the document information
      #| Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      #| Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110017984 |          123456 |
    #* follow the "link" to finish my subscription
    #Then insert a valid payment card
    #And validate that the contract was successfully created with a new user
    #Given navigate to "Selfcare RO"
    #* enter a newly created username and password
    #* click on My Account menu
    #* click on Delete Account
    #Then confirm the account is deleted
