#Author: Ronald Quintanilha
@all @PR34 @beforeYouGoPR34RO  @projectFeatures 
Feature: Before you Go Pop-in	(PR34) - Romanian
  Validation of new Before you Go Pop-in section.

  @BFYGConfigurationSplitBackOfficeRO
  Scenario: BackOffice: Validate the Subscription Page configs Split section
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs Split submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active
    
  @BFYGConfigurationPLBackOfficeRO
  Scenario: BackOffice: Validate the Subscription Page configs PL section
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs PL submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active
    
  @createContractForPR34RO
  Scenario: Subscription: Validate if the information is being displayed in the contract.
  	Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Strada Ștefan cel Mare 1 | Constanța    |      300077 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place                    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Ronald     | BeforeUgo   |       | 1982-11-10 | STR. BRĂTESCU CONSTANTIN | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And click to leave the subscription form
    Then confirm that the pop-in is being displayed
      
      
      
      
      
      
      