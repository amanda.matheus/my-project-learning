#Author: Willian Arruda
@PR53RO @PR53 @projectFeatures
Feature: TAE - Romania
Validation of TAE percentage

  @TAE3XWithFeesPR53RO 
  Scenario: Create abandon contract and verify fee 3x
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Strada Ștefan cel Mare 1 | Constanța    |      300077 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place                    | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Wilian     | Arruda    |       | 1982-11-10 | STR. BRĂTESCU CONSTANTIN | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then verify TAE percentage
    
    
    
  @TAE4XWithFeesPR53RO
  Scenario: Create abandon contract and verify fee 4x
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (with fees)"
    Then verify TAE percentage
    
    

  @TAE6XWithfeesRO @PR53RO
  Scenario: Create abandon contract and verify fee 6x
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
        Then verify TAE percentage