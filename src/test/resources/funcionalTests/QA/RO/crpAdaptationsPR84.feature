#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2421227521/PR+84+FATA+-+CRP+Adaptations+for+Norauto
#Author: Willian Arruda
@PR84RO @PR84 @projectFeatures
Feature:  CRP Adaptations  for Norauto (PR84) - Romania
Validate adaptations made for PSP list Menu and Merchant menu

   @validateBothButtonsPR84RO
  Scenario: Validate both PSP and Merchant buttons on CRP configurations tab inside Merchant details.
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Merchants menu
    * search for the Merchant "QA Automation Web"
    * click on Merchant details
    And navigate to CRP configurations tab
    Then validate Psp and Merchant buttons
    
    
    @validateBothFilesPR84RO
    Scenario: Validate both PSP and Merchant files are being generated simultaneously
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Merchants menu
    * search for the Merchant "QA Automation Web"
    * click on Merchant details
    * click on files tab
    * validate if merchant files are being generated
    And click on the Configurations menu
    * click on the PSP List
    * click on the edit PSP
    * validate if CRP files are being generated