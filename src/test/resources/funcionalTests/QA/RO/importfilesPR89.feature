#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2611150896/PR89+Brand+Merchant+user+creation+by+import+files
#Author: Willian Arruda
@all @PR89RO @PR89 @projectFeatures
Feature: Creation by import files (PR89) - Romania
  This feature aims to import and create Brand, Merchant and Users through Excel.

  @brandImportPR89RO
  Scenario: to Import and create a brand
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Brands menu
    And creating An Excel for "Brand" Import
    Then import "Brand" file
    And validate the creation

  @berchantImportPR89RO
  Scenario: to Import and create a Merchant
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Merchants menu
    And creating An Excel for "Merchant" Import
    Then import "Merchant" file
    And validate the creation

  @userImportPR89RO
  Scenario: to Import and create an user
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on userprofile Menu and Submenu
    And creating An Excel for "Users" Import
    Then import "Users" file
    And validate the creation
