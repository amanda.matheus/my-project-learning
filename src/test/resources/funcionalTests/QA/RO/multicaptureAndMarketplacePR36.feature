#@all @multicaptureAndMarketplace @multicaptureAndMarketplaceRO @criticalFeatures @RO  @createContracteCommerce @createContractInstore 
#Feature: Multicapture & Marketplace (PR36) - Romania
  #Creating contracts via Postman
#
  #@multicaptureContractCreationPR36RO @createContracteCommerce  @createContractInstore
  #Scenario: Multicapture Contract RO Creation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_MULTICAPTURE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-multicaptureRO@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_RO_MULTICAPTURE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
#
  #@marketplaceContractCreationPR36RO @createContracteCommerce  @createContractInstore
  #Scenario: Marketplace Contract RO Creation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_RO_MARKETPLACE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-marketplaceRO@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_RO_MARKETPLACE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
