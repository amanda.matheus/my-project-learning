#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/1959526401/PR62+ALTEX+Specific+CRP+Report
@all @PR62 @PR62RO @projectFeatures @RO
Feature: ALTEX Specific CRP Report (PR62) - Romania
  To have a specific financing report with different format and different fields required by Altex

  @altexPR62RO
  Scenario: Validate Altex files
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Configurations menu
    When click on the PSP List
    And click on the Checkout PSP
    And open the Altex Files
    Then validate the Altex File was generated correctly