@all @createContracteCommerce @createContracteCommerceRO @RO
Feature: Create Contract eCommerce - Romania
  Create contract with QA Automation business transactions inside eCommerce.

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUsereCommerceRO3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Strada Ștefan cel Mare 1 | Constanța    |      300077 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place                    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | STR. BRĂTESCU CONSTANTIN | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | Country   | CNP           |
      | Romanian    | Constanța | 2821110137166 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceRO3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                 | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Strada Gheorghe Lazăr 1 | Timis        |      300077 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Timiș   | 2821110355651 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceRO4xFees
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110015438 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceRO4xWithoutFees
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110407854 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceRO4xFeesGDPR
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | GDPR      |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110019187 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUsereCommerceRO3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place             | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | ro@gmail.com | 1982-11-10 | Calea Chisinaului | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommerceRO3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place             | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | ro@gmail.com | 1982-11-10 | Calea Chisinaului | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (without fees)"
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommerceRO4xFees
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place             | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | ro@gmail.com | 1982-11-10 | Calea Chisinaului | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (with fees)"
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceRO4xWithoutFees
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | ro@gmail.com | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (without fees)"
    * login as "ro@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created
