#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2404483087/PR69+LT2+MFS
#Author: Willian Arruda
@all @projectFeatures @RO @pr48LT2 @pr48LT2RO
Feature: Data Conservation (PR48 LT2) - Romania
  This feature provides rules and duration about the data conservation, in order to fit all legal obligations

  @deleteDataPR48RO
  Scenario: Verify if the data information is matching the QA requirements
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status    | Customer Name | Amount | Days | Merchant      | Brand | Product Type |
      | ABANDONED |               |        |    5 | QA Automation |       |              |
    And click on the edit button
    Then get the Contract Number
    Given navigate to "Dataconservation RO"
    And delete the contract
    When navigate to "BackOffice RO"
    And click on the Contracts menu
    Then check if the contract action successfully done

  @anonymizeDataPR48RO
  Scenario: Verify if the data information is matching the QA requirements
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status    | Customer Name | Amount | Days | Merchant      | Brand | Product Type |
      | ABANDONED |               |        |    5 | QA Automation |       |              |
    And click on the edit button
    Then get the Contract Number
    Given navigate to "Dataconservation RO"
    And paste the contract and anonymize it
    When navigate to "BackOffice RO"
    And click on the Contracts menu
    Then check if the contract action successfully done
