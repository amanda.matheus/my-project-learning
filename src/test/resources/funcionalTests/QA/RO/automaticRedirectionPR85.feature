#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2411954594/PR+85+Automatic+redirection+to+the+merchant+website
#Author: Amanda Matheus
@all @PR85 @PR85RO @projectFeatures @RO
Feature: Automatic Redirection Merchant Site (PR85) - Romania
  Redirecct automatically the user from all the Landing Page to the Merchant website.
  The configurations are in BackOffice parametrized in 60 seconds.

  @webNCAcceptedPR85RO
  Scenario: Validate the redirection when the web contract is accepted
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Alba       | Arruda    |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110015948 |
    Then insert a valid payment card
    And validate that the contract was successfully created
    And validate the automatic redirection to the merchant site

  @webNCRejectedPR85RO
  Scenario: Validate the redirection when the web contract is rejected
    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Engine     | Decision  |       | 2002-11-10 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 5021110019456 |
    Then insert a valid payment card
    And validate that the contract was rejected
    And validate the automatic redirection to the merchant site

  @instorebNCAcceptedPR85RO
  Scenario: Validate the redirection when the instore contract is accepted
    Given navigate to "Merchant RO"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address          | Postal Code |
      | Miss     | Amanda    | Matheus    |         | +400798765432 |       | Bulevardul Dacia |      013822 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card       | Document Number |
      | Bucaresti     | Alba        | Bucaresti    | Romanian    |          | 2821110019111 |          123456 |
    * follow the "link" to finish my subscription
    Then insert a valid payment card
    Then validate that the contract was successfully created
    And validate the automatic redirection to the merchant site
