#Author: Amanda Matheus
@all @paymentRetry @actionsToContract @projectFeatures @RO
Feature: Payment Retry - Romania
  New button Payment Retry inside Actions to Contract (BackOffice) to charge the unpaid installments.

  @paymentRetryRO
  Scenario: Charge an installments unpaid
    BackOffice:
    1-Charge installments to the payment return as refused, status "Unpaid"
    2-Click on "Payment Retry" to paid the unpaid installments

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 399.75 |    7 |          |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Romania" test payment page
    And charge all installment and the payment returns as refused
    Given navigate to "BackOffice RO"
    And click on the Contracts menu
    When search for "Unpaid" contract by number
    And click on the edit button
    And click on Contract Transactions tab
    And click on Actions to Contract
    And click on Payment Retry
    Then validate the Contract Transactions status is "Paid"
