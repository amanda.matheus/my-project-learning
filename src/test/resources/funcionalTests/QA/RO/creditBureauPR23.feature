#@all @createContracteCommerce @createContracteCommerceRO @RO
#Feature: Credit Bureau - Romania
  #Create contract with QA Automation Credit Bureau Rulebook
#
  #----------------------------------------------------------------------------------
                     #eCommerce - Create Contract New Customer
  #----------------------------------------------------------------------------------
  #@creditBureauRO @criticalFeatures
  #Scenario: Use the 1700809414513 CNP to get a value of 20 in Account No Non Bankings variable
    #Given navigate to "eCommerce RO"
#And add products to a total amount of "194,36" €
    #And fill in the shipping address
      #| Address          | Municipality         | Postal Code | Country | StreetNumber | Floor |
      #| Bulevardul Dacia | Bucuresti - sector 1 |      013822 | ROMANIA |            3 |    40 |
#When fill in the payment information and save the login credentials
      #| Honorific | PersonType     | First name | Last name     | Email | Birth Date | Place          | Mobile Phone  | Tenant                     |
      #| Mr.       | Private Person | Willian      | Arruda Bureau |       | 1975-01-26 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | Country | CNP           |
      #| Romanian    | Bihor   | 1750126054661 |
    #Then insert a valid payment card
    #And validate that the contract was successfully created with a new user
    #Given return to "BackOffice RO"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search for decision history by contract
    #And click on the edit button
#
    #
  #@PR23RulebookModule1RO @criticalFeatures
  #Scenario: eCommerce: 3x with fees with 11 items in the cart
    #Given navigate to "eCommerce RO"
#And add products to a total amount of "194,36" €
    #And fill in the shipping address
      #| Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      #| Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
#When fill in the payment information 
      #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone | Tenant                     |
      #| Miss      | Private Person | Amanda     | Matheus   |       | 1949-07-01 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | Country | CNP           |
      #| Romanian    | Alba    | 2490701015905 |
    #Then insert a valid payment card
    #Then validate that the contract was rejected
    #Given navigate to "BackOffice RO"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search for "Rejected" contract by reference
    #And click on the edit button
    #When click on Decision tab
    #Then validate the rejection reason is "Credit Bureau - Module 1" 
    #
  #@PR23RulebookModule1RO @criticalFeatures
  #Scenario: eCommerce: 3x with fees with 11 items in the cart
    #Given navigate to "eCommerce RO"
#And add products to a total amount of "194,36" €
    #And fill in the shipping address
      #| Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      #| Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
#When fill in the payment information 
      #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone | Tenant                     |
      #| Miss      | Private Person | Amanda     | Matheus   |       | 1952-03-02 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | Country 		| CNP           |
      #| Romanian    | Călărași    | 2520302510017 |
    #Then insert a valid payment card
    #Then validate that the contract was rejected
    #Given navigate to "BackOffice RO"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search for "Rejected" contract by reference
    #And click on the edit button
    #When click on Decision tab
    #Then validate the rejection reason is "Credit Bureau - Module 1" 
