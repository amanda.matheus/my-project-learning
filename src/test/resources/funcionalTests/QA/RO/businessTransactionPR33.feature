@all @PR33 @PR33RO @RO
Feature: Business Transaction Revamp (PR33) - Romania

  @PR33MerchantRO @projectFeatures
  Scenario: Validate the tab Business Transaction in Merchant Edition
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "QA Automation PR33"
    And click on the edit button
    * click on the "Business Transactions" tab
    Then validate the business transactions

  @contractAmountRO3xFees @projectFeatures
  Scenario: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand                     | Product Type |
      | OnGoing | Amanda        | 399.75 |    3 | QA Automation PR33 | QA Automation Tests - Web | 4X           |
    * check if the status is "ONGOING"
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "423,75"
    * validate the Capital Amount on the Contract Transactions is "99,93"

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - PR33
  ##----------------------------------------------------------------------------------
  @createContractRO4XP @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: PR33: 4x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation PR33 |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110018281 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractRO6XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: PR33: 6x with fees
    Given navigate to "eCommerce RO"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Mihai Eminescu | +400798765432 | RO - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 2821110018281 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user
