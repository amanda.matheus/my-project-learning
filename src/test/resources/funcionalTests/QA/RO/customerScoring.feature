#Author: Amanda Matheus
@all @customerScoring @projectFeatures @RO
Feature: Customer Scoring - Romania
  Verify if the contract is rejected by Customer Scoring rules.

  @customerScoringContractRO @createContracteCommerce
  Scenario: eCommerce: Validate the customer scoring rejection
    eCommerce:
    - Customer Age greater than 80
    - Email Domain equals automation.pt

    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email            | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Customer   | Scoring   | ro@automation.pt | 1939-11-10 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "ro@automation.pt" in the subscription page
    And accept the contract terms
    Then validate that the contract was rejected

  @customerScoringBackOfficeRO
  Scenario: BackOffice: Validate the customer scoring rejection
    BackOffice:
    - Validate the rejection reason
    - Validate the customer score details

    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name    | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Customer Scoring | 194.36 |    7 |          |       |              |
    And click on the edit button
    When click on Decision tab
    * validate if the rejection type is "Motor de decizie"
    * validate if the rejection reason is "Customer Scoring"
    * validate the customer scoring table
      | Scoring Acceptance Value | Customer Final Scoring | Decision |
      |                       25 |                     20 | Rejected |
    * click on the Engine Decision menu
    * search for decision history by contract
    Then validade the customer scoring details
      | Age | Email |
      |  10 |    10 |
