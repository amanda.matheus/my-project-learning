@all @checkout @checkoutRO @RO @critical
Feature: Checkout response code testing - Romania
  Create a contract with the amounts below to simulate and trigger the listed response codes
  
  ----------------------------------------------------------------------------------
                     eCommerce - PSP Insufficient funds
  ----------------------------------------------------------------------------------

  @expiredCardContractRO @criticalFeatures
  Scenario: eCommerce: Amount ends with xxx,33 - Response code 30033 Expired card - Pick up
    Given navigate to "eCommerce RO"
    And add products to a total amount of "64,41" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 1821110014039 |
    Then insert a valid VISA payment card
    Then validate that the contract was rejected by checkout
