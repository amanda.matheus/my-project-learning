@all @customerCardDetectionPR24 @customerCardDetectionPR24RO @criticalFeatures @RO
Feature: Customer Card Detection (PR24) - Romania
  Validation of payment method in the checkout frame.

  #----------------------------------------------------------------------------------
  #                          Subscription - New User
  #----------------------------------------------------------------------------------
  @subscriptionNewUserPR24RO
  Scenario: Subscription for a new user - Validate the rejection of the payment method
    Invalid card used: Visa (4658 5840 9000 0001)

    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Anderson   | Guittar   |       | 1982-11-10 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | Country | CNP           |
      | Romanian    | Alba    | 1821110014039 |
    * insert a invalid payment card
    Then validate that the card is unauthorized

  #----------------------------------------------------------------------------------
  #                          Subscription - Existing User
  #----------------------------------------------------------------------------------
  @subscriptionExistingUserPR24RO
  Scenario: Subscription for an existing user - Validate the rejection of the payment method
    Invalid card used: Visa (4658 5840 9000 0001)

    Given navigate to "eCommerce RO"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Bulevardul Dacia | Bucharest    |      013822 | ROMANIA |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | ro@gmail.com | 1982-11-10 | Rua Testes XXI | +400798765432 | RO - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "ro@gmail.com" in the subscription page
    * insert a invalid payment card to my account
    Then validate that the card is unauthorized
  ##----------------------------------------------------------------------------------
  ##                          Selfcare - Add a card / Remove a card
  ##----------------------------------------------------------------------------------
  #@selfcareAddACardValidPR24RO
  #Scenario: Selfcare > Add a Card - Validate the acceptance of the payment method
    #Valid card used: Visa (5352 1515 7000 3404)
#
    #Given navigate to "Selfcare RO"
    #And enter a valid username and password
    #When click on My Account menu
    #* click on Payment Info
    #* click on Add a card
    #* insert a "Valid" payment card on "Payment Info"
    #* validate that the card is authorized on "Payment Info"
    #* do you want to change the payment method to other contracts "No"
    #* click on Payment Info
    #Then click on remove a card
#
  #@selfcareAddACardInvalidPR24RO
  #Scenario: Selfcare > Add a Card - Validate the rejection of the payment method
    #Invalid card used: Visa (4658 5840 9000 0001)
#
    #Given navigate to "Selfcare RO"
    #And enter a valid username and password
    #When click on My Account menu
    #* click on Payment Info
    #* click on Add a card
    #* insert a "Invalid" payment card on "Payment Info"
    #Then validate that the card is unauthorized
