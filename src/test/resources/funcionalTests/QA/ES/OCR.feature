@all @ocr @projectFeatures @ES
Feature: OCR Part 1 - Spain
The OCR component is displayed only to number of installments greater than 5x.

  @ocrNewUsereCommerceES5xFees
  Scenario: 5x with fees and with OCR component shown to new customer.
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address                  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle Conde Del Serrallo | Madrid       |       28029 | SPAIN   |            1 |    05 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name   | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Luis       | Beceiro Rey |       | 1989-12-21 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "5x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 47382616V |
    And click on validate button
    And upload the ID Card
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user
    
  @ocrNewUsereCommerceES5xFees
  Scenario: Verify if the OCR was displayed in BackOffice
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    * search for the contract external reference
    And click on the edit button
    When click on Decision tab
    Then validate the OCR Decision
      | Decision                                                    | Motive        |
      | Displayed: The OCR component was displayed to the customer. | Paytimes >= 5 |

  @ocrExistingUsereCommerceES5xFees
  Scenario: 5x with fees and with OCR component shown to existing customer.
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "5x (with fees)"
    * login as "es@gmail.com" in the subscription page
    * upload the ID Card
    And accept the contract terms
    Then validate that the contract was successfully created
    
   @ocrExistingUsereCommerceES5xFees
  Scenario: Verify if the OCR was displayed in BackOffice. 
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    * search for the contract external reference
    And click on the edit button
    When click on Decision tab
    Then validate the OCR Decision
      | Decision                                                    | Motive        |
      | Displayed: The OCR component was displayed to the customer. | Paytimes >= 5 |
