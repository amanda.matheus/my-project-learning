@all @createContractInstore @createContractInstoreES @ES
Feature: Create Contract Instore - Spain
  Create contract with QA Automation business transactions inside Merchant Space

  ##----------------------------------------------------------------------------------
  ##                   Instore - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUserMerchantES3xWithFees @criticalFeatures
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | C. Conde del Serrallo 1 05 |       03015 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province         | ID Card   | Document Number |
      | Alicante      | SPAIN       | Alicante     | Spanish     | Alicante/Alacant | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantES3xWithoutFees @criticalFeatures
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                         | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | La Pinada Bloque Costa Marina 3 |       12594 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province           | ID Card   | Document Number |
      | Castellón     | SPAIN       | Castellón    | Spanish     | Castellón/Castelló | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantES4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                   | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | Carrer de Can Escursac 12 |       07001 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province       | ID Card   | Document Number |
      | Palma         | SPAIN       | Palma        | Spanish     | Balears, illes | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantES4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantES6xWithFees
  Scenario: Instore: 6x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "6x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    * upload the ID Card
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantES8xWithFees
  Scenario: Instore: 8x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "8x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    * upload the ID Card
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantES10xWithFees
  Scenario: Instore: 10x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "10x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Amanda     | Ten Times |         | +34745555965 |       | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    * upload the ID Card
    Then insert a valid payment card
    Then validate that the contract was successfully created

  ##----------------------------------------------------------------------------------
  ##                   Instore - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUserMerchantES3xWithFees @criticalFeatures
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantES3xWithoutFees @criticalFeatures
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantES4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantES4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantES4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantES4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "4x (without fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantES6xWithFees
  Scenario: Instore: 6x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "6x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    * upload the ID Card
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantES8xWithFees
  Scenario: Instore: 8x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "8x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Matheus   | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    * upload the ID Card
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantES10xWithFees
  Scenario: Instore: 10x with fees
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "10x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Amanda     | Ten Times | Valle   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    * upload the ID Card
    And accept the contract terms
    Then validate that the contract was successfully created
