#@all @checkout @checkoutBE @ES @critical
#Feature: Safewatch Rejection - Spain
  #Create a contract with the personal info below to simulate and a "bad" client
#
  #@safewatchES @criticalFeatures
  #Scenario: Safewatch - First Name, Last Name, Birth Date
    #Given navigate to "eCommerce ES"
    #And add products to a total amount of "194,36" €
    #* fill in the shipping address
      #| Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      #| Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    #When fill in the payment information
      #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      #| Miss      | Private Person | Fayssal    | ABBAS     |       | 1955-01-01 | Madrid | +34745555965 | ES - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | BirthPlace | Province | DNI       |
      #| Spanish     | SPAIN      | Madrid   | 26042907F |
    #Then insert a valid payment card
    #Then validate that the contract was rejected
    #Given navigate to "BackOffice ES"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search for "Rejected" contract by reference
    #And click on the edit button
    #When click on Decision tab
    #Then validate the rejection reason is "SafeWatch"
