@billingFilesValidation @billingFilesValidationES @projectFeatures
Feature: Billing Files Daily Validation - Spain
  Verify if the files are being generated daily

  ##----------------------------------------------------------------------------------
  ##                   Billing files validation
  ##----------------------------------------------------------------------------------
  
  @financingFileValidationES 
  Scenario: Validate the generation of yesterday s financing file	
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Billing files submenu
    Then validate if financing file was generated
    
    
 @invoicingFileValidationES 
  Scenario: Validate the generation of yesterday s invoicing file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Billing files submenu
    And click on the invoicing file tab
    Then validate if invoicing file was generated