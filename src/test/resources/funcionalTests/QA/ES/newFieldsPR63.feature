#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2088075265/PR63+FATA+-+New+Fields+3x4x+Contract+Form
#Author: Amanda Matheus
@all @ES @criticalFeatures
Feature: New Fields 3x4x Contract Form (PR63) - Spain
  Additional fields in Subscription. The additional fields are displayed only to purchase amount greater that 1.000,00 €

  @webNCPR63ES
  Scenario: Web contract with the additional fields for new customers.
  - Ocr rulebook name: AUTOMATION TESTS_2(DONT CHANGE)
  - Requirements for OCR to be shown: Number of installments >= 5
  
    Given navigate to "eCommerce ES"
    And add products to a total amount of "1870,97" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | Developer  | Automation |           1000 |              300 | Employer           |
    And upload the ID Card
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @instoreECPR63ES
  Scenario: Instore contract with the additional fields for existing customers.
  - Ocr rulebook name: AUTOMATION TESTS_2(DONT CHANGE)
  - Requirements for OCR to be shown: Number of installments >= 5
  
    Given navigate to "Merchant ES"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 1870.97 |
    When choose "6x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    And follow the "link" to finish my subscription
    And login as "es@gmail.com" in the subscription page
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | Developer  | Automation |           1000 |              300 | Employer           |
    And upload the ID Card
    And accept the contract terms
    Then validate that the contract was successfully created
