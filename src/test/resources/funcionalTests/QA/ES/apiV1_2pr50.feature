#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2149842957/PR50+FATA+-+Easy+API+-+API+v1.2
#Author: Willian Arruda
#@apiV1_2ES @createContracteCommerce @PR50 @PR50ES @all @projectFeatures
#Feature: Create Contracs by API V2 (PR50) - Spain
  #New version of payment API to reduce the number of fields requested to our merchant at the level of the payment API request when they integrate ONEY split payment solution.
  #The API v2 will not replace the v1.
#
  #@apiV2ECPR50ES
  #Scenario: Create a normal contract using the Postman web to request the subscription link by API v2.
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #* click on the "Normal Contracts API V1_2 ES" file
    #And click on the body tab
    #And click on the send button
    #* get link to finish my subscription
    #* login as "es@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@apiV2ECMarketplacePR50ES
  #Scenario: Create a marketplace contract using the Postman web to request the subscription link by API v2.
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #* click on the "Marketplace PR50 PAYMENT ES" file
    #And click on the body tab
    #And click on the send button
    #* get link to finish my subscription
    #* login as "ivotesteocr486y@mail.es" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
    #
          #@apiV2ECTravelPR50ES
  #Scenario: Create a marketplace contract using the Postman web to request the subscription link by API v2.
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "PR35 LT 2 TRAVEL - ES" file
    #And click on the body tab
    #And click on the send button
    #* get link to finish my subscription
    #And login as "es@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "BackOffice ES"
    #And enter a valid username and password
    #And click on the Contracts menu
    #And search for an contract created
      #| Status | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      #|        | Teste Travel API V2 |  |    0 |          |       |              |
    #And click on the edit button
    #When click on Items List tab
    #Then validate the items list of Travels table on "BackOffice"
