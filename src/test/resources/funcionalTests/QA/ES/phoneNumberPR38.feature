@all @PR38 @PR38ES @criticalFeatures @ES
Feature: Phone Number Changes (PR38) - Spain

  #----------------------------------------------------------------------------------
  #                        Ecommerce Validation
  #----------------------------------------------------------------------------------
  @ecommerceValidPrefixPR38ES
  Scenario: Valid prefix and invalid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    Current regex expression: ^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$
    Prefix: 0034 or +34
    Nr of digits: 9
    Must begin with: 7 or 6
    - "Is Editable" Toggle button must be DEACTIVATED inside: Backoffice > Configurations > Subscription Page Configurations

    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Madrid | +347045555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  @ecommerceInvalidPrefixPR38ES
  Scenario: Invalid prefix and valid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    
    Current regex expression: ^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$
    Prefix: 0034 or +34
    Nr of digits: 9
    Must begin with: 7 or 6

    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Madrid | +33745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  #----------------------------------------------------------------------------------
  #                        BackOffice Validation
  #----------------------------------------------------------------------------------
  @backOfficePR38ES
  Scenario: Validate the regex expression
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Configurations menu
    * click on Phone Numbers
    * validate the regrex expression matches the "^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$" string

  @PR38BackOfficeCustomerValidES
  Scenario: Validate and insert a valid phone number in BackOffice
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone |
      | +34745555965 |
    Then validate the "BackOffice" mobile phone

  @backOfficeCustomerInvalidPR38ES
  Scenario: Validate and insert a invalid phone number in BackOffice
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +347455559659 |
    * validate the "BackOffice" message error
      | Message              |
      | Phone Number Invalid |

  #----------------------------------------------------------------------------------
  #                            Selfcare Validation
  #----------------------------------------------------------------------------------
  #@selfcareValidPR38ES
  #Scenario: Validate a valid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$
  #Prefix: 0034 or +34
  #Nr of digits: 9
  #Must begin with: 6, 7
  #
  #Given navigate to "Selfcare ES"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +34745555966         | +34745555966     |
  #Then validate the "Selfcare" mobile phone
  #
  #@selfcareInvalidPR38ES
  #Scenario: Validate an invalid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$
  #Prefix: 0034 or +34
  #Nr of digits: 9
  #Must begin with: 6, 7
  #
  #Given navigate to "Selfcare ES"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +34645555965         | +34245555900     |
  #* validate the "Selfcare" message error
  #| Message              |
  #| Phone Number Invalid |
  #----------------------------------------------------------------------------------
  #                        Merchant Validation
  #----------------------------------------------------------------------------------
  @merchantValidPR38ES
  Scenario: Valid phone number in Merchant
    Current regex expression: ^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$
    Prefix: 0034 or +34
    Nr of digits: 9
    Must begin with: 7 or 6

    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price |
      | Bollinger RD | Cars & motorbikes |        1 |   300 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | C. Conde del Serrallo 1 05 |       28029 |

  @merchantInvalidPR38ES
  Scenario: Invalid phone number on Merchant
    Current regex expression: ^(?:(?:\+|00)34|)\s*[7|6](?:[\s.-]*\d{2}){4}$
    Prefix: 0034 or +34
    Nr of digits: 9
    Must begin with: 7 or 6

    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price |
      | Bollinger RD | Cars & motorbikes |        1 |   300 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34005555965 |       | C. Conde del Serrallo 1 05 |       28029 |
    * validate the "Merchant" message error
      | Message           |
      | Teléfono inválido |
