@all @criticalFeatures @regression @regressionES @ES
Feature: Regression - Spain
  This feature creates a contract for a new user with QA Automation business transactions, validates the contract values on the Back Office and Self Care

  @regressionES3xFees @createContracteCommerce
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name      | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithFees |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionES3xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name             | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression ThreeXWithFees | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 3X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "419,75"

  #* validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionES3xFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare ES"
    #* enter the new username
    #When click on My Account menu
    #* click on Email
    #* change email to "newEmailCustomerES@gmail.com"

  @regressionES3xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name         | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithoutFees |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionES3xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name                | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression ThreeXWithoutFees | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 3X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "399,75"

  #* validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionES3xWithoutFees2
  #Scenario: Change password
    #Given navigate to "Selfcare ES"
    #* enter the new username
    #When click on My Account menu
    #* click on Password
    #* change password from "Oney1testes" to "Oney2testes"
    #Then enter the new password of "oldPasswordCustomerBE@gmail.com"

  @regressionES4xFees @createContracteCommerce
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce ES"
    And add products to a total amount of "594,11" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name     | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithFees |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionES4xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name            | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression FourXWithFees | 594.11 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "623,82"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionES4xFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare ES"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @regressionES4xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce ES"
    And add products to a total amount of "594,11" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name        | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithoutFees |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionES4xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name               | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression FourXWithoutFees | 594.11 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "594,11"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionES4xWithoutFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare ES"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @onGoingToFinalizedES
  Scenario: On Going to Finalized
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Willian Arruda | 399,75 |    0 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Spain" test payment page
    * pay all the installments
    Given navigate to "BackOffice ES"
    And click on the Contracts menu
    * search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR33ES
  Scenario: On Going to Finalized PR33
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Spain" test payment page
    And pay all the installments
    Given navigate to "BackOffice ES"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR36ES
  Scenario: On Going to Finalized PR36
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Spain" test payment page
    And pay all the installments
    Given navigate to "BackOffice ES"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR47ES
  Scenario: On Going to Finalized PR47
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Spain" test payment page
    And pay all the installments
    Given navigate to "BackOffice ES"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  #@endToEndMerchantES @smokeTestMerchantES
  #Scenario: Create a contract, perform smoke tests and delete account on Selfcare
    #Instore: 3x with fess and with 1 product
#
    #Given navigate to "Merchant ES"
    #And enter a valid username and password
    #When click on create a contract
    #* select the "QA Automation Instore UX1" Merchant
    #* click on Generate Contract Number
    #* fill in the product information
      #| Description | Category          | Quantity | Price |
      #| Automa Item | Cars & motorbikes |        1 |   300 |
    #* choose "3x (without fees)" Oney transaction type
    #* fill in the client personal information
      #| Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      #|        | Willian    | Arruda    | Neves   |    756489521 |       | C. Conde del Serrallo 1 05 |       28029 |
    #* fill in the document information
      #| Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      #| Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    #* follow the "link" to finish my subscription
    #And click on validate button
    #* upload the ID Card
    #Then insert a valid payment card
    #And validate that the contract was successfully created with a new user
    #Given navigate to "Selfcare ES"
    #* enter a newly created username and password
    #* click on My Account menu
    #* click on Delete Account
    #Then confirm the account is deleted

  @regressionES10xFees
  Scenario: eCommerce: 10x with fees and with 7 items in the cart
    Subscription: Change the address
    Change billing address
    Validate in the BackOffice the billing address

    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "10x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    * upload the ID Card
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionES10xFees1
  Scenario: Validate in the BackOffice the billing address
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    * search for the contract external reference
    * check if the status is "Waiting Delivery"
    And click on the edit button
    Then validate the billing address has changed as typed in the subscription page
