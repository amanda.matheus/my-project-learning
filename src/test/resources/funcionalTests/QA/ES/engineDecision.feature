#Author: Amanda Matheus
@all @engineDecision @engineDecisionES @criticalFeatures @ES
Feature: Engine Decision - Spain
  Verify if the contract is rejected by Engine Decision rules.

  @rulebookAgeContractES
  Scenario: Validate the engine decision rulebook rejection
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Engine     | Decision  |       | 2002-11-10 | Madrid | +34758645120 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was rejected

  @rulebookAgeBackOfficeES
  Scenario: Validate the rejection reason in BackOffice
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name   | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Engine Decision | 194.36 |    0 |          |       |              |
    And click on the edit button
    When click on Decision tab
    Then validate if the rejection type is "Decision Engine"
    And validate if the rejection reason is "Age between 18 and 21"
  #@rulebookAmountContractES
  #Scenario: Validate that customers contracts greater than 2500,00 € are rejected
    #Given navigate to "eCommerce ES"
    #And add products to a total amount of "2548,97" €
    #And fill in the shipping address
      #| Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      #| Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    #When fill in the payment information
      #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      #| Mr.       | Private Person | Engine     | Decision  |       | 1982-11-10 | Madrid | +34756489521 | ES - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | BirthPlace | Province | DNI       |
      #| Spanish     | SPAIN      | Madrid   | 80213736V |
    #Then insert a valid payment card
    #Then validate that the contract was rejected
#
  #@rulebookAmountBackOfficeES
  #Scenario: Validate the Powercard rejection in BackOffice
    #Given navigate to "BackOffice ES"
    #And enter a valid username and password
    #And click on the Contracts menu
    #And search for an contract created
      #| Status   | Customer Name   | Amount  | Days | Merchant |Brand | Product Type |
      #| Rejected | Engine Decision | 2548.97 |    0 |          |||
    #And click on the edit button
    #When click on Decision tab
    #Then validate if the rejection type is "Decision Engine"
    #And validate if the rejection reason is "Basket Amount >= 2500"
