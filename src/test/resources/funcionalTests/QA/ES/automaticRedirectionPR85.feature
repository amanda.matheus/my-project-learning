#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2411954594/PR+85+Automatic+redirection+to+the+merchant+website
#Author: Amanda Matheus
@all @PR85 @PR85ES @projectFeatures @ES
Feature: Automatic Redirection Merchant Site (PR85) - Spain
  Redirecct automatically the user from all the Landing Page to the Merchant website.
  The configurations are in BackOffice parametrized in 60 seconds.

  @webNCAcceptedPR85ES
  Scenario: Validate the redirection when the web contract is accepted
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created
    And validate the automatic redirection to the merchant site

  @webNCRejectedPR85ES
  Scenario: Validate the redirection when the web contract is rejected
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Engine     | Decision  |       | 2002-11-10 | Madrid | +34758645120 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    Then insert a valid payment card
    Then validate that the contract was rejected
    And validate the automatic redirection to the merchant site

  @instorebNCAcceptedPR85ES
  Scenario: Validate the redirection when the instore contract is accepted
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address                    | Postal Code |
      |        | Willian    | Arruda    | Neves   | +34745555965 |       | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    And click on validate button
    Then insert a valid payment card
    Then validate that the contract was successfully created
    And validate the automatic redirection to the merchant site
