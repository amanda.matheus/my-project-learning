@all @PR33 @PR33ES @ES
Feature: Business Transaction Revamp (PR33) - Spain

  @PR33MerchantES @projectFeatures
  Scenario: Validate the tab Business Transaction in Merchant Edition
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "QA Automation PR33"
    And click on the edit button
    * click on the "Business Transactions" tab
    Then validate the business transactions

  @contractAmountES3xFees @projectFeatures
  Scenario: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand                     | Product Type |
      | OnGoing |               | 399.75 |    3 | QA Automation PR33 | QA Automation Tests - Web | 4X           |
    * check if the status is "ONGOING"
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "423,75"
    * validate the Capital Amount on the Contract Transactions is "99,93"

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - PR33
  ##----------------------------------------------------------------------------------
  @createContractES4XM @criticalFeatures  @createContracteCommerce @createContractInstore
  Scenario: eCommerce: 4x with fess 
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation PR33 |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractES3XM @criticalFeatures  @createContracteCommerce @createContractInstore
  Scenario: eCommerce: 3x with fess 
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation PR33 |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user
    
    
  @createContractES6XM @criticalFeatures  @createContracteCommerce @createContractInstore
  Scenario: eCommerce: 6x with fess 
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    * upload the ID Card
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user
    
