@all @checkout @checkoutES @ES @criticalFeatures
Feature: Checkout response code testing - Spain
  Create a contract with the amounts below to simulate and trigger the listed response codes

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - PSP Insufficient funds
  ##----------------------------------------------------------------------------------
  @expiredCardContractES 
  Scenario: eCommerce: Amount ends with xxx,33 - Response code 30033 Expired card - Pick up
    Given navigate to "eCommerce ES"
    And add products to a total amount of "64,41" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 26042907F |
    And click on validate button
    Then insert a valid VISA payment card
    And validate that the contract was rejected by checkout
