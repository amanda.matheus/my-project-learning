@all @createContracteCommerce @createContracteCommerceES @ES
Feature: Create Contract eCommerce - Spain
  Create contract with QA Automation business transactions inside eCommerce

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUsereCommerceES3xWithFees @criticalFeatures
  Scenario: eCommerce: 3x with fess with 10 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Alicante     |       03015 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Alicante | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province         | DNI       |
      | Spanish     | SPAIN      | Alicante/Alacant | 26042907F |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceES3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fess with 10 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceES4xWithFees
  Scenario: eCommerce: 4x with fess with 7 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Castellón    |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Castellón | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province           | DNI       |
      | Spanish     | SPAIN      | Castellón/Castelló | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceES4xWithoutFees
  Scenario: eCommerce: 4x without fess with 11 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  #@createContractNewUsereCommerceES8xWithFees
  #Scenario: eCommerce: 8x with fess with 8 items in the cart
  #Given navigate to "eCommerce ES"
  #And add products to a total amount of "2212,96" €
  #* fill in the shipping address
  #| Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
  #| Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
  #When fill in the payment information
  #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
  #| Mr.    | Private Person | Willian     | Arruda     |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
  #And choose the payment type "8x (with fees)"
  #* upload the ID Card
  #Then insert a valid payment card
  #And validate that the contract was successfully created with a new user
  #@createContractNewUsereCommerceES10xWithFees
  #Scenario: eCommerce: 10x with fess with 9 items in the cart
  #Given navigate to "eCommerce ES"
  #And add products to a total amount of "399,75" €
  #* fill in the shipping address
  #| Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
  #| Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
  #When fill in the payment information
  #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
  #| Mr.     | Private Person | Willian     | Arruda      |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
  #And choose the payment type "10x (with fees)"
  #* upload the ID Card
  #Then insert a valid payment card
  #And validate that the contract was successfully created with a new user
  @createContractNewUsereCommerceES4xWithFeesGDPR
  Scenario: eCommerce: 4x with fess with 7 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | GDPR      |       | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | DNI       |
      | Spanish     | SPAIN      | Madrid   | 80213736V |
    And click on validate button
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUsereCommerceES3xWithFees @criticalFeatures
  Scenario: eCommerce: 3x with fees with 8 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommerceES3xWithoutFees
  Scenario: eCommerce: 3x without fees with 11 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (without fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommerceES4xFees
  Scenario: eCommerce: 4x with fees with 7 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommerceES4xWithoutFees
  Scenario: eCommerce: 4x without fees with 7 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (without fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceES3xWithFees
  Scenario: eCommerce: 3x with fess with 10 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceES3xWithoutFees
  Scenario: eCommerce: 3x without fess with 10 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (without fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceES4xWithFees
  Scenario: eCommerce: 4x with fess with 7 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceES4xWithoutFees
  Scenario: eCommerce: 4x without fess with 11 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (without fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceES8xWithFees
  Scenario: eCommerce: 8x with fess with 8 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "8x (with fees)"
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECeCommerceES10xWithFees
  Scenario: eCommerce: 10x with fess with 9 items in the cart
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Ten Times | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "10x (with fees)"
    * login as "es@gmail.com" in the subscription page
    * upload the ID Card
    And accept the contract terms
    Then validate that the contract was successfully created
