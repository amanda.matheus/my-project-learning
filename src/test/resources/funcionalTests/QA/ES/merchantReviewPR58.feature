#Requirements - LT1 - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/1812463617/PR58+LT1+FATA+-+Merchant+Space+Review
#Author: Willian Arruda
@all @PR58 @PR58ES @NewComponentsContractPage @ES
Feature: Merchant Review (PR58 LT1) - Spain
  Add more information into contract details and the cancellation process will be modified:
  -Cancellation process made by item quantity and total amount 
  -New fields in contract detail

  @creationContractPR58ES @createContractInstore
  Scenario: Instore contract to be cancelled in merchant space.
    Given navigate to "Merchant ES"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Merchant   | PR58      | Review  | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    And follow the "link" to finish my subscription
    And login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @newComponentsContractPagePR58ES @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click Contracts
    And search for the customer
      | Name          | Email | Webpage  |
      | Merchant PR58 |       | Merchant |
    And select the "QA Automation Instore UX1" Merchant on the list
    And select the "Financiado" status on the list
    And validate New Contract Tab Fields
    Then cancel The Contract Partially
