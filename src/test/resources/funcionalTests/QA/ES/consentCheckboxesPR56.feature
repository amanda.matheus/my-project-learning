@all @PR56 @projectFeatures @ES
Feature: Consent CheckBoxes (PR56) - Spain

  @unmarkConsentOne
  Scenario: Unmark the consents
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    * click on the Customers menu
    * search for the customer
      | Name             | Email                          | Webpage    |
      | Consent Checkbox | testing-consentOneES@gmail.com | BackOffice |
    And click on the edit button
    When click on the "Account" tab
    * click on the "Communication preference" section
    Then unmark all consents and save

  @contractConsentCheckboxes1
  Scenario: Subscription when an Existing Customer accpet the first and second consent
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name    | Email                          | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Consent    | Checkbox One | testing-consentOneES@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "testing-consentES@gmail.com" in the subscription page
    * accept "Commercial Offers Oney" contract consent
    And accept the contract terms
    Then validate that the contract was successfully created

  @SelfcareConsentCheckboxesOffersOneyEC1
  Scenario: Selfcare when an Existing Customer accpet the first and second consent
    Given navigate to "Selfcare ES"
    * enter a valid username "testing-consentOneES@gmail.com" and password
    When click on contract details on an active contract
    * download the "Commercial Offers Oney" consent file
    #Then validate if "Commercial Offers Oney" consent on the file

  @unmarkConsentTwo
  Scenario: Unmark the consents
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    * click on the Customers menu
    * search for the customer
      | Name             | Email                          | Webpage    |
      | Consent Checkbox | testing-consentTwoES@gmail.com | BackOffice |
    And click on the edit button
    When click on the "Account" tab
    * click on the "Communication preference" section
    Then unmark all consents and save

  @contractConsentCheckboxes2
  Scenario: Subscription when an Existing Customer accpet the last consent
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name    | Email                          | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Consent    | Checkbox Two | testing-consentTwoES@gmail.com | 1982-11-10 | Rua Testes XXI | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "testing-consentES@gmail.com" in the subscription page
    * accept "Commercial Offers Partners" contract consent
    And accept the contract terms
    Then validate that the contract was successfully created

  @selfcareConsentCheckboxesOffersOneyEC2
  Scenario: Selfcare when an Existing Customer accpet the last consent
    Given navigate to "Selfcare ES"
    * enter a valid username "testing-consentTwoES@gmail.com" and password
    When click on contract details on an active contract
    * download the "Commercial Offers Partners" consent file
    #Then validate if "Commercial Offers Partners" consent on the file
