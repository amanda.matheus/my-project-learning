#@apiQAES @createContracteCommerce @apiQA
#Feature: Create Contracs by API - Spain
#
  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  #@QA_ES_3x001_194.36NC
  #Scenario: QA_ES_3x001_194.36NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_3x001_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_3x002_194.36NC
  #Scenario: QA_ES_3x002_194.36NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_3x002_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_4x001_399.75NC
  #Scenario: QA_ES_4x001_399.75NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x001_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_4x001_399.75NC_GDPR
  #Scenario: QA_ES_4x001_399.75NC_GDPR
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x001_399.75NC_GDPR" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_4x002_399.75NC
  #Scenario: QA_ES_4x002_399.75NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x002_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_6x001_399.75EC
  #Scenario: QA_ES_6x001_399.75EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_6x001_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill in the missing information
    #* login as "es@gmail.com" in the subscription page
    #* upload the ID Card
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_ES_8x001_399.75EC
  #Scenario: QA_ES_8x001_399.75EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_8x001_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill in the missing information
    #* login as "es@gmail.com" in the subscription page
    #* upload the ID Card
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_ES_10x01_399.75EC
  #Scenario: QA_ES_10x01_399.75EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_10x01_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill in the missing information
    #* login as "es@gmail.com" in the subscription page
    #* upload the ID Card
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_ES_3x001_399.75EC
  #Scenario: QA_ES_3x001_399.75EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_3x001_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #* login as "es@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_ES_3x002_399.75EC
  #Scenario: QA_ES_3x002_399.75EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_3x002_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill in the missing information
    #* login as "es@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_ES_4x001_194.36EC
  #Scenario: QA_ES_4x001_194.36EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x001_194.36EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill in the missing information
    #* login as "es@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #@QA_ES_4x002_194.36EC
  #Scenario: QA_ES_4x002_194.36EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x002_194.36EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #* login as "es@gmail.com" in the subscription page
    #And accept the contract terms
    #* validate that the contract was successfully created
#
  #----------------------------------------------------------------------------------
                     #eCommerce - Recollections
  #----------------------------------------------------------------------------------
  #@QA_ES_4x001_194.36NC
  #Scenario: 4x001 | 194.36 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x001_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_4x001_194.36NCH
  #Scenario: 4x001 | 194.36 | NCH
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4x001_194.36NCH" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #----------------------------------------------------------------------------------
                     #eCommerce - PR33
  #----------------------------------------------------------------------------------
  #@QA_ES_3XP_399.75NC
  #Scenario: 3XP | 399.75 | NCH
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_3XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_4XP_399.75NC
  #Scenario: 4XP | 399.75 | NCH
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_4XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
#
  #@QA_ES_6XP_399.75NC
  #Scenario: 6XP | 399.75 | NCH
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_6XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And click on validate button
    #Then insert a valid payment card
    #Then validate that the contract was successfully created
