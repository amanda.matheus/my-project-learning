#Author: Ronald Quintanilha
@all @PR34 @beforeYouGoPR34ES  @projectFeatures 
Feature: Before you Go Pop-in	(PR34) - Spain
  Validation of new Before you Go Pop-in section.

  @BFYGConfigurationSplitBackOfficeES
  Scenario: BackOffice: Validate the Subscription Page configs Split section
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs Split submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active
    
  @BFYGConfigurationPLBackOfficeES
  Scenario: BackOffice: Validate the Subscription Page configs PL section
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs PL submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active
    
  @createContractForPR34ES
  Scenario: Subscription: Validate if the information is being displayed in the contract.
  	Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Alicante     |       03015 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Ronald    | BeforeUgo    |       | 1982-11-10 | Alicante | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And click to leave the subscription form
    Then confirm that the pop-in is being displayed
      
      
      
      
      
      
      