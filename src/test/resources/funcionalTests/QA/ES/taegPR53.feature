#Author: Willian Arruda
@PR53ES @PR53 @projectFeatures
Feature: TAE - Spain
Validation of TAE percentage

  @TAE3XWithFeesPR53ES
 Scenario: Create abandon contract and verify fee 3x
    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Alicante     |       03015 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Alicante | +34745555965 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then verify TAE percentage
    
    
    
  @TAE4XWithFeesPR53ES
	 Scenario: Create abandon contract and verify fee 4x
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Castellón    |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Castellón | +34745555965 | ES - QA Automation Web |
    And choose the payment type "4x (with fees)"
     Then verify TAE percentage
    
    

  @TAE8XWithfeesES @PR53ES
   Scenario: Create abandon contract and verify fee 8x
    Given navigate to "eCommerce ES"
    And add products to a total amount of "399,75" €
    * fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | es@gmail.com | 1982-11-10 | Madrid | +34745555965 | ES - QA Automation Web |
    And choose the payment type "8x (with fees)"
     Then verify TAE percentage