#Author: Amanda Matheus
@all @customerScoring @projectFeatures @ES
Feature: Customer Scoring - Spain
  Verify if the contract is rejected by Engine Decision rules.

  @customerScoringContractES @createContracteCommerce @contractScoring
  Scenario: eCommerce: Validate the customer scoring rejection
    eCommerce:
    - Customer Age greater than 80
    - Email Domain equals automation.pt

    Given navigate to "eCommerce ES"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Calle de las Infantas | Madrid       |       28004 | SPAIN   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email                 | Birth Date | Place          | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Customer   | Scoring   | csSpain@automation.pt | 1939-11-10 | Rua Testes XXI | +34756489521 | ES - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "csSpain@automation.pt" in the subscription page
    And accept the contract terms
    Then validate that the contract was rejected

  @customerScoringBackOfficeES
  Scenario: BackOffice: Validate the customer scoring rejection
    BackOffice:
    - Validate the rejection reason
    - Validate the customer score details

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name    | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Customer Scoring | 194.36 |    0 |          |       |              |
    And click on the edit button
    When click on Decision tab
    * validate if the rejection type is "Decision Engine"
    * validate if the rejection reason is "Customer Scoring"
    * validate the customer scoring table
      | Scoring Acceptance Value | Customer Final Scoring | Decision |
      |                       25 |                     20 | Rejected |
    * click on the Engine Decision menu
    * search for decision history by contract
    Then validade the customer scoring details
      | Age | Email |
      |  10 |    10 |
