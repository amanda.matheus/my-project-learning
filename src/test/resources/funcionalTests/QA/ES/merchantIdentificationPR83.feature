#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2421686283/PR83+Merchant+Identification+for+cancellations+Decathlon+ESP
#Author: Willian Arruda
@all @PR83 @PR83ES @ES
Feature: Merchant Identification (PR83) - Spain
  Each country will parameterize in Backoffice, at the Brand level, the new field “Follow the in-store cancellations“.
  Displays in the detail screen of Normal Contracts in Merchant Space the new field “Store to Refund“.

  @creationContractPR83ES @createContractInstore
  Scenario: Create a instore contract to cancel in merchant space.
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido        | Mobile Phone | Email        | Address                    | Postal Code |
      |        | Merchant   | PR83      | Identification | +34745555965 | es@gmail.com | C. Conde del Serrallo 1 05 |       28029 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card   | Document Number |
      | Madrid        | SPAIN       | Madrid       | Spanish     | Madrid   | 26042907F | 26042907F       |
    * follow the "link" to finish my subscription
    * login as "es@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @cancellationAndReturnPR83ES @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant ES"
    And enter a valid username and password
    When click Contracts
    And search for the customer
      | Name          | Email | Webpage  |
      | Merchant PR83 |       | Merchant |
    And select the "QA Automation Instore UX1" Merchant on the list
    And select the "Financiado" status on the list
    * click on last contract details
    Then cancel An Amount An Return Partially
