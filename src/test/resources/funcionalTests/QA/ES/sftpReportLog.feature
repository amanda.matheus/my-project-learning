@all @sftpReportLog @sftpReportLogES @projectFeatures @ES
Feature: SFTP Files Report Logs - Spain

  ##----------------------------------------------------------------------------------
  ##														BackOffice
  ##----------------------------------------------------------------------------------
  @SFTPSuccessFilesTrueES
  Scenario: Search success files True
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Audit menu
    * click on SFTP Files Report Logs submenu
    * search for "True" success files created today
    Then validate the "File sent successfully" log message

  @SFTPSuccessFilesFalseES
  Scenario: Search success files False
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Audit menu
    * click on SFTP Files Report Logs submenu
    * search for "False" success files created today
    Then validate the "No sftp files report logs to show..." log message