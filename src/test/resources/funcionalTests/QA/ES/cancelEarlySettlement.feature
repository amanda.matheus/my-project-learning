@all @cancelEarlySettlement @cancelEarlySettlementES @actionsToContract @projectFeatures @ES
Feature: Cancel & Early Settlement - Spain
  Perform a cancel and early settlement on products in the contract

  ##----------------------------------------------------------------------------------
  ##                               Cancellation
  ##----------------------------------------------------------------------------------
  @totalCancelES
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @totalCancel10xES
  Scenario: Cancel the total amount of all the products in 10x this contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 10X          |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelES
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR33 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR33ES
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelPR33ES
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR36
  ##----------------------------------------------------------------------------------
  #@multicaptureContractCancellationPR36ES
  #Scenario: Multicapture Contract ES Cancellation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_MULTICAPTURE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-multicaptureES@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_ES_MULTICAPTURE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
    #Given click on close tab
    #When click on the "QA_ES_MULTICAPTURE-CANCEL" file
    #And click on the body tab
    #Then click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action      |
      #| ES     | CANCELLED | CancelTotal |

  #@marketplaceContractCancellationPR36ES
  #Scenario: Marketplace Contract ES Cancellation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_ES_MARKETPLACE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-marketplaceES@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_ES_MARKETPLACE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
    #Given click on close tab
    #When click on the "QA_ES_MARKETPLACE-CANCEL" file
    #And click on the body tab
    #Then click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action      |
      #| ES     | CANCELLED | CancelTotal |

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR47 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR47ES
  Scenario: Total Cancel amount of all the products in this contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When cancel the item
    And click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    And validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement
  ##----------------------------------------------------------------------------------
  @totalEarlySettlementES
  Scenario: Total Early Settlement
    Perform a early settlement to the total contract amount

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Total" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "FINALIZED"

  @partialEarlySettlementES
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    2 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "ONGOING"

  @partialEarlySettlementRefusedES
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When refusing an early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement PR33
  ##----------------------------------------------------------------------------------
  @partialEarlySettlementPR33ES
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement PR36
  ##----------------------------------------------------------------------------------
  @multicaptureContractEarlySettlementPR36ES
  Scenario: confirm multicapture contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name     | Amount | Days | Merchant                   | Brand | Product Type |
      | OnGoing | Will Multicapture |        |    0 | QA Automation Multicapture |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"

  @marketplaceContractEarlySettlementPR36ES
  Scenario: confirm multicapture contract
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing | Will Marketplace |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
