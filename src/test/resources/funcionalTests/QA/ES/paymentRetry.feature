#Author: Amanda Matheus
@all @paymentRetry @actionsToContract @projectFeatures @ES
Feature: Payment Retry - Spain
  New button Payment Retry inside Actions to Contract (BackOffice) to charge the unpaid installments.

  @paymentRetryES
  Scenario: Charge an installments unpaid
    BackOffice:
    1-Charge installments to the payment return as refused, status "Unpaid"
    2-Click on "Payment Retry" to paid the unpaid installments

    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Willian Arruda | 399.75 |    0 |          |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Spain" test payment page
    And charge all installment and the payment returns as refused
    Given navigate to "BackOffice ES"
    And click on the Contracts menu
    When search for "Unpaid" contract by number
    And click on the edit button
    And click on Contract Transactions tab
    And click on Actions to Contract
    And click on Payment Retry
    Then click on the Contracts menu
    And check if the contract action successfully done
