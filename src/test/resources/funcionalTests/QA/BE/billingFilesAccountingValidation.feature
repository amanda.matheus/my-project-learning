@billingFilesValidation @billingFilesValidationBE @projectFeatures
Feature: Billing Files Daily Validation - Belgium
  Verify if the files are being generated daily

  ##----------------------------------------------------------------------------------
  ##                   Billing files validation
  ##----------------------------------------------------------------------------------
  

  @financingFileValidationBE
  Scenario: Validate the generation of yesterday s financing file	
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Billing files submenu
    Then validate if financing file was generated
    
    
 @invoicingFileValidationBE
  Scenario: Validate the generation of yesterday s invoicing file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Billing files submenu
    And click on the invoicing file tab
    Then validate if invoicing file was generated