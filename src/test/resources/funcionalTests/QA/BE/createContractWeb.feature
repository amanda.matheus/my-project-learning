@all @createContracteCommerce @createContracteCommerceBE @BE
Feature: Create Contract eCommerce - Belgium
  Create contract with QA Automation business transactions inside eCommerce

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUsereCommerceBE3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address        | Municipality    | Postal Code | Country | StreetNumber | Floor |
      | Rue de Boneffe | Walloon Brabant |        1421 | BELGIUM |          423 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place                      | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Ophain-bois-seigneur-isaac | +320471691711 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace                 |
      | Belgium     | Ophain-bois-seigneur-isaac |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceBE3xWithoutFees
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address         | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Sur les Tailles | Liège        |        4540 | BELGIUM |          376 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Flône | +320474163234 | BE - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Flône      |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceBE4xFees
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Door Van dijckstraat | Hainaut      |        6811 | BELGIUM |          249 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place      | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Les Bulles | +320482942994 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Les Bulles |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceBE4xWithoutFees
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Pont du Chêne | Hainaut      |        7783 | BELGIUM |          238 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Bizet | +320484460497 | BE - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bizet      |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractNewUsereCommerceBE4xWithoutFeesGDPR @createContracteCommerce
  Scenario: eCommerce: 4x without fees, with Honorific Miss
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue du Manoir | Luxembourg   |        6250 | BELGIUM |          310 |     4 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | GDPR      |       | 1992-10-19 | Aiseau | +320490342064 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Aiseau     |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUsereCommerceBE3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address         | Municipality  | Postal Code | Country | StreetNumber | Floor |
      | Rue du Commerce | West Flanders |        8600 | BELGIUM |          247 |     4 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Woumen | +320492654106 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractExistingUsereCommerceBE3xWithoutFees
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address         | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Poste | Hainaut      |        7040 | BELGIUM |          201 |     6 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Quévy-le-grand | +320475791165 | BE - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractExistingUsereCommerceBE4xFees
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address     | Municipality  | Postal Code | Country | StreetNumber | Floor |
      | Rue Supexhe | East Flanders |        9112 | BELGIUM |          422 |    15 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place       | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Sinaai-waas | +320492654108 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractExistingUsereCommerceBE4xWithoutFees @criticalFeatures
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address     | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Noordstraat | Liège        |        4690 | BELGIUM |           78 |     8 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Boirs | +320481423355 | BE - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractECeCommerceBE3xFees
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractECeCommerceBE3xWithoutFees
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Raas van Gaverestraat | Namur        |        5300 | BELGIUM |          397 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Coutisse | +320476170220 | BE - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractECeCommerceBE4xFees
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Hoge Wei | Hainaut      |        6870 | BELGIUM |          268 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Awenne | +320499841857 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractECeCommerceBE4xWithoutFees
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Oostjachtpark | Luxembourg   |        6230 | BELGIUM |          459 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Buzet | +320492448680 | BE - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And login as "be@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  