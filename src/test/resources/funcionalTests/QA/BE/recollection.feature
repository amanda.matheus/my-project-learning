#Author: Amanda Matheus
@all @recollection @recollectionBE @BE
Feature: Recollection - Belgium
  Validate the SoftCollection process, Hard Collections process and Litigation.

  ##----------------------------------------------------------------------------------
  ##                  Soft Collection
  ##----------------------------------------------------------------------------------
  @recollectionCreateContractClass1BE @createContracteCommerce @createContractInstore @recollectionCreateContract
  Scenario: Contract Creation to Recollection tests
    eCommerce:
    1-Customer Age 24
    2-Contract Duration 4x
    3-Contract Type with fees

    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name   | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Soft       | Collections |       | 1998-11-10 | Rua Testes XXI | +320492654101 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @recollectionSoftCollectionClass1BE @projectFeatures
  Scenario: Charge installments to the payment return as refused, status "Unpaid".
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Soft Collections | 399.75 |    2 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    And click on Contract Transactions tab
    Then validate the number of planned installments
    And navigate to "Belgium" test payment page
    And charge the next installment and the payment returns as refused for "Technical" reason
    Given navigate to "BackOffice BE"
    And click on the Contracts menu
    When search for "Unpaid" contract by number
    And click on the edit button
    And click on Contract Transactions tab
    Then validate if the delay amount is "3.00"
    And validate if the installment status is "Unpaid"

  @recollectionSoftCollectionClass1BE @projectFeatures
  Scenario: Verify Sofcollection status and customer classification.
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Collections menu
    And click on the Soft Collections submenu
    And search for Soft Collection process by contract
    When validate if the Soft Collection status is "Active"
    And validate the strategy, customer classification and the action
      | Strategy  | Customer Classification | Action                |
      | Technical | Default                 | AT_Technical_Action_1 |
    Then navigate to "Belgium" test payment page
    And charge to complete the process
    Given navigate to "BackOffice BE"
    And click on the Collections menu
    And click on the Soft Collections submenu
    When search for Soft Collection process by contract
    Then validate if the Soft Collection status is "Completed"

  ##----------------------------------------------------------------------------------
  ##                  Hard Collections
  ##----------------------------------------------------------------------------------
  @recollectionCreateContractHardBE @createContracteCommerce @createContractInstore
  Scenario: Contract Creation to Recollection tests
    eCommerce:
    1-Customer Age 24
    2-Contract Duration 4x
    3-Contract Type with fees

    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name   | Email | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Hard       | Collections |       | 1995-11-10 | Rua Testes XXI | +320492654101 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @recollectionHardCollectionBE @projectFeatures
  Scenario: Verify Hard Collection process
    BackOffice:
    1-Put the intallment on Hard Collection
    2-Verify Sofcollection and Hardcollection status
    3-Verify the amounts of the contract transaction

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Hard Collections | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * get FPClient Code
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Belgium" test payment page
    * charge the next installment and the payment returns as refused for "Technical" reason
    * put the contract on Hard Collection
    Given navigate to "BackOffice BE"
    And click on the Contracts menu
    * search for "Hard Collection" contract by number
    And click on the edit button
    * click on Contract Transactions tab
    * validate if the delay amount is "32.79"
    * validate if the installment status is "Hard Collection"

  @recollectionHardCollection2BE @projectFeatures
  Scenario: Verify Hard Collection process
    BackOffice:
    1-Do upload of Performed Collections and Closed Processes
    2-Verify Hardcollection and contract status

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    * click on the Collections menu
    * click on the Soft Collections submenu
    * search for Soft Collection process by contract
    * validate if the Soft Collection status is "SENT_TO_HARDCOLLECTION"
    * click on the Collections menu
    * click on the HardCollections Files submenu
    * download the Hard Collection Entries
    * click on the Perfomed Collections tab
    * upload the file "Performed Collections"
    * click on the Closed Processes tab
    * upload the file "Closed Processes"
    * click on the Collections menu
    * click on the Hard Collections submenu
    * search for Hard Collection process by contract
    * validate if the Hard Collection status is "Settled"
    And click on the Contracts menu
    * search for "OnGoing" contract by number
    And click on the edit button
    * click on Contract Transactions tab
    Then validate if the installment status is "Paid"

  ##----------------------------------------------------------------------------------
  ##                  				Litigation
  ##----------------------------------------------------------------------------------
  #@recollectionCreateContractLitigationBE @createContracteCommerce @createContractInstore
  #Scenario: Contract Creation to Recollection tests
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_399.75NCL" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #Then validate that the contract was successfully created

  @recollectionLitigationBE @createContracteCommerce
  Scenario: Verify Litigation process
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name      | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Litigation Process | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Belgium" test payment page
    * charge the next installment and the payment returns as refused for "Technical" reason
    * put the contract on Hard Collection

  @recollectionLitigation2BE @projectFeatures
  Scenario: Verify Litigation process
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status          | Customer Name      | Amount | Days | Merchant          | Brand                     | Product Type |
      | HARD COLLECTION | Litigation Process | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * get FPClient Code
    And get the Contract Number
    * click on Contract Transactions tab
    * validate if the delay amount is "32.79"
    * click on the Collections menu
    * click on the HardCollections Files submenu
    * click on the Closed Processes tab
    * upload the file "Closed Processes Unpaid"
    * click on the Collections menu
    * click on the Hard Collections submenu
    * search for Hard Collection process by contract
    * validate if the Hard Collection status is "Sent to Litigation"
    And click on the Contracts menu
    * search for "Litigation" contract by number
    And click on the edit button
