@all @criticalFeatures @regression @regressionBE @BE
Feature: Regression - Belgium
  This feature creates a contract for a new user with QA Automation business transactions, validates the contract values on the Back Office and Self Care

  @regressionBE3xFees @createContracteCommerce
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address             | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Passage du Cuberdon | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name      | Email | Birth Date | Place               | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithFees |       | 1982-11-10 | Passage de la Frite | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionBE3xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name             | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression ThreeXWithFees | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 3X           |
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "419,75"
    * validate the Capital Amount on the Contract Transactions is "133,25"

  #@regressionBE3xFees2 @changeEmailSelfcareBE
  #Scenario: Selfcare: Change email
    #Given navigate to "Selfcare BE"
    #And enter the new username
    #When click on My Account menu
    #* click on Email
    #* change email to "newEmailCustomerBE@gmail.com"

  @regressionBE3xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address             | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Passage du Cuberdon | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name         | Email | Birth Date | Place               | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithoutFees |       | 1982-11-10 | Passage de la Frite | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionBE3xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name                | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression ThreeXWithoutFees | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 3X           |
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "399,75"
    * validate the Capital Amount on the Contract Transactions is "133,25"

  #@regressionBE3xWithoutFees2
  #Scenario:  Change password
    #Given navigate to "Selfcare BE"
    #And enter the new username
     #When click on My Account menu
    #* click on Password
    #* change password from "Oney1testes" to "Oney2testes"
    #Then enter the new password of "oldPasswordCustomerES@gmail.com"

  @regressionBE4xFees @createContracteCommerce
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address             | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Passage du Cuberdon | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name     | Email | Birth Date | Place               | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithFees |       | 1982-11-10 | Passage de la Frite | +320492654101 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionBE4xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name            | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression FourXWithFees | 594.11 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "623,82"
    * validate the Capital Amount on the Contract Transactions is "148,52"

  #@regressionBE4xFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare BE"
    #And enter the new username
    #When validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @regressionBE4xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address             | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Passage du Cuberdon | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name        | Email | Birth Date | Place               | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithoutFees |       | 1982-11-10 | Passage de la Frite | +320492654101 | BE - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionBE4xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name               | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression FourXWithoutFees | 594.11 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "594,11"
    * validate the Capital Amount on the Contract Transactions is "148,52"

  #@regressionBE4xWithoutFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare BE"
    #And enter the new username
    #When validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @onGoingToFinalizedBE
  Scenario: On Going to Finalized
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Regression    |        |    0 |          |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Belgium" test payment page
    And pay all the installments
    Given navigate to "BackOffice BE"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR33BE
  Scenario: On Going to Finalized PR33
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Belgium" test payment page
    And pay all the installments
    Given navigate to "BackOffice BE"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR36BE
  Scenario: On Going to Finalized PR36
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Belgium" test payment page
    And pay all the installments
    Given navigate to "BackOffice BE"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments
    
 @onGoingToFinalizedPR47BE
  Scenario: On Going to Finalized PR47
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Belgium" test payment page
    And pay all the installments
    Given navigate to "BackOffice BE"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  #@endToEndMerchantBE @smokeTestMerchantBE
  #Scenario: Create a contract, perform smoke tests and delete account on Selfcare
    #Instore: 3x with fess and with 1 product
#
    #Given navigate to "Merchant BE"
    #And enter a valid username and password
    #And click on create a contract
    #* select the "QA Automation Instore UX1" Merchant
    #And click on Generate Contract Number
    #And fill in the product information
      #| Description | Category          | Quantity | Price  |
      #| Automa Item | Cars & motorbikes |        1 | 399.75 |
    #When choose "3x (with fees)" Oney transaction type
    #And fill in the client personal information
      #| Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address       | Postal Code |
      #|        | Willian    | Arruda    |         | +320492654101 |       | Rue de la Loi |        7000 |
    #And fill in the document information
      #| Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      #| Antwerpen     | Bruxelles   | Bruxelles    | Belgium     |          |         |          123456 |
    #And follow the "link" to finish my subscription
    #Then insert a valid payment card
    #And validate that the contract was successfully created with a new user
    #Given navigate to "Selfcare BE"
    #And enter a newly created username and password
    #And click on My Account menu
    #When click on Delete Account
    #Then confirm the account is deleted
