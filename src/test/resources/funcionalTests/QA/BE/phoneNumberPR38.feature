#Author: Amanda Matheus
@all @PR38 @PR38BE @criticalFeatures @BE
Feature: Phone Number Changes (PR38) - Belgium

  ##----------------------------------------------------------------------------------
  ##                        Ecommerce Validation
  ##----------------------------------------------------------------------------------
  @ecommerceValidPrefixPR38BE
  Scenario: Valid prefix and invalid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page   
    Current regex expression: ^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$
    Prefix: 0032 or +32
    Nr of digits: 10
    Must begin with: 45, 46, 47, 48 or 49
		- "Is Editable" Toggle button must be DEACTIVATED inside: Backoffice > Configurations > Subscription Page Configurations

    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Brussels | +32592654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  @ecommerceInvalidPrefixPR38BE
  Scenario: Invalid prefix and valid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    
    Current regex expression: ^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$
    Prefix: 0032 or +32
    Nr of digits: 10
    Must begin with: 45, 46, 47, 48 or 49

    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Brussels | +31492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  ##----------------------------------------------------------------------------------
  ##                                BackOffice Validation
  ##----------------------------------------------------------------------------------
  @backOfficePR38BE
  Scenario: Validate the regex expression
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Phone Numbers
    Then validate the regrex expression matches the "^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$" string

  @PR38BackOfficeCustomerValidBE
  Scenario: Valid phone number in BackOffice
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Customers menu
    And search for a customer
    And click on the edit button
    When change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +320492654101 |
    Then validate the "BackOffice" mobile phone

  @backOfficeCustomerInvalidPR38BE
  Scenario: Invalid phone number in BackOffice
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Customers menu
    And search for a customer
    And click on the edit button
    When change the "BackOffice" mobile phone
      | Mobile Phone   |
      | +3204926541039 |
    Then validate the "BackOffice" message error
      | Message              |
      | Phone Number Invalid |

  ##----------------------------------------------------------------------------------
  ##                        Selfcare Validation
  ##----------------------------------------------------------------------------------
  #@selfcareValidPR38BE
  #Scenario: Valid phone number in Selfcare
  #Current regex expression: ^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$
  #Prefix: 0032 or +32
  #Nr of digits: 10
  #Must begin with: 045, 046, 047, 048 or 049
  #
  #Given navigate to "Selfcare BE"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +320492654104        | +320492654100    |
  #Then validate the "Selfcare" mobile phone
  #
  #@selfcareInvalidPR38BE
  #Scenario: Invalid phone number in Selfcare
  #Current regex expression: ^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$
  #Prefix: 0032 or +32
  #Nr of digits: 10
  #Must begin with: 045, 046, 047, 048 or 049
  #
  #Given navigate to "Selfcare BE"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +320492654104        | +32412654101     |
  #* validate the "Selfcare" message error
  #| Message              |
  #| Phone Number Invalid |
  ##----------------------------------------------------------------------------------
  ##                        Merchant Validation
  ##----------------------------------------------------------------------------------
  @merchantValidPR38BE
  Scenario: Valid phone number in Merchant
    Current regex expression: ^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$
    Prefix: 0032 or +32
    Nr of digits: 10
    Must begin with: 045, 046, 047, 048 or 049

    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category               | Quantity | Price |
      | Automa Item | Clothing & accessories |        1 |   300 |
    When choose "3x (with fees)" Oney transaction type
    Then fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address       | Postal Code |
      |        | Amanda     | Matheus   |         | +320492654101 |       | Rue de la Loi |        7000 |

  @merchantInvalidPR38BE
  Scenario: Invalid phone number on Merchant
    Current regex expression: ^(?:(?:\+|00)32|)[0]{1}[4]{1}[5-9]{1}[0-9]{7}$
    Prefix: 0032 or +32
    Nr of digits: 10
    Must begin with: 045, 046, 047, 048 or 049

    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category               | Quantity | Price |
      | Automa Item | Clothing & accessories |        1 |   300 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address       | Postal Code |
      |        | Amanda     | Matheus   |         | +321492654139 |       | Rue de la Loi |        7000 |
    Then validate the "Merchant" message error
      | Message              |
      | Phone Number Invalid |
