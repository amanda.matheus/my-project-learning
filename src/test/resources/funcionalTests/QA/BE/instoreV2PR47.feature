#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/1435566082/PR+47+In-store+V2
#Author: Amanda Matheus
@all @PR47 @PR47BE @BE @projectFeatures
Feature: Instore UX2 (PR47) - Belgium
  New instore V2 model to create contract by API.

  ##----------------------------------------------------------------------------------
  ##                              Subscription
  ##----------------------------------------------------------------------------------
  #@createContractPR47BE @createContractInstore
  #Scenario: Create a UX2 contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "PR47Payment_QA_BE" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-postmanBE@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
#
  #@createContractAndConfirmPR47BE @createContractInstore
  #Scenario: Create a UX2 contract by API
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "PR47Payment_QA_BE" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-postmanBE@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "BE-QAA-UX2Confirm" file
    #And click on the body tab
    #And click on the send button

  ##----------------------------------------------------------------------------------
  ##                              BackOffice
  ##----------------------------------------------------------------------------------
  @backOfficePR47BE
  Scenario: Validate the UX2 contract on BackOffice
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status           | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | Waiting Delivery | Amanda        | 399.75 |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Brand Info tab
    Then validate if the UX is "UX2: In-store direct integration"
    And click on Scoring tab
    And validate the score

  ##----------------------------------------------------------------------------------
  ##                              Merchant
  ##----------------------------------------------------------------------------------
  @merchantUX2PR47BE
  Scenario: Validate the Merchant UX2
    Given navigate to "Merchant BE"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX2" Merchant
    Then validate the merchant as "QA Automation Instore UX2"

  @merchantUX2RDTPR47BE
  Scenario: Validate the button RDT and the new status
    Given navigate to "Merchant BE"
    And enter a valid username and password
    When click Contracts
    And select the "QA Automation Instore UX2" Merchant on the list
    And select the "En attente de livraison" status on the list
    Then click on the Ready to Delivery button
    And select the "Tous les statuts" status on the list
    And search for external reference
    And validate the status "Financé"
