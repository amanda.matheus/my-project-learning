@all @customerCardDetectionPR24 @customerCardDetectionPR24BE @criticalFeatures @BE
Feature: Customer Card Detection (PR24) - Belgium
  Validation of payment method in the checkout frame.

  ##----------------------------------------------------------------------------------
  ##                          Subscription - New User
  ##----------------------------------------------------------------------------------
  @subscriptionNewUserPR24BE
  Scenario: Subscription for a new user - Validate the rejection of the payment method
    Invalid card used: Visa (4658 5840 9000 0001)

    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |           39 |    90 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a invalid payment card
    And validate that the card is unauthorized

  ##----------------------------------------------------------------------------------
  ##                          Subscription - Existing User
  ##----------------------------------------------------------------------------------
  @subscriptionExistingUserPR24BE
  Scenario: Subscription for a existing user - Validate the rejection of the payment method
    Invalid card used: Visa (4658 5840 9000 0001)

    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |           33 |    45 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   | be@gmail.com | 1982-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And login as "be@gmail.com" in the subscription page
    Then insert a invalid payment card to my account
    And validate that the card is unauthorized
  ##----------------------------------------------------------------------------------
  ##                          Selfcare - Add a card / Remove a card
  ##----------------------------------------------------------------------------------
  #@selfcareAddACardValidPR24BE
  #Scenario: Selfcare > Add a Card - Validate the acceptance of the payment method
    #Valid card used: Visa (5352 1515 7000 3404)
#
    #Given navigate to "Selfcare BE"
    #And enter a valid username and password
    #When click on My Account menu
    #And click on Payment Info
    #And click on Add a card
    #And insert a "Valid" payment card on "Payment Info"
    #And validate that the card is authorized on "Payment Info"
    #And do you want to change the payment method to other contracts "No"
    #And click on Payment Info
    #Then click on remove a card
#
  #@selfcareAddACardInvalidPR24BE
  #Scenario: Selfcare > Add a Card - Validate the rejection of the payment method
    #Invalid card used: Visa (4658 5840 9000 0001)
#
    #Given navigate to "Selfcare BE"
    #And enter a valid username and password
    #When click on My Account menu
    #And click on Payment Info
    #And click on Add a card
    #And insert a "Invalid" payment card on "Payment Info"
    #Then validate that the card is unauthorized
