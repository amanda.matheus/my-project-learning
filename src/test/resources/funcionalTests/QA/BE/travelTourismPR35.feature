#Author: Amanda Matheus
@all @travelAndTourism @travelAndTourismBE @projectFeatures @BE
Feature: Travel and Tourism	(PR35) - Belgium
  Validation of new travel services.

  @travelSingleOnePriceBE @createContractInstore
  Scenario: 3x with fees and with 1 product + 1 travel (Travel Journey Single and One Price)
    Given navigate to "Merchant BE"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * click on Add Travels
    * fill the Traveller Info
      | Number of Tickets | Type of Journey | Accommodation and Vehicle | Pricing Type |
      |                 3 | Aller           | Oui                       | Un seul prix |
    * fill the Departure Info
      | Mean of Transport | Departure City | Departure Date | Arrival City | Ticket Category | Travel with insurance | Exchangeable | Price |
      | Plane             | Lisbon         | 2025-12-20     | Madrid       | Economic        | Non                   | Non          |       |
    * fill the Stay, accommodations and vehicle rental
      | Type of Stay | City   | Arrival Date | Departure Date | Number of rooms | Stay with insurance | Vehicle Rental | Price |
      | Hotel        | Madrid | 2025-12-21   | 2025-12-22     |               1 | Non                 | Non            |       |
    * fill the travel total price
      | Price |
      |   300 |
    * check the total basket price
      | Price  |
      | 300.00 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email                      | Address       | Postal Code |
      |        | Aurora     | Matheus   |         | +320492654101 | testing-travelBE@gmail.com | Rue de la Loi |        7000 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Antwerpen     | Bruxelles   | Bruxelles    | Belgium     |          |         |          123456 |
    * follow the "link" to finish my subscription
    #* get the external reference
    * login as "testing-travelBE@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  #@travelBackAndForthMultiplePriceBE @createContractInstore
  #Scenario: 4x with fees and with 1 product + 1 travel (Travel Journey Back & Forth and Multiple Price)
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_300.00ECT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-travelBE@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
#
  #@travelRulebookBE
  #Scenario: Instore: Validate the Rulebook Rejection to "Number of Rooms >= 10"
  #-Rulebook name: AUTOMATION TESTS (DONT CHANGE)
#	-Rejection rules: 
#	.Age between 18 and 21
#	.Customer scoring < 25
#	.if Merchant type (physical) contains number of ruooms >= 10.
#	
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_300.00NCT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #Then insert a valid payment card
    #And validate that the contract was rejected

  @travelBackOfficeRulebookBE
  Scenario: BackOffice: Validate the travel rejection.
  -Rulebook name: AUTOMATION TESTS (DONT CHANGE)
	-Rejection rules: 
	.Age between 18 and 21
	.Customer scoring < 25
	.if Merchant type (physical) contains number of ruooms >= 10.
	
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name  | Amount | Days | Merchant                  | Brand | Product Type |
      | Rejected | Aurora Matheus | 300.00 |    0 | QA Automation Instore UX1 |       |              |
    And click on the edit button
    When click on Decision tab
    Then validate if the rejection type is "Decision Engine"
    * validate if the rejection reason is "Number of Rooms"

  @travelBackOfficeBE
  Scenario: BackOffice: Validate the travel items list.
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      |        | Aurora Matheus | 300.00 |    0 |          |       |              |
    And click on the edit button
    When click on Items List tab
    Then validate the items list of Travels table on "BackOffice"
