@all @checkout @checkoutBE @BE @criticalFeatures
Feature: Checkout response code testing - Belgium
  Create a contract with the amounts below to simulate and trigger the listed response codes

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - PSP Expired Card - Pick Up
  ##----------------------------------------------------------------------------------
  @expiredCardContractBE
  Scenario: Contract creation with amount ends with xxx,33 - Response code 30033 Expired card - Pick up that should be rejected
    Given navigate to "eCommerce BE"
    And add products to a total amount of "64,41" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Willian    | Arruda    |       | 1982-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid VISA payment card
    And validate that the contract was rejected by checkout
