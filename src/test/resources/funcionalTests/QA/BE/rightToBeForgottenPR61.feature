#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2274492446/PR+61+FATA+Right+to+be+forgotten
#Author: Amanda Matheus
@all @PR61 @PR61BE @BE
Feature: Right to be Forgotten (PR61 LT1) - Belgium
  Implement rules to delete or anonymize the customer data and the contracts data.

  #@createContractPR61BE @createContracteCommerce @createContractInstore
  #Scenario: Create a Contract to PR61
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_399.75PR61" file
    #And click on the body tab
    #And click on the send button
    #And get link to finish my subscription
    #Then insert a valid payment card
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "BE-QAA-WebPR61 Confirm" file
    #And click on the body tab
    #Then click on the send button

  @totalEarlySettlementPR61BE @createContracteCommerce @createContractInstore @createContractInstore
  Scenario: Total Early Settlement to PR61
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name             | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Right Forgotten Finalized | 399.75 |   -1 |          |       |             |
    And click on the edit button
    And click on Actions to Contract
    Then perform a "total" early settlement
    And click on Contract Transactions tab
    And validate the Contract Transactions status is "Paid"

  @rightToBeForgottenRulePR61BE @projectFeatures
  Scenario: Set the ritgh to be forgotten rules
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Configurations menu
    When click on the Right to be Forgotten submenu
    Then include rule
      | Section                           | Status    | Option                           |
      | Active contracts                  | Active    | Refuse demand                    |
      | Finalized and cancelled contracts | Finalized | Accept demand and delete data    |
      | Rejected contracts                | Reject    | Accept demand and anonymize data |

  @rightToBeForgottenFinalizedPR61BE @projectFeatures
  Scenario: Erase the contract and customer information in BO
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status    | Customer Name             | Amount | Days | Merchant | Brand | Product Type |
      | Finalized | Right Forgotten Finalized | 399.75 |   -1 |          |       |             |
    And click on the edit button
    And get the Contract Number
    And click on the customer name
    And click on Actions to Customer
    And click on right to be forgotten button
    And confirm the right to be forgotten
    Then click on the Customers menu
    And search for the customer by email
    And validade if the customer exist
    And click on the Contracts menu
    And search contract by number
    And validade if the contract exist

  @rightToBeForgottenRejectPR61BE @projectFeatures
  Scenario: Anonymizes the contract information in BO
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status   | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | Rejected |               |  64.41 |    0 |          |       |             |
    And click on the edit button
    And get the Contract Number
    And get the Contract link
    And click on the customer name
    And click on Actions to Customer
    And click on right to be forgotten button
    And confirm the right to be forgotten
    And click on the Contracts menu
    And search contract by number
    And validade if the contract exist
    And navigate to "link"
    Then validate the contract

  @rightToBeForgottenActivePR61BE @projectFeatures
  Scenario: Verify when the demand is refused.
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 |          |       |             |
    And click on the edit button
    And get the Contract Number
    And click on the customer name
    And click on Actions to Customer
    And click on right to be forgotten button
    Then validade the right to be forgotten message error
