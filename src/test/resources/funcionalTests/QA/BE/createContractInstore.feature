@all @createContractInstore @createContractInstoreBE @BE
Feature: Create Contract Instore - Belgium
  Create contract with QA Automation business transactions inside Merchant Space

  ##----------------------------------------------------------------------------------
  ##                   Instore - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUserMerchantBE3xWithFees @criticalFeatures
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address      | Postal Code |
      |        | Willian    | Arruda    |         | +320471533250 |       | Westdorp 177 |        4130 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Tilff         | Tilff       | Liège        | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUserMerchantBE3xWithoutFees @criticalFeatures
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address               | Postal Code |
      |        | Willian    | Arruda    |         | +320492654101 |       | Rue Camille Joset 307 |        3870 |
    And fill in the document information
      | Issuing Place    | Birth place      | Municipality | Nationality | Province | ID Card | Document Number |
      | Rukkelingen-loon | Rukkelingen-loon | Limburg      | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUserMerchantBE4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address        | Postal Code |
      |        | Willian    | Arruda    |         | +320493655993 |       | Strepestraat 3 |        6767 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Lamorteau     | Lamorteau   | Hainaut      | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUserMerchantBE4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address            | Postal Code |
      |        | Willian    | Arruda    |         | +320496778617 |       | Rue de Boneffe 356 |        3680 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Opoeteren     | Opoeteren   | Limburg      | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    Then insert a valid payment card
    And validate that the contract was successfully created

  ##----------------------------------------------------------------------------------
  ##                   Instore - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUserMerchantBE3xWithFees @criticalFeatures
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address       | Postal Code |
      |        | Amanda     | Matheus   |         | +320492654101 | be@gmail.com | Rue de la Loi |        7000 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Antwerpen     | Bruxelles   | Bruxelles    | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantBE3xWithoutFees
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    Then choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address                       | Postal Code |
      |        | Amanda     | Matheus   |         | +320498443534 | be@gmail.com | Rue du Bourgmestre Dandoy 437 |        8710 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality  | Nationality | Province | ID Card | Document Number |
      | Ooigem        | Ooigem      | West Flanders | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantBE4xWithFees @criticalFeatures
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address       | Postal Code |
      |        | Amanda     | Matheus   |         | +320492654101 | be@gmail.com | Rue de la Loi |        7000 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Antwerpen     | Bruxelles   | Bruxelles    | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantBE4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address               | Postal Code |
      |        | Amanda     | Matheus   |         | +320473605868 | be@gmail.com | Chaussée de Wavre 133 |        3850 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Binderveld    | Binderveld  | Limburg      | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantBE3xWithFees
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address         | Postal Code |
      |        | Amanda     | Matheus   |         | +320490856065 | be@gmail.com | Nieuwe Baan 176 |        3830 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Berlingen     | Berlingen   | Limburg      | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantBE3xWithoutFees
  Scenario: Instore: 3x without fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address             | Postal Code |
      |        | Amanda     | Matheus   |         | +320474383765 | be@gmail.com | Poolse Winglaan 231 |        1750 |
    And fill in the document information
      | Issuing Place       | Birth place         | Municipality    | Nationality | Province | ID Card | Document Number |
      | Sint-martens-lennik | Sint-martens-lennik | Flemish Brabant | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantBE4xWithFees
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address           | Postal Code |
      |        | Amanda     | Matheus   |         | +320474388569 | be@gmail.com | Prinsenstraat 226 |        4141 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Louveigné     | Louveigné   | Liège        | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantBE4xWithoutFees
  Scenario: Instore: 4x without fees
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address            | Postal Code |
      |        | Amanda     | Matheus   |         | +320479172429 | be@gmail.com | Stationsstraat 353 |        8480 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality  | Nationality | Province | ID Card | Document Number |
      | Ichtegem      | Ichtegem    | West Flanders | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created
