#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2421686283/PR83+Merchant+Identification+for+cancellations+Decathlon+ESP
#Author: Willian Arruda
@all @PR83 @PR83BE @BE
Feature: Merchant Identification (PR83) - Belgium
  Each country will parameterize in Backoffice, at the Brand level, the new field “Follow the in-store cancellations“.
  Displays in the detail screen of Normal Contracts in Merchant Space the new field “Store to Refund“.

  @creationContractPR83BE @createContractInstore
  Scenario: Create a instore contract to cancel in merchant space.
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email        | Address       | Postal Code |
      |        | Merchant   | PR83      |         | +320492654101 | be@gmail.com | Rue de la Loi |        7000 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Antwerpen     | Bruxelles   | Bruxelles    | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    And login as "be@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @cancellationAndReturnPR83BE @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant BE"
    And enter a valid username and password
    When click Contracts
    And search for the customer
      | Name          | Email | Webpage  |
      | Merchant PR83 |       | Merchant |
    And select the "QA Automation Instore UX1" Merchant on the list
    And select the "Financé" status on the list
    And click on last contract details
    Then cancel An Amount An Return Partially
