@all @taCompensationBE @taCompensation @BE @criticalFeatures
Feature: Create Contract Instore for TA Compensation - Belgium
  Create contract with QA Automation business transactions inside Merchant Space

  @crpMerchantBE 
  Scenario: Create a contract with CRP Merchant to validate the CRP file
    Given navigate to "Merchant BE"
    And enter a valid username and password
    When click on create a contract
    * select the "CRP" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price |
      | Automa Item | Cars & motorbikes |        1 |   300 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address       | Postal Code |
      |        | Willian    | Arruda    |         | +320492654101 |       | Rue de la Loi |        7000 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Antwerpen     | Bruxelles   | Bruxelles    | Belgium     |          |         |          123456 |
    * follow the "link" to finish my subscription
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @crpFile
  Scenario: Validate the CRP file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Configurations menu
    * click on the PSP List
    * click on the edit CRP Merchant
    * click on CRP Files and download the latest file
    * validate the CRP File
