#Author: Ronald Quintanilha
@all @PR34 @beforeYouGoPR34BE @projectFeatures 
Feature: Before you Go Pop-in	(PR34) - Belgium
  Validation of new Before you Go Pop-in section.

  @BFYGConfigurationSplitBackOfficeBE
  Scenario: BackOffice: Validate the Subscription Page configs Split section
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs Split submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active
    
  @BFYGConfigurationPLBackOfficeBE
  Scenario: BackOffice: Validate the Subscription Page configs PL section
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs PL submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active
    
  @createContractForPR34BE
  Scenario: Subscription: Validate if the information is being displayed in the contract.
  	Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |           39 |    90 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Ronald    | BeforeUgo    |       | 1982-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And click to leave the subscription form
    Then confirm that the pop-in is being displayed
      
      
      
      
      
      
      