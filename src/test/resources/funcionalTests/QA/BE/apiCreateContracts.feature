#@apiQABE @createContracteCommerce @apiQA
#Feature: Create Contracs by API - Belgium
#
  ##----------------------------------------------------------------------------------
  ##                               eCommerce - Ecommerce
  ##----------------------------------------------------------------------------------
  #@QA_BE_3x001_194.36NC
  #Scenario: 3x001 | 194.36 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_3x001_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_3x002_194.36NC
  #Scenario: 3x002 | 194.36 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_3x002_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_4x001_399.75NC
  #Scenario: 4x001 | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_4x001_399.75NC_GDPR
  #Scenario: 4x002 | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_399.75NC_GDPR" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_4x002_399.75NC
  #Scenario: 4x002 | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x002_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_3x001_399.75EC
  #Scenario: 3x001 | 399.75 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_3x001_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "be@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
#
  #@QA_BE_3x002_399.75EC
  #Scenario: 3x002 | 399.75 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_3x002_399.75EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "be@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
#
  #@QA_BE_4x001_194.36EC
  #Scenario: 4x001 | 194.36 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_194.36EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "be@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
#
  #@QA_BE_4x002_194.36EC
  #Scenario: 4x002 | 194.36 | EC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x002_194.36EC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "be@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
#
  ##----------------------------------------------------------------------------------
  ##						eCommerce - Recollections
  ##----------------------------------------------------------------------------------
  #@QA_BE_4x001_194.36NC
  #Scenario: 4x001 | 194.36 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_194.36NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_4x001_194.36NCH
  #Scenario: 4x002 | 194.36 | NCH
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4x001_194.36NCH" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  ##----------------------------------------------------------------------------------
  ##								eCommerce - PR33
  ##----------------------------------------------------------------------------------
  #@QA_BE_3XP_399.75NC
  #Scenario: 3XP | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_3XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_4XP_399.75NC
  #Scenario: 4XP | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_4XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And insert a valid payment card
    #And validate that the contract was successfully created
#
  #@QA_BE_6XP_399.75NC
  #Scenario: 6XP | 399.75 | NC
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And deactivate SSl Certificate
    #When click on the "QA_BE_6XP_399.75NC" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And fill the additional fields
      #| Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      #| Tester     | Automation |           1000 |              300 | Employer           |
    #And insert a valid payment card
    #And validate that the contract was successfully created
#		    Given navigate to "BackOffice BE"
    #And enter a valid username and password
    #And click on the Contracts menu
    #When search for an contract created
      #| Status  | Customer Name    | Amount | Days | Merchant          | Brand                     | Product Type |
      #|  |  |  |    0 | TWC_PHY |  |           |
    #And click on the edit button