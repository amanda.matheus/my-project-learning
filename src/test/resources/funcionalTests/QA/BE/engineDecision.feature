#Author: Amanda Matheus
@all @engineDecision @engineDecisionBE @criticalFeatures @BE @critical
Feature: Engine Decision - Belgium
  Verify if the contract is rejected by Engine Decision rules.

  @rulebookAgeContractBE
  Scenario: Validate the engine decision rulebook rejection
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Engine     | Decision  |       | 2002-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was rejected

  @rulebookAgeNackOfficeBE
  Scenario: Validate the rejection reason in BackOffice
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status   | Customer Name   | Amount | Days | Merchant          | Brand                     | Product Type |
      | Rejected | Engine Decision | 194.36 |    0 | QA Automation Web | QA Automation Tests - Web |             |
    And click on the edit button
    And click on Decision tab
    Then validate if the rejection type is "Moteur de décision"
    And validate if the rejection reason is "Age between 18 and 21"
