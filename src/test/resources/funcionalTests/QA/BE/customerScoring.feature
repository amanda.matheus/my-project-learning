#Author: Amanda Matheus
@all @customerScoring @projectFeatures @BE
Feature: Customer Scoring - Belgium
  Verify if the contract is rejected by Engine Decision rules.

  @customerScoringContractBE @createContracteCommerce
  Scenario: eCommerce: Validate the customer scoring rejection
    eCommerce:
    - Customer Age greater than 80
    - Email Domain equals automation.pt

    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email            | Birth Date | Place                | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Customer   | Scoring   | be@automation.pt | 1939-11-10 | Boulevard Industriel | +320492654101 | BE - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And login as "be@automation.pt" in the subscription page
    And accept the contract terms
    Then validate that the contract was rejected

  @customerScoringBackOfficeBE
  Scenario: BackOffice: Validate the customer scoring rejection
    BackOffice:
    - Validate the rejection reason
    - Validate the customer score details

    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name    | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Customer Scoring | 194.36 |    0 |          |       |             |
    And click on the edit button
    When click on Decision tab
    And validate if the rejection type is "Moteur de décision"
    And validate if the rejection reason is "Customer Scoring"
    And validate the customer scoring table
      | Scoring Acceptance Value | Customer Final Scoring | Decision |
      |                       25 |                     20 | Rejected |
    And click on the Engine Decision menu
    And search for decision history by contract
    And validate the customer scoring table
      | Scoring Acceptance Value | Customer Final Scoring | Decision |
      |                       25 |                     20 | Rejected |
    Then validade the customer scoring details
      | Age | Email |
      |  10 |    10 |
