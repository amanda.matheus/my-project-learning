@all @PR33 @PR33BE @BE
Feature: Business Transaction Revamp (PR33) - Belgium

  @PR33MerchantBE @projectFeatures
  Scenario: Validate the tab Business Transaction in Merchant Edition
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "QA Automation PR33"
    And click on the edit button
    * click on the "Business Transactions" tab
    Then validate the business transactions

  @contractAmountBE3xFees @projectFeatures
  Scenario: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand                     | Product Type |
      | OnGoing |               | 399.75 |    3 | QA Automation PR33 | QA Automation Tests - Web | 4X           |
    * check if the status is "ONGOING"
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "423,75"
    * validate the Capital Amount on the Contract Transactions is "99,93"

    
  ##-----------------------------------------------------------------------------------
  ##                   eCommerce - PR33
  ##----------------------------------------------------------------------------------
  @createContractBE4XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address     | Municipality  | Postal Code | Country | StreetNumber | Floor |
      | Place Fayat | West Flanders |        8340 | BELGIUM |          166 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Hoeke | +320474979040 | BE - QA Automation PR33 |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @createContractBE6XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: eCommerce: 6x with fees
    Given navigate to "eCommerce BE"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address    | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Heirstraat | Hainaut      |        7190 | BELGIUM |          199 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place                  | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Marche-lez-ecaussinnes | +320478618032 | BE - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user
    
    