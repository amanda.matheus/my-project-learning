#@all @checkout @checkoutBE @BE
#Feature: Safewatch Rejection - Belgium
  #Create a contract with the personal info below to simulate and a "bad" client
#
  #@safewatchBE @criticalFeatures
  #Scenario: Safewatch - First Name, Last Name, Birth Date
    #Given navigate to "eCommerce BE"
    #And add products to a total amount of "194,36" €
    #And fill in the shipping address
      #| Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      #| Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    #When fill in the payment information
      #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone  | Tenant                 |
      #| Miss      | Private Person | Fayssal    | ABBAS     |       | 1955-01-01 | Brussels | +320492654101 | BE - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | BirthPlace |
      #| Belgium     | Bruxelles  |
    #Then insert a valid payment card
    #Then validate that the contract was rejected
    #Given navigate to "BackOffice BE"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search for "Rejected" contract by reference
    #And click on the edit button
    #When click on Decision tab
    #Then validate the rejection reason is "SafeWatch"
