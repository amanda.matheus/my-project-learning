#Author: Willian Arruda
#@all @multicaptureAndMarketplace @multicaptureAndMarketplaceBE @criticalFeatures @BE @createContracteCommerce  @createContractInstore 
#Feature: Multicapture & Marketplace (PR36) - Belgium
  #Creating contracts via Postman
#
  #@multicaptureContractCreationPR36BE @createContracteCommerce  @createContractInstore
  #Scenario: Multicapture Contract BE Creation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_MULTICAPTURE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-multicaptureBE@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_BE_MULTICAPTURE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
#
  #@marketplaceContractCreationPR36BE @createContracteCommerce  @createContractInstore
  #Scenario: Marketplace Contract BE Creation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_BE_MARKETPLACE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-marketplaceBE@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_BE_MARKETPLACE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
