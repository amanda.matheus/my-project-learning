#@all @paymentModificationPR30 @paymentModificationPR30BE @BE @projectFeatures
#Feature: Selfcare Payment Method Modification (PR30) - Belgium
#
  ##----------------------------------------------------------------------------------
  ##                          Selfcare - Add a card / Remove a card
  ##----------------------------------------------------------------------------------
  #@selfcareAddACardPR30BE
  #Scenario: Validate the payment method modification on Selfcare > Add Card
    #Valid card used: Visa (5352 1515 7000 3404)
#
    #Given navigate to "Selfcare BE" 
    #And enter a valid username and password
    #When click on My Account menu
    #* click on Payment Info
    #* click on Add a card
    #And insert a "Valid" payment card on "Selfcare" 
    #And validate that the card is authorized on "Selfcare" 
    #Then do you want to change the payment method to other contracts "Yes" 
    #Given navigate to "BackOffice BE"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search contract by number
    #When click on the edit button
    #* click on Payment Data tab
    #Then validate the change credit card request
    #Given navigate to "Selfcare BE"
    #And enter a valid username and password
    #When click on My Account menu
    #* click on Payment Info
    #Then click on remove a card
    #
    #
  ##----------------------------------------------------------------------------------
  ##                          Selfcare - Change Credit Card
  ##----------------------------------------------------------------------------------
  #@selfcareChangeCreditCardPR30BE
  #Scenario: Validate the payment method modification on Selfcare > Change Credit Card
    #Valid card used: Visa (5352 1515 7000 3404)
#
    #Given navigate to "Selfcare BE"
    #And enter a valid username and password
    #* click on contract detail
    #When click on Change Credit Card on "Selfcare" 
    #And insert a "Valid" payment card on "Selfcare" 
    #And validate that the card is authorized on "Selfcare" 
    #And do you want to change the payment method to other contracts "Yes" 
    #Then validate the payment modification on the planned installments
    #Given navigate to "BackOffice BE"
    #And enter a valid username and password
    #And click on the Contracts menu
    #* search contract by number
    #When click on the edit button
    #* click on Payment Data tab
    #Then validate the change credit card request
    #Given navigate to "Selfcare BE"
    #And enter a valid username and password
    #When click on My Account menu
    #* click on Payment Info
    #Then click on remove a card