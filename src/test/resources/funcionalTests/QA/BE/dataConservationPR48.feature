#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2597322753/PR+48-2+Data+Conservation
#Author: Willian Arruda
@all @projectFeatures @BE @pr48LT2 @pr48LT2BE
Feature: Data Conservation (PR48 LT2) - Belgium
  This feature provides rules and duration about the data conservation, in order to fit all legal obligations

  @deleteDataPR48BE
  Scenario: Verify if the data information is matching the QA requirements
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status    | Customer Name | Amount | Days | Merchant      | Brand | Product Type |
      | ABANDONED |               |        |    5 | QA Automation |       |              |
    And click on the edit button
    Then get the Contract Number
    Given navigate to "Dataconservation BE"
    And delete the contract
    When navigate to "BackOffice BE"
    And click on the Contracts menu
    Then check if the contract action successfully done

  @anonymizeDataPR48BE
  Scenario: Verify if the data information is matching the QA requirements
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status    | Customer Name | Amount | Days | Merchant      | Brand | Product Type |
      | ABANDONED |               |        |    5 | QA Automation |       |              |
    And click on the edit button
    Then get the Contract Number
    Given navigate to "Dataconservation BE"
    And paste the contract and anonymize it
    When navigate to "BackOffice BE"
    And click on the Contracts menu
    Then check if the contract action successfully done
