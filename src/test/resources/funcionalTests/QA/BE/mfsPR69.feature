#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2404483087/PR69+LT2+MFS
#Author: Willian Arruda
@all @PR69BE @PR69 @projectFeatures
Feature: MFS (PR69 LT2) - Belgium
  This feature builds a better visualization of the information send from 3x4x Backoffice to MFS.

  @changeSurnamePR69BE
  Scenario: Edit an information inside the merchant
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "Teste Create Merchant"
    And click on the edit button
    And click on the "Financial" tab
    Then change Merchant Surname

  @validateJsonPR69BE
  Scenario: Validate the Log after edditing the merchant
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Audit menu
    When click on MFS logs submenu
    Then validate notePadInfo

  @contractInExcelPR69BE
  Scenario: Validate that a created contract is inside the excel file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant      | Brand | Product Type |
      | ONGOING |               |        |    0 | QA Automation |       |             |
    And click on the edit button
    And get the Contract Number
    And click on the Audit menu
    And click on MFS logs submenu
    Then download Mfs Logs Excel File
    And read Xlsx From Mfs Logs
