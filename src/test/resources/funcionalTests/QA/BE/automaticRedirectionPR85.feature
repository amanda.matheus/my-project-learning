#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2411954594/PR+85+Automatic+redirection+to+the+merchant+website
#Author: Amanda Matheus
@all @PR85 @PR85BE @projectFeatures @BE
Feature: Automatic Redirection Merchant Site (PR85) - Belgium
  Redirecct automatically the user from all the Landing Page to the Merchant website.
  The configurations are in BackOffice parametrized in 60 seconds.

  @webNCAcceptedPR85BE
  Scenario: Validate the redirection when the web contract is accepted.
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Pont du Chêne | Antwerp      |        1080 | BELGIUM |          255 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Blaasveld | +320499977582 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Blaasveld  |
    Then insert a valid payment card
    And validate that the contract was successfully created
    And validate the automatic redirection to the merchant site

  @webNCRejectedPR85BE
  Scenario: Validate the redirection when the web contract is rejected.
    Given navigate to "eCommerce BE"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Rue de la Loi | Bruxelles    |        1080 | BELGIUM |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Engine     | Decision  |       | 2002-11-10 | Brussels | +320492654101 | BE - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace |
      | Belgium     | Bruxelles  |
    Then insert a valid payment card
    And validate that the contract was rejected
    And validate the automatic redirection to the merchant site

  @instorebNCAcceptedPR85BE
  Scenario: Validate the redirection when the instore contract is accepted.
    Given navigate to "Merchant BE"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    When fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    And choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address           | Postal Code |
      |        | Willian    | Arruda    |         | +320471349939 |       | Booischotseweg 22 |        5600 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      | Jamiolle      | Jamiolle    | Namur        | Belgium     |          |         |          123456 |
    And follow the "link" to finish my subscription
    Then insert a valid payment card
    And validate that the contract was successfully created
    And validate the automatic redirection to the merchant site
