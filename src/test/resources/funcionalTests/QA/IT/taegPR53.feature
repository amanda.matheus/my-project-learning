#Author: Willian Arruda
@PR53IT @PR53 @projectFeatures
Feature: TAEG (PR53) - Italy
Validation of TAE percentage

  @TAE3XWithFeesPR53IT
  Scenario: Create abandon contract and verify fee 3x
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Giacinto Gigante | Modena       |       41057 | ITALY   |          122 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place       | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Spilamberto | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then verify TAE percentage
    
    
    
  @TAE4XWithFeesPR53IT
 Scenario: Create abandon contract and verify fee 4x
 Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Roma | Asti         |       14022 | ITALY   |           22 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Capriglio | +393533014040 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    Then verify TAE percentage
    
    

  @TA6XWithfeesIT @PR53IT
  Scenario: Create abandon contract and verify fee 6x
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Piazza Guglielmo Pepe | Macerata     |       62010 | ITALY   |          150 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place       | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Montecosaro | +393730898601 | IT - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
        Then verify TAE percentage