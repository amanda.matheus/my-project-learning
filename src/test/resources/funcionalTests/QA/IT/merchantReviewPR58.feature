#Requirements - LT1 - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/1812463617/PR58+LT1+FATA+-+Merchant+Space+Review
#Author: Willian Arruda
@all @PR58 @PR58BEIT @NewComponentsContractPage @IT
Feature: Merchant Review (PR58 LT1) - Italy
  Add more information into contract details and the cancellation process will be modified:
  -Cancellation process made by item quantity and total amount 
  -New fields in contract detail

  @creationContractPR58IT @createContractInstore
  Scenario: Instore contract to be cancelled in merchant space.
    Given navigate to "Merchant IT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name      | Apelido | Mobile Phone | Email        | Address       | Postal Code |
      | Mr     | Merchant   | PR Fifty Eight |         |   3397308986 | it@gmail.com | Via del Corso |       29310 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card          | Document Number |
      | Milano        | MILANO      | Roma         | Italiano    |          | PRFMCH82S10F205L |          123456 |
    And follow the "link" to finish my subscription
    And login as "it@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @newComponentsContractPagePR58IT @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click Contracts
    And search for the customer
      | Name                    | Email | Webpage  |
      | Merchant PR Fifty Eight |       | Merchant |
    And select the "QA Automation Instore UX1" Merchant on the list
    And select the "Finanziato" status on the list
    And validate New Contract Tab Fields
    Then cancel The Contract Partially
