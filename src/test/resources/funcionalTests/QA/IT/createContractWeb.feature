@all @createContracteCommerce @createContracteCommerceIT @IT
Feature: Create Contract eCommerce - Italy
  Create contract with QA Automation business transactions inside eCommerce.

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUsereCommerceIT3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees with 11 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Giacinto Gigante | Modena       |       41057 | ITALY   |          122 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place       | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Spilamberto | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace  | Province | Codice Fiscale   |
      | Italiano    | Spilamberto | MO       | MTHMND82S50I903I |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUsereCommerceIT3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fees with 10 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                 | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Piazza della Repubblica | Catanzaro    |       88040 | ITALY   |           33 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Carlopoli | +393283944241 | IT - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Carlopoli  | CZ       | MTHMND82S50B790A |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUsereCommerceIT4xWithFees
  Scenario: eCommerce: 4x with fees with 9 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address  | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Roma | Asti         |       14022 | ITALY   |           22 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Capriglio | +393533014040 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Capriglio  | AT       | MTHMND82S50B707H |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUsereCommerceIT4xWithoutFees
  Scenario: eCommerce: 4x without fees with 8 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Matteo Schilizzi | Genova       |       16033 | ITALY   |          136 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place   | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Lavagna | +393332435293 | IT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Lavagna    | GE       | MTHMND82S50E488M |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUsereCommerceIT4xWithFeesGDPR
  Scenario: eCommerce: 4x with fees with 9 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address                    | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Piazzetta Scalette Rubiani | Salerno      |       84134 | ITALY   |            8 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place   | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | GDPR      |       | 1982-11-10 | Salerno | +393705208333 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Salerno    | SA       | GDPWLN82S10H703W |
    Then insert a valid payment card
    And validate that the contract was successfully created

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUsereCommerceIT3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees with 11 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address        | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Donnalbina | Nuoro        |       08040 | ITALY   |          110 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | it@gmail.com | 1982-11-10 | Baunei | +393730898606 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And login as "it@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractExistingUsereCommerceIT3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fees with 9 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Matteo Schilizzi | Genova       |       16030 | ITALY   |           87 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | it@gmail.com | 1982-11-10 | Milano | +393730898607 | IT - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And login as "it@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractExistingUsereCommerceIT4xFees
  Scenario: eCommerce: 4x with fees with 7 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                 | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Colonnello Galliano | Roma         |       00040 | ITALY   |          119 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | it@gmail.com | 1982-11-10 | Ardea | +393730898608 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And login as "it@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractExistingUsereCommerceIT4xWithoutFees
  Scenario: eCommerce: 4x without fees with 11 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Matteo Schilizzi | Genova       |       16036 | ITALY   |           83 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | it@gmail.com | 1982-11-10 | Recco | +393730898609 | IT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And login as "it@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractECeCommerceIT4xWithFees
  Scenario: eCommerce: 4x with fees with 9 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Rocca de Baldi | Verona       |       37030 | ITALY   |          141 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place       | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | it@gmail.com | 1982-11-10 | Vestenanova | +393730898603 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And login as "it@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created

  @createContractECeCommerceIT4xWithoutFees
  Scenario: eCommerce: 4x without fees with 8 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address             | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Nuova del Campo | Messina      |       98131 | ITALY   |           79 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place       | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | it@gmail.com | 1982-11-10 | Mili Marina | +393730898604 | IT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And login as "it@gmail.com" in the subscription page
    Then accept the contract terms
    And validate that the contract was successfully created
