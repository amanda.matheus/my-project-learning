#@all @multicaptureAndMarketplace @multicaptureAndMarketplaceIT @criticalFeatures @IT @createContracteCommerce @createContractInstore 
#Feature: Multicapture & Marketplace (PR36) - Italy
  #Creating contracts via Postman
#
  #@multicaptureContractCreationPR36IT @createContracteCommerce  @createContractInstore
  #Scenario: Multicapture Contract IT Creation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_IT_MULTICAPTURE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-multicaptureIT@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_IT_MULTICAPTURE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
#
  #@marketplaceContractCreationPR36IT @createContracteCommerce  @createContractInstore
  #Scenario: Marketplace Contract IT Creation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_IT_MARKETPLACE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-marketplaceIT@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_IT_MARKETPLACE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
