#Author: Amanda Matheus
@all @paymentRetry @actionsToContract @projectFeatures @IT
Feature: Payment Retry - Italy
  New button Payment Retry inside Actions to Contract (BackOffice) to charge the unpaid installments.

  @paymentRetryIT
  Scenario: Charge an installments unpaid
    BackOffice:
    1-Charge installments to the payment return as refused, status "Unpaid".
    2-Click on "Payment Retry" to paid the unpaid installments.

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Italy" test payment page
    * charge all installment and the payment returns as refused
    Given navigate to "BackOffice IT"
    And click on the Contracts menu
    * search for "Unpaid" contract by number
    And click on the edit button
    * click on Contract Transactions tab
    And click on Actions to Contract
    * click on Payment Retry
    Then validate the Contract Transactions status is "Paid"
