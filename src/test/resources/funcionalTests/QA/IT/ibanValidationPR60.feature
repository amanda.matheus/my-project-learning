#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/1962475543/PR60+FATA+-+IBAN+Validation
#Author: Amanda Matheus
@all @PR60 @PR60IT @projectFeatures @IT
Feature: IBAN Validation (PR60) - Italy
  New process for editing and validating any IBAN entry in the BackOffice.

  @approveIBANPR60IT
  Scenario: Change the IBAN, approve and verify if the merchant is active.
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And verify the merchant "QA Automation PR60"
    And click on the Merchants menu
    And search for the Merchant "QA Automation PR60"
    And click on the edit button
    And click on the "Financial" tab
    When change IBAN and BIC information
    And search for the Merchant "QA Automation PR60"
    And validate if the merchant is "inactive"
    Given click on the Accounting menu
    And click on the IBAN Validation submenu
    When "Approve" the IBAN validation
    And click on the Merchants menu
    And search for the Merchant "QA Automation PR60"
    Then validate if the merchant is "active"

  @rejectIBANPR60IT
  Scenario: Change the IBAN, reject and verify if the merchant is inactive.
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And verify the merchant "QA Automation PR60"
    And click on the Merchants menu
    When search for the Merchant "QA Automation PR60"
    And click on the edit button
    And click on the "Financial" tab
    Then change IBAN and BIC information
    And search for the Merchant "QA Automation PR60"
    And validate if the merchant is "inactive"
    Given click on the Accounting menu
    And click on the IBAN Validation submenu
    When "Reject" the IBAN validation
    And click on the Merchants menu
    And search for the Merchant "QA Automation PR60"
    Then validate if the merchant is "inactive"

  @logsPR60IT
  Scenario: Validate the IBAN changes in logs.
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Audit menu
    When click on the User Actions BO Logs menu
    And search the log by screenboid "IBANValidation"
    Then validate the IBAN Validation Log
