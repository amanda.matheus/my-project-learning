@all @criticalFeatures @regression @regressionIT @IT
Feature: Regression - Italy
  This feature creates a contract for a new user with QA Automation business transactions, validates the contract values on the Back Office and Self Care

  @regressionIT3xFees @createContracteCommerce
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name      | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithFees |       | 1982-11-10 | Milano | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | THRRRS82S10F205A |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionIT3xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name             | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression ThreeXWithFees | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 3X           |
    Then click on the edit button
    And validate the Total Amount Financed on the Contract Info is "419,75"

  #And validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionIT3xFees2
  #Scenario: Change password
    #Given navigate to "Selfcare IT"
    #* enter the new username
    #* click on Password
    #* change password from "Oney1testes" to "Oney2testes"
    #Then enter the new password of "oldPasswordCustomerIT@gmail.com"

  @regressionIT3xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name         | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithoutFees |       | 1982-11-10 | Milano | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | THRRRS82S10F205A |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionIT3xWithoutFees1 @createContracteCommerce
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name                | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression ThreeXWithoutFees | 399.75 |    0 | QA Automation Web | QA Automation Tests - Web | 3X           |
    Then click on the edit button
    And validate the Total Amount Financed on the Contract Info is "399,75"

  #And validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionIT3xWithoutFees2
  #Scenario: Change email
    #Given navigate to "Selfcare IT"
    #* enter the new username
    #When click on My Account menu
    #* click on Email
    #* change email to "newEmailCustomerIT@gmail.com"

  @regressionIT4xFees @createContracteCommerce
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce IT"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name     | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithFees |       | 1982-11-10 | Milano | +393730898601 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | FRXRRS82S10F205A |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionIT4xFees1 @createContracteCommerce
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name            | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression FourXWithFees | 594.11 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "623,82"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionIT4xFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare IT"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @regressionIT4xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce IT"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information and save the login credentials
      | Honorific | PersonType     | First name | Last name        | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithoutFees |       | 1982-11-10 | Milano | +393730898601 | IT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | FRXRRS82S10F205A |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionIT4xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name               | Amount | Days | Merchant          | Brand                     | Product Type |
      | OnGoing | Regression FourXWithoutFees | 594.11 |    0 | QA Automation Web | QA Automation Tests - Web | 4X           |
    Then click on the edit button
    * validate the Total Amount Financed on the Contract Info is "594,11"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionIT4xWithoutFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare IT"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @onGoingToFinalizedIT
  Scenario: On Going to Finalized
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Willian Arruda | 399.75 |    0 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Italy" test payment page
    * pay all the installments
    Given navigate to "BackOffice IT"
    And click on the Contracts menu
    * search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR33IT
  Scenario: On Going to Finalized PR33
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Italy" test payment page
    And pay all the installments
    Given navigate to "BackOffice IT"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR36IT
  Scenario: On Going to Finalized PR36
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Italy" test payment page
    And pay all the installments
    Given navigate to "BackOffice IT"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR47IT
  Scenario: On Going to Finalized PR47
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Italy" test payment page
    And pay all the installments
    Given navigate to "BackOffice IT"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments
