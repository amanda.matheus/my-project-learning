@all @checkout @checkoutIT @IT @critical
Feature: Checkout response code testing - Italy
  Create a contract with the amounts below to simulate and trigger the listed response codes

  #----------------------------------------------------------------------------------
  #                   eCommerce - PSP Insufficient funds
  #----------------------------------------------------------------------------------
  @expiredCardContractIT @criticalFeatures
  Scenario: eCommerce: Amount ends with xxx,33 - Response code 30033 Expired card - Pick up
    Given navigate to "eCommerce IT"
    And add products to a total amount of "64,41" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Milano | +393730898601 | IT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | RRDWLN82S10F205Z |
    Then insert a valid VISA payment card
    And validate that the contract was rejected by checkout


