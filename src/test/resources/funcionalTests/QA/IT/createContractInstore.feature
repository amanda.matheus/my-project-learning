@all @criticalFeatures @createContractInstore @createContractInstoreIT @IT
Feature: Create Contract Instore - Italy
  Create contract with QA Automation business transactions inside Merchant Space.

  #----------------------------------------------------------------------------------
  #                   Instore - Create Contract New Customer
  #----------------------------------------------------------------------------------
  @createContractNewUserMerchant3xIT
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address       | Postal Code |
      | Miss   | Amanda     | Matheus   |         |   3397308986 |       | Via Nolana 37 |       19020 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | ID Card          | Document Number |
      | Carrodano     | Carrodano   | La Spezia    | Italiano    | MTHMND82S50B839Q |          123456 |
    * follow the "link" to finish my subscription
    * fill in the province
      | Province |
      | SP       |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchant4xIT
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address               | Postal Code |
      | Miss   | Amanda     | Matheus   |         |   3397308986 |       | Strada Provinciale 65 |       24010 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | ID Card          | Document Number |
      | Bracca        | Bracca      | Bergamo      | Italiano    | MTHMND82S50B112F |          123456 |
    * follow the "link" to finish my subscription
    * fill in the province
      | Province |
      | BG       |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  #----------------------------------------------------------------------------------
  #                   Instore - Create Contract Existing Customer
  #----------------------------------------------------------------------------------
  @createContractExistingUserMerchant3xIT
  Scenario: Instore: 3x with fees
    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address      | Postal Code |
      | Mr     | Willian    | Arruda    |         |   3397308986 | it@gmail.com | Via Duomo 77 |       57020 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card          | Document Number |
      | Livorno       | Livorno     | Livorno      | Italiano    |          | RRDWLN82S10E625K |          123456 |
    * follow the "link" to finish my subscription
    * login as "it@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchant4xIT
  Scenario: Instore: 4x with fees
    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 399.75 |
    * choose "4x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address          | Postal Code |
      | Mr     | Willian    | Arruda    |         |   3397308986 | it@gmail.com | Via Torricelli 5 |       38027 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card          | Document Number |
      | Trento        | Trento      | Trento       | Italiano    |          | RRDWLN82S10L174D |          123456 |
    * follow the "link" to finish my subscription
    * login as "it@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created
