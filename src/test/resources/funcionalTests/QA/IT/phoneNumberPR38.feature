@all @PR38 @PR38IT @criticalFeatures @IT
Feature: Phone Number Changes (PR38) - Italy

  #----------------------------------------------------------------------------------
  #                        Ecommerce Validation
  #----------------------------------------------------------------------------------
  @ecommerceValidPrefixPR38IT
  Scenario: Valid prefix and invalid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    Current regex expression: ^(?:(?:\+|00)39|0039|)[3]{1}[0-9]{9}$
    Prefix: 0039 or +39
    Nr of digits: 10
    Must begin with: 3
    - "Is Editable" Toggle button must be DEACTIVATED inside: Backoffice > Configurations > Subscription Page Configurations

    Given navigate to "eCommerce IT"
And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
When fill in the payment information 
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Milano | +394730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  @ecommerceInvalidPrefixPR38IT
  Scenario: Invalid prefix and valid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    
    Current regex expression: ^(?:(?:\+|00)39|0039|)[3]{1}[0-9]{9}$
    Prefix: 0039 or +39
    Nr of digits: 10
    Must begin with: 3

    Given navigate to "eCommerce IT"
And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
When fill in the payment information 
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Milano | +383730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  #----------------------------------------------------------------------------------
  #                        BackOffice Validation
  #----------------------------------------------------------------------------------
  @backOfficePR38IT
  Scenario: Validate the regex expression
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Configurations menu
    * click on Phone Numbers
    * validate the regrex expression matches the "^(?:(?:\+|00)39|0039|)[3]{1}[0-9]{9}$" string

  @PR38BackOfficeCustomerValidIT
  Scenario: Validate and insert a valid phone number in BackOffice
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +393730898601 |
    Then validate the "BackOffice" mobile phone

  @backOfficeCustomerInvalidPR38IT
  Scenario: Validate and insert a invalid phone number in BackOffice
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +390730898601 |
    * validate the "BackOffice" message error
      | Message              |
      | Phone Number Invalid |

  #----------------------------------------------------------------------------------
  #                        Selfcare Validation
  #----------------------------------------------------------------------------------
  #@selfcareValidPR38IT
  #Scenario: Validate a valid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)39|0039|)[3]{1}[0-9]{9}$
  #Prefix: 0039 or +39
  #Nr of digits: 10
  #Must begin with: 3
  #
  #Given navigate to "Selfcare IT"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +393730898601        | +393730898999    |
  #Then validate the "Selfcare" mobile phone
  #
  #@selfcareInvalidPR38IT
  #Scenario: Validate an invalid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)39|0039|)[3]{1}[0-9]{9}$
  #Prefix: 0039 or +39
  #Nr of digits: 10
  #Must begin with: 3
  #
  #Given navigate to "Selfcare IT"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +393730898601        | +399136665550    |
  #* validate the "Selfcare" message error
  #| Message              |
  #| Phone Number Invalid |
  #----------------------------------------------------------------------------------
  #                        Merchant Validation
  #----------------------------------------------------------------------------------
  @merchantValidPR38IT
  Scenario: Valid phone number in Merchant
    Prefix: 0039 or +39
    Nr of digits: 10
    Must begin with: 3

    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price |
      | Automa Item | Cars & motorbikes |        1 |   300 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address       | Postal Code |
      | Mr     | Willian    | Arruda    |         | +393730898601 |       | Via del Corso |       29310 |

  @merchantInvalidPR38IT
  Scenario: Valid phone number in Merchant
    Prefix: 0039 or +39
    Nr of digits: 10
    Must begin with: 3

    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price |
      | Automa Item | Cars & motorbikes |        1 |   300 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone  | Email | Address       | Postal Code |
      | Mr     | Willian    | Arruda    |         | +383730898601 |       | Via del Corso |       29310 |
    * validate the "Merchant" message error
      | Message              |
      | Phone Number Invalid |
