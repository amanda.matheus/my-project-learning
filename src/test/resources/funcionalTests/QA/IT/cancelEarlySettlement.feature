@all @cancelEarlySettlement @cancelEarlySettlementIT @actionsToContract @projectFeatures @IT
Feature: Cancel & Early Settlement - Italy
  Perform a cancel and early settlement on products in the contract

  ##----------------------------------------------------------------------------------
  ##                               Cancellation
  ##----------------------------------------------------------------------------------
  @totalCancelIT
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelIT
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR33 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR33IT
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelPR33IT
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR36
  ##----------------------------------------------------------------------------------
  #@multicaptureContractCancellationPR36IT
  #Scenario: Multicapture Contract IT Cancellation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_IT_MULTICAPTURE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-multicaptureIT@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_IT_MULTICAPTURE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
    #Given click on close tab
    #When click on the "QA_IT_MULTICAPTURE-CANCEL" file
    #And click on the body tab
    #Then click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action      |
      #| IT     | CANCELLED | CancelTotal |
#
  #@marketplaceContractCancellationPR36IT
  #Scenario: Marketplace Contract IT Cancellation
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_IT_MARKETPLACE-PAYMENT" file
    #And click on the body tab
    #And click on the send button
    #Then get link to finish my subscription
    #And login as "testing-marketplaceIT@gmail.com" in the subscription page
    #And accept the contract terms
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "QA_IT_MARKETPLACE-CONFIRM" file
    #And click on the body tab
    #Then click on the send button
    #Given click on close tab
    #When click on the "QA_IT_MARKETPLACE-CANCEL" file
    #And click on the body tab
    #Then click on the send button
    #Then validate if the response
      #| Tenant | Response  | Action      |
      #| IT     | CANCELLED | CancelTotal |

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR47 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR47IT
  Scenario: Total Cancel amount of all the products in this contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When cancel the item
    And click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    And validate the contract status is "ONGOING"
    
  ##----------------------------------------------------------------------------------
  ##                               Early Settlement
  ##----------------------------------------------------------------------------------
  @totalEarlySettlementIT
  Scenario: Total Early Settlement
    Perform a early settlement to the total contract amount

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Total" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "FINALIZED"

  @partialEarlySettlementIT
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "ONGOING"

  @partialEarlySettlementRefusedIT
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When refusing an early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement PR33
  ##----------------------------------------------------------------------------------
  @partialEarlySettlementPR33IT
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    2 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"

  ##----------------------------------------------------------------------------------
  ##                               Early Settlement PR36
  ##----------------------------------------------------------------------------------
  @multicaptureContractEarlySettlementPR36IT
  Scenario: confirm multicapture contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name     | Amount | Days | Merchant                   | Brand | Product Type |
      | OnGoing | Will Multicapture |        |    0 | QA Automation Multicapture |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"

  @marketplaceContractEarlySettlementPR36IT
  Scenario: confirm multicapture contract
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing | Will Marketplace |        |    0 | QA Automation Marketplace |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
