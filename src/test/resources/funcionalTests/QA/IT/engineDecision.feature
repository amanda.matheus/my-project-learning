#Author: Amanda Matheus
@all @engineDecision @engineDecisionIT @criticalFeatures @IT @critical
Feature: Engine Decision - Italy
  Verify if the contract is rejected by Engine Decision rules.

  @rulebookAgeContractIT
  Scenario: Validate the engine decision rulebook rejection
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Engine     | Decision  |       | 2002-11-10 | Milano | +393730898603 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Milano     | MI       | DCSNGN02S50F205C |
    Then insert a valid payment card
    And validate that the contract was rejected

  @rulebookAgeBackOfficeIT
  Scenario: Validate the rejection reason in BackOffice
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name   | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Engine Decision | 194.36 |    0 |          |       |              |
    And click on the edit button
    Then click on Decision tab
    Then validate if the rejection type is "Decision Engine"
    And validate if the rejection reason is "Age between 18 and 21"
