#@all @safewatch @safewatchIT @IT
#Feature: Safewatch Rejection - Italy
  #Create a contract with the personal info below to simulate and a "bad" client
#
  #@safewatchIT @criticalFeatures
  #Scenario: Safewatch - First Name, Last Name, Birth Date
    #Given navigate to "eCommerce IT"
#And add products to a total amount of "194,36" €
    #And fill in the shipping address
      #| Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      #| Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
#When fill in the payment information 
      #| Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      #| Mr.       | Private Person | Fayssal    | ABBAS     |       | 1955-01-01 | Milano | +393730898601 | IT - QA Automation Web |
    #And choose the payment type "3x (with fees)"
    #And fill in the missing information
      #| Nationality | BirthPlace | Province | Codice Fiscale   |
      #| Italiano    | MILANO     | MI       | BBSFSS55A01F205M |
    #Then insert a valid payment card
    #And validate that the contract was rejected
    #Given navigate to "BackOffice IT"
    #And enter a valid username and password
    #When click on the Contracts menu
    #And search for "Rejected" contract by reference
    #And click on the edit button
    #Then click on Decision tab
    #And validate the rejection reason is "SafeWatch"
