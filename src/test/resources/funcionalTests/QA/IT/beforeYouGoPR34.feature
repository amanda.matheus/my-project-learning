#Author: Ronald Quintanilha
@all @PR34 @beforeYouGoPR34IT @projectFeatures
Feature: Before you Go Pop-in	(PR34) - Italy
  Validation of new Before you Go Pop-in section.

  @BFYGConfigurationSplitBackOfficeIT
  Scenario: BackOffice: Validate the Subscription Page configs Split section
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs Split submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active

  @BFYGConfigurationPLBackOfficeIT
  Scenario: BackOffice: Validate the Subscription Page configs PL section
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Configurations menu
    When click on Subscription Page Configs PL submenu
    Then validate the Before you go PopIn section
    And check that the toggle button is active

  @createContractForPR34IT
  Scenario: Subscription: Validate if the information is being displayed in the contract.
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Giacinto Gigante | Modena       |       41057 | ITALY   |          122 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place       | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Ronald     | BeforeUgo |       | 1982-11-10 | Spilamberto | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And click to leave the subscription form
    Then confirm that the pop-in is being displayed
