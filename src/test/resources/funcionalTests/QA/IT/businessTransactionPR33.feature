@all @PR33 @PR33IT @IT
Feature: Business Transaction Revamp (PR33) - Italy

  @PR33MerchantIT @projectFeatures
  Scenario: Validate the tab Business Transaction in Merchant Edition
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "QA Automation PR33"
    And click on the edit button
    * click on the "Business Transactions" tab
    Then validate the business transactions

  @contractAmountIT3xFees @projectFeatures
  Scenario: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand                     | Product Type |
      | OnGoing | Amanda        | 399.75 |    3 | QA Automation PR33 | QA Automation Tests - Web | 4X           |
    * check if the status is "ONGOING"
    And click on the edit button
    Then validate the Total Amount Financed on the Contract Info is "423,75"
    * validate the Capital Amount on the Contract Transactions is "99,93"

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - PR33
  ##----------------------------------------------------------------------------------
  @createContractIT4XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: PR33: 4x with fees with 11 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address                | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via Alessandro Manzoni | Pavia        |       27010 | ITALY   |           82 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place     | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Filighera | +393730898601 | IT - QA Automation PR33 |
    And choose the payment type "4x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Filighera  | PV       | MTHMND82S50D594G |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractIT6XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: PR33: 6x with fees with 11 items in the cart
    Given navigate to "eCommerce IT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address               | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Piazza Guglielmo Pepe | Macerata     |       62010 | ITALY   |          150 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place       | Mobile Phone  | Tenant                  |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Montecosaro | +393730898601 | IT - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace  | Province | Codice Fiscale   |
      | Italiano    | Montecosaro | MC       | MTHMND82S50F482Z |
    Then insert a valid payment card
    And validate that the contract was successfully created
