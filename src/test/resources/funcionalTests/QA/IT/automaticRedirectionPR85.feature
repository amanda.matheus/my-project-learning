#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2411954594/PR+85+Automatic+redirection+to+the+merchant+website
#Author: Amanda Matheus
@all @PR85 @PR85IT @projectFeatures @IT
Feature: Automatic Redirection Merchant Site (PR85) - Italy
  Redirecct automatically the user from all the Landing Page to the Merchant website.
  The configurations are in BackOffice parametrized in 60 seconds.

  @webNCAcceptedPR85IT
  Scenario: Validate the redirection when the web contract is accepted
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Milano | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | MTHMND82S50F205V |
    Then insert a valid payment card
    And validate that the contract was successfully created
    And validate the automatic redirection to the merchant site

  @webNCRejectedPR85IT
  Scenario: Validate the redirection when the web contract is rejected
    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Engine     | Decision  |       | 2002-11-10 | Milano | +393730898603 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | Milano     | MI       | DCSNGN02S50F205C |
    Then insert a valid payment card
    And validate that the contract was rejected
    And validate the automatic redirection to the merchant site

  @instorebNCAcceptedPR85IT
  Scenario: Validate the redirection when the instore contract is accepted
    Given navigate to "Merchant IT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description | Category          | Quantity | Price  |
      | Automa Item | Cars & motorbikes |        1 | 194.36 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email | Address       | Postal Code |
      | Miss   | Amanda     | Matheus   |         |   3397308986 |       | Via del Corso |       29310 |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | ID Card          | Document Number |
      | Milano        | MILANO      | Roma         | Italiano    | MTHMND82S50F205V |          123456 |
    * follow the "link" to finish my subscription
    * fill in the province
      | Province |
      | MI       |
    Then insert a valid payment card
    Then validate that the contract was successfully created
    And validate the automatic redirection to the merchant site
