#Author: Amanda Matheus
@all @customerScoring @projectFeatures @IT
Feature: Customer Scoring - Italy
  Verify if the contract is rejected by Engine Decision rules.

  @customerScoringContractIT @createContracteCommerce @contractScoring
  Scenario: eCommerce: Validate the customer scoring rejection
    eCommerce:
    - Customer Age greater than 80
    - Email Domain equals automation.pt

    Given navigate to "eCommerce IT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email            | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Customer   | Scoring   | it@automation.pt | 1939-11-10 | Rua Testes XXI | +393730898601 | IT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And login as "it@automation.pt" in the subscription page
    Then accept the contract terms
    And validate that the contract was rejected

  @customerScoringBackOfficeIT
  Scenario: BackOffice: Validate the customer scoring rejection
    BackOffice:
    - Validate the rejection reason
    - Validate the customer score details

    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name    | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Customer Scoring | 194.36 |    0 |          |       |              |
    And click on the edit button
    Then click on Decision tab
    * validate if the rejection type is "Decision Engine"
    * validate if the rejection reason is "Customer Scoring"
    And validate the customer scoring table
      | Scoring Acceptance Value | Customer Final Scoring | Decision |
      |                       25 |                     20 | Rejected |
    And click on the Engine Decision menu
    And search for decision history by contract
    And validade the customer scoring details
      | Age | Email |
      |  10 |    10 |
