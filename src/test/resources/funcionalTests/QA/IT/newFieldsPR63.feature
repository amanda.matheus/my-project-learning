#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2088075265/PR63+FATA+-+New+Fields+3x4x+Contract+Form
#Author: Amanda Matheus
@all @PR63 @IT @criticalFeatures
Feature: New Fields 3x4x Contract Form (PR63) - Italy
  Additional fields in Subscription. The additional fields are displayed only to purchase amount greater that 1.000,00 €

  @webNCPR63IT
  Scenario: Web contract with the additional fields for new customers.
    Given navigate to "eCommerce IT"
    And add products to a total amount of "1870,97" €
    And fill in the shipping address
      | Address       | Municipality | Postal Code | Country | StreetNumber | Floor |
      | Via del Corso | Roma         |       29310 | ITALY   |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Amanda     | Matheus   |       | 1982-11-10 | Milano | +393730898602 | IT - QA Automation Web |
    And choose the payment type "6x (with fees)"
    And fill in the missing information
      | Nationality | BirthPlace | Province | Codice Fiscale   |
      | Italiano    | MILANO     | MI       | MTHMND82S50F205V |
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | Tester     | Automation |           1000 |              300 | Employer           |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @instoreECPR63IT
  Scenario: Instore Contract with the additional fields for existing customers.
    Given navigate to "Merchant IT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price   |
      | Bollinger RD | Cars & motorbikes |        1 | 1870.97 |
    When choose "6x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Apelido | Mobile Phone | Email        | Address       | Postal Code |
      | Miss   | Amanda     | Matheus   |         |   3397308986 | it@gmail.com | Via del Corso |       29310 |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | ID Card          | Document Number |
      | Milano        | MILANO      | Roma         | Italiano    | MTHMND82S50F205V |          123456 |
    And follow the "link" to finish my subscription
    And login as "it@gmail.com" in the subscription page
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | Tester     | Automation |           1000 |              300 | Employer           |
    Then accept the contract terms
    And validate that the contract was successfully created
