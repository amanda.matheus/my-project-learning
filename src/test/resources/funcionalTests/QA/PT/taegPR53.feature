#Author: Willian Arruda
@PR53PT @PR53 @projectFeatures
Feature: TAEG - Portugal
Validation of TAE percentage

  @TAE3XWithFeesPR53PT 
  Scenario: Create abandon contract and verify fee 3x
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Renato     | Guerra    |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then verify TAE percentage 
    
    
    
  @TAE4XWithFeesPR53PT
 Scenario: Create abandon contract and verify fee 4x
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address           | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Travessa Moura Sá | Brasfemes    | 3020-541    | Portugal |           62 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place   | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Sarah      | Salvador  |       | 1982-11-10 | Coimbra | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    Then verify TAE percentage
    
    

  @TAE6SXWithfeesPT @PR53PT
 Scenario: Create abandon contract and verify fee 6x
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Avenida Júlio S Dias | Azurara      | 4480-152    | Portugal |           50 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Porto | +351915888578 | PT - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
        Then verify TAE percentage