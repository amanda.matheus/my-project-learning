#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2411954594/PR+85+Automatic+redirection+to+the+merchant+website
#Author: Amanda Matheus
@all @PR85 @PR85PT @projectFeatures @PT
Feature: Automatic Redirection Merchant Site (PR85) - Portugal
  Redirecct automatically the user from all the Landing Page to the Merchant website.
  The configurations are in BackOffice parametrized in 60 seconds.

  @webNCAcceptedPR85PT
  Scenario: Validate the redirection when the web contract is accepted
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Renato     | Guerra    |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created
    And validate the automatic redirection to the merchant site

  @webNCRejectedPR85PT
  Scenario: Validate the redirection when the web contract is rejected
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Engine     | Decision  |       | 1995-06-30 | Milano | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   32644273 | 281332800 |
    Then insert a valid payment card
    And validate that the contract was rejected
    And validate the automatic redirection to the merchant site

  @instorebNCAcceptedPR85PT
  Scenario: Validate the redirection when the instore contract is accepted
    Given navigate to "Merchant PT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    * click on Generate Contract Number
    * fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    * choose "3x (with fees)" Oney transaction type
    * fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email | Address            | Address Number | Postal Code |
      |        | Renato     | Salvador  |    916555444 |       | Rua António Aleixo |             18 | 2735-388    |
    * fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Sintra       |             |          |         |                 |
    * follow the "link" to finish my subscription
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created
    And validate the automatic redirection to the merchant site
