#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2088075265/PR63+FATA+-+New+Fields+3x4x+Contract+Form
#Author: Amanda Matheus
@all @PT @criticalFeatures
Feature: New Fields 3x4x Contract Form (PR63) - Portugal
  Additional fields in Subscription. The additional fields are displayed only to purchase amount greater that 1.000,00 €

  @webNCPR63PT
  Scenario: Web contract with the additional fields for new customers.
    Given navigate to "eCommerce PT"
    And add products to a total amount of "1870,97" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "6x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |           1000 |              300 | Employer           |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @instoreECPR63PT
  Scenario: Instore contract with the additional fields for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price   |
      | Bollinger RD | Cars & motorbikes |        1| 1870.97 |
    When choose "6x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address            | Address Number | Postal Code |
      |        | Willian    | Arruda    |    916555444 | pt@gmail.com | Rua António Aleixo |             18 | 2735-388    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Sintra       |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |           1000 |              300 | Employer           |
    And accept the contract terms
    Then validate that the contract was successfully created
