@all @createContracteCommerce @createContracteCommercePT @PT
Feature: Create Contract eCommerce - Portugal
  Create contract with QA Automation business transactions inside eCommerce

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContracteNewUserCommercePT3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Renato     | Guerra    |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContracteNewUserCommercePT3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address           | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | R Desidério Bessa | Dafundo      | 1495-716    | Portugal |           16 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Renato     | Salvador  |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContracteNewUserCommercePT4xFees
  Scenario: eCommerce: 4x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address           | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Travessa Moura Sá | Brasfemes    | 3020-541    | Portugal |           62 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place   | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Sarah      | Salvador  |       | 1982-11-10 | Coimbra | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContracteNewUserCommercePT4xWithoutFees
  Scenario: eCommerce: 4x without fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address        | Municipality    | Postal Code | Country  | StreetNumber | Floor |
      | Rua Luísa Tody | Quinta Do Conde | 2975-291    | Portugal |           95 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place   | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Sarah      | Salvador  |       | 1982-11-10 | Setúbal | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContracteNewUserCommercePT4xWithoutFeesMiss
  Scenario: eCommerce: 4x without fees with Honorific Miss and with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "1870,97" €
    And fill in the shipping address
      | Address                | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Colónia Agrícola Casal | Murtosa      | 3870-171    | Portugal |           70 |     4 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Helena     | Gomes     |       | 1982-11-10 | Aveiro | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContracteNewUserCommercePT4xFeesGDPR
  Scenario: eCommerce: 4x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address          | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | R Comércio Porto | Porto        | 4050-560    | Portugal |           18 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Porto | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
    | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
    | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUsereCommercePT3xFees @criticalFeatures
  Scenario: eCommerce: 3x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address           | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | R Poeta João Ruiz | Alpedrinha   | 6230-077    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | pt@gmail.com | 1982-11-10 | Castelo Branco | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommercePT3xWithoutFees @criticalFeatures
  Scenario: eCommerce: 3x without fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address                | Municipality         | Postal Code | Country  | StreetNumber | Floor |
      | Avenida Almirante Reis | Quinta Da Chocapalha | 2580-641    | Portugal |           40 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | pt@gmail.com | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (without fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommercePT4xFees
  Scenario: eCommerce: 4x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address           | Municipality     | Postal Code | Country  | StreetNumber | Floor |
      | R Miguel Bombarda | Vieira De Leiria | 2430-777    | Portugal |           39 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | pt@gmail.com | 1982-11-10 | Leiria | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUsereCommercePT4xWithoutFees
  Scenario: eCommerce: 4x without fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address     | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | R Cimo Povo | Padroncelos  | 4615-536    | Portugal |           50 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    | pt@gmail.com | 1982-11-10 | Porto | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContracteECeCommercePT4xFees
  Scenario: eCommerce: 4x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                     | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | R Engenheiro Duarte Pacheco | Ladoeiro     | 6060-236    | Portugal |           60 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place          | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Sarah      | Salvador  | pt@gmail.com | 1982-11-10 | Castelo Branco | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContracteECeCommercePT4xWithoutFees
  Scenario: eCommerce: 4x without fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address                 | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Doutor José Marques | Alcorriol    | 2350-675    | Portugal |           56 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place    | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Sarah      | Salvador  | pt@gmail.com | 1982-11-10 | Santarém | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContracteECeCommercePT4xWithoutFeesMiss
  Scenario: eCommerce: 4x without fees with Honorific Miss and with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Praceta Conde Arnoso | Albarraque   | 2635-073    | Portugal |           14 |     4 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email        | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Helena     | Gomes     | pt@gmail.com | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    * login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created
