@all @checkout @checkoutES @PT
Feature: Checkout response code testing - Portugal
  Create a contract with the amounts below to simulate and trigger the listed response codes
  
  ----------------------------------------------------------------------------------
                     eCommerce - PSP Insufficient funds
  ----------------------------------------------------------------------------------

  @expiredCardContractPT @criticalFeatures
  Scenario: eCommerce: Amount ends with xxx,33 - Response code 30033 Expired card - Pick up
    Given navigate to "eCommerce PT"
    And add products to a total amount of "64,41" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid VISA payment card
    And validate that the contract was rejected by checkout


