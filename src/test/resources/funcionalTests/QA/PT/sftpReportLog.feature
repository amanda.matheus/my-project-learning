@all @sftpReportLog @sftpReportLogPT @projectFeatures @PT
Feature: SFTP Files Report Logs - Portugal

  ##----------------------------------------------------------------------------------
  ##														BackOffice
  ##----------------------------------------------------------------------------------
  @SFTPSuccessFilesTruePT
  Scenario: Search success files True
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Audit menu
    * click on SFTP Files Report Logs submenu
    * search for "True" success files created today
    Then validate the "File sent successfully" log message

  @SFTPSuccessFilesFalsePT
  Scenario: Search success files False
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Audit menu
    * click on SFTP Files Report Logs submenu
    * search for "False" success files created today
    Then validate the "No sftp files report logs to show..." log message