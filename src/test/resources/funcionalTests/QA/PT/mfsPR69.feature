#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2404483087/PR69+LT2+MFS
#Author: Willian Arruda
@all @PR69PT @PR69 @projectFeatures
Feature: MFS (PR69 LT2) - Portugal
  This feature builds a better visualization of the information send from 3x4x Backoffice to MFS.

  @changeSurnamePR69PT
  Scenario: Edit an information inside the merchant
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "Teste 3 60"
    And click on the edit button
    And click on the "Financial" tab
    Then change Merchant Surname

  @validateJsonPR69PT
  Scenario: Validate the Log after edditing the merchant
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Audit menu
    When click on MFS logs submenu
    Then validate notePadInfo

  @contractInExcelPR69PT
  Scenario: Validate that a created contract is inside the excel file
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant      | Brand | Product Type |
      | ONGOING |               |        |    0 | QA Automation |       |              |
    And click on the edit button
    When get the Contract Number
    And click on the Audit menu
    And click on MFS logs submenu
    Then download Mfs Logs Excel File
    And read Xlsx From Mfs Logs
