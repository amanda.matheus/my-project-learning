#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2421686283/PR83+Merchant+Identification+for+cancellations+Decathlon+ESP
#Author: Willian Arruda
@all @PR83 @PR83PT @PT
Feature: Merchant Identification (PR83) - Portugal
  Each country will parameterize in Backoffice, at the Brand level, the new field “Follow the in-store cancellations“.
  Displays in the detail screen of Normal Contracts in Merchant Space the new field “Store to Refund“.
  The Multicapture and Marketplace Contracts is out of scope.

  @creationContractPR83PT @createContractInstore
  Scenario: Create a instore contract to cancel in merchant space.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name       | Mobile Phone | Email        | Address            | Address Number | Postal Code |
      |        | Merchant   | PR Eighty Three |    916555444 | pt@gmail.com | Rua António Aleixo |             18 | 2735-388    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Sintra       |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @cancellationAndReturnPR83PT @projectFeatures
  Scenario: Validate if the new buttons on contract tab are available
    Given navigate to "Merchant PT"
    And enter a valid username and password
    When click Contracts
    And search for the customer
      | Name                     | Email | Webpage  |
      | Merchant PR Eighty Three |       | Merchant |
    And select the "QA Automation Instore UX1" Merchant on the list
    And select the "Financiado" status on the list
    And click on last contract details
    Then cancel An Amount An Return Partially
