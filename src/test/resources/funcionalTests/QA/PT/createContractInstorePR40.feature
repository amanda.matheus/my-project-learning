#Author: Amanda Matheus
@all @createContractInstore @createContractInstorePT @PT
Feature: Create Contract Instore (PR40) - Portugal
  Create contract with QA Automation business transactions inside Merchant Space

  ##----------------------------------------------------------------------------------
  ##                   Instore - Create Contract New Customer
  ##----------------------------------------------------------------------------------
  @createContractNewUserMerchantPT3xWithFees @criticalFeatures
  Scenario: Creation of instore contract 3x with fees for new customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    And choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email | Address           | Address Number | Postal Code |
      |        | Renato     | Salvador  |    916555444 |       | Estrada Logo Deus |             52 | 3720-379    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Cucujães     |             |          |         |                 |
    And follow the "link" to finish my subscription
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantPT3xWithoutFees @criticalFeatures
  Scenario: Creation of instore contract 3x without fees.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    And choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email | Address                | Address Number | Postal Code |
      |        | Willian    | Arruda    |    916555444 |       | R Doutor Pires Miguens |             71 | 7450-112    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Monforte     |             |          |         |                 |
    And follow the "link" to finish my subscription
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractNewUserMerchantPT4xWithFees
  Scenario: Creation of instore contract 4x with fees for new customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email | Address  | Address Number | Postal Code |
      |        | Renato     | Salvador  |    916555444 |       | R Camões |             32 | 3525-621    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Lapa Do Lobo |             |          |         |                 |
    And follow the "link" to finish my subscription
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @createContractNewUserMerchantPT4xWithoutFees
  Scenario: Creation of instore contract 4x without fees for new customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email | Address          | Address Number | Postal Code |
      |        | Amanda     | Matheus   |    916555444 |       | Rua São Salvador |             47 | 4730-160    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Revenda      |             |          |         |                 |
    And follow the "link" to finish my subscription
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created

  ##----------------------------------------------------------------------------------
  ##                   Instore - Create Contract Existing Customer
  ##----------------------------------------------------------------------------------
  @createContractExistingUserMerchantPT3xWithFees @criticalFeatures
  Scenario: Creation of instore contract 3x with fees for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address         | Address Number | Postal Code |
      |        | Willian    | Arruda    |    916555444 | pt@gmail.com | Avenida Noruega |             87 | 4880-307    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Cruz         |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantPT3xWithoutFees @criticalFeatures
  Scenario: Creation of instore contract 3x without fees for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address        | Address Number | Postal Code |
      |        | Willian    | Arruda    |    916555444 | pt@gmail.com | R Irene Lisboa |              2 | 2680-574    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Camarate     |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantPT4xWithFees
  Scenario: Creation of instore contract 4x with fees for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address                  | Address Number | Postal Code |
      |        | Willian    | Arruda    |    916555444 | pt@gmail.com | R Conselheiro João Cunha |            100 | 4905-274    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality             | Nationality | Province | ID Card | Document Number |
      |               |             | Moreira De Geraz Do Lima |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractExistingUserMerchantPT4xWithoutFees
  Scenario: Creation of instore contract 4x without fees for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address         | Address Number | Postal Code |
      |        | Willian    | Arruda    |    916555444 | pt@gmail.com | Rua Portas Água |            111 | 2490-589    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Ourém        |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantPT4xWithFees
  Scenario: Creation of instore contract 4x with fees for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (with fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address      | Address Number | Postal Code |
      |        | Renato     | Salvador  |    916555444 | pt@gmail.com | R Pelourinho |             50 | 3460-519    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Tondela      |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created

  @createContractECMerchantPT4xWithoutFees
  Scenario: Creation of instore contract 4x without fees for existing customers.
    Given navigate to "Merchant PT"
    And enter a valid username and password
    And click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    When choose "4x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone | Email        | Address | Address Number | Postal Code |
      |        | Amanda     | Matheus   |    916555444 | pt@gmail.com | R Galé  |             83 | 2860-222    |
    And fill in the document information
      | Issuing Place | Birth place | Municipality | Nationality | Province | ID Card | Document Number |
      |               |             | Barra Cheia  |             |          |         |                 |
    And follow the "link" to finish my subscription
    And login as "pt@gmail.com" in the subscription page
    And accept the contract terms
    Then validate that the contract was successfully created
