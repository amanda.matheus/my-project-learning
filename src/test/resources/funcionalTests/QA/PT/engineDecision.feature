#Author: Amanda Matheus
@all @engineDecision @engineDecisionPT @criticalFeatures @PT @critical
Feature: Engine Decision - Portugal
  Verify if the contract is rejected by Engine Decision rules.

  @rulebookAgeContractPT
  Scenario: Validate the engine decision rulebook rejection
    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Miss      | Private Person | Engine     | Decision  |       | 1995-06-30 | Milano | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   32644273 | 281332800 |
    Then insert a valid payment card
    And validate that the contract was rejected

  @rulebookAgeBackOfficePT
  Scenario: Validate the rejection reason in BackOffice
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name   | Amount | Days | Merchant | Brand | Product Type |
      | Rejected | Engine Decision | 194.36 |    0 |          |       |              |
    And click on the edit button
    Then click on Decision tab
    Then validate if the rejection type is "Decision Engine"
    And validate if the rejection reason is "Age between 25 and 27"
