@all @cancelEarlySettlement @cancelEarlySettlementPT @actionsToContract @projectFeatures @PT
Feature: Cancel & Early Settlement - Portugal
  Perform a cancel and early settlement on products in the contract

  ##----------------------------------------------------------------------------------
  ##                               Cancellation
  ##----------------------------------------------------------------------------------
  @totalCancelPT
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelPT
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "134" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR33 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR33PT
  Scenario: Cancel the total amount of all the products in this contract
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    1 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a total cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "CANCELLED"

  @partialCancelPR33PT
  Scenario: Cancel the partial amount of the contract
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               | 399.75 |    1 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "145" € cancel
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    * validate the contract status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                               Cancellation PR47 Contracts
  ##----------------------------------------------------------------------------------
  @totalCancelPR47PT
  Scenario: Total Cancel amount of all the products in this contract
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Actions to Contract
    When cancel the item
    And click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
    And validate the contract status is "ONGOING"
  ##----------------------------------------------------------------------------------
  ##                               Early Settlement
  ##----------------------------------------------------------------------------------
  @totalEarlySettlementPT
  Scenario: Total Early Settlement
    Perform a early settlement to the total contract amount

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Total" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "FINALIZED"

  @partialEarlySettlementPT
  Scenario: Partial Early Settlement
    Perform a early settlement to the Partial contract amount

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 | QA Automation Web |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "Partial" early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Paid"
    * validate the contract status is "ONGOING"

  @partialEarlySettlementRefusedPT
  Scenario: Partial Early Settlement
    Perform a early settlement to the partial contract amount

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 194.36 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When refusing an early settlement
    * click on Contract Transactions tab
    Then validate the Contract Transactions status is "Cancelled"
