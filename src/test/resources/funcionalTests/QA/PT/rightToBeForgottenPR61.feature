#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2274492446/PR+61+FATA+Right+to+be+forgotten
#Author: Amanda Matheus
@all @PR61 @PR61PT @PT
Feature: Right to be Forgotten (PR61 LT1) - Portugal
  Implement rules to delete or anonymize the customer data and the contracts data.

  #@createContractPR61PT @createContracteCommerce @createContractInstore
  #Scenario: Create a Contract to PR61
    #Given navigate to "Postman"
    #And enter a valid username and password
    #And click on the Workspace menu
    #And click on QA Workspace
    #And deactivate SSl Certificate
    #When click on the "QA_PT_4x001_399.75PR61" file
    #And click on the body tab
    #And click on the send button
    #And get link to finish my subscription
    #And fill the additional fields
      #| Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      #| QATest     | Automation |                |                  |                    |
    #And fill in the missing information
      #| NrDocument | NIF       |
      #|   12204415 | 236508520 |
    #Then insert a valid payment card
    #And validate that the contract was successfully created
    #Given navigate to "Postman"
    #And click on the Workspace menu
    #And click on QA Workspace
    #When click on the "PT-QAA-WebPR61 Confirm" file
    #And click on the body tab
    #Then click on the send button

  @totalEarlySettlementPR61PT @createContracteCommerce @createContractInstore
  Scenario: Total Early Settlement to PR61
    Perform a early settlement to the total contract amount

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name             | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Right Forgotten Finalized | 399.75 |    0 |          |       |              |
    And click on the edit button
    And click on Actions to Contract
    When perform a "total" early settlement
    Then click on Contract Transactions tab

  @rightToBeForgottenRulePR61PT @projectFeatures
  Scenario: Set the ritgh to be forgotten rules
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Configurations menu
    When click on the Right to be Forgotten submenu
    Then include rule
      | Section                           | Status    | Option                           |
      | Active contracts                  | Active    | Refuse demand                    |
      | Finalized and cancelled contracts | Finalized | Accept demand and delete data    |
      | Rejected contracts                | Reject    | Accept demand and anonymize data |

  @rightToBeForgottenFinalizedPR61PT @projectFeatures
  Scenario: Erase the contract and customer information in BO
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status    | Customer Name             | Amount | Days | Merchant | Brand | Product Type |
      | Finalized | Right Forgotten Finalized | 399.75 |    0 |          |       |              |
    And click on the edit button
    And get the Contract Number
    When click on the customer name
    And click on Actions to Customer
    And click on right to be forgotten button
    And confirm the right to be forgotten
    And click on the Customers menu
    And search for the customer by email
    And validade if the customer exist
    And click on the Contracts menu
    And search contract by number
    Then validade if the contract exist

  @rightToBeForgottenRejectPR61PT @projectFeatures
  Scenario: Anonymizes the contract information in BO
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status   | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | Rejected |               |  64.42 |    0 |          |       |              |
    And click on the edit button
    And get the Contract Number
    And get the Contract link
    When click on the customer name
    And click on Actions to Customer
    And click on right to be forgotten button
    And confirm the right to be forgotten
    And click on the Contracts menu
    And search contract by number
    And validade if the contract exist
    Then navigate to "link"
    And validate the contract

  @rightToBeForgottenActivePR61PT @projectFeatures
  Scenario: Verify when the demand is refused.
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 |          |       |              |
    And click on the edit button
    And get the Contract Number
    When click on the customer name
    And click on Actions to Customer
    And click on right to be forgotten button
    Then validade the right to be forgotten message error
