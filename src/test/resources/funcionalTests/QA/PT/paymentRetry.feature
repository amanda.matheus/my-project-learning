#Author: Amanda Matheus
@all @actionsToContract @projectFeatures @paymentRetry @PT
Feature: Payment Retry - Portugal
  New button Payment Retry inside Actions to Contract (BackOffice) to charge the unpaid installments.

  @paymentRetryPT
  Scenario: Charge an installments unpaid
    BackOffice:
    1-Charge installments to the payment return as refused, status "Unpaid".
    2-Click on "Payment Retry" to paid the unpaid installments.

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Amanda Matheus | 399.75 |    0 |          |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Portugal" test payment page
    * charge all installment and the payment returns as refused
    Given navigate to "BackOffice PT"
    And click on the Contracts menu
    * search for "Unpaid" contract by number
    And click on the edit button
    And click on Contract Transactions tab
    And click on Actions to Contract
    * click on Payment Retry
    Then validate the Contract Transactions status is "Paid"
