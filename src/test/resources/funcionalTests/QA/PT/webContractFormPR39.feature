#Author: Amanda Matheus
@all @PT @projectFeatures
Feature: Web Contract Form (PR39) - Portugal
  New fields: Occupation, Employer and Address and cookies disclaimer.

  @cookiesPR39PT
  Scenario: eCommerce: Validate the cookie disclaimer
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    Then click on cookie disclaimer
    And verify the privacy police

  @selfcarePR39PT
  Scenario: Selfcare: Validate new fiels Occupation, Employer and Address
    Given navigate to "Selfcare PT"
    And enter a valid username and password
    When click on My Account menu
    And click on Personal info
    Then validate the fields Gender, Occupation, Employer and Address on "Selfcare"

  @backOfficePR39PT
  Scenario: BackOffice: Validate new fiels Occupation, Employer and Address
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Customers menu
    And search for the customer by email "pt@gmail.com"
    And click on the edit button
    Then validate the fields Gender, Occupation, Employer and Address on "BackOffice"
