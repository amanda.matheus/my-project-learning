@all @smokeTest @smokeTestPT @criticalFeatures @PT @critical
Feature: Smoke tests - Portugal

  ##----------------------------------------------------------------------------------
  ##														BackOffice
  ##----------------------------------------------------------------------------------
  @contractBackOfficePT @smokeTestBackOfficePT
  Scenario: Search on going contract
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    Then validate if the page contains the correct information

  @onGoingBackOfficePT @smokeTestBackOfficePT
  Scenario: Check the status Refund, Change Credit Card and Early Settlement
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing |               | 399.75 |    0 | QA Automation Web |       |              |
    And click on the edit button
    Then check for the visibility of the status Refund, Change Credit Card and Early Settlement

  @unpaidBackOfficePT @smokeTestBackOfficePT
  Scenario: Check the status Change Credit Card, Early Settlement and Payment Retry
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name   | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Soft Collection | 399.75 |    2 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Portugal" test payment page
    * charge the next installment and the payment returns as refused for "Technical" reason
    Given navigate to "BackOffice PT"
    And click on the Contracts menu
    * search for "Unpaid" contract by number
    And click on the edit button
    Then check for the visibility of the status Change Credit Card, Early Settlement and Payment Retry

  @customersBackOfficePT @smokeTestBackOfficePT
  Scenario: Search for a costumer
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Customers menu
    And search for a customer
    Then click the reset button

  @merchantBackOfficePT @smokeTestBackOfficePT
  Scenario: Check the Merchants feature
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Merchants menu
    * click on New Merchant
    * return to the previous page by clicking on the Merchants link
    * click on the view icon of any merchant
    * return to the previous page by clicking on the Merchants link
    Then click on the edit icon of any merchant

  @brandsBackOfficePT @smokeTestBackOfficePT
  Scenario: Check the Brands feature
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Brands menu
    * click on New Brand
    * return to the previous page by clicking on the Brands link
    * click on the view icon of any brand
    * return to the previous page by clicking on the Brands link
    Then click on the edit icon of any brand

  @multicapture&marketplacePT @smokeTestBackOfficePT
  Scenario: Check the Multicapture and Marketplace checkbox rules
    If the merchant Is Marketplace checkbox is selected then the Is Multicapture checkbox is mandatory
    If the merchant Is Multicapture is selected then the Is Marketplace is optional

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Merchants menu
    * click on New Merchant
    Then validate the Multi-capture and Marketplace flags

  @multicapture&marketplaceConfigurationPT @smokeTestBackOfficePT
  Scenario: Check the Multi-capture and Marketplace checkbox rules
    If the merchant Is Marketplace checkbox is selected then the Is Multi-capture checkbox is mandatory
    If the merchant Is Multi-capture is selected then the Is Marketplace is optional

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Configurations menu
    * click on MC & MP Configuration
    * open Multicapture Configuration tab
    Then validate the Multicapture configuration

  ##----------------------------------------------------------------------------------
  ##														Selfcare
  ##----------------------------------------------------------------------------------
  #@myAccountSelfcarePT @smokeTestSelfcarePT
  #Scenario: Check My Account tab
    #Given navigate to "Selfcare PT"
    #And enter a valid username and password
    #* click on My Account menu
    #* click on Email
    #* click on Password
    #* click on Payment Info
    #* click on Documents
    #* click on Phone Number
    #* click on Personal info
    #* click on Preferences
    #* click on Delete Account
    #Then should be able to logout
#
  #@homeSelfcarePT @smokeTestSelfcarePT
  #Scenario: Check Active and Past Contracts
    #Given navigate to "Selfcare PT"
    #And enter a valid username and password
    #* click on Past contracts
    #* click on contract details on a past contract
    #* click on Home link
    #* click on contract details on an active contract
    #* should be able to logout

  #@changeEmailSelfcarePT @smokeTestSelfcarePT
  #Scenario: Change email
    #Given navigate to "Selfcare PT"
    #And enter a valid username "oldEmailCustomerPT@gmail.com" and password
    #* click on My Account menu
    #* click on Email
    #* change email from "oldEmailCustomerPT@gmail.com" to "newEmailCustomerPT@gmail.com"
    #And enter a valid username "newEmailCustomerPT@gmail.com" and password
    #* click on My Account menu
    #* click on Email
    #* change email from "newEmailCustomerPT@gmail.com" to "oldEmailCustomerPT@gmail.com"
#
  #@changePasswordSelfcarePT @smokeTestSelfcarePT
  #Scenario: Change password
    #Given navigate to "Selfcare PT"
    #And enter a valid username "oldPasswordCustomerPT@gmail.com" and password
    #When click on My Account menu
    #* click on Password
    #* change password from "Oney1testes" to "Oney2testes"
    #* enter the new password of "oldPasswordCustomerPT@gmail.com"
    #* click on My Account menu
    #* click on Password
    #* change password from "Oney2testes" to "Oney1testes"
    #* navigate to "Selfcare PT"
    #And enter a valid username "oldPasswordCustomerPT@gmail.com" and password

  ##----------------------------------------------------------------------------------
  ##														Merchant
  ##----------------------------------------------------------------------------------
  @logoutMerchantPT @smokeMerchantPT
  Scenario: Login and logout with valid credentials
    Given navigate to "Merchant PT"
    And enter a valid username and password
    * should be able to logout

  @forgotPasswordMerchantPT @smokeMerchantPT
  Scenario: Request a new password
    Given navigate to "Merchant PT"
    And enter a valid username and password
    * should be able to logout
    * click on the forgot password
    * insert my email
    * should receive a new password

  @contractsMerchantPT @smokeMerchantPT
  Scenario: Search for cancelled contracts
    Given navigate to "Merchant PT"
    And enter a valid username and password
    * click Contracts
    * verify cancelled contracts
    * click export to excel
    * click on the last 30 days link

  @contractsSearchMerchantPT @smokeMerchantPT
  Scenario: Search for a customer name
    Given navigate to "Merchant PT"
    And enter a valid username and password
    * click Contracts
    * search for the customer
      | Name    | Email | Webpage  |
      | Willian |       | Merchant |

  @filesMerchantPT @smokeMerchantPT
  Scenario: Check for files
    Given navigate to "Merchant PT"
    And enter a valid username and password
    * click Files
    * check if contract table is displayed
    * click download if any file exists

  @usersMerchantPT @smokeMerchantPT
  Scenario: Validate existing user and create a new one
    Given navigate to "Merchant PT"
    And enter a valid username and password
    * click Users
    * change status to "Inativo"
    * change status to "Estado"
    * search for the user "QAAutomationPT"
    * click on New User
    * click on Cancel on the new user page
