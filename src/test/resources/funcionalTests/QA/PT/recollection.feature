#Author: Amanda Matheus
@all @recollection @recollectionPT @PT
Feature: Recollection - Portugal

  ##----------------------------------------------------------------------------------
  ##                  Soft Collection
  ##----------------------------------------------------------------------------------
  @recollectionCreateClass1PT @createContracteCommerce @createContractInstore @recollectionCreateContract
  Scenario: Contract Creation to Recollection tests
    eCommerce:
    1-Customer Age 24
    2-Contract Duration 4x
    3-Contract Type with fees

    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name   | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Soft       | Collections |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @recollectionSoftCollectionClass1PT @projectFeatures
  Scenario: Charge installments to the payment return as refused, status "Unpaid"
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Soft Collections | 399.75 |    2 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Portugal" test payment page
    * charge the next installment and the payment returns as refused for "Technical" reason
    Given navigate to "BackOffice PT"
    And click on the Contracts menu
    * search for "Unpaid" contract by number
    And click on the edit button

  ## ----------------------------------------------------------------------------------
  ##                 				Hard Collection
  ##----------------------------------------------------------------------------------
  @recollectionCreateHContractardPT @createContracteCommerce @createContractInstore @recollectionCreateContract
  Scenario: Contract Creation to Recollection tests
    eCommerce:
    1-Customer Age 24
    2-Contract Duration 4x
    3-Contract Type with fees

    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name   | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Hard       | Collections |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created

  @recollectionHardCollectionPT @projectFeatures
  Scenario: Verify Hard Collection process
    BackOffice:
    1-Put the intallment on Hard Collection
    2-Verify Sofcollection and Hardcollection status
    3-Verify the amounts of the contract transaction

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name    | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Hard Collections | 399.75 |    2 |          |       |              |
    And click on the edit button
    * get FPClient Code
    * click on Contract Transactions tab
    * validate the number of planned installments
    * navigate to "Portugal" test payment page
    * charge the next installment and the payment returns as refused for "Technical" reason
    * put the contract on Hard Collection
    Given navigate to "BackOffice PT"
    And click on the Contracts menu
    * search for "Hard Collection" contract by number
    And click on the edit button
