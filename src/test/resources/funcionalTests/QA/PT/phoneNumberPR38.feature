@all @PR38 @PR38PT @criticalFeatures @PT
Feature: Phone Number Changes (PR38) - Portugal

  #----------------------------------------------------------------------------------
  #                        Ecommerce Validation
  #----------------------------------------------------------------------------------
  @ecommerceValidPrefixPR38PT
  Scenario: Valid prefix and invalid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    Current regex expression: ^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}$
    Prefix: 00351 or +351
    Nr of digits: 9
    Must begin with: 91, 92, 93 or 96
    - "Is Editable" Toggle button must be DEACTIVATED inside: Backoffice > Configurations > Subscription Page Configurations

    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone   | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Lisboa | +3519515888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  @ecommerceInvalidPrefixPR38PT
  Scenario: Invalid prefix and valid phone number
    Validate the regex expression on eCommerce Checkout Step 3 page
    
    Current regex expression: ^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}$
    Prefix: 00351 or +351
    Nr of digits: 9
    Must begin with: 91, 92, 93 or 96

    Given navigate to "eCommerce PT"
    And add products to a total amount of "194,36" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Lisboa | +354915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    * validate the "eCommerce" message error
      | Message                                                                                                   |
      | There was an error processing your payment request. Please review your payment information and try again. |

  #----------------------------------------------------------------------------------
  #                        BackOffice Validation
  #----------------------------------------------------------------------------------
  @backOfficePR38PT
  Scenario: Validate the regex expression
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Configurations menu
    * click on Phone Numbers
    * validate the regrex expression matches the "^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}$" string

  @PR38BackOfficeCustomerValidPT
  Scenario: Validate and insert a valid phone number in BackOffice
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone  |
      | +351916777655 |
    Then validate the "BackOffice" mobile phone

  @backOfficeCustomerInvalidPR38PT
  Scenario: Validate and insert a invalid phone number in BackOffice
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    * click on the Customers menu
    * search for a customer
    And click on the edit button
    * change the "BackOffice" mobile phone
      | Mobile Phone   |
      | +3518515888578 |
    * validate the "BackOffice" message error
      | Message                      |
      | Número de Telemóvel Inválido |

  #----------------------------------------------------------------------------------
  #                        Selfcare Validation
  #----------------------------------------------------------------------------------
  #@selfcareValidPR38PT
  #Scenario: Validate a valid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}
  #Prefix: 00351 or +351
  #Nr of digits: 9
  #Must begin with: 91, 92, 93, 96
  #
  #Given navigate to "Selfcare PT"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +351917888777        | +351917881234    |
  #Then validate the "Selfcare" mobile phone
  #
  #@selfcareInvalidPR38PT
  #Scenario: Validate an invalid phone number on Selfcare:
  #Current regex expression: ^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}
  #Prefix: 00351 or +351
  #Nr of digits: 9
  #Must begin with: 91, 912, 93, 96
  #
  #Given navigate to "Selfcare PT"
  #And enter a valid username and password
  #When click on My Account menu
  #* click on Phone Number
  #* change the "Selfcare" mobile phone
  #| Current Mobile Phone | New Mobile Phone |
  #| +351917888777        | +351947888711    |
  #* validate the "Selfcare" message error
  #| Message                      |
  #| Número de Telemóvel Inválido |
  ##----------------------------------------------------------------------------------
  ##                        Merchant Validation
  ##----------------------------------------------------------------------------------
  @merchantValidPR38PT
  Scenario: Valid phone number in Merchant
    Current regex expression: ^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}$
    Prefix: 00351 or +351
    Nr of digits: 09
    Must begin with: 91, 92, 93, 96

    Given navigate to "Merchant PT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    And choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone  | Email | Address            | Address Number | Postal Code |
      |        | Willian    | Arruda    | +351916555444 |       | Rua António Aleixo |             18 | 2735-388    |

  @merchantInvalidPR38PT
  Scenario: Invalid phone number on Merchant
    Current regex expression: ^(?:(?:\+|00)351|)[9]{1}[1|2|3|6]{1}[0-9]{7}$
    Prefix: 00351 or +351
    Nr of digits: 09
    Must begin with: 91, 92, 93, 96

    Given navigate to "Merchant PT"
    And enter a valid username and password
    When click on create a contract
    * select the "QA Automation Instore UX1" Merchant
    And click on Generate Contract Number
    And fill in the product information
      | Description  | Category          | Quantity | Price  |
      | Bollinger RD | Cars & motorbikes |        1 | 399.75 |
    And choose "3x (without fees)" Oney transaction type
    And fill in the client personal information
      | Gender | First name | Last name | Mobile Phone   | Email | Address            | Address Number | Postal Code |
      |        | Willian    | Arruda    | +3518515888578 |       | Rua António Aleixo |             18 | 2735-388    |
    Then validate the "Merchant" message error
      | Message                      |
      | Número de Telemóvel Inválido |
