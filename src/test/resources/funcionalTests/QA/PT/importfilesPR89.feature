#Requirements - https://oneyfactory.atlassian.net/wiki/spaces/34ORGAA/pages/2611150896/PR89+Brand+Merchant+user+creation+by+import+files
#Author: Willian Arruda
@all @PR89PT @PR89 @projectFeatures
Feature: Creation by import files (PR89) - Portugal
  This feature aims to import and create Brand, Merchant and Users through Excel.

  @brandImportPR89PT
  Scenario: to Import and create a brand
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Brands menu
    And creating An Excel for "Brand" Import
    Then import "Brand" file
    And validate the creation

  @merchantImportPR89PT
  Scenario: to Import and create a Merchant
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Merchants menu
    And creating An Excel for "MerchantPT" Import
    Then import "MerchantPT" file
    And validate the creation

  @userImportPR89PT
  Scenario: to Import and create a user
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on userprofile Menu and Submenu
    And creating An Excel for "Users" Import
    Then import "Users" file
    And validate the creation
