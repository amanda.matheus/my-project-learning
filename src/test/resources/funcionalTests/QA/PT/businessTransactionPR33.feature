@all @PR33 @PR33PT @PT
Feature: Business Transaction Revamp (PR33) - Portugal

  @PR33MerchantPT @projectFeatures
  Scenario: Validate the tab Business Transaction in Merchant Edition
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Merchants menu
    When search for the Merchant "QA Automation PR33"
    And click on the edit button
    * click on the "Business Transactions" tab
    Then validate the business transactions

  @contractAmountPT3xFees @projectFeatures
  Scenario: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing | Willian Arruda | 399.75 |    3 | QA Automation PR33 |       |              |
    * check if the status is "ONGOING"

  ##----------------------------------------------------------------------------------
  ##                   eCommerce - PR33
  ##----------------------------------------------------------------------------------
  @createContractePT4XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: PR33: 4x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address                   | Municipality               | Postal Code | Country  | StreetNumber | Floor |
      | Avenida José Costa Mealha | São Bartolomeu De Messines | 8375-147    | Portugal |          107 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Faro  | +351915888578 | PT - QA Automation PR33 |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created

  @createContractePT6XM @criticalFeatures @createContracteCommerce @createContractInstore
  Scenario: PR33: 6x with fees with 6 items in the cart
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address              | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Avenida Júlio S Dias | Azurara      | 4480-152    | Portugal |           50 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name | Email | Birth Date | Place | Mobile Phone  | Tenant                  |
      | Mr.       | Private Person | Willian    | Arruda    |       | 1982-11-10 | Porto | +351915888578 | PT - QA Automation PR33 |
    And choose the payment type "6x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    Then validate that the contract was successfully created
