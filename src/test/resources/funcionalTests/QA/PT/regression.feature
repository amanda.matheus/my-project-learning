@all @criticalFeatures @regression @regressionPT @PT @critical
Feature: Regression - Portugal
  By paying all the installments from an on going contract, the contract status must move to finalized.

  @onGoingToFinalizedPT
  Scenario: Open contract transactions and pay the planned installments, validate that the same parcels are paid
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name  | Amount | Days | Merchant | Brand | Product Type |
      | OnGoing | Willian Arruda | 399.75 |    0 |          |       |              |
    And click on the edit button
    * click on Contract Transactions tab
    * validate the number of planned installments
    When navigate to "Portugal" test payment page
    And pay all the installments
    * navigate to "BackOffice PT"
    And click on the Contracts menu
    * search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR33PT
  Scenario: On Going to Finalized PR33
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant           | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation PR33 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Portugal" test payment page
    And pay all the installments
    Given navigate to "BackOffice PT"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @onGoingToFinalizedPR47PT
  Scenario: On Going to Finalized PR47
    By paying all the installments from an on going contract, the contract status must move to finalized

    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    When search for an contract created
      | Status  | Customer Name | Amount | Days | Merchant                  | Brand | Product Type |
      | OnGoing |               |        |    0 | QA Automation Instore UX2 |       |              |
    And click on the edit button
    And click on Contract Transactions tab
    And validate the number of planned installments
    Then navigate to "Portugal" test payment page
    And pay all the installments
    Given navigate to "BackOffice PT"
    And click on the Contracts menu
    When search for "Finalized" contract by number
    Then check if the status is "Finalized" after paying the installments

  @regressionPT3xFees @createContracteCommerce
  Scenario: eCommerce: 3x with fees
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name      | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithFees |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionPT3xFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name             | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing | Regression ThreeXWithFees | 399.75 |    0 | QA Automation Web |       |              |
    Then click on the edit button
    And validate the Total Amount Financed on the Contract Info is "419,75"

  #And validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionPT3xFees2
  #Scenario: Change password
    #Given navigate to "Selfcare PT"
    #* enter the new username
    #* click on Password
    #* change password from "Oney1testes" to "Oney2testes"
    #Then enter the new password of "oldPasswordCustomerPT@gmail.com"

  @regressionPT3xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 3x without fees
    Given navigate to "eCommerce PT"
    And add products to a total amount of "399,75" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name         | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | ThreeXWithoutFees |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "3x (without fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionPT3xWithoutFees1 @createContracteCommerce
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name                | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing | Regression ThreeXWithoutFees | 399.75 |    0 | QA Automation Web |       |              |
    Then click on the edit button
    And validate the Total Amount Financed on the Contract Info is "399,75"

  #And validate the Capital Amount on the Contract Transactions is "133,25"
  #@regressionPT3xWithoutFees2
  #Scenario: Selfcare: Change email
    #Given navigate to "Selfcare PT"
    #* enter the new username
    #When click on My Account menu
    #* click on Email
    #* change email to "newEmailCustomerPT@gmail.com"

  @regressionPT4xFees @createContracteCommerce
  Scenario: eCommerce: 4x with fees
    Given navigate to "eCommerce PT"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name     | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithFees |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (with fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionPT4xFees1 @createContracteCommerce
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name            | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing | Regression FourXWithFees | 594.11 |    0 | QA Automation Web |       |              |
    And click on the edit button
    * validate the Total Amount Financed on the Contract Info is "623,82"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionPT4xFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare PT"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status

  @regressionPT4xWithoutFees @createContracteCommerce
  Scenario: eCommerce: 4x without fees
    Given navigate to "eCommerce PT"
    And add products to a total amount of "594,11" €
    And fill in the shipping address
      | Address            | Municipality | Postal Code | Country  | StreetNumber | Floor |
      | Rua Damião de Góis | Amadora      | 2650-321    | Portugal |            3 |    40 |
    When fill in the payment information
      | Honorific | PersonType     | First name | Last name        | Email | Birth Date | Place  | Mobile Phone  | Tenant                 |
      | Mr.       | Private Person | Regression | FourXWithoutFees |       | 1982-11-10 | Lisboa | +351915888578 | PT - QA Automation Web |
    And choose the payment type "4x (without fees)"
    And fill the additional fields
      | Occupation | Employer   | Monthly Income | Monthly Expenses | Main Source Income |
      | QATest     | Automation |                |                  |                    |
    And fill in the missing information
      | NrDocument | NIF       |
      |   12204415 | 236508520 |
    Then insert a valid payment card
    And validate that the contract was successfully created with a new user

  @regressionPT4xWithoutFees1
  Scenario: BackOffice: Verify the Status, the Amount Financed and the Debt Amount
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    When click on the Contracts menu
    And search for an contract created
      | Status  | Customer Name               | Amount | Days | Merchant          | Brand | Product Type |
      | OnGoing | Regression FourXWithoutFees | 594.11 |    0 | QA Automation Web |       |              |
    Then click on the edit button
    * validate the Total Amount Financed on the Contract Info is "594,11"

  #* validate the Capital Amount on the Contract Transactions is "148,52"
  #@regressionPT4xWithoutFees2
  #Scenario: Selfcare: Verify the contract
    #Given navigate to "Selfcare PT"
    #* enter the new username
    #* validate Active Contracts table is empty for On Going status
    #Then validate Past Contracts is empty for On Goins status
