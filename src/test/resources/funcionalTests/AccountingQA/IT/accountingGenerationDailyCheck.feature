@accounting @accountingIT @accountingEventGenerateIT @dailyAccounting
Feature: Accounting Support Feature - Italy
  Verify is the events of all contracts created by automation are generated

  @accountingContractCreatedFileIT
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "ContractCreated" details in the file
    
  @accountingContractCancelledFileIT
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "CancelTotal" details in the file
    
        
 @accountingEventGenerate27IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 27
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "27" was generated daily

  ##----------------------------------------------------------------------------------
  ##                   Contract Created
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate01IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "01" was generated after the "ContractCreated"

  @accountingEventGenerate14IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 14
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "14" was generated after the "ContractCreated"

  @accountingEventGenerate03IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 03
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "ContractCreated"

  @accountingEventGenerate19IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 19
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "19" was generated after the "ContractCreated"

  @accountingEventGenerateC20IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 20
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "ContractCreated"

  ##----------------------------------------------------------------------------------
  ##                   Cancellation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate06TIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelTotal"

  @accountingEventGenerate10TIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelTotal"
        
  @accountingEventGenerate21TIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelTotal"

  @accountingEventGenerate18TIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelTotal"
    
  @accountingEventGenerate22TIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelTotal"

  @accountingEventGenerate23IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 23
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "23" was generated after the "CancelTotal"

  @accountingEventGenerate24IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 24
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "24" was generated after the "CancelTotal"

  @accountingEventGenerate06PIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelPartial"

  @accountingEventGenerate10PPIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelPartial"
    
  @accountingEventGenerate21PIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelPartial"

  @accountingEventGenerate18PIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelPartial"
    
  @accountingEventGenerate22PIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelPartial"
    

  ##----------------------------------------------------------------------------------
  ##                   Early Settlement
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate05IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 05
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "05" was generated after the "EarlySettlement"

  @accountingEventGenerate07IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 07
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "07" was generated after the "EarlySettlement"

  ##----------------------------------------------------------------------------------
  ##                   Soft
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate04IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 04
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "04" was generated after the "Soft"

  @accountingEventGenerate15IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 15
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "15" was generated after the "Soft"

  ##se for pago no mesmo dia, tem que gerar 2 03 né?
  @accountingEventGenerate03SPIT @dailyAccountingIT
  Scenario: Validate the generation of EventType 03
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "SoftPaid"

  @accountingEventGenerate25IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 25
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "25" was generated after the "SoftPaid"

  @accountingEventGenerateS20IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 20
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "SoftPaid"

  @accountingEventGenerateS20IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 26
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "26" was generated after the "SoftPaid"

  ##----------------------------------------------------------------------------------
  ##                   Hard
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 08
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "08" was generated after the "Hard"

  @accountingEventGenerate09IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 09
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "09" was generated after the "Hard"

  ##----------------------------------------------------------------------------------
  ##                   Litigation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 16
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "16" was generated after the "Litigation"

  ##----------------------------------------------------------------------------------
  ##                   MFS
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate02IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 02
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "02" was generated after the "ContractCreated"


  @accountingEventGenerate13IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 13
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "13" was generated after the "ContractCreated"
    
    
  ##----------------------------------------------------------------------------------
  ##                   Validate Errors file
  ##----------------------------------------------------------------------------------
  
  @accountingEventError01IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "1" days
    
    @accountingEventError02IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "2" days
    
    @accountingEventError03IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "3" days
    
    @accountingEventError04IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "4" days
    
    @accountingEventError05IT @dailyAccountingIT
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "5" days
    
    
