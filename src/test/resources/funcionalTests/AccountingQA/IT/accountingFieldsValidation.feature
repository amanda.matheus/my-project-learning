@accounting @accountingIT @accountingFieldsValidation
Feature: Accounting Fields Validation - Italy
  Validate the fields of all EventTypes

  @accountingEventFields01IT
  Scenario: Validate the fields of EventType 01
    Given navigate to "BackOffice IT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the fields of all eventTypes "01"

  @accountingEventFields02IT
  Scenario: Validate the fields of EventType 02v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "02v2"

  @accountingEventFields03IT
  Scenario: Validate the fields of EventType 03
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "03"

  @accountingEventFields04IT
  Scenario: Validate the fields of EventType 04v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "04v2"

  @accountingEventFields05IT
  Scenario: Validate the fields of EventType 05
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "05"

  @accountingEventFields06IT
  Scenario: Validate the fields of EventType 06v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "06v2"

  @accountingEventFields07IT
  Scenario: Validate the fields of EventType 07v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "07v2"

  @accountingEventFields08IT
  Scenario: Validate the fields of EventType 08v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "08v2"

  @accountingEventFields09IT
  Scenario: Validate the fields of EventType 09
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "09"

  @accountingEventFields10IT
  Scenario: Validate the fields of EventType 10v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "10v2"

  @accountingEventFields11IT
  Scenario: Validate the fields of EventType 11v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "11v2"

  @accountingEventFields12IT
  Scenario: Validate the fields of EventType 12v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "12v2"

  @accountingEventFields13IT
  Scenario: Validate the fields of EventType 13v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "13v2"

  @accountingEventFields14IT
  Scenario: Validate the fields of EventType 14
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "14"

  @accountingEventFields15IT
  Scenario: Validate the fields of EventType 15
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "15"

  @accountingEventFields16IT
  Scenario: Validate the fields of EventType 16v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "16v2"

  @accountingEventFields17IT
  Scenario: Validate the fields of EventType 17
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "17"

  @accountingEventFields18IT
  Scenario: Validate the fields of EventType 18v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "18v2"

  @accountingEventFields19IT
  Scenario: Validate the fields of EventType 19v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "19v2"

  @accountingEventFields20IT
  Scenario: Validate the fields of EventType 20v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "20v2"

  @accountingEventFields21IT
  Scenario: Validate the fields of EventType 21v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "21v2"

  @accountingEventFields22IT
  Scenario: Validate the fields of EventType 22v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "22v2"

  @accountingEventFields23IT
  Scenario: Validate the fields of EventType 23v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "23v2"

  @accountingEventFields24IT
  Scenario: Validate the fields of EventType 24v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "24v2"

  @accountingEventFields25IT
  Scenario: Validate the fields of EventType 25v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "25v2"

  @accountingEventFields26IT
  Scenario: Validate the fields of EventType 26v2
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "26v2"

  @accountingEventFields27IT
  Scenario: Validate the fields of EventType 27
    Given acess the accounting file "IT"
    When open the file
    Then validate the fields of all eventTypes "27"

    
    
    
    
    
    
    
  #@accountingEventFields02IT
  #Scenario: Validate the fields of EventType 02
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "02"
#
  #@accountingEventFields04IT
  #Scenario: Validate the fields of EventType 04
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "04"
#
  #@accountingEventFields06IT
  #Scenario: Validate the fields of EventType 06
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "06"
#
  #@accountingEventFields07IT
  #Scenario: Validate the fields of EventType 07
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "07"
#
  #@accountingEventFields08IT
  #Scenario: Validate the fields of EventType 08
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "08"
#
  #@accountingEventFields10IT
  #Scenario: Validate the fields of EventType 10
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "10"
#
  #@accountingEventFields11IT
  #Scenario: Validate the fields of EventType 11
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "11"
#
  #@accountingEventFields12IT
  #Scenario: Validate the fields of EventType 12
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "12"
#
  #@accountingEventFields13IT
  #Scenario: Validate the fields of EventType 13
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "13"
 #
  #@accountingEventFields16IT
  #Scenario: Validate the fields of EventType 16
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "16"
#
  #@accountingEventFields18IT
  #Scenario: Validate the fields of EventType 18
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "18"
#
  #@accountingEventFields19IT
  #Scenario: Validate the fields of EventType 19
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "19"
#
  #@accountingEventFields20IT
  #Scenario: Validate the fields of EventType 20
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "20"
#
  #@accountingEventFields21IT
  #Scenario: Validate the fields of EventType 21
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "21"
#
  #@accountingEventFields22IT
  #Scenario: Validate the fields of EventType 22
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "22"
#
  #@accountingEventFields23IT
  #Scenario: Validate the fields of EventType 23
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "23"
#
  #@accountingEventFields24IT
  #Scenario: Validate the fields of EventType 24
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "24"
#
  #@accountingEventFields25IT
  #Scenario: Validate the fields of EventType 25
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "25"
#
  #@accountingEventFields26IT
  #Scenario: Validate the fields of EventType 26
    #Given acess the accounting file "IT"
    #When open the file
    #Then validate the fields of all eventTypes "26"
#
 