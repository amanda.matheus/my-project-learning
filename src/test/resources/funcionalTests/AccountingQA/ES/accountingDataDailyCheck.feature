@accounting  @accountingES @accountingDataValidationES  @dailyAccounting
Feature: Accounting Data Daily Check - Spain
  Validate the data of all EventTypes
  
  ##---Precisa correr antes a tag accountingContractCreatedFileES e accountingContractCancelledFileES
  @accountingDataDetailsFileContractCreatedES  
  Scenario: Write the contract details of eventTypes 01, 03, 14, 19, 20 in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    Then write all "ContractCreated" data
    
 	@accountingDataDetailsFileEarlySettlementES  
  Scenario: Write the contract details of eventTypes 05 and 07 in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    Then write all "EarlySettlement" data
 
 	@accountingDataDetailsCancelPartialES  
  Scenario: Write the contract details of eventTypes 06, 10, 18, 21 and 22 in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    Then write all "CancelPartial" data
    
  @accountingDataDetailsCancelTotalES  
  Scenario: Write the contract details of eventTypes 06, 10, 18, 21, 22, 23 and 24 in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    Then write all "CancelTotal" data
    
 	@accountingDataDetailsSoftES  
  Scenario: Write the contract details of eventTypes 03, 04 and 15 in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    Then write all "Soft" data