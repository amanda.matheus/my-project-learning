@accounting  @accountingValidationOfEventsGeneration
Feature: Accounting Validation Of Events Generation - Spain
  Verify the generation of all week´s accounting eventTypes

  @accountingEventErrorWeekES
  Scenario: Create an unique week´s error file
    Given acess the accounting file "ES"
    When open the file
    Then create an unique error file for week

  @accountingEventErrorWeekValidationES
  Scenario: Double-check that the events of the week´s error file was not generated
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    When click on the Accounting Files submenu
    Then download the accounting files and validate the errors file of the week

  @accountingEventType01GenerationWeekES
  Scenario: Verify if the eventType 01 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "01" was generated

  @accountingEventType02GenerationWeekES
  Scenario: Verify if the eventType 02 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "02" was generated

  @accountingEventType03GenerationWeekES
  Scenario: Verify if the eventType 03 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "03" was generated

  @accountingEventType04GenerationWeekES
  Scenario: Verify if the eventType 04 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "04" was generated

  @accountingEventType05GenerationWeekES
  Scenario: Verify if the eventType 05 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "05" was generated

  @accountingEventType06GenerationWeekES
  Scenario: Verify if the eventType 06 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "06" was generated

  @accountingEventType07GenerationWeekES
  Scenario: Verify if the eventType 07 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "07" was generated

  @accountingEventType08GenerationWeekES
  Scenario: Verify if the eventType 08 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "08" was generated

  @accountingEventType09GenerationWeekES
  Scenario: Verify if the eventType 09 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "09" was generated

  @accountingEventType10GenerationWeekES
  Scenario: Verify if the eventType 10 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "10" was generated

  @accountingEventType13GenerationWeekES
  Scenario: Verify if the eventType 13 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "13" was generated

  @accountingEventType14GenerationWeekES
  Scenario: Verify if the eventType 14 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "14" was generated

  @accountingEventType15GenerationWeekES
  Scenario: Verify if the eventType 15 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "15" was generated

  @accountingEventType16GenerationWeekES
  Scenario: Verify if the eventType 16 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "16" was generated

  @accountingEventType18GenerationWeekES
  Scenario: Verify if the eventType 18 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "18" was generated

  @accountingEventType19GenerationWeekES
  Scenario: Verify if the eventType 19 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "19" was generated

  @accountingEventType20GenerationWeekES
  Scenario: Verify if the eventType 20 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "20" was generated

  @accountingEventType21GenerationWeekES
  Scenario: Verify if the eventType 21 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "21" was generated

  @accountingEventType22GenerationWeekES
  Scenario: Verify if the eventType 22 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "22" was generated

  @accountingEventType23GenerationWeekES
  Scenario: Verify if the eventType 23 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "23" was generated

  @accountingEventType24GenerationWeekES
  Scenario: Verify if the eventType 24 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "24" was generated

  @accountingEventType25GenerationWeekES
  Scenario: Verify if the eventType 25 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "25" was generated

  @accountingEventType26GenerationWeekES
  Scenario: Verify if the eventType 26 was generated
    Given acess the "ES" error file of the week
    When open the file
    Then validate if the eventType "26" was generated

 @accountingEventType27GenerationWeekES
  Scenario: Verify if the eventType 27 was generated
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "27" was generated daily