@all @ES @accounting @accountingES @accountingEventGenerateES @dailyAccounting
Feature: AAccounting Support Feature - Spain
  Verify is the events of all contracts created by automation are generated


  @accountingContractCreatedFileES 
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "ContractCreated" details in the file
    
    
  @accountingContractCancelledFileES 
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "CancelTotal" details in the file
    
        
 @accountingEventGenerate27ES @dailyAccountingES
  Scenario: Validate the generation of EventType 27
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "27" was generated daily

  ##----------------------------------------------------------------------------------
  ##                   Contract Created
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate01ES @dailyAccountingES
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "01" was generated after the "ContractCreated"

  @accountingEventGenerate14ES @dailyAccountingES
  Scenario: Validate the generation of EventType 14
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "14" was generated after the "ContractCreated"

  @accountingEventGenerate03ES @dailyAccountingES
  Scenario: Validate the generation of EventType 03
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "ContractCreated"

  @accountingEventGenerate19ES @dailyAccountingES
  Scenario: Validate the generation of EventType 19
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "19" was generated after the "ContractCreated"

  @accountingEventGenerateC20ES @dailyAccountingES
  Scenario: Validate the generation of EventType 20
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "ContractCreated"

  ##----------------------------------------------------------------------------------
  ##                   Cancellation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate06TES @dailyAccountingES
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelTotal"

  @accountingEventGenerate10TES @dailyAccountingES
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelTotal"
        
  @accountingEventGenerate21TES @dailyAccountingES
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelTotal"

  @accountingEventGenerate18TES @dailyAccountingES
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelTotal"
    
  @accountingEventGenerate22TES @dailyAccountingES
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelTotal"

  @accountingEventGenerate23ES @dailyAccountingES
  Scenario: Validate the generation of EventType 23
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "23" was generated after the "CancelTotal"

  @accountingEventGenerate24ES @dailyAccountingES
  Scenario: Validate the generation of EventType 24
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "24" was generated after the "CancelTotal"

  @accountingEventGenerate06PES @dailyAccountingES
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelPartial"

  @accountingEventGenerate10PPES @dailyAccountingES
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelPartial"
    
  @accountingEventGenerate21PES @dailyAccountingES
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelPartial"

  @accountingEventGenerate18PES @dailyAccountingES
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelPartial"
    
  @accountingEventGenerate22PES @dailyAccountingES
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelPartial"
    

  ##----------------------------------------------------------------------------------
  ##                   Early Settlement
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate05ES @dailyAccountingES
  Scenario: Validate the generation of EventType 05
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "05" was generated after the "EarlySettlement"

  @accountingEventGenerate07ES @dailyAccountingES
  Scenario: Validate the generation of EventType 07
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "07" was generated after the "EarlySettlement"  
    

  ##----------------------------------------------------------------------------------
  ##                   Soft
  ##----------------------------------------------------------------------------------
 
  @accountingEventGenerate04ES @dailyAccountingES
  Scenario: Validate the generation of EventType 04
Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "04" was generated after the "Soft"

  @accountingEventGenerate15ES @dailyAccountingES
  Scenario: Validate the generation of EventType 15
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "15" was generated after the "Soft"

  ##se for pago no mesmo dia, tem que gerar 2 03 né?
  @accountingEventGenerate03SPES @dailyAccountingES
  Scenario: Validate the generation of EventType 03
Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "SoftPaid"

  @accountingEventGenerate25ES @dailyAccountingES
  Scenario: Validate the generation of EventType 25
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "25" was generated after the "SoftPaid"

  @accountingEventGenerateS20ES @dailyAccountingES
  Scenario: Validate the generation of EventType 20
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "SoftPaid"
    
  @accountingEventGenerateS20ES @dailyAccountingES
  Scenario: Validate the generation of EventType 26
   Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "26" was generated after the "SoftPaid"

  ##----------------------------------------------------------------------------------
  ##                   Hard
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08ES @dailyAccountingES
  Scenario: Validate the generation of EventType 08
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "08" was generated after the "Hard"

  @accountingEventGenerate09ES @dailyAccountingES
  Scenario: Validate the generation of EventType 09
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "09" was generated after the "Hard"
    
    
  ##----------------------------------------------------------------------------------
  ##                   Litigation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08ES @dailyAccountingES
  Scenario: Validate the generation of EventType 16
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "16" was generated after the "Litigation"
    
        
  ##----------------------------------------------------------------------------------
  ##                   MFS
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate02ES @dailyAccountingES
  Scenario: Validate the generation of EventType 02
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "02" was generated after the "ContractCreated" 
  
  @accountingEventGenerate13ES @dailyAccountingES
  Scenario: Validate the generation of EventType 13
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "13" was generated after the "ContractCreated"
    
    
  ##----------------------------------------------------------------------------------
  ##                   Validate Errors file
  ##----------------------------------------------------------------------------------
  
  @accountingEventError01ES @dailyAccountingES
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "1" days
    
    @accountingEventError02ES @dailyAccountingES
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "2" days
    
    @accountingEventError03ES @dailyAccountingES
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "3" days
    
    @accountingEventError04ES @dailyAccountingES
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "4" days
    
    @accountingEventError05ES @dailyAccountingES
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice ES"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "5" days
    
    

