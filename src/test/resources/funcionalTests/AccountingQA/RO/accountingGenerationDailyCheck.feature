@accounting @accountingRO @accountingEventGenerateRO @dailyAccounting
Feature: Accounting Support Feature - Romania
  Verify is the events of all contracts created by automation are generated

  @accountingContractCreatedFileRO
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "ContractCreated" details in the file
    
    
  @accountingContractCancelledFileRO
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "CancelTotal" details in the file
    
        
 @accountingEventGenerate27RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 27
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "27" was generated daily

  ##----------------------------------------------------------------------------------
  ##                   Contract Created
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate01RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "01" was generated after the "ContractCreated"

  @accountingEventGenerate14RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 14
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "14" was generated after the "ContractCreated"

  @accountingEventGenerate03RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 03
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "ContractCreated"

  @accountingEventGenerate19RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 19
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "19" was generated after the "ContractCreated"

  @accountingEventGenerateC20RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 20
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "ContractCreated"

  ##----------------------------------------------------------------------------------
  ##                   Cancellation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate06TRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelTotal"

  @accountingEventGenerate10TRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelTotal"
        
  @accountingEventGenerate21TRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelTotal"

  @accountingEventGenerate18TRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelTotal"
    
  @accountingEventGenerate22TRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelTotal"

  @accountingEventGenerate23RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 23
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "23" was generated after the "CancelTotal"

  @accountingEventGenerate24RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 24
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "24" was generated after the "CancelTotal"

  @accountingEventGenerate06PRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelPartial"

  @accountingEventGenerate10PPRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelPartial"
    
  @accountingEventGenerate21PRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelPartial"

  @accountingEventGenerate18PRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelPartial"
    
  @accountingEventGenerate22PRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelPartial"
    
  ##----------------------------------------------------------------------------------
  ##                   Early Settlement
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate05RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 05
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "05" was generated after the "EarlySettlement"

  @accountingEventGenerate07RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 07
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "07" was generated after the "EarlySettlement"

  ##----------------------------------------------------------------------------------
  ##                   Soft
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate04RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 04
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "04" was generated after the "Soft"

  @accountingEventGenerate15RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 15
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "15" was generated after the "Soft"

  ##se for pago no mesmo dia, tem que gerar 2 03 né?
  @accountingEventGenerate03SPRO @dailyAccountingRO
  Scenario: Validate the generation of EventType 03
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "SoftPaid"

  @accountingEventGenerate25RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 25
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "25" was generated after the "SoftPaid"

  @accountingEventGenerateS20RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 20
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "SoftPaid"

  @accountingEventGenerateS20RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 26
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "26" was generated after the "SoftPaid"

  ##----------------------------------------------------------------------------------
  ##                   Hard
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 08
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "08" was generated after the "Hard"

  @accountingEventGenerate09RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 09
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "09" was generated after the "Hard"

  ##----------------------------------------------------------------------------------
  ##                   Litigation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 16
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "16" was generated after the "Litigation"

  ##----------------------------------------------------------------------------------
  ##                   MFS
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate02RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 02
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "02" was generated after the "ContractCreated"

  @accountingEventGenerate13RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 13
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "13" was generated after the "ContractCreated"


  ##----------------------------------------------------------------------------------
  ##                   Validate Errors file
  ##----------------------------------------------------------------------------------
  
  @accountingEventError01RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "1" days
    
    @accountingEventError02RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "2" days
    
    @accountingEventError03RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "3" days
    
    @accountingEventError04RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "4" days
    
    @accountingEventError05RO @dailyAccountingRO
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice RO"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "5" days
    
