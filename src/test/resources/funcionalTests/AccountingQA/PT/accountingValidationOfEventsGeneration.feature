@accounting  @accountingValidationOfEventsGeneration
Feature: Accounting Validation Of Events Generation - Portugal
  Verify the generation of all week´s accounting eventTypes

  @accountingEventErrorWeekPT
  Scenario: Create an unique week´s error file
    Given acess the accounting file "PT"
    When open the file
    Then create an unique error file for week

  @accountingEventErrorWeekValidationPT
  Scenario: Double-check that the events of the week´s error file was not generated
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Accounting menu
    When click on the Accounting Files submenu
    Then download the accounting files and validate the errors file of the week

  @accountingEventType01GenerationWeekPT
  Scenario: Verify if the eventType 01 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "01" was generated

  @accountingEventType02GenerationWeekPT
  Scenario: Verify if the eventType 02 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "02" was generated

  @accountingEventType03GenerationWeekPT
  Scenario: Verify if the eventType 03 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "03" was generated

  @accountingEventType04GenerationWeekPT
  Scenario: Verify if the eventType 04 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "04" was generated

  @accountingEventType05GenerationWeekPT
  Scenario: Verify if the eventType 05 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "05" was generated

  @accountingEventType06GenerationWeekPT
  Scenario: Verify if the eventType 06 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "06" was generated

  @accountingEventType07GenerationWeekPT
  Scenario: Verify if the eventType 07 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "07" was generated

  @accountingEventType08GenerationWeekPT
  Scenario: Verify if the eventType 08 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "08" was generated

  @accountingEventType09GenerationWeekPT
  Scenario: Verify if the eventType 09 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "09" was generated

  @accountingEventType10GenerationWeekPT
  Scenario: Verify if the eventType 10 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "10" was generated

  @accountingEventType13GenerationWeekPT
  Scenario: Verify if the eventType 13 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "13" was generated

  @accountingEventType14GenerationWeekPT
  Scenario: Verify if the eventType 14 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "14" was generated

  @accountingEventType15GenerationWeekPT
  Scenario: Verify if the eventType 15 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "15" was generated

  @accountingEventType16GenerationWeekPT
  Scenario: Verify if the eventType 16 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "16" was generated

  @accountingEventType18GenerationWeekPT
  Scenario: Verify if the eventType 18 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "18" was generated

  @accountingEventType19GenerationWeekPT
  Scenario: Verify if the eventType 19 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "19" was generated

  @accountingEventType20GenerationWeekPT
  Scenario: Verify if the eventType 20 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "20" was generated

  @accountingEventType21GenerationWeekPT
  Scenario: Verify if the eventType 21 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "21" was generated

  @accountingEventType22GenerationWeekPT
  Scenario: Verify if the eventType 22 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "22" was generated

  @accountingEventType23GenerationWeekPT
  Scenario: Verify if the eventType 23 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "23" was generated

  @accountingEventType24GenerationWeekPT
  Scenario: Verify if the eventType 24 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "24" was generated

  @accountingEventType25GenerationWeekPT
  Scenario: Verify if the eventType 25 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "25" was generated

  @accountingEventType26GenerationWeekPT
  Scenario: Verify if the eventType 26 was generated
    Given acess the "PT" error file of the week
    When open the file
    Then validate if the eventType "26" was generated
    
 @accountingEventType27GenerationWeekPT
  Scenario: Verify if the eventType 27 was generated
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "27" was generated daily
