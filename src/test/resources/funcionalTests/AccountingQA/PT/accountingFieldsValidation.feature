@accounting @accountingPT @accountingFieldsValidation
Feature: Accounting Fields Validation - Portugal
  Validate the fields of all EventTypes

  
  @accountingEventFields01PT
  Scenario: Validate the fields of EventType 01
    Given navigate to "BackOffice PT"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the fields of all eventTypes "01"

  @accountingEventFields02PT
  Scenario: Validate the fields of EventType 02v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "02v2"

  @accountingEventFields03PT
  Scenario: Validate the fields of EventType 03
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "03"

  @accountingEventFields04PT
  Scenario: Validate the fields of EventType 04v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "04v2"

  @accountingEventFields05PT
  Scenario: Validate the fields of EventType 05
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "05"

  @accountingEventFields06PT
  Scenario: Validate the fields of EventType 06v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "06v2"

  @accountingEventFields07PT
  Scenario: Validate the fields of EventType 07v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "07v2"

  @accountingEventFields08PT
  Scenario: Validate the fields of EventType 08v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "08v2"

  @accountingEventFields09PT
  Scenario: Validate the fields of EventType 09
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "09"

  @accountingEventFields10PT
  Scenario: Validate the fields of EventType 10v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "10v2"

  @accountingEventFields11PT
  Scenario: Validate the fields of EventType 11v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "11v2"

  @accountingEventFields12PT
  Scenario: Validate the fields of EventType 12v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "12v2"

  @accountingEventFields13PT
  Scenario: Validate the fields of EventType 13v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "13v2"

  @accountingEventFields14PT
  Scenario: Validate the fields of EventType 14
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "14"

  @accountingEventFields15PT
  Scenario: Validate the fields of EventType 15
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "15"

  @accountingEventFields16PT
  Scenario: Validate the fields of EventType 16v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "16v2"

  @accountingEventFields17PT
  Scenario: Validate the fields of EventType 17
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "17"

  @accountingEventFields18PT
  Scenario: Validate the fields of EventType 18v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "18v2"

  @accountingEventFields19PT
  Scenario: Validate the fields of EventType 19v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "19v2"

  @accountingEventFields20PT
  Scenario: Validate the fields of EventType 20v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "20v2"

  @accountingEventFields21PT
  Scenario: Validate the fields of EventType 21v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "21v2"

  @accountingEventFields22PT
  Scenario: Validate the fields of EventType 22v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "22v2"

  @accountingEventFields23PT
  Scenario: Validate the fields of EventType 23v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "23v2"

  @accountingEventFields24PT
  Scenario: Validate the fields of EventType 24v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "24v2"

  @accountingEventFields25PT
  Scenario: Validate the fields of EventType 25v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "25v2"

  @accountingEventFields26PT
  Scenario: Validate the fields of EventType 26v2
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "26v2"

  @accountingEventFields27PT
  Scenario: Validate the fields of EventType 27
    Given acess the accounting file "PT"
    When open the file
    Then validate the fields of all eventTypes "27"

    
    
    
    
    
    
    
  #@accountingEventFields02PT
  #Scenario: Validate the fields of EventType 02
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "02"
#
  #@accountingEventFields04PT
  #Scenario: Validate the fields of EventType 04
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "04"
#
  #@accountingEventFields06PT
  #Scenario: Validate the fields of EventType 06
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "06"
#
  #@accountingEventFields07PT
  #Scenario: Validate the fields of EventType 07
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "07"
#
  #@accountingEventFields08PT
  #Scenario: Validate the fields of EventType 08
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "08"
#
  #@accountingEventFields10PT
  #Scenario: Validate the fields of EventType 10
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "10"
#
  #@accountingEventFields11PT
  #Scenario: Validate the fields of EventType 11
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "11"
#
  #@accountingEventFields12PT
  #Scenario: Validate the fields of EventType 12
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "12"
#
  #@accountingEventFields13PT
  #Scenario: Validate the fields of EventType 13
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "13"
 #
  #@accountingEventFields16PT
  #Scenario: Validate the fields of EventType 16
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "16"
#
  #@accountingEventFields18PT
  #Scenario: Validate the fields of EventType 18
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "18"
#
  #@accountingEventFields19PT
  #Scenario: Validate the fields of EventType 19
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "19"
#
  #@accountingEventFields20PT
  #Scenario: Validate the fields of EventType 20
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "20"
#
  #@accountingEventFields21PT
  #Scenario: Validate the fields of EventType 21
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "21"
#
  #@accountingEventFields22PT
  #Scenario: Validate the fields of EventType 22
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "22"
#
  #@accountingEventFields23PT
  #Scenario: Validate the fields of EventType 23
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "23"
#
  #@accountingEventFields24PT
  #Scenario: Validate the fields of EventType 24
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "24"
#
  #@accountingEventFields25PT
  #Scenario: Validate the fields of EventType 25
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "25"
#
  #@accountingEventFields26PT
  #Scenario: Validate the fields of EventType 26
    #Given acess the accounting file "PT"
    #When open the file
    #Then validate the fields of all eventTypes "26"
#
 