@accounting  @accountingDE @accountingDataValidationDE @accountingDataValidation
Feature: Accounting Data Validation - Germany
  Validate the data of all EventTypes

    
 @accountingDataValidationEventsDE 
  Scenario: Validate the contract data of eventTypes 01, 03, 14, 19, 20 json accounting
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    When click on the JSON Files submenu
    Then validate all "ContractCreated" data