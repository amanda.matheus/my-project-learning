@accounting  @accountingDE @accountingFieldsValidation
Feature: Accounting Fields Validation - Germany 
  Validate the fields of all EventTypes

  
  @accountingEventFields01DE
  Scenario: Validate the fields of EventType 01
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate the fields of all eventTypes "01"

  @accountingEventFields02DE
  Scenario: Validate the fields of EventType 02v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "02v2"

  @accountingEventFields03DE
  Scenario: Validate the fields of EventType 03
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "03"

  @accountingEventFields04DE
  Scenario: Validate the fields of EventType 04v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "04v2"

  @accountingEventFields05DE
  Scenario: Validate the fields of EventType 05
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "05"

  @accountingEventFields06DE
  Scenario: Validate the fields of EventType 06v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "06v2"

  @accountingEventFields07DE
  Scenario: Validate the fields of EventType 07v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "07v2"

  @accountingEventFields08DE
  Scenario: Validate the fields of EventType 08v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "08v2"

  @accountingEventFields09DE
  Scenario: Validate the fields of EventType 09
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "09"

  @accountingEventFields10DE
  Scenario: Validate the fields of EventType 10v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "10v2"

  @accountingEventFields11DE
  Scenario: Validate the fields of EventType 11v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "11v2"

  @accountingEventFields12DE
  Scenario: Validate the fields of EventType 12v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "12v2"

  @accountingEventFields13DE
  Scenario: Validate the fields of EventType 13v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "13v2"

  @accountingEventFields14DE
  Scenario: Validate the fields of EventType 14
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "14"

  @accountingEventFields15DE
  Scenario: Validate the fields of EventType 15
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "15"

  @accountingEventFields16DE
  Scenario: Validate the fields of EventType 16v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "16v2"

  @accountingEventFields17DE
  Scenario: Validate the fields of EventType 17
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "17"

  @accountingEventFields18DE
  Scenario: Validate the fields of EventType 18v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "18v2"

  @accountingEventFields19DE
  Scenario: Validate the fields of EventType 19v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "19v2"

  @accountingEventFields20DE
  Scenario: Validate the fields of EventType 20v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "20v2"

  @accountingEventFields21DE
  Scenario: Validate the fields of EventType 21v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "21v2"

  @accountingEventFields22DE
  Scenario: Validate the fields of EventType 22v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "22v2"

  @accountingEventFields23DE
  Scenario: Validate the fields of EventType 23v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "23v2"

  @accountingEventFields24DE
  Scenario: Validate the fields of EventType 24v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "24v2"

  @accountingEventFields25DE
  Scenario: Validate the fields of EventType 25v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "25v2"

  @accountingEventFields26DE
  Scenario: Validate the fields of EventType 26v2
    Given acess the accounting file "DE"
    When open the file
    Then validate the fields of all eventTypes "26v2"
  
    
    
  #@accountingEventFields02DE
  #Scenario: Validate the fields of EventType 02
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "02"
#
  #@accountingEventFields04DE
  #Scenario: Validate the fields of EventType 04
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "04"
#
  #@accountingEventFields06DE
  #Scenario: Validate the fields of EventType 06
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "06"
#
  #@accountingEventFields07DE
  #Scenario: Validate the fields of EventType 07
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "07"
#
  #@accountingEventFields08DE
  #Scenario: Validate the fields of EventType 08
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "08"
#
  #@accountingEventFields10DE
  #Scenario: Validate the fields of EventType 10
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "10"
#
  #@accountingEventFields11DE
  #Scenario: Validate the fields of EventType 11
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "11"
#
  #@accountingEventFields12DE
  #Scenario: Validate the fields of EventType 12
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "12"
#
  #@accountingEventFields13DE
  #Scenario: Validate the fields of EventType 13
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "13"
 #
  #@accountingEventFields16DE
  #Scenario: Validate the fields of EventType 16
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "16"
#
  #@accountingEventFields18DE
  #Scenario: Validate the fields of EventType 18
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "18"
#
  #@accountingEventFields19DE
  #Scenario: Validate the fields of EventType 19
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "19"
#
  #@accountingEventFields20DE
  #Scenario: Validate the fields of EventType 20
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "20"
#
  #@accountingEventFields21DE
  #Scenario: Validate the fields of EventType 21
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "21"
#
  #@accountingEventFields22DE
  #Scenario: Validate the fields of EventType 22
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "22"
#
  #@accountingEventFields23DE
  #Scenario: Validate the fields of EventType 23
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "23"
#
  #@accountingEventFields24DE
  #Scenario: Validate the fields of EventType 24
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "24"
#
  #@accountingEventFields25DE
  #Scenario: Validate the fields of EventType 25
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "25"
#
  #@accountingEventFields26DE
  #Scenario: Validate the fields of EventType 26
    #Given acess the accounting file "DE"
    #When open the file
    #Then validate the fields of all eventTypes "26"
#
 