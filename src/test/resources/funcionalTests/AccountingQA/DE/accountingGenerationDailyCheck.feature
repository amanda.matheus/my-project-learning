@accounting @accountingDE @accountingEventGenerateDE @dailyAccounting
Feature: Accounting Support Feature - Germany
  Verify is the events of all contracts created by automation are generated and save inside an error file those that was not generated


  @accountingContractCreatedFileDE 
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "ContractCreated" details in the file
    And write all "Cancel" details in the file


  ##----------------------------------------------------------------------------------
  ##                   Contract Created
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate01DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "01" was generated after the "ContractCreated"

  @accountingEventGenerate14DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 14
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "14" was generated after the "ContractCreated"

  @accountingEventGenerate03DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 03
   Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "ContractCreated"

  @accountingEventGenerate19DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 19
   Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "19" was generated after the "ContractCreated"


  ##----------------------------------------------------------------------------------
  ##                   Cancellation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate06TDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelTotal"

  @accountingEventGenerate10TDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelTotal"
        
  @accountingEventGenerate21TDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelTotal"

  @accountingEventGenerate18TDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelTotal"
    
  @accountingEventGenerate22TDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelTotal"


  @accountingEventGenerate06PDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelPartial"

  @accountingEventGenerate10PPDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelPartial"
    
  @accountingEventGenerate21PDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelPartial"

  @accountingEventGenerate18PDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelPartial"
    
  @accountingEventGenerate22PDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelPartial"
    

  ##----------------------------------------------------------------------------------
  ##                   Early Settlement
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate05DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 05
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When get the name of the day file
    Then validate if the eventType "05" was generated after the "EarlySettlement"

  @accountingEventGenerate07DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 07
   Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When get the name of the day file
    Then validate if the eventType "07" was generated after the "EarlySettlement"  


  ##----------------------------------------------------------------------------------
  ##                   Soft
  ##----------------------------------------------------------------------------------

  @accountingEventGenerate04DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 04
	Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "04" was generated after the "Soft"

  @accountingEventGenerate15DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 15
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "15" was generated after the "Soft"

  ##se for pago no mesmo dia, tem que gerar 2 03 né?
  @accountingEventGenerate03SPDE @dailyAccountingDE
  Scenario: Validate the generation of EventType 03
	Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "SoftPaid"

  @accountingEventGenerate25DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 25
   Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "25" was generated after the "SoftPaid"

  @accountingEventGenerateS20DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 20
   Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "SoftPaid"
    
  @accountingEventGenerateS20DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 26
   Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "26" was generated after the "SoftPaid"

  ##----------------------------------------------------------------------------------
  ##                   Hard
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 08
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "08" was generated after the "Hard"

  @accountingEventGenerate09DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 09
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "09" was generated after the "Hard"
    
    
  ##----------------------------------------------------------------------------------
  ##                   Litigation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 16
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "16" was generated after the "Litigation"
    
        
  ##----------------------------------------------------------------------------------
  ##                   MFS
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate02DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 02
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "02" was generated after the "ContractCreated" 
    
  @accountingEventGenerate13DE @dailyAccountingDE
  Scenario: Validate the generation of EventType 13
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate if the eventType "13" was generated after the "ContractCreated"

        
  ##----------------------------------------------------------------------------------
  ##                   Validate Errors file
  ##----------------------------------------------------------------------------------
  
  @accountingEventError01DE @dailyAccountingDE
  Scenario: Validate the errors of 1 days
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate the errors of "1" days
    
    @accountingEventError02DE @dailyAccountingDE
  Scenario: Validate the errors of 2 days
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate the errors of "2" days
    
    @accountingEventError03DE @dailyAccountingDE
  Scenario: Validate the errors of 3 days
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate the errors of "3" days
    
    @accountingEventError04DE @dailyAccountingDE
  Scenario: Validate the errors of 4 days
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate the errors of "4" days
    
    @accountingEventError05DE @dailyAccountingDE
  Scenario: Validate the errors of 5 days
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the JSON Files submenu
    When download the day file
    Then validate the errors of "5" days
    
