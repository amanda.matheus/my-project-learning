@accounting @accountingValidationOfEventsGeneration
Feature: Accounting Validation Of Events Generation - Germany
  Verify the generation of all week´s accounting eventTypes

  @accountingEventErrorWeekDE
  Scenario: Create an unique week´s error file
    Given acess the accounting file "DE"
    When open the file
    Then create an unique error file for week

  @accountingEventErrorWeekValidationDE
  Scenario: Double-check that the events of the week´s error file was not generated
    Given navigate to "BackOffice DE"
    And enter a valid username and password
    And click on the Accounting menu
    When click on the JSON Files submenu
    Then download the accounting files and validate the errors file of the week

  @accountingEventType01GenerationWeekDE
  Scenario: Verify if the eventType 01 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "01" was generated

  @accountingEventType02GenerationWeekDE
  Scenario: Verify if the eventType 02 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "02" was generated

  @accountingEventType03GenerationWeekDE
  Scenario: Verify if the eventType 03 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "03" was generated

  @accountingEventType04GenerationWeekDE
  Scenario: Verify if the eventType 04 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "04" was generated

  @accountingEventType05GenerationWeekDE
  Scenario: Verify if the eventType 05 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "05" was generated

  @accountingEventType06GenerationWeekDE
  Scenario: Verify if the eventType 06 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "06" was generated

  @accountingEventType07GenerationWeekDE
  Scenario: Verify if the eventType 07 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "07" was generated

  @accountingEventType08GenerationWeekDE
  Scenario: Verify if the eventType 08 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "08" was generated

  @accountingEventType09GenerationWeekDE
  Scenario: Verify if the eventType 09 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "09" was generated

  @accountingEventType10GenerationWeekDE
  Scenario: Verify if the eventType 10 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "10" was generated

  @accountingEventType13GenerationWeekDE
  Scenario: Verify if the eventType 13 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "13" was generated

  @accountingEventType14GenerationWeekDE
  Scenario: Verify if the eventType 14 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "14" was generated

  @accountingEventType15GenerationWeekDE
  Scenario: Verify if the eventType 15 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "15" was generated

  @accountingEventType16GenerationWeekDE
  Scenario: Verify if the eventType 16 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "16" was generated

  @accountingEventType18GenerationWeekDE
  Scenario: Verify if the eventType 18 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "18" was generated

  @accountingEventType19GenerationWeekDE
  Scenario: Verify if the eventType 19 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "19" was generated

  @accountingEventType21GenerationWeekDE
  Scenario: Verify if the eventType 21 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "21" was generated

  @accountingEventType22GenerationWeekDE
  Scenario: Verify if the eventType 22 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "22" was generated


  @accountingEventType25GenerationWeekDE
  Scenario: Verify if the eventType 25 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "25" was generated

  @accountingEventType26GenerationWeekDE
  Scenario: Verify if the eventType 26 was generated
    Given acess the "DE" error file of the week
    When open the file
    Then validate if the eventType "26" was generated
