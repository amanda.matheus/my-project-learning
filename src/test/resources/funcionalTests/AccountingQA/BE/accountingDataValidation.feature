@accounting  @accountingBE @accountingDataValidationBE @accountingDataValidation
Feature: Accounting Data Validation - Belgium
  Validate the data of all EventTypes
  
 
    
 @accountingDataValidationEventsContractCreatedBE 
  Scenario: Validate the contract data of eventTypes 01, 03, 14, 19, 20 json accounting
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    When click on the Accounting Files submenu
    Then validate all "ContractCreated" data
    
    @accountingDataValidationEventsEarlySettlementBE 
  Scenario: Validate the contract data of eventTypes 01, 03, 14, 19, 20 json accounting
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    When click on the Accounting Files submenu
    Then validate all "EarlySettlement" data