@accounting @accountingBE @accountingEventGenerateBE @dailyAccounting
Feature: Accounting Generation Daily Check - Belgium
  Verify is the events of all contracts created by automation are generated and save inside an error file those that was not generated


  @accountingContractCreatedFileBE 
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "ContractCreated" details in the file
    
  @accountingContractCancelledFileBE 
  Scenario: Write the contract number in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    When click on the Contracts menu
    Then write all "CancelTotal" details in the file
    
 @accountingEventGenerate27BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 27
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "27" was generated daily

  ##----------------------------------------------------------------------------------
  ##                   Contract Created
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate01BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "01" was generated after the "ContractCreated"

  @accountingEventGenerate14BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 14
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "14" was generated after the "ContractCreated"

  @accountingEventGenerate03BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 03
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "ContractCreated"

  @accountingEventGenerate19BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 19
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "19" was generated after the "ContractCreated"

  @accountingEventGenerateC20BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 20
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "ContractCreated"

  ##----------------------------------------------------------------------------------
  ##                   Cancellation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate06TBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelTotal"

  @accountingEventGenerate10TBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelTotal"
        
  @accountingEventGenerate21TBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelTotal"

  @accountingEventGenerate18TBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelTotal"
    
  @accountingEventGenerate22TBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelTotal"

  @accountingEventGenerate23BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 23
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "23" was generated after the "CancelTotal"

  @accountingEventGenerate24BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 24
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "24" was generated after the "CancelTotal"

  @accountingEventGenerate06PBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 06
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "06" was generated after the "CancelPartial"

  @accountingEventGenerate10PPBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 10
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "10" was generated after the "CancelPartial"
    
  @accountingEventGenerate21PBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 21
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "21" was generated after the "CancelPartial"

  @accountingEventGenerate18PBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 18
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "18" was generated after the "CancelPartial"
    
  @accountingEventGenerate22PBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 22
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "22" was generated after the "CancelPartial"
    

  ##----------------------------------------------------------------------------------
  ##                   Early Settlement
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate05BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 05
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "05" was generated after the "EarlySettlement"

  @accountingEventGenerate07BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 07
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When get the name of the day file
    Then validate if the eventType "07" was generated after the "EarlySettlement"  


  ##----------------------------------------------------------------------------------
  ##                   Soft
  ##----------------------------------------------------------------------------------

  @accountingEventGenerate04BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 04
	Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "04" was generated after the "Soft"

  @accountingEventGenerate15BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 15
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "15" was generated after the "Soft"

  ##se for pago no mesmo dia, tem que gerar 2 03 né?
  @accountingEventGenerate03SPBE @dailyAccountingBE
  Scenario: Validate the generation of EventType 03
	Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "03" was generated after the "SoftPaid"

  @accountingEventGenerate25BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 25
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "25" was generated after the "SoftPaid"

  @accountingEventGenerateS20BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 20
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "20" was generated after the "SoftPaid"
    
  @accountingEventGenerateS20BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 26
   Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "26" was generated after the "SoftPaid"

  ##----------------------------------------------------------------------------------
  ##                   Hard
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 08
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "08" was generated after the "Hard"

  @accountingEventGenerate09BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 09
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "09" was generated after the "Hard"
    
    
  ##----------------------------------------------------------------------------------
  ##                   Litigation
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate08BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 16
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "16" was generated after the "Litigation"
    
        
  ##----------------------------------------------------------------------------------
  ##                   MFS
  ##----------------------------------------------------------------------------------
  @accountingEventGenerate02BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 02
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "02" was generated after the "ContractCreated" 
    
  @accountingEventGenerate13BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 13
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate if the eventType "13" was generated after the "ContractCreated"

        
  ##----------------------------------------------------------------------------------
  ##                   Validate Errors file
  ##----------------------------------------------------------------------------------
  
  @accountingEventError01BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "1" days
    
    @accountingEventError02BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "2" days
    
    @accountingEventError03BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "3" days
    
    @accountingEventError04BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "4" days
    
    @accountingEventError05BE @dailyAccountingBE
  Scenario: Validate the generation of EventType 01
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    And click on the Accounting menu
    And click on the Accounting Files submenu
    When download the day file
    Then validate the errors of "5" days
    
