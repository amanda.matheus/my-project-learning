@accounting  @accountingBE @accountingDataValidationBE @dailyAccounting
Feature: Accounting Data Daily Check - Belgium
  Validate the data of all EventTypes
  
  ##---Precisa correr antes a tag accountingContractCreatedFileBE e accountingContractCancelledFileBE
  @accountingDataDetailsFileContractCreatedBE  
  Scenario: Write the contract details of eventTypes 01, 03, 14, 19, 20 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "ContractCreated" data
    
 	@accountingDataDetailsFileEarlySettlementBE  
  Scenario: Write the contract details of eventTypes 05 and 07 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "EarlySettlement" data
 
 	@accountingDataDetailsCancelPartialBE  
  Scenario: Write the contract details of eventTypes 06, 10, 18, 21 and 22 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "CancelPartial" data
    
  @accountingDataDetailsCancelTotalBE  
  Scenario: Write the contract details of eventTypes 06, 10, 18, 21, 22, 23 and 24 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "CancelTotal" data
    
 	@accountingDataDetailsSoftBE  
  Scenario: Write the contract details of eventTypes 03, 04 and 15 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "Soft" data
    
  @accountingDataDetailsSoftPaidBE  
  Scenario: Write the contract details of eventTypes 03, 25 and 26 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "SoftPaid" data
    
  @accountingDataDetailsHardBE  
  Scenario: Write the contract details of eventTypes 08 and 09 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "Hard" data
    
  @accountingDataDetailsLitigationBE  
  Scenario: Write the contract details of eventTypes 16 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "Litigation" data
    
  @accountingDataDetailsMFSBE  
  Scenario: Write the contract details of eventTypes 02, 13 and 21 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "ContractCreated" data
    
  @accountingDataDetailsPaidBE  
  Scenario: Write the contract details of eventTypes 03 and 20 in the file
    Given navigate to "BackOffice BE"
    And enter a valid username and password
    Then write all "Paid" data
    
    
    