package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/funcionalTests", glue = { "stepDefinitions" }, 
				tags = {"@webNCPR63PT"},
//						+ " @accountingEventMFSFinancingBE, @accountingEventMFSInvoiceBE,  @accountingEventMFSFinancingES, @accountingEventMFSInvoiceES "},
				plugin = { "pretty", 
						"html:target/cucumber-reports/cucumber.html",
						"json:target/json-reports/cucumber.json"}) 

				

public class TestRunner  {


}