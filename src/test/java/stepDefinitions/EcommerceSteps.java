package stepDefinitions;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import helper.Utils;
import pageObjects.eCommerce.CartReviewECommerce;
import pageObjects.eCommerce.CheckoutECommerce;
import pageObjects.eCommerce.HomePageECommerce;

public class EcommerceSteps {

	private GeneralSteps general;
	WebDriver driver;
	public WebDriverWait wait;
	private static Utils utils;
	private static HomePageECommerce homePageECommerce;
	private static CartReviewECommerce cartReviewECommerce;
	private static CheckoutECommerce checkoutECommerce;
	public String tenant = "";
	public static String BASKET_AMOUNT = null;
	public static String btsType;


	public EcommerceSteps(GeneralSteps general)  throws MalformedURLException {
		this.general=general;
		driver = this.general.getDriver();
		utils = PageFactory.initElements(driver, Utils.class);
		homePageECommerce = PageFactory.initElements(driver, HomePageECommerce.class);
		cartReviewECommerce = PageFactory.initElements(driver, CartReviewECommerce.class);
		checkoutECommerce = PageFactory.initElements(driver, CheckoutECommerce.class);
		tenant = general.getTenant();

	}
	
	@And("^fill in the shipping address$")
	public void fill_in_the_shipping_address(DataTable arg1) throws InterruptedException {

		checkoutECommerce.insertShippingAddress(arg1);
		checkoutECommerce.clickSaveReviewOrder();
		checkoutECommerce.clickProceedCheckout();
	}

	@When("^fill in the payment information$")
	public void fill_in_the_payment_information(DataTable arg1) throws Throwable {

		checkoutECommerce.insertPaymentInformationData(arg1);
		checkoutECommerce.insertEmail(arg1);
	}

	
	@When("^insert the banking information$")
	public void insert_the_banking_information(DataTable arg1) throws Throwable {

		checkoutECommerce.insertBankingInfo(arg1);
	}

	@When("^fill in the payment information and save the login credentials$")
	public void fill_in_the_payment_information_and_save_the_login_credentials(DataTable arg1) throws Throwable {

		checkoutECommerce.insertPaymentInformationData(arg1);
		checkoutECommerce.insertAndSaveEmail(arg1);
	}

	@And("^choose the payment type \"([^\"]*)\"$")
	public void choose_the_payment_type(String fees) throws Throwable {

		switch (fees) {
		case "3x (with fees)":
			checkoutECommerce.click3xWithFees();
			btsType = "3x (with fees)";
			break;
			
		case "PL14F":
			checkoutECommerce.clickPL14F();
			btsType = "PL14F";
			break;

		case "3x (without fees)":
			checkoutECommerce.click3xWithoutFees();
			btsType = "3x (without fees)";
			break;

		case "4x (with fees)":
			checkoutECommerce.click4xWithFees();
			btsType = "4x (with fees)";
			break;

		case "4x (without fees)":
			checkoutECommerce.click4xWithoutFees();
			btsType = "4x (without fees)";
			break;

		case "5x (with fees)":
			checkoutECommerce.click5xWithFees();
			btsType = "5x (with fees)";
			break;

		case "6x (with fees)":
			checkoutECommerce.click6xWithFees();
			btsType = "6x (with fees)";
			break;

		case "8x (with fees)":
			checkoutECommerce.click8xWithFees();
			btsType = "8x (with fees)";
			break;

		case "8x (without fees)":
			checkoutECommerce.click8xWithoutFees();
			btsType = "8x (without fees)";
			break;

		case "10x (with fees)":
			checkoutECommerce.click10xWithFees();
			btsType = "10x (with fees)";
			break;

		case "12x (with fees)":
			checkoutECommerce.click12xWithFees();
			btsType = "12x (with fees)";
			break;

		case "12x (without fees)":
			checkoutECommerce.click12xWithoutFees();
			btsType = "12x (without fees)";
			break;

		default:
			System.out.println("no match");
		}
	}

	
	@And("^add products to a total amount of \"([^\"]*)\" €$")
	public void add_products_to_a_total_amount_of_(String total) throws Throwable {

//		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		switch (total) {
		case "399,75":
			
			System.out.println("Amount 399,75");
			homePageECommerce.addBollingerRdOnClick();
			homePageECommerce.clickCheckoutItems();
			break;
			
		case "194,36":
			
			System.out.println("Amount 194,36");
			homePageECommerce.addGreenock();
			homePageECommerce.addChateauMonbousquet();
			Thread.sleep(2500);
			homePageECommerce.clickCheckoutItems();
			break;
			
		case "594,11":
			
			System.out.println("Amount 594,11");
			homePageECommerce.addGreenock();
			homePageECommerce.addChateauMonbousquet();
			homePageECommerce.addBollingerRdOnClick();
			Thread.sleep(2500);
			homePageECommerce.clickCheckoutItems();
			break;
			
			
		case "64,41":
			
			System.out.println("Checkout Response Code 30033 - Expired card - Pick up");
			homePageECommerce.addChateauMonbousquet();
			Thread.sleep(2500);
			homePageECommerce.clickCheckoutItems();
			break;

			
		case "64,42":
			
			System.out.println("PT - Checkout Response Code 30033 - Expired card - Pick up");
			homePageECommerce.addChateauMonbousquet();
			Thread.sleep(2500);
			homePageECommerce.addSacaRolhas1("1");
			break;
			
		case "1870,97":
			System.out.println("PORTUGAL");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBarcaVelha("2");
			homePageECommerce.addSacaRolha3("1");
			break;

		case "2212,96":
			System.out.println("REFUND - PARTIAL CANCELLATION");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolhas2("2");
			break;

		case "2213,01":
			System.out.println("REFUND - TOTAL CANCELLATION");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolhas2("3");
			break;

		case "2213,06":
			System.out.println("PARTIAL EARLY SETTLEMENT");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolhas2("4");
			break;

		case "2213,11":
			System.out.println("TOTAL EARLY SETTLEMENT");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolhas2("5");
			break;

		case "2213,36":
			System.out.println("ON GOING TO FINALIZED");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolha3("1");
			break;

		case "2214,48":
			System.out.println("PSP - RESTRICTED CARD");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolha3("3");
			homePageECommerce.addSacaRolhas2("2");
			homePageECommerce.addSacaRolhas1("2");
			break;

		case "2548,97":
			System.out.println("REJECTION - RULEBOOK AMOUNT");
			homePageECommerce.addBollingerRd("3");
			homePageECommerce.addBrunoGiacosaSantoStefano("3");
			homePageECommerce.addSacaRolha3("1");
			homePageECommerce.addBarcaVelha("1");
			break;

		default:
			System.out.println("Please insert correct basket amount in Cucumber Step");
		}

		BASKET_AMOUNT = total;
		utils.scrollDown();
		System.out.println("BASKET AMOUT TOTAL: " + BASKET_AMOUNT);
		
		Thread.sleep(3000);
		cartReviewECommerce.clickProceedShipping();
		
	}

	@When("^validate the eCommerce message error$")
	public void validate_the_eCommerce_message_error(DataTable mobile) throws Throwable {

			checkoutECommerce.checkmobileMessageError(mobile);
	}

	@When("^change the eCommerce mobile phone$")
	public void change_the_mobile_phone(DataTable mobile) throws Throwable {

		checkoutECommerce.changeMobilePhone(mobile);

	}

}