package stepDefinitions;


import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.selfcare.HomePageSelfcare;
import pageObjects.subscription.PaymentPageSubscription;


public class SelfcareSteps {

	private GeneralSteps general;
	WebDriver driver;
	public WebDriverWait wait;
	private static HomePageSelfcare selfcare;
	public String tenant = "";


	public SelfcareSteps(GeneralSteps general) throws MalformedURLException {

		this.general=general;
		driver = this.general.getDriver();
		wait = this.general.getWait();
		selfcare = PageFactory.initElements(driver, HomePageSelfcare.class);
		tenant = general.getTenant();

	}
		
	@Given("^enter a valid username \"([^\"]*)\" and password$")
	public void enter_a_valid_username_and_password(String arg1) throws Throwable {

		selfcare.insertUsername(arg1, "Oney1testes");
		selfcare.clickLoginSelfcare();
		Thread.sleep(1000);

	}
	
	@Given("^click on My Account menu$")
	public void click_on_My_Account_menu() throws Throwable {

		selfcare.validateMyAccount();
		selfcare.clickMyAccount();
		Thread.sleep(500);
	}

	@Given("^click on Email$")
	public void click_on_Email() throws Throwable {

		selfcare.clickEmail();
		Thread.sleep(500);
		selfcare.validateEmail();
	}

	@Given("^click on Password$")
	public void click_on_Password() throws Throwable {

		selfcare.clickPassword();
		Thread.sleep(500);
		selfcare.validatePassword();
	}

	@Given("^click on Payment Info$")
	public void click_on_Payment_Info() throws Throwable {

		selfcare.clickPaymentInfo();
		Thread.sleep(500);
		selfcare.validatePaymentInfo();
		Thread.sleep(800);
	}

	@Given("^click on Documents$")
	public void click_on_Documents() throws Throwable {
		selfcare.clickDocuments();
		Thread.sleep(500);
		selfcare.validateDocuments();
	}

	@Given("^click on Phone Number$")
	public void click_on_Phone_Number() throws Throwable {

		selfcare.clickPhoneNumber();
		Thread.sleep(500);
		selfcare.validatePhoneNumber();
	}

	@Given("^click on Personal info$")
	public void click_on_Personal_info() throws Throwable {

		selfcare.clickPersonalInfo();

		if (driver.getCurrentUrl().contains("3x4x.oney.pt")) {

			Thread.sleep(500);
			selfcare.validatePersonalInfoPT();
		}

		else if (driver.getCurrentUrl().contains("oney.es")) {

			Thread.sleep(500);
			selfcare.validatePersonalInfoES();
		}

		else {

			Thread.sleep(500);
			selfcare.validatePersonalInfo();
		}

	}

	@Given("^click on Preferences$")
	public void click_on_Preferences() throws Throwable {

		selfcare.clickPreferences();
		Thread.sleep(500);
		selfcare.validatePreferences();
	}

	@Given("^click on Delete Account$")
	public void click_on_Delete_Account() throws Throwable {

		selfcare.clickDeleteAccount();
		Thread.sleep(500);
		selfcare.validateDeleteAccount();
	}

	@Given("^click on Past contracts$")
	public void click_on_Past_contracts() throws Throwable {

		selfcare.validateGreeting();
		selfcare.clickPastContracts();
	}

	@Given("^click on contract details on a past contract$")
	public void click_on_contract_details() throws Throwable {

		selfcare.clickContractDetailsPast();
		selfcare.validateInstallmentsPlan();

	}

	@When("^download the \"([^\"]*)\" consent file$")
	public void download_file(String consent) throws Throwable {

		selfcare.downloadFileConsent(consent);
	}

	@Given("^click on Home link$")
	public void click_on_Home_link() throws Throwable {

		selfcare.clickHomeLink();

	}

	@Given("^click on contract details on an active contract$")
	public void click_on_contract_details_on_an_active_contract() throws Throwable {

		selfcare.clickContractDetailsActive();
		Thread.sleep(500);
		selfcare.validateInstallmentsPlan();
	}

	@Given("^change email to \"([^\"]*)\"$")
	public void change_email_from_to( String newEmail) throws Throwable {

		selfcare.changeEmail(PaymentPageSubscription.RANDOM_EMAIL, newEmail);
		selfcare.insertPasswordSelfcare();
		selfcare.clickSaveEmail();
	}

	@Given("^change password from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void change_password_from_to(String currentPassword, String newPassword) throws Throwable {

		selfcare.changePassword(currentPassword, newPassword);
		selfcare.clickSavePassword();
		Thread.sleep(1000);
	}

	@Given("^enter the new password of \"([^\"]*)\"$")
	public void enter_the_new_password(String username) throws Throwable {
		Thread.sleep(3500);
		selfcare.insertUsername(username, "Oney2testes");
		selfcare.clickLoginSelfcare();
	}

	@And("^enter a newly created username and password$")
	public void enter_a_newly_created_username_and_password() throws Throwable {

		selfcare.insertNewUsernameAndPassword();
		selfcare.clickLoginSelfcare();
	}

	@And("^confirm the account is deleted$")
	public void confirm_the_account_is_deleted() throws Throwable {

		selfcare.confirmDeleteAccount();
		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));
		selfcare.clickYesDeleteAccount();
	}
	
	@Given("^enter the new username$")
	public void enter_the_new_username() throws Throwable {

		selfcare.getEmailJson();
		Thread.sleep(1000);
		selfcare.clickLoginSelfcare();
	}

	@Given("^validate Active Contracts table is empty for On Going status$")
	public void validate_Active_Contracts_table_is_empty_for_On_Going_status() throws Throwable {

		selfcare.validateActiveContractsIsEmpty();
	}

	@Given("^validate Past Contracts is empty for On Goins status$")
	public void validate_Past_Contracts_is_empty_for_On_Goins_status() throws Throwable {

		selfcare.clickPastContracts();
		selfcare.validatePastContractsIsEmpty();

	}

	@Given("^login in customer page$")
	public void login_in_customer_page() throws Throwable {

		selfcare.insertDataNewCustomer();
//			selfcare.insertPasswordSelfcare();

	}

	@Then("^do you want to change the payment method to other contracts \"([^\"]*)\"$")
	public void change_payment_method_other_contracts(String arg1) throws Throwable {

		if (arg1.equalsIgnoreCase("Yes")) {
			selfcare.clickYesModifyCard();
			selfcare.selectCardOnTheList();
			selfcare.clickModifyCardOnContract();
			selfcare.clickOnClose();
		} else {
			selfcare.clickOnClose();
		}
	}

	@When("^click on contract detail$")
	public void click_on_contract_detail() throws Throwable {

		selfcare.clickContractDetail();
	}

	@When("^click on Add a card$")
	public void click_on_Add_card() throws Throwable {

		selfcare.clickRemoveCard();
		Thread.sleep(500);
		selfcare.clickPaymentInfo();
		Thread.sleep(500);
		selfcare.clickAddCard();
		Thread.sleep(1000);
	}

	@Then("^click on remove a card$")
	public void click_on_remove_card() throws Throwable {
		selfcare.clickRemoveCard();
		Thread.sleep(500);
	}

	// Faz login para contas multicapture criadas pela API
	@Given("^enter a valid username and password for multicapture$")
	public void enter_a_valid_username_and_password_for_multicapture() throws Throwable {

		String url = driver.getCurrentUrl();

		switch (url) {

		case "https://qalogin.oney.es/Customer/":
			selfcare.insertUsernameAndPasswordSelfcarePR36MC();
			selfcare.clickLoginSelfcare();
			break;

		}
	}

	// Faz login para contas marketplace criadas pela API
	@Given("^enter a valid username and password for marketplace$")
	public void enter_a_valid_username_and_password_for_marketplace() throws Throwable {

		String url = driver.getCurrentUrl();

		switch (url) {

		case "https://qalogin.oney.es/Customer/":
			selfcare.insertUsernameAndPasswordSelfcarePR36MP();
			selfcare.clickLoginSelfcare();
			break;

		}
	}

	// clica no icone cancelamento pr36
	@Given("^click on thumbsdown icon to validate new icon$")
	public void click_on_thumbsdown_icon_to_validate_new_icon() throws Throwable {

		selfcare.clickValidateCancelledIcon();

	}

	// verifica os nomes dos 5 items multicapture
	@Given("^validate if all the items are available inside contracts details$")
	public void validate_if_all_the_items_are_available_inside_contracts_details() throws Throwable {

		selfcare.clickItemBenjamin();
		selfcare.clickItemCariblanco();
		selfcare.clickItemCarm();
		selfcare.clickItemProsecco();
		selfcare.clickItemAstica();

	}

	@Given("^validate if all items are available for marketplace contracts inside contracts details$")
	public void validate_if_all_items_are_available_for_marketplace_contracts_inside_contracts_details()
			throws Throwable {

		selfcare.clickItemBenjamin();
		selfcare.clickItemCarm();
		selfcare.clickItemProsecco();
		selfcare.clickItemAstica();
		selfcare.clickItemGreenock();
		selfcare.clickItemPesquera();
		selfcare.clickItemChateau();

	}

	// sai do selfcare
	@Then("^log out from selfcare$")
	public void log_out_from_selfcare() throws Throwable {

		Thread.sleep(1000);
		selfcare.clickLogoutSelfcare();

	}

	// valida se existe marketplace chamado 'basket'
	@Given("^check if marketplace merchant name is available$")
	public void check_if_marketplace_merchant_name_is_available() throws Throwable {

		selfcare.clickMarketplaceName();

	}
	
//	@Then("^validate if \"([^\"]*)\" consent on the file$")
//	public void validate_the_consent(String consent) throws Throwable {
//
//		selfcare.validateConsent(consent);
//	}
}