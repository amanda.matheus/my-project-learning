package stepDefinitions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.accounting.Accounting;
import helper.Utils;
import io.github.millij.poi.SpreadsheetReadException;
import pageObjects.backOffice.HomePageBackOffice;


public class AccountingSteps {

	private GeneralSteps general;
	WebDriver driver;
	public WebDriverWait wait;
	private static Utils utils;
	private static utils.Utils utilsJava;
	private static HomePageBackOffice backOffice;
	private static Accounting accounting;
	public String tenant = "";
	public String url;


	public AccountingSteps(GeneralSteps general)  throws MalformedURLException {

		this.general=general;
		driver = this.general.getDriver();
		wait = this.general.getWait();
		utils = PageFactory.initElements(driver, Utils.class);
		backOffice = PageFactory.initElements(driver, HomePageBackOffice.class);
		accounting = PageFactory.initElements(driver, Accounting.class);
		tenant = general.getTenant();
		url = "";

	}
		
	@Then("^validate the data of the eventType \"([^\"]*)\"$")
	public void validate_the_data_eventType(String eventType) throws Throwable {

	}
	
	@Then("^validate if the eventType \"([^\"]*)\" was generated after the \"([^\"]*)\"$")
	public void validate_if_the_eventType_generated_after_the_action(String eventType, String action) throws Throwable {

		if(eventType.equals("02")|| eventType.equals("13") || eventType.equals("21")) {
			accounting.validateGenerationEventTypeByAction(eventType, backOffice.getActionContractPath(tenant,2), action , tenant);
		} else if(eventType.equals("20")|| eventType.equals("22") || eventType.equals("24")|| eventType.equals("26")|| eventType.equals("07")) {
			accounting.validateGenerationEventTypeByAction(eventType, backOffice.getActionContractPath(tenant,3), action , tenant);
		} else {
			System.out.println("hello");
			accounting.validateGenerationEventTypeByAction(eventType, backOffice.getActionContractPath(tenant,1), action , tenant);
		}

	}
	
	@Then("^validate if the eventType \"([^\"]*)\" was generated daily$")
	public void validate_if_the_eventType_generated_daily(String eventType) throws Throwable {

		
		accounting.validateGenerationEventType(eventType, tenant);
	
	}
	
	@Then("^validate the errors of \"([^\"]*)\" days$")
	public void validate_the_eventType_error(int days) throws Throwable {

//		accounting.removeRowErrorFile("C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\error\\20211006_BE_Error.xlsx","01", "T900039003");
		accounting.validateGenerationError(backOffice.getErrorFiletPath(tenant,days), tenant);

	}
	
	@Then("^download the accounting files and validate the errors file of the week$")
	public void validate_the_error_file_of_the_week() throws Throwable {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String dateBegin = utils.getLastWeekBegin().minusDays(4).format(formatter);
		System.out.println(dateBegin);
		String dateEnd = LocalDateTime.now().format(formatter); //utils.getLastWeekEnd()
		
		int count = backOffice.searchAccountingFile(dateBegin,dateEnd);
		System.out.println(count + " files accounting");
		for (int i = 0; i < count; i++) {
			backOffice.downloadAccountingSpecificFile(i);
			Thread.sleep(700);
			accounting.validateGenerationError(backOffice.getErrorFiletPathOfTheWeek(tenant), tenant);
		}

	}
	
	@Then("^validate all \"([^\"]*)\" data$")
	public void validate_all_data(String action) throws Throwable {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String dateBegin = utils.getLastWeekBegin().minusDays(4).format(formatter);
		String dateEnd = LocalDateTime.now().format(formatter); //utils.getLastWeekEnd()
		
		int count = backOffice.searchAccountingFile(dateBegin,dateEnd);
		System.out.println(count + " files accounting");
		for (int i = 0; i < count; i++) {
			backOffice.downloadAccountingSpecificFile(i);
			Thread.sleep(700);
			switch (action) {
			case "ContractCreated":
				System.out.println(HomePageBackOffice.fileName);
				accounting.validateDataEventTypeByAction("01", backOffice.getActionContractPath(tenant,1),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,  
						action, tenant);
				accounting.validateDataEventTypeByAction("14", backOffice.getActionContractPath(tenant,1),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,  
						action, tenant);
				accounting.validateDataEventTypeByAction("03", backOffice.getActionContractPath(tenant,1),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,  
						action, tenant);
				accounting.validateDataEventTypeByAction("19", backOffice.getActionContractPath(tenant,1),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,  
						action, tenant);
				accounting.validateDataEventTypeByAction("20", backOffice.getActionContractPath(tenant,1),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,  
						action, tenant);
				break;				
			case "EarlySettlement":
				accounting.validateDataEventTypeByAction("05", backOffice.getActionContractPath(tenant,1),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,
						action, tenant);
				accounting.validateDataEventTypeByAction("07", backOffice.getActionContractPath(tenant,2),
						"C:\\Users\\Amanda\\git\\oney-ablewise-chrome\\Documents\\Accounting\\dailyFile\\" + HomePageBackOffice.fileName,
						action, tenant);
				break;	
			default:
				System.out.println("Please insert correct action");
		}	
		}

	}
	
	@Then("^validate if the eventType \"([^\"]*)\" was generated$")
	public void validate_if_the_eventType_generated(String eventType) throws Throwable {
		
			accounting.validateGenerationEventTypeOfTheWeek(eventType, tenant);
			
	}
	
	@Then("^validate the fields of all eventTypes \"([^\"]*)\"$")
	public void validate_the_fields_of_eventType(String eventType) throws Throwable {

		accounting.validateFieldsEventType(eventType, tenant);

	}

	@When("^open the file$")
	public void open_the_file() throws Throwable {
	    
	}
	
	@Then("delete the \"([^\"]*)\" files$")
	public void delete_the_files(String method) throws Throwable {
				
		for (int i=0;i < HomePageBackOffice.countGeneral;i++) {
		
			LocalDateTime now = LocalDateTime.now();
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String today = now.format(formatter);

			String filename = method + "_" + today + "_Request" + i + ".txt";
			System.out.println(filename);
		
			utilsJava.deleteFile(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\MFS\\"+filename);
		}
	}
	
	@Then("^create an unique error file for week$")
	public void create_an_unique_error_file_for_week() throws Throwable {
	    
		accounting.createWeekErrorFile(tenant);
	}
	
	@Then("verify the response body")
	public void verify_the_response_body() throws InterruptedException, FileNotFoundException, SpreadsheetReadException, IOException {

		accounting.createWeekErrorFile(tenant);
	}

}