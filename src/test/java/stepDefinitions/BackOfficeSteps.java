package stepDefinitions;



import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.excelFile.ExcelObject;
import helper.Utils;
import io.github.millij.poi.SpreadsheetReadException;
import io.github.millij.poi.ss.reader.XlsxReader;
import pageObjects.backOffice.HomePageBackOffice;
import pageObjects.eCommerce.CheckoutECommerce;
import pageObjects.subscription.PaymentPageSubscription;


public class BackOfficeSteps {

	private GeneralSteps general;
	WebDriver driver;
	public WebDriverWait wait;
	public static String actionFile = null;
	private static Utils utils;
	private static HomePageBackOffice backOffice;
	public String externalRef = null;
	public String url;
	public String tenant = "";
	public static String BASKET_AMOUNT = null;
	public static String btsType;


	public BackOfficeSteps(GeneralSteps general)  throws MalformedURLException {

		this.general=general;
		driver = this.general.getDriver();
		wait = this.general.getWait();
		utils = PageFactory.initElements(driver, Utils.class);
		backOffice = PageFactory.initElements(driver, HomePageBackOffice.class);
		tenant = general.getTenant();
		url = "";

	}
	
	
	/*
	 * Abre os links do SoftCollection. Todos os países.
	 */
	@Given("^navigate to \"([^\"]*)\" test payment page$")
	public void navigate_to_test_payment_page(String testPage) throws Throwable {

		switch (testPage) {

		case "Spain":
			driver.navigate().to("https://qalogin.oney.es/SoftCollections_Test/SoftColletions_Test.aspx");
			break;

		case "Belgium":
			driver.navigate().to("https://qalogin.oney.be/SoftCollections_Test/SoftColletions_Test.aspx");
			break;

		case "Italy":
			driver.navigate().to("https://qalogin.oney.it/SoftCollections_Test/SoftColletions_Test.aspx");
			break;

		case "Portugal":
			driver.navigate().to("https://qa3x4x.oney.pt/SoftCollections_Test/SoftColletions_Test.aspx");
			break;

		case "Romania":
			driver.navigate().to("https://qalogin.oney.ro/SoftCollections_Test/SoftColletions_Test.aspx");
			break;
		}
	}

	/**
	 * We have here ACCOUNTING method to write all contracts in Soft Collections
	 * 
	 */
	@Given("^charge the next installment and the payment returns as refused for \"([^\"]*)\" reason$")
	public void unpaid_all_installment(String reason) throws Throwable {

		if (reason.equalsIgnoreCase("Technical")) {

			backOffice.payNextInstallmentPlanned("10,14");

		} else if (reason.equalsIgnoreCase("Non-Technical")) {

			backOffice.payNextInstallmentPlanned("10,51");
		}
		
	}
	
	
	@When("^click on the Contracts menu$")
	public void click_on_the_Contracts_menu() throws Throwable {

		backOffice.clickContractsMenu();
	}
	
	@When("^write all \"([^\"]*)\" details in the file$")
	public void write_all_contract_details_in_the_file(String action) throws Throwable {

		backOffice.writeContractDetails(tenant, action);
	}
	
	@When("^write all \"([^\"]*)\" details$")
	public void write_all_contract_details(String action) throws Throwable {

		backOffice.writeDetails(tenant, action);
	}

	@Then("^validate the Pay Later contracts$")
	public void validate_the_Pay_Later_contracts() throws Throwable {

		backOffice.validatePayLaterContracts();
	}

	@When("^click on the Audit menu$")
	public void click_on_the_Audit_menu() throws Throwable {

		backOffice.clickAuditMenu();
	}
	
	@When("^click on the API Logs submenu$")
	public void click_on_the_API_Logs_submenu() throws Throwable {

		backOffice.clickAPILogsSubmenu();
	}

	@When("^click on the User Actions BO Logs menu$")
	public void click_on_the_User_Actions_BO_Logs_menu() throws Throwable {

		backOffice.clickUserActionsBOLogsMenu();
		Thread.sleep(500);
	}

	@Then("^validate the IBAN Validation Log$")
	public void validate_IBAN_Validation_log_day() throws Throwable {

		backOffice.validateIBANValidationLogDay();
	}

	@When("^click on the sell secure log submenu$")
	public void click_on_the_sell_secure_log_submenu() throws Throwable {

		backOffice.clicksellSecureSubmenu();
		utils.waitForPageToLoad();
	}

	@When("^paste contract id on search bar$")
	public void paste_contract_id_on_search_bar() throws Throwable {

		backOffice.clickSellSecureSearchBar();
	}

	@When("^click on search button$")
	public void click_on_search_button() throws Throwable {

		backOffice.clickSearchButtonSellSecureLogs();
	}

	@When("^click on the Soft Collections submenu$")
	public void click_on_the_Soft_Ccollections_submenu() throws Throwable {

		backOffice.clickSoftCollectionsSubmenu();
	}

	@When("^click on SFTP Files Report Logs submenu$")
	public void click_on_SFTP_Files_Report_Log_submenu() throws Throwable {

		backOffice.clickSFTPFilesReportLogsSubmenu();
	}

	@When("^search for Soft Collection process by contract$")
	public void search_for_Soft_Collection_process_by_contract() throws Throwable {

		backOffice.searchSoftcollectionByContract();
	}

	@When("^search the log by screenboid \"([^\"]*)\"$")
	public void search_log_screenboid(String log) throws Throwable {

		backOffice.searchScreenBOID(log);
	}

	@When("^validate if the Soft Collection status is \"([^\"]*)\"$")
	public void validate_the_Soft_Collection_status(String status) throws Throwable {

		backOffice.validateStatusSoftCollection(status);
	}

	@When("^validate if the delay amount is \"([^\"]*)\"$")
	public void validate_delay_amount(String amount) throws Throwable {

		backOffice.validateDelayAmount(amount);
		
		
	}

	@Given("^change the strategy to \"([^\"]*)\"$")
	public void change_the_strategy(String reason) throws Throwable {

		if (reason.equalsIgnoreCase("Technical")) {

			Thread.sleep(1000);
			backOffice.changeStrategy("10,14");
			Thread.sleep(1000);

		} else if (reason.equalsIgnoreCase("Non-Technical")) {

			Thread.sleep(1000);
			backOffice.changeStrategy("10,51");
			Thread.sleep(1000);
		}
	}

	@Given("^charge to complete the process$")
	public void charge_to_complete_process() throws Throwable {

		Thread.sleep(1000);
		backOffice.chargeCompleteProcess();
		Thread.sleep(1000);
		actionFile = "SoftPaid";
		backOffice.writeActionsToContract(tenant, "SoftPaid");
	}

	@When("^validate if the installment status is \"([^\"]*)\"$")
	public void validate_installment_status(String status) throws Throwable {

		Thread.sleep(5000);
		backOffice.validateInstallmentStatus(status);
		Thread.sleep(5000);
		if (status.contains("Unpaid")) {
			backOffice.writeActionsToContract(tenant, "Soft");
		}else if (status.contains("Hard Collection")) {
			backOffice.writeActionsToContract(tenant, "Hard");
		}
	}

	@When("^validate the strategy, customer classification and the action$")
	public void validate_strategy_customerClassification_action(DataTable arg1) throws Throwable {

		backOffice.clickIdSoftcollection();
		backOffice.validateStrategy(arg1);
		backOffice.clickStrategy();
		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

		backOffice.validateClassificationAction(arg1);

		Thread.sleep(1000);
		driver.switchTo().defaultContent();
	}

	@When("^click on the Collections menu$")
	public void click_on_the_Collections_menu() throws Throwable {

		backOffice.clickCollectionsMenu();
	}

	@Given("^search for an contract created$")
	public void search_for_an_on_going_contract_customerx_created_days_ago(DataTable arg1) throws Throwable {

		backOffice.searchOnGoingContractXdaysAmountXCustomerX(arg1);
		utils.waitForPageToLoad();
	}
	
	@Given("get external reference")
	public void get_external_reference() throws Throwable {
		
		backOffice.getExternalreference();
	}
	
	@Given("^finalize the GDPR contracts created$")
	public void finalize_the_GDPR_contracts_created(DataTable arg1) throws Throwable {

		backOffice.searchOnGoingContractXdaysAmountXCustomerX(arg1);
		utils.waitForPageToLoad();
		backOffice.getNumberOfContracts(arg1);
	}

	@When("^click on the Customers menu$")
	public void click_on_the_Customers_tab() throws Throwable {

		backOffice.clickCustomersMenu();
//		Thread.sleep(2000);

		if (driver.getCurrentUrl().contains("pplogin")) {

			backOffice.clickCustomersSubMenu();
		}
	}

	@When("^click on the Merchants menu$")
	public void click_on_the_Merchants_Menu() throws Throwable {

		backOffice.clickMerchantsMenu();
	}
	
	@When("^click on the customer name$")
	public void click_on_the_customer_name() throws Throwable {
	
		backOffice.clickContractName();
	}

	@When("^verify the merchant \"([^\"]*)\"$")
	public void verify_the_Merchant(String merchant) throws Throwable {

		click_on_the_Accounting_menu();
		click_on_the_IBAN_Validation_submenu();
		backOffice.existIBAN(merchant);		
//		Thread.sleep(1000);
	}
	
	@When("^search for the Merchant \"([^\"]*)\"$")
	public void search_the_Merchant(String merchant) throws Throwable {

		backOffice.searchMerchantByName(merchant);
		Thread.sleep(5000);
	}
	
	@When("click on Merchant details")
	public void click_on_Merchant_details()throws Throwable {

		backOffice.accessMerchantDetails();

	}
	
	
	@When("validate Merchant category options")
	public void validate_Merchant_category_options()throws Throwable {

		backOffice.validateMerchantCategoryOptions();

	}
	@When("click on files tab")
	public void click_on_files_tab()throws Throwable {

		backOffice.clickonFilesTab();

	}
	
	@When("validate if merchant files are being generated")
	public void validate_if_merchant_files_are_being_generated()throws Throwable {

		backOffice.validateMerchantFiles();

	}
	
	@When("click on the edit PSP")
	public void click_on_the_edit_PSP()throws Throwable {

		backOffice.clickPSPconfEditing();

	}
	
	@When("validate if CRP files are being generated")
	public void validate_if_CRP_files_are_being_generated()throws Throwable {

		backOffice.validateCRPfiles();

	}
		
	@When("navigate to CRP configurations tab")
	public void navigate_to_CRP_configurations_tab()throws Throwable {

		backOffice.accesscrpConfigurationsDetails();

	}
	
	@When("validate Psp and Merchant buttons")
	public void validate_Psp_and_Merchant_buttons()throws Throwable {

		backOffice.validateMerchantAndPSPButtons();

	}

	@When("^validate if the merchant is \"([^\"]*)\"$")
	public void validate_the_Merchant(String status) throws Throwable {

		backOffice.validateMerchantStatus(status);
	}

	@When("^\"([^\"]*)\" the IBAN validation$")
	public void validate_the_IBAN(String approval) throws Throwable {

		backOffice.validateIBAN(approval);
		Thread.sleep(8000);
	}

	@Given("^click on the Configurations menu$")
	public void click_on_the_Configurations_menu() throws Throwable {
		
		backOffice.clickConfigurationsMenu();
	}
	
	@When("^click on the Right to be Forgotten submenu$")
	public void click_on_the_Right_to_be_Forgotten_submenu() throws Throwable {

		backOffice.clickRightToBeForgottenSubmenu();
	}
	
	@Then("^include rule$")
	public void include_rule(DataTable rule) throws Throwable {

		backOffice.clickRightForgottenSection(rule);
	}

	@Given("^click on the \"([^\"]*)\" tab$")
	public void click_on_the_tab(String tab) throws Throwable {

		backOffice.clickOnTheTab(tab);
		Thread.sleep(2000);
	}

	@Given("^click on the \"([^\"]*)\" section$")
	public void click_on_the_section(String section) throws Throwable {

		backOffice.clickOnTheSection(section);
	}

	@Given("^unmark all consents and save$")
	public void unmark_all_consents_and_save() throws Throwable {

		backOffice.unmarkConsentsAndSave();
	}

	@Then("^validate the business transactions$")
	public void validate_the_business_transaction() throws Throwable {

		backOffice.validateBusinessTransaction();
	}

	@Given("^click on the Finance submenu$")
	public void click_on_the_Finance_submenu() throws Throwable {

		backOffice.clickFinanceSubmenu();
	}

	@Given("^click on the PSP List$")
	public void click_on_the_PSP_List() throws Throwable {

		backOffice.clickPSPList();
	}

	@When("^click on the Checkout PSP$")
	public void click_on_the_Checkout_PSP() throws Throwable {

		backOffice.clickCheckoutPSP();
	}

	@When("^open the Altex Files$")
	public void open_the_Altex_Files() throws Throwable {

		backOffice.clickAltexFiles();
	}
	
	@When("^download the day file$")
	public void download_the_day_file() throws Throwable {

		backOffice.downloadAccountingFile();
		Thread.sleep(700);
	}
	
	@When("^get the name of the day file$")
	public void get_name_the_day_file() throws Throwable {

		backOffice.getNameAccountingFile();
		Thread.sleep(700);
	}
	
	@Then("^validate the Altex File was generated correctly$")
	public void validate_the_Altex_File_was_generated_correctly() throws Throwable {

		backOffice.validateLastAltexFileGenerated();
		backOffice.downloadAltexFile();
		backOffice.readTxtFile();
	}

	@Given("^click on the edit CRP Merchant$")
	public void click_on_the_edit_CRP_Merchant() throws Throwable {

		backOffice.clickMerchantPSPList();
	}

	@Given("^click on CRP Files and download the latest file$")
	public void click_on_CRP_Files() throws Throwable {

		backOffice.clickCRPFiles();
		backOffice.downloadCRPFile();
	}

	@Given("^validate the CRP File$")
	public void validate_the_CRP_File() throws Throwable {

		backOffice.readCSVFile();

	}

	@When("^click on MC & MP Configuration$")
	public void click_on_MC_MP_Configuration() throws Throwable {

		backOffice.clickMCMPTab();
	}

	@When("^open Multicapture Configuration tab$")
	public void open_Multi_capture_Configuration_tab() throws Throwable {

		backOffice.clickMulticaptureTab();
	}

	@Then("^validate the Multicapture configuration$")
	public void validate_the_Multi_capture_configuration() throws Throwable {

		backOffice.clickAD_RTD();
		Thread.sleep(1000);
		backOffice.clickTSPI();
	}

	@Given("^click on Phone Numbers$")
	public void click_on_Phone_Numbers() throws Throwable {

		backOffice.clickPhoneNumbersTab();
	}

	@Given("^validate the regrex expression matches the \"([^\"]*)\" string$")
	public void validate_the_regrex_expression_matches_the_string(String regex) throws Throwable {

		backOffice.validatePhoneNumberRegex(regex);
	}

	@When("^click on the Brands menu$")
	public void click_on_the_Brands_menu() throws Throwable {

		backOffice.clickBrandsMenu();
	}

	@When("^click on Payment Retry$")
	public void click_on_Payment_Retry() throws Throwable {

		backOffice.clickPaymentRetry();
		actionFile="SoftPaid";
	}

	@When("^click on New Brand$")
	public void click_on_New_Brand() throws Throwable {

		backOffice.clickNewBrand();
	}
	
	@When("import \"([^\"]*)\" file")
	public void import_Brand_files(String file) throws Throwable {

		backOffice.uploadFileImport(file);
		
	}

	@When("^click on New Merchant$")
	public void click_on_New_Merchant() throws Throwable {

		backOffice.clickNewMerchant();
	}

	@Then("^validate the Multi-capture and Marketplace flags$")
	public void validate_the_rule_descripted_in_this_scenario() throws Throwable {

		backOffice.validateMulticaptureMarketplace();
	}

	@When("^click on the edit button$")
	public void click_on_the_edit_contract_button() throws Throwable {

		Thread.sleep(35000);
		backOffice.clickEditButton();
	}

	@Given("^change IBAN and BIC information$")
	public void change_IBAN_BIC_information() throws Throwable {

		Thread.sleep(10000);
		backOffice.changeIBANBIC();
	}

	@When("^click on Items List tab$")
	public void click_on_items_list() throws Throwable {

		backOffice.clickItemListTab();
	}

	@When("^click on Items details icon$")
	public void click_on_Items_details_icon() throws Throwable {

		backOffice.clickItemListTabItemsDetails();
	}

	@When("^validate Items Details Content$")
	public void validate_Items_Details_Content() throws Throwable {

		backOffice.validateItemsDetailsContent();
	}

	@When("^validate the travel service table$")
	public void validate_the_travel_service_table(DataTable arg1) throws Throwable {

		utils.waitForPageToLoad();
		backOffice.checkCustomerScoringTable(arg1);
	}

	@When("^click on Contract Transactions tab$")
	public void click_on_Contract_Transactions() throws Throwable {

		Thread.sleep(20000);
		backOffice.clickContractTransactionsTab();
		Thread.sleep(3000);
	}

	@When("^click on Brand Info tab$")
	public void click_on_Brand_Info() throws Throwable {

		backOffice.clickBrandInfo();
	}

	@When("^get FPClient Code$")
	public void get_FPCliente_code() throws Throwable {

		backOffice.getFPClientCode();
	}

	@When("^validate if the UX is \"([^\"]*)\"$")
	public void validade_if_ux(String ux) throws Throwable {

		backOffice.validateUX(ux);
	}

	@When("^click on Scoring tab$")
	public void click_scoring_tab() throws Throwable {

		backOffice.clickScoringTab();
	}

	@Then("^validate the score$")
	public void validate_score() throws Throwable {

		backOffice.validateScore();
	}

	@When("^click on Actions to Contract$")
	public void click_on_Actions_to_Contract() throws Throwable {

		backOffice.clickActionsToContract();
		backOffice.getContractNumber();
	}
	
	@When("^click on Actions to Customer$")
	public void click_on_Actions_to_Customer() throws Throwable {

		backOffice.clickActionsToCustomer();
	}
	
	@When("^click on right to be forgotten button$")
	public void click_on_right_to_be_forgotten_button() throws Throwable {

		backOffice.clickRightForgotten();
	}
	
	@When("^confirm the right to be forgotten$")
	public void confirm_right_to_be_forgotten() throws Throwable {

		backOffice.confirmRightForgotten();
	}

	/**
	 * We have here ACCOUNTING method to write all contracts with early settlement
	 * 
	 */
	@When("^perform a \"([^\"]*)\" early settlement$")
	public void perform_an_early_settlement(String type) throws Throwable {

		backOffice.clickEarlySettlement();

		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

		backOffice.confirmEarlySettlement(type);
		

		Thread.sleep(50000);
		actionFile = "EarlySettlement";
		driver.switchTo().defaultContent();
	}
	
	@When("refusing an early settlement")
	public void refusing_an_early_settlement() throws Throwable {

		backOffice.clickEarlySettlement();

		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

		backOffice.refusingEarlySettlement();

		Thread.sleep(50000);
	}
	
	/**
	 * We have here ACCOUNTING method to write all contracts cancelled
	 * 
	 */
	@When("^perform a total cancel$")
	public void perform_a_total_cancel() throws Throwable {

		backOffice.clickCancelButton();

		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

//		backOffice.getItemsLastPrice();
//		backOffice.insertAmountToRefund();
		backOffice.clickCancelAll();
		Thread.sleep(5000);
		backOffice.confirmCancel();
		Thread.sleep(60000);
		actionFile = "CancelTotal";
		driver.switchTo().defaultContent();
		Thread.sleep(20000);
	}
	
	@When("^cancel the item$")
	public void cancel_the_item() throws Throwable {

		backOffice.clickCancelButton();

		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

//		backOffice.getItemsLastPrice();
//		backOffice.insertAmountToRefund();
		backOffice.cancelItems("1");
		Thread.sleep(5000);
		backOffice.confirmCancel();
		Thread.sleep(60000);
		actionFile = "CancelTotal";
		driver.switchTo().defaultContent();
		Thread.sleep(20000);
	}
	/**
	 * We have here ACCOUNTING method to write all contracts cancelled
	 * 
	 */
	@When("^perform a \"([^\"]*)\" € cancel$")
	public void perform_a_€_cancel(String amountToCancel) throws Throwable {

		backOffice.clickCancelButton();

		driver.switchTo().frame(driver.findElement(
				By.xpath("//div[@class='os-internal-ui-dialog-content os-internal-ui-widget-content']//iframe")));

//		backOffice.insertAmountToRefundTaCompesation(amountToCancel);
		backOffice.cancelPartial(amountToCancel);
		Thread.sleep(5000);
		backOffice.confirmCancel();
		Thread.sleep(60000);
		actionFile= "CancelPartial";
		
		driver.switchTo().defaultContent();
		Thread.sleep(20000);
	}

	@Then("^validate the Contract Transactions status is \"([^\"]*)\"$")
	public void validate_the_Contract_Transactions_status_is(String contractTransactionsStatus) throws Throwable {

		Thread.sleep(15000);
		backOffice.validateStatusContractTransactions(contractTransactionsStatus);

		if (!contractTransactionsStatus.contains("Paid")) {
			if (!actionFile.contains("EarlySettlement")) {
				backOffice.writeActionsToContract(tenant, actionFile);
			}
		} else {
			backOffice.writeActionsToContract(tenant, actionFile);
		}
		
	}

	@Then("^validate the contract status is \"([^\"]*)\"$")
	public void validate_the_contract_status_is(String contractStatus) throws Throwable {

		Thread.sleep(15000);
		backOffice.getContractNumber();
		backOffice.clickContractsMenu();
		backOffice.searchContractNumber();
		backOffice.searchByContractStatus(contractStatus);
		backOffice.checkContractStatus(contractStatus);

		if (contractStatus.equalsIgnoreCase("ONGOING") || contractStatus.equalsIgnoreCase("UNPAID")) {
			backOffice.clickEditButton();
			backOffice.clickActionsToContract();
			perform_an_early_settlement("total");
		}
	}
	
	@Then("^validate if the contract status is \"([^\"]*)\"$")
	public void validate_if_the_contract_status_is(String contractStatus) throws Throwable {

		Thread.sleep(15000);
		backOffice.getContractNumber();
		backOffice.clickContractsMenu();
		backOffice.searchContractNumber();
		backOffice.checkContractStatus(contractStatus);

	}

	@When("^validate the number of planned installments$")
	public void validate_the_number_of_planned_installments() throws Throwable {

		backOffice.validateNumberOfInstallments();
	}

	@Then("^validate if the page contains the correct information$")
	public void validate_if_the_page_contains_the_correct_information() throws Throwable {

		utils.waitForPageToLoad();
		backOffice.validateStringContractPage();
	}

//	@Given("^search for an contract on going status and \"([^\"]*)\" € amount, created (\\d+) days ago$")
//	public void search_for_an_on_going_contract_created_days_ago(String amountToSearch, int days) throws Throwable {
//
//		backOffice.searchOnGoingContractXdays(amountToSearch, days);
//		utils.waitForPageToLoad();
//	}

	@Given("^search for a \"([^\"]*)\" contract and Customer \"([^\"]*)\"$")
	public void search_for_a_contract_and_Customer(String statusToSearch, String customer) throws Throwable {

		backOffice.searchByContractStatus(statusToSearch);
		backOffice.searchContractsByCustomerName(customer);
		utils.waitForPageToLoad();
	}

//	@Given("^search for a contract$")
//	public void search_for_a_contract(DataTable customContractSearch) throws Throwable {
//		
//		backOffice.searchContract(customContractSearch);
//	}


	@Given("^get the Contract Number$")
	public void get_the_Contract_Number() throws Throwable {

		backOffice.getContractNumber();
	}

	@Given("^get the FP Client Code$")
	public void get_the_FP_Client_Code() throws Throwable {

		backOffice.getFPClientCode();
	}

	@When("^search for a customer$")
	public void search_for_a_customer() throws Throwable {

		
		backOffice.searchCustomer();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,200)");
	}

	@Then("^check for the visibility of the status Refund, Change Credit Card and Early Settlement$")
	public void check_for_the_visibility_of_the_status_Refund_Change_Credit_Card_and_Early_Settlement()
			throws Throwable {

		backOffice.checkActionsOnGoing();
	}

	@Then("^check for the visibility of the status Change Credit Card, Early Settlement and Payment Retry$")
	public void check_for_the_visibility_of_the_status_Change_Credit_Card_Early_Settlement_and_Payment_Retry()
			throws Throwable {

		backOffice.checkActionsUnpaid();
	}

	@Then("^click the reset button$")
	public void click_the_reset_button() throws Throwable {

		backOffice.clickReset();
	}

	@When("^return to the previous page by clicking on the Merchants link$")
	public void return_to_the_previous_page_by_clicking_on_the_Merchants_link() throws Throwable {

		backOffice.clickMerchantsLink();
	}

	@When("^return to the previous page by clicking on the Brands link$")
	public void return_to_the_previous_page_by_clicking_on_the_Brands_link() throws Throwable {

		backOffice.clickBrandsLink();
	}

	@When("^click on the view icon of any merchant$")
	public void click_on_the_view_icon_of_any_merchant() throws Throwable {

		backOffice.clickViewButton();
	}

	@When("^click on the view icon of any brand$")
	public void click_on_the_view_icon_of_any_brand() throws Throwable {

		backOffice.clickViewButton();
	}

	@Then("^click on the edit icon of any merchant$")
	public void click_on_the_edit_icon_of_any_merchant() throws Throwable {

		backOffice.clickEditButton();
	}

	@Then("^click on the edit icon of any brand$")
	public void click_on_the_edit_icon_of_any_brand() throws Throwable {

		backOffice.clickEditButton();
	}

	@Then("^validate the OCR Decision$")
	public void validate_the_OCR_Decision(DataTable arg1) throws Throwable {

		backOffice.checkOCRDecision(arg1);
		Thread.sleep(10000);
	}

	@Given("^search for the contract external reference$")
	public void search_for_the_contract_external_reference() throws Throwable {

		backOffice.searchExternalReference();
		backOffice.clickSearch();
	}

	@Given("^check if the status is \"([^\"]*)\"$")
	public void check_if_the_status_is(String contractStatus) throws Throwable {

		backOffice.checkContractStatus(contractStatus);
	}

	@Given("^validate the Total Amount Financed on the Contract Info is \"([^\"]*)\"$")
	public void validate_the_Total_Amount_Financed_on_the_Contract_Info_is(String totalAmountFinanced)
			throws Throwable {

		backOffice.clickContractInfoTab();
		Thread.sleep(2000);
		backOffice.validateTotalAmountFinanced(totalAmountFinanced);
	}
	
	
	
	@Then("^validate the New Amount Financed on the Contract Info is \"([^\"]*)\"$")
	public void validate_the_New_Amount_Financed_on_the_Contract_Info_is(String newAmountFinanced)
			throws Throwable {

		backOffice.clickContractInfoTab();
		Thread.sleep(2000);
		backOffice.validateNewAmountFinanced(newAmountFinanced);
	}

	@Given("^validate the Capital Amount on the Contract Transactions is \"([^\"]*)\"$")
	public void validate_the_Capital_Amount_on_the_Contract_Transactions_is(String capitalAmount) throws Throwable {

		backOffice.clickContractTransactionsTab();
		Thread.sleep(2000);

		backOffice.validateCapitalAmount(capitalAmount);
	}

	@Then("^validate the Total Order Cost and Fees on the Contract Info is \"([^\"]*)\"$")
	public void validate_the_total_order_cost_fee(String amount) throws Throwable {

		backOffice.validateTotalOrderCostFees(amount);
	}

	/**
	 * We have here ACCOUNTING method to write all contracts with paid installments
	 * 
	 */
	@Given("^pay all the installments")
	public void pay_all_the_installments() throws Throwable {

		backOffice.payInstallments("1");
		backOffice.writeActionsToContract(tenant, "Paid");
		
	}

	/**
	 * We have here ACCOUNTING method to write all contracts in Soft Collections
	 * 
	 */
	@Given("^charge all installment and the payment returns as refused") // rever este step
	public void unpaid_all_installment() throws InterruptedException, IOException  {

		backOffice.payInstallments("10,14");
		backOffice.writeActionsToContract(tenant, "Soft");
	}

	@Given("^search for \"([^\"]*)\" contract by number$")
	public void search_for_status_contract_by_number(String statusToSearch) throws Throwable {

		backOffice.clickReset();
		Thread.sleep(1000);
		utils.waitForPageToLoad();
		backOffice.searchContractNumber();
		Thread.sleep(1000);
		backOffice.searchByContractStatus(statusToSearch);
		Thread.sleep(20000);
		utils.waitForPageToLoad();
	}

	@Given("^search contract by number$")
	public void search_contract_by_number() throws Throwable {

		backOffice.clickReset();
		utils.waitForPageToLoad();
		backOffice.searchContractNumber();
		utils.waitForPageToLoad();
		backOffice.clickOnSearch();
	}
	
	@Then("^validade if the contract exist$")
	public void validade_if_the_contract_exist() throws Throwable {

		backOffice.validateSearchResultNoContractsToShow();
	}
	
	@Then("^validate the contract$")
	public void validade_contract() throws Throwable {

		backOffice.validateContract();
	}
	
	@Then("^validade the right to be forgotten message error$")
	public void validade_right_forgotten_message_error() throws Throwable {

		backOffice.validateRightForgottenMessageError();
	}
	
	@Then("^validade if the customer exist$")
	public void validade_if_the_customer_exist() throws Throwable {

		backOffice.validateSearchResultNoCustomersToShow();
	}

	@Given("^search for \"([^\"]*)\" contract by reference$")
	public void search_for_status_contract_by_reference(String statusToSearch) throws Throwable {

		backOffice.clickReset();
		utils.waitForPageToLoad();
		backOffice.searchContractReference(PaymentPageSubscription.CONTRACT_REF);
		utils.waitForPageToLoad();
		backOffice.searchByContractStatus(statusToSearch);
		Thread.sleep(5000);
		backOffice.clickSearch();
		Thread.sleep(7000);
	}

	@Given("^search for \"([^\"]*)\" contract$")
	public void search_for_status_contract(String statusToSearch) throws Throwable {

		backOffice.searchByContractStatus(statusToSearch);
		Thread.sleep(90000);
	}

	@Given("^search for \"([^\"]*)\" success files created today$")
	public void search_for_status_success_files_created_today(String success) throws Throwable {

		backOffice.searchForSFTPToday();
		backOffice.searchbySuccessStatus(success);
	}

	@Given("^validate the \"([^\"]*)\" log message$")
	public void validate_the_log_message(String logMessage) throws Throwable {

		backOffice.validateLogMessage(logMessage);
	}

	@Given("^check if the status is \"([^\"]*)\" after paying the installments$")
	public void check_if_the_status_is_after_paying_the_installments(String finalized) throws Throwable {

		utils.waitForPageToLoad();
		backOffice.checkContractStatus(finalized);
	}

	@When("^click on Decision tab$")
	public void click_on_Decision() throws Throwable {

		Thread.sleep(10000);
		backOffice.clickDecisionTab();
		backOffice.getContractNumber();
		Thread.sleep(10000);
	}

	@Then("^validate if the rejection reason is \"([^\"]*)\"$")
	public void validate_the_rejection_reason(String rejectionReason) throws Throwable {

		utils.waitForPageToLoad();
//		Thread.sleep(10000);
		if (rejectionReason.equalsIgnoreCase("Safewatch")) {
			backOffice.validateRejectionReasonSafewatch(rejectionReason);

		} else if (rejectionReason.equalsIgnoreCase("Expired Card - Pick Up")) {
			backOffice.validateRejectionReasonPSP(rejectionReason);
			
		} else {
			backOffice.validateRejectionReasonEngineDecision(rejectionReason);
		}
	}
	
	@Then("^validate if the rejection type is \"([^\"]*)\"$")
	public void validate_the_rejection_type(String rejectionType) throws Throwable {

			backOffice.validateRejectionType(rejectionType);

	}

	@When("^validate the customer scoring table$")
	public void validate_the_customer_scoring_table(DataTable arg1) throws Throwable {

//		Thread.sleep(10000);
		utils.waitForPageToLoad();
		backOffice.checkCustomerScoringTable(arg1);
	}

	@Then("^validade the customer scoring details$")
	public void validate_the_customer_scoring_details(DataTable arg1) throws Throwable {

		utils.waitForPageToLoad();
		backOffice.checkCustomerScoringDetails(arg1);
	}

	@When("^click on the Engine Decision menu$")
	public void click_on_the_Engine_Decision_menu() throws Throwable {


		
//		Thread.sleep(5000);
		utils.waitForPageToLoad();
		backOffice.clickEngineDecisionMenu();
		utils.waitForPageToLoad();
//		Thread.sleep(5000);
		String currentURL = driver.getCurrentUrl();
		if (currentURL.contains("oney.es")) {
			backOffice.clickDecisionHistorySubmenuES();
		} else {
			backOffice.clickDecisionHistorySubmenu();
		}
		

	}

	@When("^click on the Accounting menu$")
	public void click_on_the_Accounting_menu() throws Throwable {

		backOffice.clickAccountingMenu();
	}
	
	@When("click on the Billing files submenu")
	public void click_on_the_Billing_Files_menu() throws Throwable {

		backOffice.clickBillingFiles();
	}
	
	@When("click on the invoicing file tab")
	public void click_on_the_invoicing_file_tab() throws Throwable {

		backOffice.clickInvoicingFilesTab();
	}
	
	@When("validate if financing file was generated")
	public void validate_if_financing_file_was_generated() throws Throwable {

		backOffice.validateFinancingFileGeneration();
	}
	
	@When("validate if invoicing file was generated")
	public void validate_if_invoicing_file_was_generated() throws Throwable {

		backOffice.validateInvoicingFileGeneration();
	}
	
	@And("^click on the Accounting Files submenu$")
	public void click_on_the_Accounting_Files_submenu() throws Throwable {

		backOffice.clickAccountingFilesSubmenu();
		Thread.sleep(700);
		backOffice.clickAccountingFilesSection();
		Thread.sleep(700);
	}
	
	@And("^click on the JSON Files submenu$")
	public void click_on_the_JSON_Files_submenu() throws Throwable {

		backOffice.clickJSONFilesSubmenu();
		Thread.sleep(700);
		backOffice.clickAccountingFilesSection();
		Thread.sleep(700);
	}

	@When("^click on the IBAN Validation submenu$")
	public void click_on_the_IBAN_Validation_submenu() throws Throwable {

		backOffice.clickIBANValidationSubmenu();
	}

	@When("^search for decision history by contract$")
	public void search_for_decision_history_by_contract() throws Throwable {

//		utils.waitForPageToLoad();
		backOffice.searchDecisionHistoryByContract();
//		utils.waitForPageToLoad();
		backOffice.clickCustomerScoringRseultsTab();
	}

	@When("^check the contract mobile phone$")
	public void check_contract_mobile_phone() throws Throwable {

		backOffice.checkMobilePhone();
	}

	@When("^search for the customer by contract email$")
	public void search_for_the_customer() throws Throwable {

		backOffice.searchCustomerbyEmail(CheckoutECommerce.customerEmail);

	}

	@When("^search for the customer by email \"([^\"]*)\"$")
	public void search_for_the_customer_by_email(String email) throws Throwable {

		backOffice.searchCustomerbyEmail(email);
	}
	
	@When("^search for the customer by email$")
	public void search_for_the_customer_by_email() throws Throwable {

		backOffice.searchCustomerbyEmail(backOffice.getCustomerEmail());
	}

	@Then("^validate the billing address has changed as typed in the subscription page$")
	public void validate_the_billing_address_has_changed_as_typed_in_the_subscription_page() throws Throwable {

		backOffice.validateBillingAddress();
	}
	
	/**
	 * We have here ACCOUNTING method to write all contracts in Hard Collections
	 * 
	 */
	@Given("^put the contract on Hard Collection$")
	public void put_hard_collection() throws Throwable {
		
		Thread.sleep(5000);
		backOffice.getIDSoftcollections();
		Thread.sleep(5000);
		backOffice.clickLinkHardCollection();
		Thread.sleep(5000);
		backOffice.putOnHardCollection();
		Thread.sleep(5000);

		
	}

	@When("^click on the Hard Collections submenu$")
	public void click_on_the_Hard_Collections_submenu() throws Throwable {

		backOffice.clickHardCollectionsSubmenu();
	}

	@When("^click on Marketplace Rulebook submenu$")
	public void click_on_the_Marketplace_Rulebook_submenu() throws Throwable {

		backOffice.clickMarketplaceRulebookSubmenu();
	}

	@When("^click on Import Rulebook$")
	public void click_on_Import_Rulebook() throws Throwable {

		backOffice.clickImportRulebook();
	}

	@Then("^validate that you can import a XML file$")
	public void validate_that_you_can_import_a_XML_file() throws Throwable {

		backOffice.validateImportRulebookMarketplace();
	}

	@When("^click on New Rulebook$")
	public void click_on_New_Rulebook() throws Throwable {

		backOffice.clickNewRulebook();
	}

	@Then("^validate that you can create a new marketplace rulebook$")
	public void validate_that_you_can_create_a_new_marketplace_rulebook() throws Throwable {

		backOffice.validateMarketplaceNewRulebook();
	}

	@When("^search \"([^\"]*)\" Rulebook$")
	public void search_Rulebook(String label) throws Throwable {

		backOffice.searchMarketplaceRulebook(label);
	}

	@Then("^validate that the search \"([^\"]*)\" is showing$")
	public void validate_that_the_search_is_showing(String label) throws Throwable {

		backOffice.validateSearchedLabelMarketplace(label);
	}

	@When("^search for Hard Collection process by contract$")
	public void search_for_Hard_Collection_process_by_contract() throws Throwable {

		backOffice.searchHardcollectionByContract();
	}

	@When("^validate if the Hard Collection status is \"([^\"]*)\"$")
	public void validate_the_Hard_Collection_status(String status) throws Throwable {

		backOffice.validateStatusHardCollection(status);
	}

	@When("^click on the HardCollections Files submenu$")
	public void click_on_the_Hard_Collections_Files_submenu() throws Throwable {

		Thread.sleep(200);
		backOffice.clickHardCollectionFilesSubmenu();
		Thread.sleep(500);
	}

	@When("^click on the Perfomed Collections tab$")
	public void click_on_the_Perfomed_Collections_tab() throws Throwable {

		backOffice.clickPerformedCollectionTab();
		Thread.sleep(500);
		
			
	}

	@When("^click on the Closed Processes tab$")
	public void click_on_the_Closed_Processes_tab() throws Throwable {

		backOffice.clickClosedProcessesTab();
		Thread.sleep(500);
		
		

	}
	
public void generateFileRecolletions(String file) throws IOException {
	
		if (file.contains("Performed")) {

			backOffice.performedCollectionsExcel(tenant);
		
		} else if (file.equalsIgnoreCase("Closed Processes Unpaid")) {
			
			backOffice.closedProcessesExcel(tenant, "Unpaid");
			
		} else if (file.equalsIgnoreCase("Closed Processes")) {
		
			backOffice.closedProcessesExcel(tenant, "Paid");
			
		}
		
		
	}

	/**
	 * We have here ACCOUNTING method to write all contracts in Hard Collections or Litigation
	 * 
	 */
	@When("^upload the file \"([^\"]*)\"$")
	public void upload_file_HardCollection(String file) throws Throwable {
		
		generateFileRecolletions(file);
		Thread.sleep(3000);
		backOffice.uploadFileHardCollection(file, tenant);
		Thread.sleep(8000);
		
		if (file.contains("Unpaid")) {
			backOffice.writeActionsToContract(tenant, "Litigation");
		} else {
			backOffice.writeActionsToContract(tenant, "HardPaid");
		}

	}

	@When("^download the Hard Collection Entries$")
	public void download_hard_collection_entries() throws Throwable {

		Thread.sleep(200);
		backOffice.downloadHardCollectionEntries();
		Thread.sleep(500);
	}

	@Given("^navigate to \"([^\"]*)\" test notification page$")
	public void navigate_to_test_notification_page(String testPage) throws Throwable {

		switch (testPage) {

		case "Spain":
			driver.navigate().to("https://qalogin.oney.es/ContactLab_AUX/Notification.aspx");
			break;

		case "Belgium":
			driver.navigate().to("https://qalogin.oney.be/ContactLab_AUX/Notification.aspx");
			break;

		case "Italy":
			driver.navigate().to("https://qalogin.oney.it/ContactLab_AUX/Notification.aspx");
			break;

		case "Portugal":
			driver.navigate().to("https://qa3x4x.oney.pt/ContactLab_AUX/Notification.aspx");
			break;

		case "Romania":
			driver.navigate().to("https://qalogin.oney.ro/ContactLab_AUX/Notification.aspx");
			break;
		}
		backOffice.getLink();
		backOffice.navigateToNotificationURL();
	}

	@When("^click on Change Credit Card on BackOffice$")
	public void click_change_credit_card(String arg1) throws Throwable {

		backOffice.getContractNumber();
		backOffice.clickChangeCreditCardButton();

	}

	@Then("validate the items list of Travels table on BackOffice$")
	public void validate_items_list_travels_table() throws Throwable {

		backOffice.validateItemsListTravelTable();

	}

	@When("^click on Payment Data tab$")
	public void click_on_payment_data() throws Throwable {

		Thread.sleep(10000);
		backOffice.clickPaymentDataTab();
	}

	@Then("^validate the change credit card request$")
	public void validate_the_change_credit_card_request() throws Throwable {

		backOffice.validateChangeCreditCardRequest();
		click_on_Actions_to_Contract();
		perform_an_early_settlement("total");

	}

	@Then("^click on cookie disclaimer$")
	public void click_cookie_disclaimer() throws InterruptedException {

		backOffice.clickCookieDisclaimer();
	}

	@Given("creating An Excel for \"([^\"]*)\" Import")
	public void creating_An_Excel_for_Import(String file) throws Throwable {
		
		backOffice.createExcelFileImport(file);
	}
			
	@Given("change Merchant Surname")
	public void change_Merchant_Surname() throws Throwable {
		
		backOffice.validateMerchantSurname();
	}
	
	@Given("click on MFS logs submenu")
	public void click_on_MFS_logs_submenu() throws Throwable {
		
		backOffice.clickMFSlogsSubmenu();
	}	
	
	@Given("click on IP logs submenu")
	public void click_on_IP_logs_submenu() throws Throwable {
		
		backOffice.clickIPlogsSubmenu();
	}	
	
	
	@Given("validate notePadInfo")
	public void validate_notePadInfo() throws Throwable {
		
		backOffice.validatenotePadInfo(tenant);
	}
	
	@Given("download Mfs Logs Excel File")
	public void download_Mfs_Logs_Excel_File() throws Throwable {
		
		backOffice.validatecontractID();
	}
	
	@Given("read Xlsx From Mfs Logs")
	public void read_Xlsx_From_Mfs_Logs() throws Throwable {
		
		backOffice.readXlsxFromMfsLogs(tenant);
	}

	@Then("validate the creation")
	public void validate_the_creation() throws InterruptedException {
		
		backOffice.validateTheCreation();
	}
	
	@Given("click on userprofile Menu and Submenu")
	public void click_on_userprofile_Menu_and_Submenu() throws Throwable {
		
		backOffice.clickonUsersProfilemenu();
	}
	
	@Given("paste the contract and anonymize it")
	public void paste_the_contract_and_anonymize_it() throws Throwable {
		
		backOffice.anonymizeContract();
	}
	
	@Given("delete the contract")
	public void delete_the_contract() throws Throwable {
		
		backOffice.deleteContract();
	}
	
	@Given("check if the contract action successfully done")
	public void check_if_the_contract_action_successfully_done() throws Throwable {
		
		backOffice.validateIfcontractNumberStillAvailable();
	}
		
	@When("write softcollections fees inside the Soft collections accounting file")
	public void write_softcollections_fees_inside_the_Soft_collections_accounting_file() throws Throwable {
	    
		backOffice.addExtraaccountingSoftCollectioninfo(tenant);
		
	}
	
	@When("^search for the method \"([^\"]*)\"$")
	public void search_for_the_method(String method) throws Throwable {
	   
		backOffice.searchByMethod(method, tenant);
	}
	
	@When("^download the MFS file$")
	public void download_the_MFS_file() throws Throwable {
		backOffice.downloadMFSFile(tenant);
	}	
	
	@When("^click on Subscription Page Configs Split submenu$")
	public void click_on_Configuration_and_Subscription_Page_Configs_Split_submenu () throws Throwable {
		backOffice.validateBeforeYouGoSection();
		Thread.sleep(3000);
	}
	
	@When("^click on Subscription Page Configs PL submenu$")
	public void click_on_Configuration_and_Subscription_Page_Configs_PL_submenu () throws Throwable {
		backOffice.validateBeforeYouGoSectionForPL();
		Thread.sleep(3000);
	}
	
	@Then("^validate the Before you go PopIn section$")
	public void validate_the_Before_you_go_Pop_In_section() throws Throwable {
		backOffice.validateBeforeYouGoSectioninfo();
	}
	
	@Then("^check that the toggle button is active$")
	public void check_that_the_toggle_button_is_active() throws Throwable {
		backOffice.checkthatthetogglebuttonisactive();
	}

	@When("^write all \"([^\"]*)\" data$")
	public void getAccountingContractInfo(String action) throws SpreadsheetReadException, IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, InterruptedException {
		
		File file = new File(backOffice.getActionContractPath(tenant,1));
		List<ExcelObject> contractList = null;
		if (file.exists() ){			
			XlsxReader reader = new XlsxReader();
			contractList = reader.read(ExcelObject.class, file);
		}
		
		 
		int countContractList = contractList.size();
		System.out.println(countContractList);
		for (int i = 0; i < countContractList; i++) {
			if (!contractList.get(i).actions.equals(action)) {
				continue;
			} else if (contractList.get(i).fundingReference==null) {
				continue;
			} else if (contractList.get(i).countryCode!=null) {
				continue;
			} else {
				System.out.println(contractList.get(i).actions + " "+contractList.get(i).fundingReference);
				//meter métodos por evento para buscar no BO os dados.
				
				switch (action) {
				case "ContractCreated":
					backOffice.getDataEventTypeContractCreated(tenant, contractList.get(i).fundingReference);
					break;				
				case "EarlySettlement":
				
					backOffice.getDataEventTypeEarlySettlement(tenant, contractList.get(i).fundingReference, contractList.get(i).amount);
					break;			
				case "CancelPartial":
					
					backOffice.getDataEventTypeCancel(tenant, contractList.get(i).fundingReference, action, contractList.get(i).amount);
					break;
				case "CancelTotal":
					
					backOffice.getDataEventTypeCancel(tenant, contractList.get(i).fundingReference, action, contractList.get(i).amount);
					break;
				case "Cancel":
					
					backOffice.getDataEventTypeCancel(tenant, contractList.get(i).fundingReference, action, contractList.get(i).amount);
					break;					
				case "Soft":
					
					backOffice.getDataEventTypeSoft(tenant, contractList.get(i).fundingReference, contractList.get(i).amount, contractList.get(i).amountFees);
					break;
				case "Paid":
					
					backOffice.getDataEventTypePaid(tenant, contractList.get(i).fundingReference, contractList.get(i).amount, contractList.get(i).amountFees);
					break;
				case "MFS":
					
					backOffice.getDataEventTypePaid(tenant, contractList.get(i).fundingReference, contractList.get(i).amount, contractList.get(i).amountFees);
					break;
	
				default:
						System.out.println("Please insert correct eventType");
				}
				
				backOffice.saveDataEventType(action, contractList.get(i).fundingReference, tenant);
				
			}
		}
		
		
	}

	
}