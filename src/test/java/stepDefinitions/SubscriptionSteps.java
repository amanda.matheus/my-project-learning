package stepDefinitions;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.Utils;
import pageObjects.subscription.PaymentPageSubscription;


public class SubscriptionSteps{

	private GeneralSteps general;
	WebDriver driver;
	public WebDriverWait wait;
	private static Utils utils;
	private static PaymentPageSubscription paymentPageSubscription;
	public String tenant = "";
	public String url;


	public SubscriptionSteps(GeneralSteps general) throws MalformedURLException {

		this.general=general;
		driver = this.general.getDriver();
		wait = this.general.getWait();
		utils = PageFactory.initElements(driver, Utils.class);
		paymentPageSubscription = PageFactory.initElements(driver, PaymentPageSubscription.class);
		tenant = general.getTenant();
		url = "";

	}
		
	
	@Then("^fill in the province$")
	public void fill_in_the_province(DataTable arg1) throws Throwable {

		utils.waitForPageToLoad();

		List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
		String birth = "";
		for (int i = 0; i < list.size(); i++) {
			
			birth = list.get(i).get("Province");
			
		}
		String birthString = GeneralSteps.birthPlace;
		paymentPageSubscription.insertBrithPlaceIT(birth, birthString);
	}

	@When("^login as \"([^\"]*)\" in the subscription page$")
	public void login_as_in_the_subscription_page(String email) throws Throwable {

		System.out.println(tenant);

		if (tenant.contains("PT")) {
			paymentPageSubscription.saveContractReferencePT();
			paymentPageSubscription.loginExistingCustomerPT(email);

			String XXX = driver.getPageSource();
			System.out.println("URL : " + XXX);
			System.out.println(XXX.contains("UploadDocument"));
			if (XXX.contains("UploadDocument")) {
				paymentPageSubscription.clickUploadPersonalDocument();
				paymentPageSubscription.clickContinueWaiting();
			}
			} else if (tenant.contains("ES")) {
				Thread.sleep(10000);
				paymentPageSubscription.loginExistingCustomer();
			} else {

			paymentPageSubscription.loginExistingCustomer();
		}
	}

	@Then("^accept \"([^\"]*)\" contract consent$")
	public void accept_the_consent(String consent) throws Throwable {
//		Thread.sleep(1000);
		paymentPageSubscription.clickConsentEC(consent);
	}

	@Then("^accept the contract terms$")
	public void accept_the_contract_terms() throws Throwable {

		
		if (tenant.contains("PT")) {
			Thread.sleep(2000);
//			if (driver.getCurrentUrl().contains("Upload")) {
//				Thread.sleep(2000);
//			} 
			paymentPageSubscription.choseCardPT();
			Thread.sleep(500);

			paymentPageSubscription.acceptTermsAndConditionsExistingCustomerPT();
		} else {

			if (paymentPageSubscription.verifyCreditCardListIsEmpty() == true) {
				System.out.println("CreditCardIsEmpty");
//				Thread.sleep(20000);

				if (tenant.contains("IT")) {

					paymentPageSubscription.clickVISA();

				}
				utils.randomValidCreditCard();
			}
			paymentPageSubscription.saveContractReference();
			paymentPageSubscription.acceptTermsAndConditions();

		}
	
	}

	@Then("^validate that the card is unauthorized$")
	public void validate_that_the_card_is_unauthorized() throws Throwable {

		Thread.sleep(1000);
		paymentPageSubscription.checkUnauthorizedCard();
	}

	@Then("insert a invalid payment card to my account")
	public void insert_a_invalid_payment_card_to_my_account() throws Throwable {

		paymentPageSubscription.insertNewInvalidPaymentCard();
		utils.fillCreditCard("Visa", "Debit", "Consumer");
	}

	@Then("insert a invalid payment card$")
	public void insert_a_invalid_payment_card() throws Throwable {

		//Inclui sempre o cartão VIsa Debit Consumer para verificar se é inválido. (4658 5840 9000 0001)
		JavascriptExecutor js = (JavascriptExecutor) driver;

		if (tenant.contains("IT")) {

			paymentPageSubscription.clickVISA();
			Thread.sleep(500);
			utils.fillCreditCard("Visa", "Debit", "Consumer");
			paymentPageSubscription.acceptTermsAndConditions();
			
		} else if (tenant.contains("PT")) {

			js.executeScript("document.querySelector(\"div.TermsAndConditionsInfoWrapper input\").click()");
			Thread.sleep(500);
			utils.fillCreditCard("Visa", "Debit", "Consumer");
			paymentPageSubscription.clicksubmitPT();
			
		} else {

			Thread.sleep(1500);
			utils.fillCreditCard("Visa", "Debit", "Consumer");
			Thread.sleep(10000);
		}

	}

	@When("upload the ID Card$")
	public void upload_ID_card() throws Throwable {

		paymentPageSubscription.clickUploadIDCardES();
	}
	
	@When("^fill the additional fields$")
	public void fill_additional_fields(DataTable arg1) throws Throwable {

		paymentPageSubscription.insertAdditionalFields(arg1, tenant);

		Thread.sleep(1000);
	}
	
	@When("click on validate button")
	public void click_on_validate_button() throws InterruptedException {
		
		paymentPageSubscription.closeCookies();
		paymentPageSubscription.validateButton();

	}
	
	@Then("insert a valid payment card$")
	public void insert_a_valid_payment_card() throws Throwable {

		//aqui há um método de escolha de cartões random, clica no submit contract.
		JavascriptExecutor js = (JavascriptExecutor) driver;

		if (tenant.contains("PT")) {
			
			utils.randomValidCreditCard();
			
			js.executeScript("document.querySelector(\"div.TermsAndConditionsInfoWrapper input\").click()");
			Thread.sleep(20000);
			
			paymentPageSubscription.clicksubmitPT();


		}  else if (tenant.contains("IT")) {
			
			paymentPageSubscription.closeCookies();
			paymentPageSubscription.clickVISA();
			Thread.sleep(500);
			utils.randomValidCreditCard();
			paymentPageSubscription.acceptTermsAndConditions();

		} else {
			paymentPageSubscription.closeCookies();
			Thread.sleep(500);
			utils.randomValidCreditCard();
			paymentPageSubscription.acceptTermsAndConditions();

		} 

	}
	
	@Then("insert a valid VISA payment card$")
	public void insert_a_valid_VISA_payment_card() throws Throwable {

		JavascriptExecutor js = (JavascriptExecutor) driver;

		if (tenant.contains("PT")) {
			
			utils.fillCreditCard("Visa", "Debit", "Commercial");
			
			js.executeScript("document.querySelector(\"div.TermsAndConditionsInfoWrapper input\").click()");
			Thread.sleep(20000);
			
			paymentPageSubscription.clicksubmitPT();


		}  else if (tenant.contains("IT")) {

			paymentPageSubscription.clickVISA();
			utils.fillCreditCard("Visa", "Debit", "Commercial");
			paymentPageSubscription.acceptTermsAndConditions();

		} else {
	
			Thread.sleep(500);
			utils.fillCreditCard("Visa", "Debit", "Commercial");
			paymentPageSubscription.acceptTermsAndConditions();

		} 

	}
	
	public void threeDS() throws InterruptedException {
		//Este método será sempre acionado, mas não quer dizer que vai entrar no 3DS sempre.		
		String url = driver.getCurrentUrl();
		
		System.out.println("Sandox?" +  url);
		int i=0;
		/*Faça o teste esperar segundos (com sleep) enquanto o link tem PaymentPage (link do new customer subscription) ou 
		  PaymentAccount (link existing customer subscription) ou PaymentProcessing (link de processamento após o submit) ou i < 50 (para evitar loop infinito)*/
		do {
			i++;
			Thread.sleep(1500);	
			System.out.println(driver.getCurrentUrl());
			System.out.println(i);
		}
		while ((driver.getCurrentUrl().contains("PaymentPage")|| driver.getCurrentUrl().contains("PaymentAccount") 
				|| driver.getCurrentUrl().contains("PaymentProcessing")) && i < 50);

		Thread.sleep(2000);	
		url = driver.getCurrentUrl();
		System.out.println("new link" + url);
		/*Se o novo link NÃO tiver CreateSelfCareAccount (quer dizer que não passou pelo 3DS como new customer) ou LandingPage (não passou pelo 3DS como existing customer) ou
		 PaymentRefusedPage (Contrato recusado) então você entrou no 3DS e incluirá a autenticação no método payWithAuthentication*/
		if (!(url.contains("CreateSelfCareAccount") || url.contains("LandingPage") || url.contains("PaymentProcessing")|| url.contains("PaymentRefusedPage"))) {
			
			System.out.println("aqui");
			paymentPageSubscription.payWithAuthentication();
		}
			
	
	}

	@And("^fill in the missing information$")
	public void fill_in_the_missing_information_and_Credit_Card_information_to_accept_the_contract(DataTable arg1)
			throws Throwable {
		//aqui inclui as informações que não são preenchidas no subscription
		//1º fecha os cookies, depois inclue os campos que faltam e salva o external reference para meter no ficheiro do accounting se o contrato concluir. 
		Thread.sleep(1000);

		if (tenant.contains("PT")) {//PT Sempre é diferente

			paymentPageSubscription.insertMissingInformation(arg1, tenant);
			paymentPageSubscription.clickAllCheckBoxes();

			paymentPageSubscription.clickContinue();
			utils.waitForPageToLoad();
			List<Map<String, String>> list = arg1.asMaps(String.class, String.class);
			String nif="";
			for (int i = 0; i < list.size(); i++) {
				nif = (list.get(i).get("NIF"));
			}
			
			if (nif.equals("281332800")) {
				paymentPageSubscription.clickUploadPersonalDocument2();
			} else {
				paymentPageSubscription.clickUploadPersonalDocument();
			}
			paymentPageSubscription.clickContinueWaiting();
			
			get_the_external_reference();
		} else if (tenant.contains("ES")) {//Aqui diferencia pq há mais campos no subscription de ES e precisa de scroll, fora isso tem os mesmos métodos.
		
			JavascriptExecutor js1 = (JavascriptExecutor) driver;
			js1.executeScript("window.scroll(0,50)");
			Thread.sleep(1500);
			paymentPageSubscription.closeCookies();
			Thread.sleep(1500);
			paymentPageSubscription.insertMissingInformation(arg1, tenant);
			paymentPageSubscription.saveContractReference();
		} else  { //IT, BE e RO não tem diferença aqui.
			Thread.sleep(1500);
			paymentPageSubscription.closeCookies();
			Thread.sleep(1500);
			paymentPageSubscription.insertMissingInformation(arg1, tenant);
			paymentPageSubscription.saveContractReference();
		}
	}
	
	@Then("^validate the automatic redirection to the merchant site$")
	public void validate_automatic_redirection() throws Throwable {
		
		paymentPageSubscription.checkAutomaticRedirection();
		
	}

	
	/**
	 * We have here ACCOUNTING method to write all contracts created
	 * 
	 */
	@Then("^validate that the contract was successfully created with a new user$")
	public void validate_that_the_contract_was_successfully_created_with_a_new_user() throws Throwable {
		//Clica no método threeDS, para entender melhor
		threeDS();
//		Thread.sleep(25000);

		if (tenant.contains("PT")) {

			//dentro deste método tem a gravação do external reference num ficheiro para depois ser verificado no accounting
			paymentPageSubscription.checkCreatedContractPT();
			paymentPageSubscription.insertPasswordPT();
			paymentPageSubscription.activateSelfcareAccountPT();
		}

		else {

			//dentro deste método tem a gravação do external reference num ficheiro para depois ser verificado no accounting
			paymentPageSubscription.checkCreatedContract(tenant);
			paymentPageSubscription.insertPassword();
			paymentPageSubscription.activateSelfcareAccount();
		}
	}
	
	@Then("^validate Contract Creation DE$")
	public void validate_Contract_Creation_DE() throws Throwable {

		
		paymentPageSubscription.checkCreatedContractDE();
	}

	/**
	 * We have here ACCOUNTING method to write all contracts created
	 * 
	 */
	@Then("^validate that the contract was successfully created$")
	public void validate_that_the_contract_was_successfully_created() throws Throwable {
		//Clica no método threeDS, para entender melhor
		threeDS();
		if (tenant.contains("PT")) {
			//dentro deste método tem a gravação do external reference num ficheiro para depois ser verificado no accounting
			paymentPageSubscription.checkCreatedContractPT();
			
		} else
			//dentro deste método tem a gravação do external reference num ficheiro para depois ser verificado no accounting
			paymentPageSubscription.checkCreatedContract(tenant);
	}

	@When("^get the external reference$")
	public void get_the_external_reference() throws Throwable {

		utils.waitForPageToLoad();
		
		if (tenant.contains("PT")) {
			paymentPageSubscription.saveContractReferencePT();
		} else {
			paymentPageSubscription.saveContractReference();
		}
	}

	@When("^get the mobile phone$")
	public void get_the_phone_number() throws Throwable {

		paymentPageSubscription.getMobilePhone();
	}

	@Then("^validate that the contract was rejected$")
	public void validate_that_the_contract_was_rejected() throws Throwable {

		if (tenant.contains("PT")) {
			threeDS();
			paymentPageSubscription.checkRejectedContractPT();
		} else {

			paymentPageSubscription.checkRejectedContract();
		}
	}
	
	@Then("^validate that the contract was rejected by checkout$")
	public void validate_that_the_contract_was_rejected_by_checkout() throws Throwable {

		Thread.sleep(20000);
		if (tenant.contains("PT")) {

			paymentPageSubscription.checkRejectedCard();
		} else if (tenant.contains("BE") || tenant.contains("IT")) {
			
		} else {
			
			paymentPageSubscription.scrollIntoViewPaymentArea();
			paymentPageSubscription.checkCardAutenticationFailded();
		}
	}
	
	@When("verify TAE percentage")
	public void verify_TAE_percentage() throws Throwable {
		
		if (tenant.equals("PT")) {
				paymentPageSubscription.validateTaegPortugal(EcommerceSteps.btsType);
		} else {
			paymentPageSubscription.validateTae(EcommerceSteps.btsType);
		}

	}
	
	@When("validate second try message")
	public void validate_second_try_message() throws Throwable {
		
		paymentPageSubscription.validateSecondTry();

	}
	
	@And("^click to leave the subscription form$")
	public void click_to_leave_the_subscription_form() throws Throwable {
		
		paymentPageSubscription.clicktoleavethesubscriptionform();
	}
	
	@Then("^confirm that the pop-in is being displayed$")
	public void confirm_that_the_popin_is_displayed() throws Throwable {
		
		paymentPageSubscription.confirmthatthepopinisdisplayed();
	}
}