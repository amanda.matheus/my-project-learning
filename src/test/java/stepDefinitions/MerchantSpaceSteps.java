package stepDefinitions;



import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.Utils;
import pageObjects.merchant.CreateContractMerchant;
import pageObjects.merchant.DashboardMerchant;
import pageObjects.merchant.HomePageMerchant;


public class MerchantSpaceSteps{

	private GeneralSteps general;
	WebDriver driver;
	public WebDriverWait wait;
	private static Utils utils;
	private static HomePageMerchant homePageMerchant;
	private static DashboardMerchant dashboardMerchant;
	private static CreateContractMerchant createContractMerchant;
	public String tenant = "";
	public String externalRef = null;


	public MerchantSpaceSteps(GeneralSteps general)  throws MalformedURLException {

		this.general=general;
		driver = this.general.getDriver();
		wait = this.general.getWait();
		utils = PageFactory.initElements(driver, Utils.class);
		homePageMerchant = PageFactory.initElements(driver, HomePageMerchant.class);
		dashboardMerchant = PageFactory.initElements(driver, DashboardMerchant.class);
		createContractMerchant = PageFactory.initElements(driver, CreateContractMerchant.class);
		tenant = general.getTenant();

	}
	
		
	
	@Then("^should be able to logout$")
	public void should_be_able_to_logout() throws Throwable {

		dashboardMerchant.clickLogoutMerchant();
	}

	@When("^click on create a contract$")
	public void click_on_create_a_contract() throws Throwable {

		dashboardMerchant.clickCreateContract();
	}

	@When("^validate the merchant as \"([^\"]*)\"$")
	public void validate_the_merchant(String merchant) throws Throwable {

		createContractMerchant.validateMerchant(merchant);
	}

	@When("^select the \"([^\"]*)\" Merchant$")
	public void select_the_Merchant(String merchantName) throws Throwable {
		Thread.sleep(5000);
		createContractMerchant.changeMerchant(merchantName);
	}

	@When("^select the \"([^\"]*)\" Merchant on the list$")
	public void select_the_merchant_contract(String merchantName) throws Throwable {

		dashboardMerchant.selectMerchantCombo(merchantName);
	}

	@When("^select the \"([^\"]*)\" status on the list$")
	public void select_the_status_contract(String statusName) throws Throwable {
		Thread.sleep(15000);
		dashboardMerchant.selectStatusCombo(statusName);
	}

	@Then("^click on the Ready to Delivery button$")
	public void click_on_the_Ready_to_Delivery_button() throws Throwable {

		Thread.sleep(30000);
		dashboardMerchant.getExternalReference();
		dashboardMerchant.clickRTDButton();
		Thread.sleep(30000);
	}

	@When("^validate the status \"([^\"]*)\"$")
	public void validate_status_contract(String status) throws Throwable {

		Thread.sleep(20000);
		dashboardMerchant.validateContractStatus(status);
	}

	@When("^click on Generate Contract Number$")
	public void click_Generate_Contract_Number() throws Throwable {

		utils.waitForPageToLoad();
		createContractMerchant.clickGenerateContractNumber();
	}

	@When("^fill in the product information$")
	public void fill_in_the_product_information(DataTable arg1) throws Throwable {

//		createContractMerchant.clickGenerateContractNumber();
		utils.waitForPageToLoad();
		createContractMerchant.insertProductInformation(arg1);
//		createContractMerchant.clickAddProduct();
		utils.waitForPageToLoad();
	}

	@When("^choose \"([^\"]*)\" Oney transaction type$")
	public void choose_Oney_transaction_type(String fees) throws Throwable {

		Thread.sleep(5000);
		switch (fees) {
		case "3x (with fees)":
			createContractMerchant.click3xOneyWithFees();
			break;

		case "3x (without fees)":
			createContractMerchant.click3xOneyWithoutFees();
			break;

		case "4x (with fees)":
			createContractMerchant.click4xOneyWithFees();
			break;

		case "4x (without fees)":
			createContractMerchant.click4xOneyWithoutFees();
			break;

		case "6x (with fees)":
			createContractMerchant.click6xOneyWithFees();
			break;

		case "8x (with fees)":
			createContractMerchant.click8xOneyWithFees();
			break;

		case "8x (without fees)":
			createContractMerchant.click8xOneyWithoutFees();
			break;

		case "10x (with fees)":
			createContractMerchant.click10xOneyWithFees();
			break;

		case "12x (with fees)":
			createContractMerchant.click12xOneyWithFees();
			break;

		case "12x (without fees)":
			createContractMerchant.click12xOneyWithoutFees();
			break;

		default:
			System.out.println("no match");

		}
		Thread.sleep(5000);
		createContractMerchant.clickConfirm();
	}

	@When("^fill in the document information$")
	public void fill_in_the_document_information(DataTable arg1) throws Throwable {

		utils.waitForPageToLoad();
		Thread.sleep(500);
		String url = driver.getCurrentUrl();

		if (url.contains("qalogin.oney.be")) {

//			utils.waitForPageToLoad();
//			Thread.sleep(500);
			createContractMerchant.insertMunicipality(arg1, tenant);
			createContractMerchant.insertPlaceOfBirth(arg1, tenant);
			createContractMerchant.insertNacionality(arg1, tenant);
			createContractMerchant.insertDocumentType(arg1);

			utils.waitForPageToLoad();
			createContractMerchant.insertEmissionDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertExpiracyDate();

		} else if (url.contains("qalogin.oney.it")) {

			createContractMerchant.insertMunicipality(arg1, tenant);
			createContractMerchant.insertPlaceOfBirth(arg1, tenant);
			GeneralSteps.birthPlace = createContractMerchant.getBirthPlace();
			createContractMerchant.insertNacionality(arg1, tenant);
			createContractMerchant.insertIdCard(arg1);
			createContractMerchant.insertDocumentType(arg1);

			utils.waitForPageToLoad();
			createContractMerchant.insertEmissionDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertExpiracyDate();

		} else if (url.contains("qalogin.oney.es")) {

			utils.waitForPageToLoad();
			createContractMerchant.insertPlaceOfBirth(arg1, tenant);
			createContractMerchant.insertMunicipality(arg1, tenant);
			createContractMerchant.insertProvincia(arg1);
			createContractMerchant.insertNacionality(arg1, tenant);
			createContractMerchant.insertDocumentType(arg1);

			utils.waitForPageToLoad();
			createContractMerchant.insertEmissionDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertExpiracyDate();

		} else if (url.contains("qalogin.oney.ro")) {

			createContractMerchant.insertMunicipality(arg1, tenant);
			utils.waitForPageToLoad();
			createContractMerchant.insertPlaceOfBirth(arg1, tenant);
			utils.waitForPageToLoad();
			createContractMerchant.insertNacionality(arg1, tenant);
			createContractMerchant.insertIdCard(arg1);
			createContractMerchant.insertDocumentType(arg1);

			utils.waitForPageToLoad();
			createContractMerchant.insertEmissionDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertExpiracyDate();

		} else if (url.contains("qa3x4x.oney.pt")) {

			createContractMerchant.insertMunicipality(arg1, tenant);

		}

		Thread.sleep(5000);
		createContractMerchant.clickConfirmContract();

//		if (url.contains("qa3x4x.oney.pt")) {
//			Thread.sleep(5000);
//			wait.until(ExpectedConditions.alertIsPresent());
//			driver.switchTo().alert().accept();
//		}
		Thread.sleep(10000);
	}

	@When("^fill in the client personal information$")
	public void fill_in_the_personal_information(DataTable arg1) throws Throwable {

		utils.waitForPageToLoad();
		String url = driver.getCurrentUrl();

		if (url.contains("qalogin.oney.be")) {

			createContractMerchant.insertPersonalInfo(arg1, tenant);
			utils.waitForPageToLoad();
			createContractMerchant.insertEmail(arg1);
			utils.waitForPageToLoad();
			createContractMerchant.insertBirthDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertPersonalInfo_2(arg1, tenant);
			utils.waitForPageToLoad();
		} else if (url.contains("qalogin.oney.it") || url.contains("qalogin.oney.ro")) {

			createContractMerchant.insertGender(arg1, tenant);
			createContractMerchant.insertPersonalInfo(arg1, tenant);
			utils.waitForPageToLoad();
			createContractMerchant.insertEmail(arg1);
			createContractMerchant.insertBirthDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertPersonalInfo_2(arg1,tenant);
		} else if (url.contains("qalogin.oney.es")) {

			createContractMerchant.insertPersonalInfo(arg1, tenant);
			utils.waitForPageToLoad();
			createContractMerchant.insertSegundoApelido(arg1);
			createContractMerchant.insertEmail(arg1);
			createContractMerchant.insertBirthDate();
			utils.waitForPageToLoad();
			createContractMerchant.insertPersonalInfo_2(arg1, tenant);
		} else if (url.contains("qa3x4x.oney.pt")) {

			createContractMerchant.insertPersonalInfo(arg1, tenant);
			utils.waitForPageToLoad();
			createContractMerchant.insertEmail(arg1);
			createContractMerchant.insertBirthDatePT();
			Thread.sleep(1000);
			createContractMerchant.insertPersonalInfo_2(arg1, tenant);
		}
	}

	@When("^follow the \"([^\"]*)\" to finish my subscription$")
	public void follow_the_to_finish_my_subscription(String arg1) throws Throwable {

		Thread.sleep(30000);
		createContractMerchant.navigateToConfirmationURL(arg1);
		Thread.sleep(20000);

	}

	@Given("^click on the forgot password$")
	public void click_on_the_forgot_password() throws Throwable {

		homePageMerchant.clickForgotPassword();
	}

	@Given("^insert my email$")
	public void insert_my_email() throws Throwable {

		homePageMerchant.insertEmailToRecoverPassword();
	}

	@Then("^should receive a new password$")
	public void should_receive_a_new_password() throws Throwable {

		homePageMerchant.clickRecoverPassword();
	}

	@Given("^click Contracts$")
	public void click_Contracts() throws Throwable {

		dashboardMerchant.clickContracts();
	}

	@Given("^verify cancelled contracts$")
	public void verify_cancelled_contracts() throws Throwable {

		if (driver.getCurrentUrl().contains("oney.be")) {

			dashboardMerchant.selectStatusCombo("Annulé");
		}

		else if (driver.getCurrentUrl().contains("oney.it")) {

			dashboardMerchant.selectStatusCombo("Annullato");
		}

		else if (driver.getCurrentUrl().contains("oney.pt")) {

			dashboardMerchant.selectStatusCombo("Cancelado");
		}
	}

	@Given("^click export to excel$")
	public void click_export_to_excel() throws Throwable {

		dashboardMerchant.clickExportToExcel();
	}

	@Given("^click on the last (\\d+) days link$")
	public void click_on_the_last_days_link(int arg1) throws Throwable {

		dashboardMerchant.clickLast30Days();
	}

	@Given("^search for the user \"([^\"]*)\"$")
	public void search_for_user(String user) throws Throwable {
		
		dashboardMerchant.searchUser(user);
	}

	@Then("^search for external reference$")
	public void search_for_external_reference() throws Throwable {

		Thread.sleep(10000);
		String ref = dashboardMerchant.getexternalRef();
		dashboardMerchant.searchCustomer(ref);
		Thread.sleep(1000);
	}

	@Given("^click Files$")
	public void click_Files() throws Throwable {

		dashboardMerchant.clickFiles();
	}

	@Given("^check if contract table is displayed$")
	public void check_if_contract_table_is_displayed() throws Throwable {

		dashboardMerchant.checkFilesTable();
	}

	@Given("^click download if any file exists$")
	public void click_download_if_any_file_exists() throws Throwable {

		dashboardMerchant.downloadTableFile();
	}

	@Given("^click Users$")
	public void click_Users() throws Throwable {

		dashboardMerchant.clickUsers();
	}

	@Given("^change status to \"([^\"]*)\"$")
	public void change_status_to(String status) throws Throwable {

		dashboardMerchant.changeStatus(status);
		Thread.sleep(2000);
	}

	@Given("^click on New User$")
	public void click_on_New_User() throws Throwable {

		dashboardMerchant.clickNewUser();
	}

	@Given("^click on Cancel on the new user page$")
	public void click_on_Cancel_on_the_new_user_page() throws Throwable {

		dashboardMerchant.clickCancelNewUser();
	}

	@When("^click on Add Travels$")
	public void click_on_Add_Travels() throws Throwable {

		createContractMerchant.clickAddTravels();
	}

	@When("^click on Add Products$")
	public void click_on_Add_Products() throws Throwable {

		createContractMerchant.clickAddProducts();
	}

	@When("^fill the Traveller Info$")
	public void fill_the_traveller_info(DataTable arg1) throws Throwable {

		createContractMerchant.fillTravellerInfo(arg1);
	}

	@When("^fill the Departure Info$")
	public void fill_the_departure_info(DataTable arg1) throws Throwable {

		createContractMerchant.fillDepartureInfo(arg1);
	}

	@When("^fill the Return Info$")
	public void fill_the_return_info(DataTable arg1) throws Throwable {

		createContractMerchant.fillReturnInfo(arg1);
	}

	@When("^click on Add another destination$")
	public void click_on_Add_Another_Destination() throws Throwable {

		createContractMerchant.clickAddAnotherDestination();
	}

	@When("^click on Add another travel$")
	public void click_on_Add_Another_Travel() throws Throwable {

		createContractMerchant.clickAddAnotherTravel();
	}

	@When("^check if Add another destination is disabled$")
	public void check_if_add_another_destination_disable() throws Throwable {

		createContractMerchant.checkIfAddAnotherDestinationIsDisable();
	}

	@When("^check the total basket price$")
	public void check_total_basket_price(DataTable arg1) throws Throwable {

		createContractMerchant.checkTotalBasketPrice(arg1);
	}

	@When("^click on Add stay or vehicle$")
	public void click_on_Add_stay_vehicle() throws Throwable {

		createContractMerchant.clickAddStayOrVehicle();
	}

	@When("^check if Add stay or vehicle is disabled$")
	public void check_if_add_stay_vehicle_disable() throws Throwable {

		createContractMerchant.checkIfAddStayOrVehicleIsDisable();
	}

	@When("^fill the Stay, accommodations and vehicle rental$")
	public void fill_stay_accommodations_vehicle_rental(DataTable arg1) throws Throwable {

		createContractMerchant.fillStayAccommodationsVehicle(arg1);
	}

	@Given("validate New Contract Tab Fields")
	public void validade_New_Contract_Tab_Fields() throws Throwable {

		dashboardMerchant.validateNewRemainingBasketField();
		dashboardMerchant.validateNewInitialBasketField();
		dashboardMerchant.validateNewCodeField();
		dashboardMerchant.validateMerchantContractDetails();
	}
	
	@Given("click on last contract details")
	public void click_on_last_contract_details() throws Throwable {

		dashboardMerchant.validateMerchantContractDetails();
	}
	
	@Given("cancel An Amount An Return Partially")
	public void cancel_An_Amount_An_Return_Partially() throws Throwable {
		
		dashboardMerchant.validatecancelPartially();
		dashboardMerchant.validateAnMerchantCancellationReturn();
	}	
	
	@Given("cancel The Contract Partially")
	public void cancel_The_Contract_Partially() throws Throwable {
		
		dashboardMerchant.validatecancelPartially();
		dashboardMerchant.submitPartialCancellation();
	}
	
	@When("^fill the travel total price$")
	public void fill_the_travel_total_price(DataTable arg1) throws Throwable {

		createContractMerchant.fillTravelTotalPrice(arg1);
		Thread.sleep(1000);
	}

}