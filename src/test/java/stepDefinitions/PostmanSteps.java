/*
 * 
 */
package stepDefinitions;

import java.io.IOException;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.Utils;
import pageObjects.backOffice.HomePageBackOffice;
import pageObjects.postman.Postman;
import pageObjects.subscription.PaymentPageSubscription;


// TODO: Auto-generated Javadoc
/**
 * The Class PostmanSteps.
 */
public class PostmanSteps {

	/** The general. */
	private GeneralSteps general;
	
	/** The driver. */
	WebDriver driver;
	
	/** The wait. */
	public WebDriverWait wait;
	
	/** The payment page subscription. */
	private static PaymentPageSubscription paymentPageSubscription;
	
	/** The utils. */
	private static Utils utils;
	
	/** The tenant. */
	public String tenant = "";
	
	/** The postman. */
	private static Postman postman;
	
	/** The url. */
	public String url;


	/**
	 * Instantiates a new postman steps.
	 *
	 * @param general the general
	 * @throws MalformedURLException the malformed URL exception
	 */
	public PostmanSteps(GeneralSteps general) throws MalformedURLException {

		this.general=general;
		driver = this.general.getDriver();
		wait = this.general.getWait();
		postman = PageFactory.initElements(driver, Postman.class);
		utils = PageFactory.initElements(driver, Utils.class);
		paymentPageSubscription = PageFactory.initElements(driver, PaymentPageSubscription.class);
		tenant = general.getTenant();
		System.out.println(tenant);
		url = "";

	}

	/**
	 * Click on the workspace menu.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@When("click on the Workspace menu")
	public void click_on_the_Workspace_menu() throws InterruptedException {

		postman.clickOnWorkspace();
		Thread.sleep(800);
//		postman.verificarsync();
	}
	
	/**
	 * Put variable values.
	 *
	 * @throws Throwable the throwable
	 */
	@When("put the variables value$")
	public void put_variable_values() throws Throwable {
		
			postman.fillPreRequestScript();
			Thread.sleep(800);
	}

	/**
	 * Click on my workspace.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@When("click on My Workspace")
	public void click_on_my_workspace() throws InterruptedException {

		postman.clickOnMyWorkspace();
	}
	
	/**
	 * Click on qa workspace.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@When("click on QA Workspace")
	public void click_on_qa_workspace() throws InterruptedException {

		postman.clickOnQAWorkspace();
	}
	
	/**
	 * Deactivate SS L certificate.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@And("deactivate SSl Certificate")
	public void deactivate_SSL_Certificate() throws InterruptedException {

		postman.deactivate_SSL_Certificate();
	}
	
	/**
	 * Change environment.
	 *
	 * @param environment the environment
	 * @throws InterruptedException the interrupted exception
	 */
	@When("change to \"([^\"]*)\" environment")
	public void change_environment(String environment) throws InterruptedException {

		postman.changeEnvironment(environment);
	}

	/**
	 * Click on the folder.
	 *
	 * @param folder the folder
	 */
	@And("click on the \"([^\"]*)\" folder")
	public void click_on_the_folder(String folder) {

		postman.clickOnFolder(folder);
	}

	/**
	 * Click on the file.
	 *
	 * @param file the file
	 * @throws InterruptedException the interrupted exception
	 */
	@And("click on the \"([^\"]*)\" file")
	public void click_on_the_file(String file) throws InterruptedException {

		postman.clickOnFile(file);
	}

	@And("click on two pane view")
	public void click_on_two_pane_view() throws InterruptedException {

		postman.clickPaneView();
	}
	/**
	 * Search folder.
	 *
	 * @param folder the folder
	 * @throws InterruptedException the interrupted exception
	 */
	@And("search the folder \"([^\"]*)\"")
	public void search_folder(String folder) throws InterruptedException {

		postman.searchFolder(folder);
	}

	/**
	 * Click on send.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@When("click on the send button")
	public void click_on_send() throws InterruptedException {

		postman.clickOnSend();
		Thread.sleep(800);
	}
	
	/**
	 * Validate if the response is.
	 *
	 * @param arg the arg
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Then("validate if the response")
	public void validate_if_the_response_is(DataTable arg) throws InterruptedException, IOException {

		List<Map<String, String>> list = arg.asMaps(String.class, String.class);
		String response = null;
		String tenant = null;
		String action = null;

		for (int i = 0; i < list.size(); i++) {
			
			response = list.get(i).get("Response");
			tenant = list.get(i).get("Tenant");
			action = list.get(i).get("Action");
		}
			
		assert(postman.getResponse().contains(response));
		postman.clickConsole();
		Thread.sleep(800);
		paymentPageSubscription.saveContractReferenceAPIDE(postman.getExternalReference());
		postman.exitConsole();
		System.out.println(tenant);
		if (action.contains("ContractCreated")) {
			paymentPageSubscription.writeContractCreated(tenant);
		} else if (action.contains("Cancel")) {
			postman.writeActionsToContract(tenant, action);
		}
		
		Thread.sleep(800);
	}
	
	/**
	 * Click on close tab.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@When("click on close tab")
	public void click_on_close_tab() throws InterruptedException {

		postman.clickOnCloseTab();
		Thread.sleep(800);
	}

	/**
	 * Gets the link subscription.
	 *
	 * @return the link subscription
	 * @throws Throwable the throwable
	 */
	@Then("get link to finish my subscription")
	public void get_link_subscription() throws Throwable {

		postman.getLinkSubscription();
		url = postman.getLink();
		driver.navigate().to(url);
		Thread.sleep(1000);
		tenant = general.getTenant();
		GeneralSteps.birthPlace = "MILANO";
	}

	/**
	 * Click on body tab.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	@When("click on the body tab")
	public void click_on_body_tab() throws InterruptedException {

		postman.clickOnBodyTab();
	}

//	@When("change the external reference \"([^\"]*)\"$")
//	public void change_the_external_reference(String arg) throws InterruptedException {
//		
//		postman.changeExternalReference(arg);
//	}

	/**
 * Validate the response.
 *
 * @param status the status
 * @throws InterruptedException the interrupted exception
 */
@Then("^validate if the response is the status \"([^\"]*)\"$")
	public void validate_the_response(String status) throws InterruptedException {

		postman.validateResponse(status);
	}

	
	/**
	 * Put variable value file.
	 *
	 * @param method the method
	 * @param file the file
	 * @throws Throwable the throwable
	 */
	@When("put the \"([^\"]*)\" variables value on the \"([^\"]*)\" file$")
	public void put_variable_value_file(String method, String file) throws Throwable {
		
		put_variable_value_file_day(method, file, 0, HomePageBackOffice.countGeneral, tenant);
//		put_variable_value_file_day(method, file, 0, 4, "PT");
	}
	
	/**
	 * Put variable value file day.
	 *
	 * @param method the method
	 * @param file the file
	 * @param day the day
	 * @param countGeneral the count general
	 * @param tenant the tenant
	 * @throws Throwable the throwable
	 */
	public void put_variable_value_file_day(String method, String file, int day, int countGeneral, String tenant) throws Throwable {
		
		
		for (int i=0;i < countGeneral;i++) { //
			
			postman.clickOnFile(file);
			LocalDateTime now = LocalDateTime.now().minusDays(day);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String today = now.format(formatter);
			String filename = method+"_" + tenant + "_" + today + "_Request" + i + ".txt";
			System.out.println(filename);
			postman.fillPreRequestScript(filename);
			Thread.sleep(800);
			postman.clickOnSend();
			Thread.sleep(800);
			if (postman.getResponse().contains("ERR_11")) {
					postman.closeTab();
					Thread.sleep(1000);
					postman.clickOnFile(file);
					postman.fillPreRequestScript(filename);
					Thread.sleep(800);
					postman.clickOnSend();
					Thread.sleep(800);	
					postman.closeTab();
			} else {
				Thread.sleep(800);	
				postman.closeTab();
				System.out.println(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\MFS\\"+ filename);
				utils.deleteFile(System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\Accounting\\MFS\\"+ filename);
			}
				
		}
	}
}