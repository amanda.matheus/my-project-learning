package stepDefinitions;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.Utils;
import managers.WebDriverManager;
import pageObjects.backOffice.HomePageBackOffice;
import pageObjects.eCommerce.CheckoutECommerce;
import pageObjects.management.TenantManagement;
import pageObjects.merchant.CreateContractMerchant;
import pageObjects.merchant.DashboardMerchant;
import pageObjects.merchant.HomePageMerchant;
import pageObjects.postman.Postman;
import pageObjects.selfcare.HomePageSelfcare;
import pageObjects.subscription.PaymentPageSubscription;

public class GeneralSteps{

	WebDriver driver;
	public static WebDriverWait wait;
	private WebDriverManager webDriverManager;
	private static Utils utils;
	private static HomePageMerchant homePageMerchant;
	private static HomePageBackOffice backOffice;
	private static DashboardMerchant dashboardMerchant;
	private static CreateContractMerchant createContractMerchant;
	private static PaymentPageSubscription paymentPageSubscription;
	private static CheckoutECommerce checkoutECommerce;
	private static HomePageSelfcare selfcare;
	private static TenantManagement management;
	public String tenant = "";
	private static Postman postman;
	public String url;
	public static String birthPlace;


	public GeneralSteps() throws MalformedURLException {

		webDriverManager = new WebDriverManager();
		driver = webDriverManager.getDriver();
		wait = new WebDriverWait(driver, 60);
		utils = PageFactory.initElements(driver, Utils.class);
		homePageMerchant = PageFactory.initElements(driver, HomePageMerchant.class);
		backOffice = PageFactory.initElements(driver, HomePageBackOffice.class);
		dashboardMerchant = PageFactory.initElements(driver, DashboardMerchant.class);
		createContractMerchant = PageFactory.initElements(driver, CreateContractMerchant.class);
		paymentPageSubscription = PageFactory.initElements(driver, PaymentPageSubscription.class);
		checkoutECommerce = PageFactory.initElements(driver, CheckoutECommerce.class);
		selfcare = PageFactory.initElements(driver, HomePageSelfcare.class);
		postman = PageFactory.initElements(driver, Postman.class);
		management = PageFactory.initElements(driver, TenantManagement.class);
		url = "";

	}
	
	public WebDriver getDriver() {
	      return driver;
	   }
	
	public WebDriverWait getWait() {
	      return wait;
	   }
	
	/*
	 * Open links specifics environments if the case is requested
	 * 
	 */
	@Given("^navigate to \"([^\"]*)\"$")
	public void navigate_to(String webpage) throws Throwable {

		switch (webpage) {

		case "BackOffice PT":
			driver.navigate().to("https://qa3x4x.oney.pt/backoffice/");
			tenant = "PT";
			break;

		case "BackOffice ES":
			driver.navigate().to("https://qalogin.oney.es/BackOffice/");
			tenant = "ES";
			break;
			
		case "Tenant Management":
			driver.navigate().to("https://195.22.22.198/TenantManagement/Login.aspx");
			break;


		case "BackOffice IT":
			driver.navigate().to("https://qalogin.oney.it/BackOffice/");
			tenant = "IT";
			break;

		case "BackOffice BE":
			driver.navigate().to("https://qalogin.oney.be/BackOffice/");
			tenant = "BE";
			break;
			
		case "BackOffice BE DEV":
			driver.navigate().to("https://dvlogin.oney.be/BackOffice/");
			tenant = "BE";
			break;
			
		case "BackOffice RO DEV":
			driver.navigate().to("https://dvlogin.oney.ro/BackOffice/");
			tenant = "RO";
			break;

			
		case "BackOffice ES DEV":
			driver.navigate().to("https://dvlogin.oney.es/BackOffice/");
			tenant = "ES";
			break;

			
		case "BackOffice IT DEV":
			driver.navigate().to("https://dvlogin.oney.it/BackOffice/");
			tenant = "IT";
			break;

		case "BackOffice RO":
			driver.navigate().to("https://qalogin.oney.ro/BackOffice/");
			tenant = "RO";
			break;

		case "BackOffice RO PP":
			driver.navigate().to("https://pplogin.oney.ro/BackOffice/");
			tenant = "RO";
			break;

		case "BackOffice DE":
			driver.navigate().to("https://qalogin.oneygmbh.de/BackOffice/");
			tenant = "DE";
			break;

		case "Merchant PT":
			driver.navigate().to("https://qa3x4x.oney.pt/Merchant/");
			tenant = "PT";
			break;
		case "Merchant ES":
			driver.navigate().to("https://qalogin.oney.es/Merchant/");
			tenant = "ES";
			break;

		case "Merchant IT":
			driver.navigate().to("https://qalogin.oney.it/Merchant/");
			tenant = "IT";
			break;

		case "Merchant BE":
			driver.navigate().to("https://qalogin.oney.be/Merchant/");
			tenant = "BE";
			break;
			
		case "Merchant RO":
			driver.navigate().to("https://qalogin.oney.ro/Merchant/");
			tenant = "RO";
			break;
			
		case "Merchant DE":
			driver.navigate().to("https://qalogin.oneygmbh.de/Merchant/");
			tenant = "DE";
			break;

		case "eCommerce PT":
			driver.navigate().to("https://qa3x4x.oney.pt/ecommerce/");
			tenant = "PT";
			break;

		case "eCommerce ES":
			driver.navigate().to("https://qalogin.oney.es/eCommerce/HomePage.aspx");
			tenant = "ES";
			break;

		case "eCommerce IT":
			driver.navigate().to("https://qalogin.oney.it/eCommerce/HomePage.aspx");
			tenant = "IT";
			break;

		case "eCommerce BE":
			driver.navigate().to("https://qalogin.oney.be/eCommerce/HomePage.aspx");
			tenant = "BE";
			break;

		case "eCommerce RO":
			driver.navigate().to("https://qalogin.oney.ro/eCommerce/HomePage.aspx");
			tenant = "RO";
			break;

		case "Selfcare PT":
			driver.navigate().to("https://qa3x4x.oney.pt/Customer/");
			tenant = "PT";
			break;

		case "Selfcare ES":
			driver.navigate().to("https://qalogin.oney.es/Customer/");
			tenant = "ES";
			break;

		case "Selfcare IT":
			driver.navigate().to("https://qalogin.oney.it/Customer/");
			tenant = "IT";
			break;

		case "Selfcare BE":
			driver.navigate().to("https://qalogin.oney.be/Customer/");
			tenant = "BE";
			break;

		case "Selfcare RO":
			driver.navigate().to("https://qalogin.oney.ro/Customer/");
			tenant = "RO";
			break;

		case "link":
			driver.navigate().to(url);
			Thread.sleep(1000);
			tenant = getTenant();
			break;

		case "Postman":
			driver.navigate().to("http://ablewiseqa.postman.co");
			break;

		case "Postman work":
			driver.navigate().to(
					"https://ablewiseqa.postman.co/build/workspace/My-Workspace~cf80f7e7-7136-4a97-8ba5-1a717dc7515b");
			break;
			
		case "Dataconservation ES":
			driver.navigate().to(
					"https://qalogin.oney.es/AblewiseTest/DC_Anonymize_Delete_Test.aspx");
			break;
			
		case "Dataconservation BE":
			driver.navigate().to(
					"https://qalogin.oney.be/AblewiseTest/DC_Anonymize_Delete_Test.aspx");
			break;
			
		case "Dataconservation RO":
			driver.navigate().to(
					"https://qalogin.oney.ro/AblewiseTest/DC_Anonymize_Delete_Test.aspx");
			break;
			
		case "Dataconservation IT":
			driver.navigate().to(
					"https://qalogin.oney.it/AblewiseTest/DC_Anonymize_Delete_Test.aspx");
			break;
			
		case "Dataconservation PT":
			driver.navigate().to(
					"https://qa3x4x.oney.pt/AblewiseTest/DC_Anonymize_Delete_Test.aspx");
			break;
			
		default:
			System.out.println("Please insert correct page to navigate in Cucumber Step");
			break;
		}

		String activeWindow = driver.getWindowHandle();
		driver.switchTo().window(activeWindow);
		driver.switchTo().activeElement();

		boolean currentUrl;
		currentUrl = ((driver.getCurrentUrl().contains("qalogin.oney"))
				|| ((driver.getCurrentUrl().contains("pplogin.oney"))|| driver.getCurrentUrl().contains("dvlogin.oney")
						|| ((driver.getCurrentUrl().contains("pp3x4x.oney"))
								|| (driver.getCurrentUrl().contains("qa3x4x.oney"))
								|| (driver.getCurrentUrl().contains("postman"))
										|| (driver.getCurrentUrl().contains("TenantManagement")))));
		if ((!currentUrl) || (driver.getCurrentUrl().contains("ecommerce/sucess"))) {
			int i = 0;
			System.out.println("foi");
			while ((i < 10) || (!currentUrl)) {
				navigate_to(webpage);
				Thread.sleep(1000);
				System.out.println("entrou");
				currentUrl = ((driver.getCurrentUrl().contains("qalogin.oney"))
						|| (driver.getCurrentUrl().contains("qa3x4x.oney"))
						|| (driver.getCurrentUrl().contains("postman")));
				i++;
			}
		}
		
	}
	
	
	/*
	 * insert specifics login access depending on the URL requested
	 */
	@Given("^enter a valid username and password$")
	public void enter_a_valid_username_and_password() throws Throwable {

		String url = driver.getCurrentUrl();
		
		if (url.contains("ackOffice")||url.contains("ackoffice")) {
			backOffice.insertUsernameAndPasswordBackOffice(tenant);
			backOffice.clickLoginBackOffice();
		} else if (url.contains("Merchant")||url.contains("merchant")) {
			homePageMerchant.insertUsernameAndPasswordMerchant(tenant);
			homePageMerchant.clickLoginMerchant();
		} else if (url.contains("Customer")||url.contains("customer")) {
			selfcare.insertUsernameAndPasswordSelfcare(tenant);
			selfcare.clickLoginSelfcare();
		} else if (url.contains("postman")) {
			postman.insertUsernameAndPassword();
			postman.clickSignIn();
		} else if (url.contains("195.22.22.198")) {
			System.out.println(driver.getCurrentUrl());
			management.insertUsernameAndPasswordManagement();
		} else {
			System.out.println(driver.getCurrentUrl());
			management.insertUsernameAndPasswordDataConservation();
		}
		
		Thread.sleep(10000);
		url = driver.getCurrentUrl();
		if (url.contains("BackOffice/ChangePassword")) {
			backOffice.changePassword();
			driver.navigate().to("https://195.22.22.198/TenantManagement/Login.aspx");
			Thread.sleep(1000);
			management.insertUsernameAndPasswordManagement();
			String currentUser = backOffice.getCurrentUsername();
			String curretPassword = backOffice.getCurrentPassword();
			management.changePassword(tenant, currentUser, curretPassword);
			navigate_to("BackOffice " + tenant);	
			Thread.sleep(1000);
			enter_a_valid_username_and_password();
			
		} else if (url.contains("Merchant/Login.aspx")) {
			homePageMerchant.changePassword();
			Thread.sleep(3000);
			driver.navigate().to("https://195.22.22.198/TenantManagement/Login.aspx");
			Thread.sleep(3000);
			management.insertUsernameAndPasswordManagement();
			String currentUser = homePageMerchant.getCurrentUsername();
			String curretPassword = homePageMerchant.getCurrentPassword();
			management.changePassword(tenant, currentUser, curretPassword);
			navigate_to("Merchant " + tenant);	
			Thread.sleep(3000);
			enter_a_valid_username_and_password();
		}
	}
	
	@Then("^validate the \"([^\"]*)\" API Error$")
	public void validate_the_Api_Error(String page) throws Throwable {

		if (page.equalsIgnoreCase("Merchant")) {
			createContractMerchant.validateAPIError();
		} else {
			checkoutECommerce.validateAPIError();
		}
	}
		
	@When("^get the Contract link$")
	public void get_the_Contract_link() throws Throwable {

		url = driver.getCurrentUrl();
		System.out.println(url);
	}
	
	@Then("^validate the fields Gender, Occupation, Employer and Address on \"([^\"]*)\"$")
	public void validate_new_fields(String page) throws Throwable {

		if (page.equals("Selfcare")) {
			selfcare.validateNewFields();
		} else if (page.equals("BackOffice")) {
			backOffice.validateNewFields(tenant);
		}

	}

	@Given("^search for the customer$")
	public void search_for_customer(DataTable customer) throws Throwable {

		List<Map<String, String>> list = customer.asMaps(String.class, String.class);

		for (int i = 0; i < list.size(); i++) {
			String webpage = list.get(i).get("Webpage");
			if (webpage.contains("Merchant")) {
				String name = list.get(i).get("Name");
				dashboardMerchant.searchCustomer(name);
			} else if (webpage.contains("BackOffice")) {
				backOffice.searchCustomer(customer);
			}
		}
	}
	
	@Then("^change all passwords$")
	public void change_all_passwords(DataTable arg1) throws Throwable {
		management.changeAllPasswords(arg1);
	}
	
	@When("^validate the \"([^\"]*)\" message error$")
	public void validate_the_message_error(String page, DataTable mobile) throws Throwable {

		switch (page) {
		case "eCommerce":
			checkoutECommerce.checkmobileMessageError(mobile);
			break;

		case "Merchant":
			createContractMerchant.checkmobileMessageError(mobile);
			break;

		case "Subscription":
			paymentPageSubscription.checkmobileMessageError(mobile);
			break;

		case "BackOffice":
			backOffice.checkmobileMessageError(mobile);
			break;

		case "Selfcare":
			selfcare.checkmobileMessageError(mobile);
			break;

		default:
			System.out.println("no match");
		}
	}

	@When("^change the \"([^\"]*)\" mobile phone$")
	public void change_the_mobile_phone(String page, DataTable mobile) throws Throwable {

		switch (page) {
		case "eCommerce":
			checkoutECommerce.changeMobilePhone(mobile);
			break;

		case "Merchant":
			createContractMerchant.changeMobilePhone(mobile);
			break;

		case "Subscription":
			paymentPageSubscription.changeMobilePhone(mobile);
			break;

		case "BackOffice":
			backOffice.changeMobilePhone(mobile);
			break;

		case "Selfcare":
			selfcare.changeMobilePhone(mobile);
			break;

		default:
			System.out.println("no match");
		}
	}
	
	@When("^validate the \"([^\"]*)\" mobile phone$")
	public void validate_the_mobile_phone(String page) throws Throwable {

		switch (page) {
		case "BackOffice":
			backOffice.checkIfMobilephoneIsSaved();
			break;

		case "Customer":
			backOffice.checkMobilePhoneView();
			break;

		case "Selfcare":
			selfcare.checkIfMobilephoneIsSaved();
			break;

		default:
			System.out.println("no match");
		}
	}

	@When("^click on Change Credit Card on \"([^\"]*)\"$")
	public void click_change_credit_card(String arg1) throws Throwable {

		if (arg1.equalsIgnoreCase("BackOffice")) {

			backOffice.getContractNumber();
			backOffice.clickChangeCreditCardButton();
		} else {
			selfcare.clickChangeCreditCardButton();
			Thread.sleep(1000);

		}

	}

	@Then("validate the items list of Travels table on \"([^\"]*)\"$")
	public void validate_items_list_travels_table(String arg1) throws Throwable {

		if (arg1.equalsIgnoreCase("BackOffice")) {

			backOffice.validateItemsListTravelTable();

		} else {
			selfcare.validateItemsListTravelTable();
		}

	}

	@Then("insert a \"([^\"]*)\" payment card on \"([^\"]*)\"$")
	public void insert_a_vali_invalid_payment_card(String arg1, String page) throws Throwable {

		Thread.sleep(1000);
		if (arg1.equalsIgnoreCase("Invalid")) {
			if (!page.equalsIgnoreCase("BackOffice")) {
				selfcare.clickUseAnotherCard(page);
			}
			Thread.sleep(1000);
			utils.fillCreditCard("Visa", "Debit", "Consumer");
			Thread.sleep(1000);
			if (!page.equalsIgnoreCase("BackOffice")) {
				selfcare.clickUseAnotherCard(page);
			}

		} else if (arg1.equalsIgnoreCase("Valid")) {
			Thread.sleep(1000);
			if (!page.equalsIgnoreCase("BackOffice")) {
				selfcare.clickUseAnotherCard(page);
			}
			Thread.sleep(1000);
			utils.randomValidCreditCard();
//			utils.fillCreditCard("Visa", "Credit", "Consumer");
			Thread.sleep(1000);
		}
	}

	@Then("^validate that the card is authorized on \"([^\"]*)\"$")
	public void validate_that_the_card_is_authorized(String arg1) throws Throwable {

		selfcare.clickSubmitCard(arg1);

		Thread.sleep(60000);
		utils.waitForPageToLoad();
		if (arg1.equalsIgnoreCase("BackOffice")) {
			Thread.sleep(80000);
			assert ((driver.getCurrentUrl().contains("Success")));
		} else if (arg1.equalsIgnoreCase("Selfcare")) {
		}
	}

	@Then("^validate the payment modification on the planned installments$")
	public void validate_payment_modification() throws Throwable {

		String url = driver.getCurrentUrl();

		if (url.contains("BackOffice")) {
			
			backOffice.validatePaymentOnNextInstallment(tenant);
			
		} else if (url.contains("Customer")) {
			selfcare.validatePaymentOnNextInstallment();
		}
	}

	@Then("^verify the privacy police$")
	public void verify_privacy_police() throws InterruptedException {

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		String url = driver.getCurrentUrl();
		System.out.println(url);
		assert (url.contains("/politica-privacidade-protecao-dados"));
	}
	

	
	public String getTenant() {
		String url = driver.getCurrentUrl();
		
		if (url.contains("oney.be")) {
			tenant="BE";
		} else if (url.contains("oney.es")) {
			tenant="ES";
		} else if (url.contains("oney.it")) {
			tenant="IT";
		} else if (url.contains("oney.ro")) {
			tenant="RO";
		} else if (url.contains("oneygmbh.de")) {
			tenant="DE";
		} else if (url.contains("oney.pt")) {
			tenant="PT";
		}
		
		return tenant;
	}
		
	@Given("^acess the accounting file \"([^\"]*)\"$")
	public void acess_the_accounting_file(String tenant) throws Throwable {
	    
		this.tenant = tenant;
	    
	}
	
	@Given("^acess the \"([^\"]*)\" error file of the week$")
	public void acess_the_error_file(String tenant) throws Throwable {
	    
		this.tenant = tenant;   
	}
		
	@After
	public void takeScreenShotFailedScenario(Scenario scenario) throws IOException {

		if (scenario.isFailed()) {

			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		}

		driver.close();
		driver.quit();

	}

}