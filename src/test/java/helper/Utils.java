package helper;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//import ru.yandex.qatools.ashot.AShot;
//import ru.yandex.qatools.ashot.Screenshot;
//import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class Utils {

	public WebDriver driver;
//	static Screenshot screenshot;

	public Utils(WebDriver driver) {

		this.driver = driver;
	}

	/*
	 * With this method it's possible choose one of four cards random.
	 */
	
	public void randomValidCreditCard() throws InterruptedException {
		
		Random random = new Random();
		int option = random.nextInt(4); 
						
		System.out.println(option);
		String cardNumber = null;
		String cardMonth = "10";
		String cardYear = "3" + RandomStringUtils.randomNumeric(1);
		String cardCvv = null;

		if (option==0) {
			cardNumber = "5436031030606378";
			cardCvv = "257";
		} else
		if (option==1) {
			cardNumber = "4242424242424242";
			cardCvv = "100";
		}  else
		if (option==2) {
				cardNumber = "4543474002249996";
				cardCvv = "956";
		} else
		if (option==3) {
				cardNumber = "5199992312641465";
				cardCvv = "010";
		}
		
		By iframe = By.xpath("//iframe[@id='cko-iframe-id']");
		if (driver.findElements(iframe).size() != 0) {
			checkout(cardNumber, cardMonth, cardYear, cardCvv);
		} else {
			checkout2(cardNumber, cardMonth, cardYear, cardCvv);
		}
	}
	
	public void fillCreditCard(String cardScheme, String cardType, String cardCategory) throws InterruptedException {

		String cardNumber = null;
		String cardMonth = "10";
		String cardYear = "3" + RandomStringUtils.randomNumeric(1);
		String cardCvv = null;

		if (cardScheme == "Mastercard" && cardType == "Credit" && cardCategory == "Consumer") {
			cardNumber = "5436031030606378";
			cardCvv = "257";
		} else 
		if (cardScheme == "Visa" && cardType == "Debit" && cardCategory == "Consumer") {
			cardNumber = "4658584090000001";
			cardCvv = "257";
		} else
		if (cardScheme == "Visa" && cardType == "Credit" && cardCategory == "Consumer") {
			cardNumber = "4242424242424242";
			cardCvv = "100";
		} else
		if (cardScheme == "Visa" && cardType == "Credit" && cardCategory == "Commercial") {
			cardNumber = "4484070000035519";
			cardCvv = "257";
		} else
		if (cardScheme == "Mastercard" && cardType == "Debit" && cardCategory == "Commercial") {
			cardNumber = "5352151570003404";
			cardCvv = "100";
		} else
		if (cardScheme == "JCB" && cardType == "Credit" && cardCategory == "Consumer") {
				cardNumber = "3530111333300000";
				cardCvv = "100";
		} else
		if (cardScheme == "Visa" && cardType == "Debit" && cardCategory == "Commercial") {
				cardNumber = "4543474002249996";
				cardCvv = "956";
		}
		
		By iframe = By.xpath("//iframe[@id='cko-iframe-id']");
		if (driver.findElements(iframe).size() != 0) {
			checkout(cardNumber, cardMonth, cardYear, cardCvv);
		} else {
			checkout2(cardNumber, cardMonth, cardYear, cardCvv);
		}

	}
	
	public void checkout(String cardNumber, String cardMonth, String cardYear, String cardCvv) throws InterruptedException {
		
		WebElement iframe1 = driver.findElement(By.xpath("//iframe[@id='cko-iframe-id']"));
		driver.switchTo().frame(iframe1);
		driver.switchTo().activeElement();
		Thread.sleep(500);
		driver.findElement(By.xpath("//*[contains(@data-checkout,'card-number')]")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//*[contains(@data-checkout,'card-number')]")).sendKeys(cardNumber);
		Thread.sleep(1500);
		driver.findElement(By.xpath("//*[contains(@data-checkout,'expiry-month')]")).sendKeys(cardMonth);
		Thread.sleep(1500);
		driver.findElement(By.xpath("//*[contains(@data-checkout,'expiry-year')]")).sendKeys(cardYear);
		Thread.sleep(500);
		driver.findElement(By.xpath("//*[contains(@data-checkout,'cvv')]")).sendKeys(cardCvv);
		Thread.sleep(500);
		
		
		driver.switchTo().defaultContent();
	}
	
	public void checkout2(String cardNumber, String cardMonth, String cardYear,  String cvv) throws InterruptedException {
		WebElement iframeCardNumber = driver.findElement(By.xpath("//iframe[@id='cardNumber']"));
		WebElement iframeExpireDate = driver.findElement(By.xpath("//iframe[@id='expiryDate']"));
		WebElement iframeCVV = driver.findElement(By.xpath("//iframe[@id='cvv']"));
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", iframeCardNumber);
		Thread.sleep(1500);
		iframeCardNumber.click();
		iframeCardNumber.sendKeys(cardNumber);
		Thread.sleep(2000);
		iframeExpireDate.click();
		iframeExpireDate.sendKeys(cardMonth+cardYear);

		Thread.sleep(1500);
		System.out.println(cvv);
		iframeCVV.click();
		iframeCVV.sendKeys(cvv);
		
	}

	public void verifyWebElementIsDisplayed(WebElement el) {

		if (el.isDisplayed()) {

			System.out.println("Element is present: " + el.getLocation());
		}

		else {

			System.out.println("Element is NOT present");
		}
	}

	public String uploadFile(String fileName) throws Exception {

		String filePath = System.getProperty("user.home") + "\\git\\oney-ablewise-chrome\\Documents\\";

		StringSelection stringSelection = new StringSelection(filePath + fileName);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

		Robot robot = new Robot();
		Thread.sleep(500);
		robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
		robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
		robot.keyPress(java.awt.event.KeyEvent.VK_V);
		Thread.sleep(500);
		robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
		robot.keyRelease(java.awt.event.KeyEvent.VK_V);
		Thread.sleep(500);
		robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
		Thread.sleep(3000);

		return fileName;
	}

	/*
	 * Takes a screenshot and saves it on the project folder cucumber.html Uses the
	 * method timeStamp to generate a unique name for each .png captured When
	 * implementing this method you can chose witch locator do you want to "hide"
	 */

//	public void takeFullPageScreenShot(By locator) throws IOException {
//
//		waitForPageToLoad();
//
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//
//		js.executeScript("arguments[0].style.position='absolute'", driver.findElement(locator));
//		Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
//				.takeScreenshot(driver);
//		js.executeScript("arguments[0].style.position='fixed'", driver.findElement(locator));
//		ImageIO.write(fpScreenshot.getImage(), "PNG", new File(System.getProperty("user.home")
//				+ "\\git\\oney-ablewise-chrome\\target\\cucumber-reports\\cucumber.html" + timestamp() + ".png"));
//	}

	public static String timestamp() {

		return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
		//hhhh
	}

	/*
	 * Waits for the page to fully load before taking any further action
	 */

	public void waitForPageToLoad() {

		JavascriptExecutor js = (JavascriptExecutor) driver;

		// This loop will rotate for 100 times to check If page Is ready after every 1
		// second.
		// You can replace your if you wants to Increase or decrease wait time.
		for (int i = 0; i < 100; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			// To check page ready state.

			if ((js.executeScript("return document.readyState").toString().equals("complete"))
					&& (js.executeScript("return jQuery.active").toString().equalsIgnoreCase("0"))) {
				break;
			}
		}
	}

	public void scrollDown() {

		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	
	public void renameFile(String pathFile, String pathNewFile) throws InterruptedException, IOException {
		
		File file = new File(pathFile);
		File file2 = new File(pathNewFile);

		if (file2.exists()) {
		   throw new java.io.IOException("file exists");
		} 

		boolean success = file.renameTo(file2);
		
		assert(success);
		System.out.println("FOI");
		Thread.sleep(1000);
	
	}
	
	public void deleteFile(String filepath) throws IOException {
//    	File file = new File(filepath);    
//
//    	System.out.println(file.exists());
//    	if (file.exists()) {
//			file.delete();
//			System.out.println("File erased.");
//		}   
    	System.out.println(filepath);
    	try {
            boolean result = Files.deleteIfExists(Paths.get(filepath));
            if (result) {
                System.out.println("File is deleted!");
            } else {
                System.out.println("Sorry, unable to delete the file.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	    
    	
//    	System.out.println(file.exists());
    	
    	
    }
	
	public LocalDateTime getLastWeekEnd() {
		
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		LocalDateTime lastSunday = null;

		switch (day) {

		case Calendar.MONDAY:
			lastSunday = LocalDateTime.now().minusDays(1);
			break;

		case Calendar.TUESDAY:

			lastSunday = LocalDateTime.now().minusDays(2);
			break;

		case Calendar.WEDNESDAY:
			lastSunday = LocalDateTime.now().minusDays(3);
			break;

		case Calendar.THURSDAY:
			lastSunday = LocalDateTime.now().minusDays(4);
			break;

		case Calendar.FRIDAY:
			lastSunday = LocalDateTime.now().minusDays(5);
			break;

		default:
			System.out.println("This test should not run in the weekend");
		}
		
		
		return lastSunday;
	}
	
	public LocalDateTime getLastWeekBegin() {
		

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		LocalDateTime lastMonday = null;
		
		System.out.println(day);
		System.out.println(Calendar.MONDAY);
		switch (day) {

		case Calendar.MONDAY:
			lastMonday = LocalDateTime.now().minusDays(8);
			break;

		case Calendar.TUESDAY:
			lastMonday = LocalDateTime.now().minusDays(9);
			break;

		case Calendar.WEDNESDAY:
			lastMonday = LocalDateTime.now().minusDays(10);
			break;

		case Calendar.THURSDAY:
			lastMonday = LocalDateTime.now().minusDays(11);
			break;

		case Calendar.FRIDAY:
			lastMonday = LocalDateTime.now().minusDays(12);
			break;

		default:
			System.out.println("This test should not run in the weekend");
		}
		
		return lastMonday;
	}
	
	
}
