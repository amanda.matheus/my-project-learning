var reporter = require('cucumber-html-reporter');

var options = {
        theme: 'bootstrap',
        jsonFile: 'target/json-reports/cucumber.json',
        output: 'target/cucumber-reports/cucumber_report.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        launchReport: true,
        metadata: {
            "Test Environment": "QA",
            "Assigned": "QA Automation Team",
            "Browser": "Chrome  94.0.4606.81",
            "Tenants": "BE, ES and IT"
        }
    };

    reporter.generate(options);